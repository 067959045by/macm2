package com.mail.comm.video.exo;

import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import org.xutils.x;

import java.io.File;

public class VideoCache {

    private static SimpleCache sDownloadCache;

    public static SimpleCache getInstance() {

//        SimpleCache simpleCache = new SimpleCache(new File(context.getCacheDir(), "media_yitong"), evictor);

        if (sDownloadCache == null)
            sDownloadCache = new SimpleCache(new File(x.app().getCacheDir(), "exoCache"), new NoOpCacheEvictor());
        return sDownloadCache;
    }
}
