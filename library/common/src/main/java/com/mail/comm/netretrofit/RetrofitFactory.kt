package com.mail.comm.netretrofit

import android.text.TextUtils
import com.mail.comm.app.AppConfig
import com.mail.comm.net.DataManager
import com.mail.comm.utils.MyUtils2
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject
import org.xutils.x
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitFactory private constructor() {

    private var api: Api? = null

    init {
        val mOkHttpClient = OkHttpClient.Builder()
            .connectTimeout(2, TimeUnit.SECONDS)
            .readTimeout(2, TimeUnit.SECONDS)
            .writeTimeout(2, TimeUnit.SECONDS)
//            .addInterceptor(InterceptorUtils.logInterceptor())//添加日志拦截器
            .addInterceptor { chain ->
                var map = HashMap<String, String>()

                map["device_type"] = "A"

                map["device_no"] = MyUtils2.getAndroidId(x.app())

                map["version"] = MyUtils2.getAppVersionName()

                if (!TextUtils.isEmpty(MyUtils2.getToken())) {
                    map["token"] = MyUtils2.getToken()
                }

                var singHeader = DataManager.singPostBody(JSONObject(map as Map<*, *>))

                val original: Request = chain.request()
                original.body()
                var request = original.newBuilder()
                    .header("X-TOKEN", singHeader)
                    .build()

                chain.proceed(request)
            }
            .build()

        api = Retrofit.Builder()
            .baseUrl(AppConfig.Host_Url)
//          .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .client(mOkHttpClient)
            .build()
            .create(Api::class.java)
    }

    companion object {

        private var mRetrofitFactory: RetrofitFactory? = null
            get() {
                if (field == null) {
                    synchronized(RetrofitFactory::class.java) {
                        if (field == null) {
                            field = RetrofitFactory()
                        }
                    }
                }
                return field
            }

        fun getInstence(): RetrofitFactory? {
            return mRetrofitFactory
        }
    }

    fun api(): Api? = api

}