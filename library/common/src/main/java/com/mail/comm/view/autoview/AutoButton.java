package com.mail.comm.view.autoview;

import android.content.Context;
import android.util.AttributeSet;

public class AutoButton extends com.rey.material.widget.Button {

    public AutoButton(Context context) {
        super(context);
    }

    public AutoButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
