package com.mail.comm.base

import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.net.ApiListener
import org.xutils.common.Callback
import org.xutils.http.RequestParams

abstract class XBaseAdaper : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ApiListener {


    override fun onCancelled(var1: Callback.CancelledException?) {
    }


    override fun onComplete(var2: String?, type: String?) {
    }

    override fun onCompleteChild(var2: String?, type: String?) {
        onComplete(var2,type)
    }

    override fun onError(var1: Map<String?, String?>?, var2: RequestParams?) {}

    override fun onExceptionType(type: String?) {}

    override fun onExceptionTypeChild(type: String?) {
        onExceptionType(type)
    }

    override fun onLoading(type: String, total: Long, current: Long, isDownloading: Boolean) {}

}