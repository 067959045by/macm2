package com.mail.comm.image

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.TextUtils
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.mail.comm.R
import com.mail.comm.net.AESFileCrypt
import com.mail.comm.utils.FileCompressUtil
import com.mail.comm.utils.MyUtils2
import com.mail.comm.utils.PreferencesUtils
import com.zhy.autolayout.config.AutoLayoutConifg
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil
import org.xutils.x
import java.io.File

class ImageLoader {

    companion object {

        fun loadImage(context: Context?, url: String?, imgv: ImageView?,isNet:Boolean =false) {
            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return@loadImage
            }
            var mUrl = url
            if (!url!!.startsWith("http")&&isNet){
                var resource_cdn =PreferencesUtils.getString(x.app(),"resource_cdn")
                mUrl = resource_cdn +url
            }
            Glide.with(context)
                .load(mUrl)
                .into(imgv)
        }

        fun loadImageCircle(context: Context?, url: String?, imgv: ImageView?) {
            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return@loadImageCircle
            }
            val requestOptions = RequestOptions()
            requestOptions.apply(RequestOptions.bitmapTransform(CircleCrop()))
            Glide.with(context)
                .load(url)
                .apply(requestOptions)
                .into(imgv)
        }

        fun loadImageCircle2(context: Context?, url: String, imgv: ImageView?) {
            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return@loadImageCircle2
            }
            val requestOptions = RequestOptions()
            requestOptions.transform(GlideCircleTransform(context))
            Glide.with(context)
                .load(url)
                .apply(requestOptions)
                .into(imgv)
        }

        fun loadImageRadius(context: Context, url: String?, imgv: ImageView?, radius: Int = 20) {
            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return@loadImageRadius
            }
            val transformation = CornerTransform(
                context,
                AutoUtils.getPercentWidthSize(radius).toFloat()
            )
            val requestOptions = RequestOptions()
            requestOptions.transform(transformation)
            Glide.with(context)
                .load(url).apply(requestOptions)
                .into(imgv)
        }

        fun loadImageBitmap(context: Context?, url: String?, callback: BimapCallback?) {
            if (context == null || TextUtils.isEmpty(url)) {
                return@loadImageBitmap
            }
            Glide.with(context)
                .asBitmap().skipMemoryCache(false)
                .load(url)
                .into(object : SimpleTarget<Bitmap?>() {
                    override fun onResourceReady(resource: Bitmap,
                        transition: Transition<in Bitmap?>?
                    ) {
                        callback?.onLoadSuccess(resource)
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        callback?.onLoadFailed()
                    }

                })

        }

        fun loadImageDrawable(context: Context?, url: String?, callback: DrawableCallback?) {
            if (context == null || TextUtils.isEmpty(url)) {
                return@loadImageDrawable
            }
            Glide.with(context)
                .asDrawable()
                .load(url)
                .into(object : SimpleTarget<Drawable?>() {

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        callback?.onLoadFailed()
                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable?>?
                    ) {
                        callback?.onLoadSuccess(resource)
                    }
                })

        }

        /**
         * 显示视频封面缩略图
         */
        fun loadVideoThumb(context: Context?, file: File?, imageView: ImageView?) {
            if (context == null) {
                return
            }

            try {
                Glide.with(context)
                    .asDrawable()
                    .load(Uri.fromFile(file))
                    .skipMemoryCache(false)
                    .into(imageView!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        fun loadImageAes(context: Context?=null, url: String?, imgv: ImageView?,maxW:Int,maxH:Int,inSampleSize:Int=1) {

            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return
            }

            var mUrl = url
            if (!url!!.startsWith("http")){
                var resource_cdn =PreferencesUtils.getString(x.app(),"resource_cdn")
                mUrl = resource_cdn +url
            }

            val simpleTarget: SimpleTarget<File?> = object : SimpleTarget<File?>() {

                override fun onResourceReady(resource: File, transition: Transition<in File?>?) {

                    try {
                        val decryptBytes = AESFileCrypt.decryptData(resource)
                        if (decryptBytes == null || decryptBytes.size == 0) {
                            LogUtil.e("simpleTarget=null")
                            return
                        }

                        var bitmap = MyUtils2.byteToBitmap(decryptBytes,inSampleSize,maxW,maxH)

                        if (bitmap==null){
                            return
                        }
                        var width = bitmap.width

                        var height = bitmap.height

                        if (bitmap.width > maxW) {
                            var r =  (maxW.toFloat())/bitmap.width
                            width = maxW
                            height = (height * r).toInt()
                        }

                        if (maxH==0){
                            height = bitmap.height
                        }else{
                            if (height>maxH){
                                var r = (maxH.toFloat())/height
                                height = maxH
                                width =  (width * r).toInt()
                            }
                        }

                        if (bitmap.width==width&&bitmap.height==height){
                            imgv.setImageBitmap(bitmap)
                            return
                        }

                        var inputBmp = Bitmap.createScaledBitmap(bitmap, width, height, false)

                        imgv.setImageBitmap(inputBmp)

                        if(bitmap!=null && !bitmap.isRecycled){
                            bitmap.recycle()
                            System.gc()
                            bitmap=null
                        }
//                        bitmap.recycle()

                    }catch (e:Exception){ }

                }
            }

            var requestManager : RequestManager
            if (context == null){
                requestManager = Glide.with(imgv)
            }else{
                requestManager = Glide.with(context)
            }
            requestManager
                .asFile()
                .load(mUrl)
                .dontAnimate()
                .skipMemoryCache(false)
                .into(simpleTarget)
        }

        fun loadImageAesCallback(context: Context?, url: String?, callback: BimapCallback) {

            if (context == null || TextUtils.isEmpty(url)) {
                return
            }

            var mUrl = url
            if (!url!!.startsWith("http")){
                var resource_cdn =PreferencesUtils.getString(x.app(),"resource_cdn")
                mUrl = resource_cdn +url
            }

            val simpleTarget: SimpleTarget<File?> = object : SimpleTarget<File?>() {
                override fun onResourceReady(resource: File, transition: Transition<in File?>?) {

                    try {
                        val decryptBytes = AESFileCrypt.decryptData(resource)
                        if (decryptBytes == null || decryptBytes.isEmpty()) {
                            callback.onLoadFailed()
                            return
                        }

                        var bitmap = BitmapFactory.decodeByteArray(decryptBytes, 0, decryptBytes.size)
                        if (bitmap == null) {
                            callback.onLoadFailed()
                            return
                        }

                        if (callback != null) {
                            callback.onLoadSuccess(bitmap)
                        }

                    } catch (e: Exception) {
                        callback.onLoadFailed()
                    }

                }

                override fun onLoadFailed(errorDrawable: Drawable?) {
                    callback.onLoadFailed()
                }
            }

            val requestOptions = RequestOptions()
            Glide.with(context)
                .asFile()
                .load(mUrl)
                .skipMemoryCache(false)
                .apply(requestOptions)
                .into(simpleTarget)
        }

    }


    interface BimapCallback {
        fun onLoadSuccess(bitmap: Bitmap?)
        fun onLoadFailed()
    }

    interface DrawableCallback {
        fun onLoadSuccess(drawable: Drawable?)
        fun onLoadFailed()
    }
}