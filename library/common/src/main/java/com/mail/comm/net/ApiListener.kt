package com.mail.comm.net

import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.CoroutineScope
import org.xutils.common.Callback.CancelledException
import org.xutils.http.RequestParams

interface ApiListener {

    fun onCancelled(var1: CancelledException?)

    fun onComplete(var2: String?, type: String?)

    fun onCompleteChild(var2: String?, type: String?)

    fun onExceptionType(type: String?)

    fun onExceptionTypeChild(type: String?)

    fun getCoroutineScope(): CoroutineScope?

    fun onLoading(type:String,total: Long, current: Long, isDownloading: Boolean)

    fun onError(var1: Map<String?, String?>?, var2: RequestParams?)


}