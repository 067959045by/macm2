package com.mail.comm.base

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import com.mail.comm.R
import com.mail.comm.app.AppConfig
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.net.ApiListener
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils2
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.load.XLoadDialog
import org.xutils.common.Callback
import org.xutils.common.util.LogUtil
import org.xutils.http.RequestParams

abstract class BaseFrg: Fragment(),ApiListener{

    var rootView: View? = null

    var loadingDialog: XLoadDialog? = null
    var typeNet = ""

    abstract fun getLayoutId(): Int

    open fun getLayoutView(): View{
        return View(activity)
    }

    abstract fun initView()

    abstract fun requestData()

    fun refreshData(data: Any?) {}

    fun initRefreshData(data: Any?) {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {

            if(getLayoutId()!=0){
                rootView = inflater.inflate(getLayoutId(), container, false)
            }else{
                rootView = getLayoutView()
            }
            rootView!!.isClickable = true
        }
        initView()
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        typeNet ="onActivityCreated"
        loadingDialog?.destroy()
        requestData()
    }

//    R.anim.abc_grow_fade_in_from_bottom,
//    R.anim.abc_fade_out,
//    R.anim.abc_fade_in,
//    R.anim.abc_shrink_fade_out_from_botto

    fun startActivity(cls: Class<*>,isOverAnim :Boolean= false) {
        startActivity(cls, null)
    }

    fun startActivity(cls: Class<*>, bundle: Bundle?) {
        val intent = Intent(activity, cls)
        bundle?.let { intent.putExtras(it) }
        startActivity(intent)

    }


    fun startOverAnimActivity(cls: Class<*>, bundle: Bundle?,vararg sharedElements:Pair<View, String>) {
        val intent = Intent(activity, cls)
        bundle?.let { intent.putExtras(it) }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(requireActivity(), *sharedElements)
            ActivityCompat.startActivity( requireActivity(), intent, activityOptions.toBundle())
        }else{
            startActivity(cls,bundle)
        }
    }


    fun startActivityForResult(cls: Class<*>, requestCode: Int) {
        startActivityForResult(cls, null, requestCode)
    }

     fun startActivityForResult(cls: Class<*>, bundle: Bundle?, requestCode: Int) {
        val intent = Intent(activity, cls)
        bundle?.let { intent.putExtras(it) }
        startActivityForResult(intent, requestCode)
        if(activity is BaseAty){
//            activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }
    }

    fun showToastS(text: String?) = ToastUitl.showShort( text)

    fun showToastL(text: String?) = ToastUitl.showLong( text)

    fun startProgressDialog() {
        if (loadingDialog == null) loadingDialog = XLoadDialog()
        loadingDialog?.showDialogForLoading(activity)
    }

    fun stopProgressDialog() = loadingDialog?.cancelDialogForLoading()

    override fun onDestroy() {
        super.onDestroy()
        typeNet = "onDestroy"
        loadingDialog?.destroy()
    }

    override fun onCancelled(var1: Callback.CancelledException?) {}

    override fun onComplete(var2: String?, type: String?) {
        var map = JSONUtils.parseKeyAndValueToMap(var2)
        LogUtil.e("onComplete2=========" + type+",,,,"+var2)
        if (map["code"] == "200"&& AppConfig.debug) {
            var str = AESCBCCrypt.aesDecrypt(map["data"])
            MyUtils2.log("onComplete3333","onComplete3=======" + type+",,,,"+ str)
        }
    }

    override fun onCompleteChild(var2: String?, type: String?) {
        if (typeNet == "onDestroy"){
            return
        }
        onComplete(var2, type)
    }

    override fun onError(var1: Map<String?, String?>?, var2: RequestParams?) {
    }

    override fun onPause() {
        super.onPause()
        ToastUitl.destroy()
    }

    override fun onExceptionType(type: String?) {
    }

    override fun onExceptionTypeChild(type: String?) {
        if (typeNet == "onDestroy"){
            return
        }
        onExceptionType(type)
    }

    override fun onLoading(type: String, total: Long, current: Long, isDownloading: Boolean) {
    }

    override fun getCoroutineScope(): LifecycleCoroutineScope {
        return  lifecycleScope
    }
}