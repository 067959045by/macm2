package com.mail.comm.app;


public class AppConfig {

    public static String TOKEN = "";
    public static String Host_Url = "";
    public static String upload_url = "";
    public static String handshake = "";
    public static String type = "my2";
    public static boolean debug = true;
    public static boolean isFormal = true;

    static {
        if (isFormal) {
            handshake = "v20210601";
        } else {
            handshake = "v20210501";
        }
    }


}
