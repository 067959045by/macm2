package com.mail.comm.view;

import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.TextureView;
import android.widget.TextView;


public class AppCountdown extends CountDownTimer {

    private TextView textView;
    private static long surplusTime;
    private static AppCountdown appCountdown;

    private String text ="";
    public static AppCountdown getInstance() {
        if (appCountdown == null) {
            appCountdown = new AppCountdown();
        }
        appCountdown.setMyText("");
        return appCountdown;
    }



    private AppCountdown() {
        super(surplusTime > 0 ? surplusTime : 60000, 1000);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        surplusTime = millisUntilFinished;
        if (textView!=null){
            textView.setText(millisUntilFinished / 1000 + "s可重新发送");
        }
    }

    @Override
    public void onFinish() {
        surplusTime = 0;
        if (textView!=null){

            textView.setText(text);
            textView.setEnabled(true);
        }
        cancel();
    }

    public int getTime(){
        int s = (int) (surplusTime / 1000);
        return  s ;
    }

    public void setMyText(String str){
        this.text = str;
    }

    public void play(TextView textView) {
        this.textView = textView;
        if (surplusTime > 0) {
            if (textView!=null){
                textView.setEnabled(false);
            }
        } else {
            if (textView!=null){
                textView.setText(text);
                textView.setEnabled(true);
            }

        }
    }

    public void reSet() {
        surplusTime = 0;
        if (textView!=null){
            textView.setEnabled(true);
            textView.setText(text);
            textView =null;
        }
        cancel();
    }

    public void mStart() {
        if (textView!=null){
            textView.setEnabled(false);
        }
        start();
    }
}