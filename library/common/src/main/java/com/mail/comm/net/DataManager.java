package com.mail.comm.net;


import org.json.JSONObject;
import org.xutils.common.util.LogUtil;
import java.security.GeneralSecurityException;

public class DataManager {


    public static String singPostBody(JSONObject jsonObject){
        String jsonBody = jsonObject.toString();
        LogUtil.e("singPostBody jsonBody:"+jsonBody);

        String aesBody = null;
        try {
            aesBody = AESCBCCrypt.aesEncrypt(jsonBody);
            LogUtil.e("singPostBody:"+aesBody);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aesBody;
    }

    public static String singPostBody2(String jsonObject){
        String jsonBody = jsonObject.toString();
        LogUtil.e("singPostBody jsonBody:"+jsonBody);

        String aesBody = null;
        try {
            aesBody = AESCBCCrypt.aesEncrypt(jsonBody);
            LogUtil.e("singPostBody:"+aesBody);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aesBody;
    }

    public static String singPostBody2(JSONObject jsonObject){
        String jsonBody = jsonObject.toString();
        String aesBody = null;
        try {
            aesBody = AESCBCCrypt.aesEncrypt(jsonBody);
            LogUtil.e("singPostBody:"+aesBody);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aesBody;
    }

    public static String singFileBody(String timestamp){
//        String souseData = "A"+timestamp+"C8pvY0YQWqtfFLHb";
        String souseData = "A"+timestamp+"C8pvY0YQWqtfFLHb";
        String md5Data = MD5Utils.md5(souseData).toUpperCase();
        return md5Data;
    }

}
