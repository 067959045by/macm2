package com.mail.comm.function.chooseimg

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.R
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.zhy.autolayout.utils.AutoUtils

class ChooseImagePreviewAty : BaseAty(){

    var relay_top_bg:RelativeLayout?=null
    var tv_title:TextView?=null
    var tv_right:TextView?=null
    var recyclerview:RecyclerView?=null
    var list = ArrayList<String>()
    var index =0
    override fun getLayoutId(): Int = R.layout.aty_choose_img_previdew

    override fun initView() {
        list = intent.getStringArrayListExtra("list") as ArrayList<String>
        relay_top_bg =findViewById(R.id.relay_top_bg)
        tv_title =findViewById(R.id.tv_title)
        tv_right =findViewById(R.id.tv_right)
        recyclerview =findViewById(R.id.recyclerview)

        initTopview(relay_top_bg,"#000000")
        tv_title?.text= "预览"
        tv_right?.text = "${index+1}/${list.size}"
        tv_title?.setTextColor(Color.parseColor("#ffffff"))
        tv_right?.setTextColor(Color.parseColor("#ffffff"))
        tv_right?.visibility = View.VISIBLE

    }

    override fun requestData() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL;
        recyclerview?.layoutManager = linearLayoutManager
        var mAdapter = GoldRecyclerAdapter(this)
        recyclerview?.adapter = mAdapter
    }

    fun mainClick(v:View){

        when(v.id){
            R.id.relay_back->{
                finish()
            }
        }
    }

    inner class GoldRecyclerAdapter(context: Context?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(inflater.inflate(R.layout.item_choose_img_previdew, parent, false))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int = position

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if ( holder is fGoldViewHolder){
                ImageLoader.loadImage(this@ChooseImagePreviewAty,list[position],holder.imgv)
            }

        }

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv: ImageView? = null
            init {
                AutoUtils.autoSize(itemView)
                imgv = itemView.findViewById(R.id.imgv)
            }
        }

        override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
            super.onAttachedToRecyclerView(recyclerView)
            var mLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
            val pagerSnapHelper = PagerSnapHelper()
            pagerSnapHelper.attachToRecyclerView(recyclerView)
            recyclerView.setOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    val position: Int = mLayoutManager!!.findFirstCompletelyVisibleItemPosition()
                    if (position >= 0 && index != position) {
                        index = position
                        tv_right?.text = "${index+1}/${list.size}"
                    }
                }
            })
        }

    }



}