package com.mail.comm.utils;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mail.comm.R;
import com.zhy.autolayout.utils.AutoUtils;

import org.xutils.x;


public class ToastUitl {

    private static  Toast toast;

//    public static ToastUitl getInstance() {
//        if (instance==null)
//        return new ToastUitl();
//    }
    public static void showShort( CharSequence str) {
       showToast( str, Toast.LENGTH_SHORT);
    }


    public static void showLong(CharSequence str) {
        showToast( str, Toast.LENGTH_LONG);
    }

    public static  void showToast( CharSequence text, int duration) {
        if (toast != null) {
            toast.cancel();
            toast = null;
        }
//        if (toast == null) {
        toast = new Toast(x.app());
        LayoutInflater inflater = (LayoutInflater) x.app().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.toast_img, null);
        TextView textView = (TextView) layout.findViewById(R.id.tv_content);
        RelativeLayout relay_bg = (RelativeLayout) layout.findViewById(R.id.relay_bg);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) relay_bg.getLayoutParams();
        layoutParams.height = AutoUtils.getPercentWidthSizeBigger(360);
        layoutParams.width = AutoUtils.getPercentWidthSizeBigger(360);
        int paddings = AutoUtils.getPercentWidthSizeBigger(20);
        relay_bg.setPadding(paddings, paddings, paddings, paddings);
        relay_bg.setLayoutParams(layoutParams);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentWidthSizeBigger(42));
        textView.setText(text);
        toast.setView(layout);
        toast.setGravity(Gravity.CENTER | Gravity.TOP, 0, AutoUtils.getPercentHeightSize(650));
        toast.setDuration(duration);
//        }
//        else{
//            View  v =toast.getView();
//            TextView textView =v.findViewById(R.id.tv_content);
//            textView.setText(text);
////            toast.show();
//        }
        toast.show();
    }

    public static  void destroy() {
        if (toast != null) {
            toast.cancel();
        }
    }
}
