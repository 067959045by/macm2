package com.mail.comm.netretrofit

import retrofit2.http.*


interface Api {

    @FormUrlEncoded
    @POST
    suspend fun getData(@Url url: String,@Field("handshake")  handshake:String,
                           @Field("data") data:String ):String
}