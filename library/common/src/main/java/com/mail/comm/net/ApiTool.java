package com.mail.comm.net;


import android.widget.Toast;

import com.mail.comm.app.AppConfig;
import com.mail.comm.netretrofit.ApiRetrofit;
import com.mail.comm.utils.JSONUtils;
import com.mail.comm.utils.MyUtils2;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.common.util.KeyValue;
import org.xutils.common.util.LogUtil;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ApiTool {

    public ApiTool() {
    }

    RequestParams params;
    String type;

    public Callback.Cancelable getApi(RequestParams params, ApiListener apiListener, String type) {
        this.params = params;
        this.type = type;
        params.setConnectTimeout(7000);
        return x.http().get(params, new DefaultRequestCallBack(apiListener));
    }

    public Callback.Cancelable getApi2(RequestParams params, ApiListener apiListener, String type) {
        this.params = params;
        this.type = type;
        params.setConnectTimeout(7000);

        HashMap<String, String> map = new HashMap();
        map.put("device_type", "A");
        map.put("device_no", MyUtils2.getAndroidId(x.app()));
        map.put("version", MyUtils2.getAppVersionName());
        map.put("token", MyUtils2.getToken());
        String singHeader = DataManager.singPostBody(new JSONObject(map));
        params.setHeader("X-TOKEN", singHeader);
//        initParams(params);
        return x.http().get(params, new DefaultRequestCallBack(apiListener));
    }

    public Callback.Cancelable postApi(RequestParams params, ApiListener apiListener, String type) {
        this.params = params;
        this.type = type;
        params.setConnectTimeout(7000);
        return x.http().post(params, new DefaultRequestCallBack(apiListener));
    }

    public Callback.Cancelable postApiFile(RequestParams params, ApiListener apiListener, String type) {
        this.params = params;
        this.type = type;
        params.setConnectTimeout(7000);
        return x.http().post(params, new DefaultRequestCallBack(apiListener,true));
    }

    public void postApi2(RequestParams params, ApiListener apiListener, String type,boolean isShowError) {
        new ApiRetrofit().postApi(params, apiListener, type, isShowError);
    }

    public void postApi2(RequestParams params, ApiListener apiListener, String type) {
        new ApiRetrofit().postApi(params, apiListener, type, true);
//        this.params = params;
//        this.type = type;
//        params.setConnectTimeout(7000);
//
//        HashMap<String,String> map = new HashMap();
//        map.put("device_type", "A");
//        map.put("device_no", MyUtils2.getAndroidId(x.app()));
//        map.put("version", MyUtils2.getAppVersionName());
//        if (!TextUtils.isEmpty(MyUtils2.getToken())){
//            map.put("token",  MyUtils2.getToken());
//        }
//
//        String singHeader = DataManager.singPostBody( new JSONObject(map));
//        params.setHeader("X-TOKEN",singHeader);
//        LogUtil.e("initParams head ="+ new JSONObject(map));
//        initParams(params);
//        x.http().post(params, new DefaultRequestCallBack(apiListener));
//        return x.http().post(params, new DefaultRequestCallBack(apiListener));

    }

    public void initParams(RequestParams params) {
        List<KeyValue> list = params.getBodyParams();
        HashMap map = new HashMap<String, String>();

        for (KeyValue keyValue : list) {
            map.put(keyValue.key, keyValue.value);
        }
        String data = DataManager.singPostBody(new JSONObject(map));
        LogUtil.e("initParams boby==" + AESCBCCrypt.aesDecrypt(data));
        map.clear();
        map.put("handshake", AppConfig.handshake);
        map.put("data", data);
        params.clearParams();
        params.setBodyContent(new JSONObject(map).toString());
    }

    public Callback.Cancelable postApiTime(RequestParams params, ApiListener apiListener, String type, int time) {
        this.params = params;
        this.type = type;
        params.setConnectTimeout(time);
        return x.http().post(params, new DefaultRequestCallBack(apiListener));
    }

    private class DefaultRequestCallBack implements Callback.ProgressCallback<String> {
        private ApiListener apiListener;
        private Boolean isShowError;

        public DefaultRequestCallBack(ApiListener apiListener) {
            this.apiListener = apiListener;
        }

        public DefaultRequestCallBack(ApiListener apiListener,Boolean isShowError) {
            this.apiListener = apiListener;
            this.isShowError = isShowError;
        }

        @Override
        public void onSuccess(String result) {
            try {
                if (this.apiListener != null) {
                    this.apiListener.onCompleteChild(result, type);
                }

            } catch (Exception var4) {
                if (this.apiListener != null) {
                }
            }

        }

        public void onError(Throwable ex, boolean isOnCallback) {
            if (isShowError){
                Toast.makeText(x.app(), ex.getMessage(), Toast.LENGTH_LONG).show();
            }
            if (this.apiListener != null) {
                this.apiListener.onExceptionTypeChild(type);
            }
        }

        public void onCancelled(CancelledException cex) {

            if (this.apiListener != null) {
                this.apiListener.onCancelled(cex);
            }

        }

        public void onFinished() {

        }

        @Override
        public void onWaiting() {

        }

        @Override
        public void onStarted() {

        }

        @Override
        public void onLoading(long total, long current, boolean isDownloading) {
            if (this.apiListener != null) {
                this.apiListener.onLoading(type, total, current, isDownloading);
            }
            LogUtil.e("onLoading====total=" + total + ",current=" + current + ",isDownloading" + isDownloading);
        }
    }

    public static Map<String, String> parseError(String json) {
        JSONObject jsonObject = null;
        if (json.startsWith("[") && json.endsWith("]")) {
            return null;
        } else {
            try {
                jsonObject = new JSONObject(json);
            } catch (JSONException var4) {
                return null;
            }
            String flag = jsonObject.optString("flag");
            return flag != null && flag.equals("error") ? JSONUtils.parseKeyAndValueToMap(json) : null;
        }
    }
}

