package com.mail.comm.base

import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.transition.Transition
import android.view.View
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import com.mail.comm.app.AppManager
import com.mail.comm.app.AppStatusManager
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.load.XLoadDialog
import kotlin.collections.ArrayList
import kotlin.system.exitProcess

abstract class BaseAty : AbsAty() {

    var fragments: ArrayList<BaseFrg>? = null

    var currentFragment: BaseFrg? = null

    var isTwoBack: Boolean = false

    var firstTime: Long = 0

    var isAnimFish = "1"  //1 手动 2系统 3无

    var isAnimOver = false  //1 手动 2系统 3无

    var loadingDialog: XLoadDialog? = null

    abstract fun getLayoutId(): Int

    abstract fun initView()

    abstract fun requestData()

    open fun getFragmentContainerId(): Int = 0

    open fun getFragmentContainerId2(): Int = 0

    open fun getLayoutView(): View{
        return View(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (AppStatusManager.getInstance().appStatus !== 1) {
            protectApp()
            return
        }
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT;//竖屏
        typeNet = "onCreate"
        if(getLayoutId()!=0){
            setContentView(getLayoutId())
        }else{
            setContentView(getLayoutView())
        }
        AppManager.getInstance().addActivity(this)
        setStatusColor("#ffffff")
        setAndroidNativeLightStatusBar(true)
        loadingDialog?.destroy()
        initView()
        if (!isAnimOver){
            requestData()
        }
    }

    fun setEnableOverAnim(isAnimOver:Boolean){
        this.isAnimOver = isAnimOver
    }

    fun initOverAnim(list:Array<View>, list2:Array<String>){
        for (i in list.indices){
            ViewCompat.setTransitionName(list[i], list2[i])
        }
        addTransitionListener()
    }

     fun addTransitionListener(): Boolean {
        val transition = window.sharedElementEnterTransition
        if (transition != null) {
            transition.addListener(object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition) {
                    requestData()
                    transition.removeListener(this)
                }

                override fun onTransitionStart(transition: Transition) {
                }

                override fun onTransitionCancel(transition: Transition) {
                    transition.removeListener(this)
                }

                override fun onTransitionPause(transition: Transition) {
                }

                override fun onTransitionResume(transition: Transition) {
                }
            })
            return true
        }
        return false
    }

    private fun protectApp() {
        AppManager.getInstance().killAllActivity()
        exitProcess(0)
    }

     fun setStatusColor(color: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.parseColor(color)
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.parseColor(color)
        }
    }

    override fun onPause() {
        super.onPause()
        ToastUitl.destroy()
    }

    fun setAndroidNativeLightStatusBar( dark: Boolean) {
        val decor: View = window.decorView
        if (dark) {
            decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        }
    }

    fun setTranslanteBar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = window
                val decorView = window.decorView
                val option =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                decorView.systemUiVisibility = option
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = Color.TRANSPARENT
            } else {
                val window = window
                val attributes = window.attributes
                val flagTranslucentStatus = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                attributes.flags = attributes.flags or flagTranslucentStatus
                window.attributes = attributes
            }
        }
    }

    fun startActivity(cls: Class<*>) {
        startActivity(cls, null)
    }

    fun startActivity(cls: Class<*>, bundle: Bundle?,isAnim :Boolean = true) {
        val intent = Intent(this, cls)
        bundle?.let { intent.putExtras(it) }
        startActivity(intent)
        if (isAnim){
//            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }else{
//            overridePendingTransition(0, 0)
        }
    }

    fun startOverAnimActivity(cls: Class<*>, bundle: Bundle?,vararg sharedElements: Pair<View, String>) {
        val intent = Intent(this, cls)
        bundle?.let { intent.putExtras(it) }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, *sharedElements)
            ActivityCompat.startActivity( this, intent, activityOptions.toBundle())
        }else{
            startActivity(cls,bundle)
        }
    }

    fun startActivityForResult(cls: Class<*>, requestCode: Int) {
        startActivityForResult(cls, null, requestCode)
    }

    fun startActivityForResult(cls: Class<*>, bundle: Bundle?, requestCode: Int,isAnim :Boolean = true) {
        val intent = Intent(this, cls)
        bundle?.let { intent.putExtras(it) }
        startActivityForResult(intent, requestCode)

        if (isAnim){
//            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }else{
            overridePendingTransition(0, 0)
        }
    }

    fun showToastS(text: String?) = ToastUitl.showShort(text)

    fun showToastL(text: String?) = ToastUitl.showLong(text)

    fun startProgressDialog(text :String="",b:Boolean = true) {
        if (loadingDialog == null) loadingDialog = XLoadDialog()
        loadingDialog?.showDialogForLoading(this)
        loadingDialog?.setText(text)
        loadingDialog?.setCancelable(b)
    }

    fun stopProgressDialog() = loadingDialog?.cancelDialogForLoading()

    fun setBackTwo(isTwoBack: Boolean) {
        this.isTwoBack = isTwoBack
    }

    override fun finish() {
        super.finish()
        when(isAnimFish){

            "1" ->{
//                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
            }
            "2" ->{

            }
            "3"->{
                overridePendingTransition(0,0)
            }
        }
//        overridePendingTransition(R.anim.abc_fade_out, R.anim.abc_shrink_fade_out_from_bottom)
    }

    fun finshPage(isAnim :String = "1"){
        isAnimFish = isAnim
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        typeNet = "onDestroy"
        AppManager.getInstance().removeActivity(this)
        loadingDialog?.destroy()
    }

    override fun onBackPressed() {
        if (isTwoBack) {
            if (System.currentTimeMillis() - firstTime < 3000) {
                AppManager.getInstance().AppExit(this)
            } else {
                firstTime = System.currentTimeMillis()
                showToastS("再按一次返回桌面")
            }
        } else {
            finishAfterTransition()
        }
    }

    fun addFragment(cls: Class<*>, data: Any?) {
        val param = FragmentParam()
        param.cls = cls
        param.data = data
        param.addToBackStack = false
        processFragement(param)
    }

    private fun getFragmentTag(param: FragmentParam): String = StringBuilder(param.cls.toString() + param.tag).toString()

    private fun addDistinctEntry(sourceList: ArrayList<BaseFrg>?, entry: BaseFrg): Boolean {
        return sourceList != null && !sourceList.contains(entry) && sourceList.add(entry)
    }

    private fun processFragement(param: FragmentParam) {
        val containerId: Int = if (!TextUtils.isEmpty(param.Id)) getFragmentContainerId2() else getFragmentContainerId()
        val cls = param.cls
//        if (cls == null) return
        try {
            val e = getFragmentTag(param)
            var f1 = supportFragmentManager.findFragmentByTag(e)
            var fragment = f1?.let { it as BaseFrg } ?: cls.newInstance() as BaseFrg

            if (this.fragments == null) this.fragments = ArrayList()
            addDistinctEntry(this.fragments, fragment)
            val ft = supportFragmentManager.beginTransaction()

            if (param.type != FragmentParam.TYPE.ADD)
                ft.replace(containerId, fragment, e)
            else if (!fragment.isAdded) {
                if (this.currentFragment != null) this.currentFragment!!.onPause()
                ft.add(containerId, fragment, e)
                if (param.data != null) fragment.initRefreshData(param.data)
            } else {
                val var7 = this.fragments!!.iterator()
                while (var7.hasNext()) ft.hide(var7.next())
                if (this.currentFragment != null) this.currentFragment!!.onPause()
                ft.show(fragment)
                fragment.onResume()
                if (param.data != null) fragment.refreshData(param.data)
            }
            this.currentFragment = fragment
            if (param.addToBackStack) ft.addToBackStack(e)
            ft.commitAllowingStateLoss()
        } catch (var9: InstantiationException) {
            var9.printStackTrace()
        } catch (var10: IllegalAccessException) {
            var10.printStackTrace()
        }
    }

}