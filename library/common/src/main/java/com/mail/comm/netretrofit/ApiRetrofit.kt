package com.mail.comm.netretrofit

import android.widget.Toast
import com.google.gson.JsonSyntaxException
import com.mail.comm.app.AppConfig
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.net.ApiListener
import com.mail.comm.net.DataManager
import com.mail.comm.utils.NetworkUtils
import com.mail.comm.utils.ToastUitl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import org.xutils.common.util.LogUtil
import org.xutils.http.RequestParams
import org.xutils.x
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception
import java.net.ConnectException

class ApiRetrofit {

    var type = ""

    fun postApi(params: RequestParams, apiListener: ApiListener, type: String,isShowError:Boolean = true) {
        this.type = type
        var list = params.bodyParams
        var map = HashMap<String, String>()
        for (keyValue in list) {
            map[keyValue.key] = keyValue.value.toString()
        }
        var data = DataManager.singPostBody(JSONObject(map as Map<*, *>))
        LogUtil.e("initParams boby==" + AESCBCCrypt.aesDecrypt(data));
        map.clear()
        map["handshake"] = AppConfig.handshake
        map["data"] = data
        map["url"] = params.uri
        params.clearParams()

        apiListener.getCoroutineScope()?.launch {

            try {

                val data = withContext(IO) {
                    RetrofitFactory.getInstence()?.api()?.getData(map["url"]!!, map["handshake"]!!, map["data"]!!)
                }

                withContext(Dispatchers.Main) {

                    apiListener.onCompleteChild(data, type)
                }

            } catch (e: Exception) {

                withContext(Dispatchers.Main) {

                    apiListener.onExceptionTypeChild(type)

                    if (!isShowError){
                        return@withContext
                    }
                    if(!NetworkUtils.isConnected(x.app())){
                        LogUtil.e("withContext=="+"isConnected")
                        Toast.makeText(x.app(),"网络未连接",Toast.LENGTH_SHORT).show()
                        return@withContext
                    }
                    when (e) {

                        is ConnectException -> {
                            LogUtil.e("withContext=="+"ConnectException")
                            Toast.makeText(x.app(),"网络连接失败：请检查您的网络后重试\n详细信息：${e.message}",Toast.LENGTH_SHORT).show()
                        }

                        is HttpException ->{
                            LogUtil.e("withContext=="+"HttpException")
                            when (e.code()) {
                                404 ->  Toast.makeText(x.app(),"网络连接失败：您所请求的资源无法找到\n错误码：${e.code()}\n详细信息：${e.message()}",Toast.LENGTH_SHORT).show()
                                500 ->   Toast.makeText(x.app(),"网络连接失败：服务器异常，请稍后再试\n错误码：${e.code()}\n详细信息：${e.message()}",Toast.LENGTH_SHORT).show()
                                else ->   Toast.makeText(x.app(),"网络连接失败：网络异常，请稍后再试\n错误码：${e.code()}\n详细信息：${e.message()}",Toast.LENGTH_SHORT).show()
                            }
                        }

                        is IOException -> {
                            LogUtil.e("withContext=="+"IOException")
                            e.printStackTrace()
                        }

                        is JSONException -> {
                            LogUtil.e("withContext=="+"JSONException")
                            Toast.makeText(x.app(),"数据错误：服务器数据异常\n详细信息：${e.message}",Toast.LENGTH_SHORT).show()
                        }

                        is JsonSyntaxException ->  {
                            LogUtil.e("withContext=="+"JsonSyntaxException")
                            Toast.makeText(x.app(),"数据错误：服务器数据异常\n详细信息：${e.message}",Toast.LENGTH_SHORT).show()
                        }

                        is NoSuchFileException ->{
                            LogUtil.e("withContext=="+"NoSuchFileException")
                            Toast.makeText(x.app(),"文件不存在",Toast.LENGTH_SHORT).show()
                        }

                        else ->  {
                            LogUtil.e("withContext=="+"else")
                            Toast.makeText(x.app(),"未知错误：未知原因错误\n详细信息：${e.message}",Toast.LENGTH_SHORT).show()
                        }

                    }
                }


            }

        }

    }
}