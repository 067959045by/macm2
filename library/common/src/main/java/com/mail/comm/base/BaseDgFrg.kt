package com.mail.comm.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import java.lang.ref.WeakReference


abstract class BaseDgFrg : DialogFragment() {

    var mContext: BaseAty? = null

    var mRootView: View? = null

    abstract fun getLayoutId(): Int

    abstract fun getDialogStyle(): Int

    abstract fun getLayoutView(): View

    abstract fun canCancel(): Boolean

    abstract fun setWindowAttributes(window: Window?)


    fun <T : View> findViewById(id: Int): T? {
        return mRootView?.findViewById(id)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mContext = WeakReference(getActivity()).get() as BaseAty
        dialog?.getWindow()?.setWindowAnimations(getDialogStyle())
        if (getLayoutId() != 0) {
            mRootView = LayoutInflater.from(mContext).inflate(getLayoutId(), null);
        } else {
            mRootView = getLayoutView()
        }
        dialog?.setCancelable(canCancel());
        dialog?.setCanceledOnTouchOutside(canCancel())
        setWindowAttributes(dialog?.window)
        setWindowStyle(dialog?.window)
        return mRootView
    }


    fun  setWindowStyle( window:Window?) {
        if (window == null || getContext() == null) {
            return;
        }
        window.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
//        window.setGravity(Gravity.CENTER)
        window.getDecorView().setPadding(0, 0, 0, 0)
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        var lps = window?.getAttributes()
        lps.verticalMargin = 0f
        window.setAttributes(lps);
    }

}