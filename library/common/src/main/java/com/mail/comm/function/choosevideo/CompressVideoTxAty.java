package com.mail.comm.function.choosevideo;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mail.comm.R;
import com.mail.comm.base.BaseAty;
import com.mail.comm.utils.MyUtils;
import com.mail.comm.utils.MyUtils2;
import com.tencent.liteav.basic.log.TXCLog;
import com.tencent.ugc.TXVideoEditConstants;
import com.tencent.ugc.TXVideoEditer;
import com.tencent.ugc.TXVideoInfoReader;


public class CompressVideoTxAty extends BaseAty {

    private final String TAG = "CompressVideoTxAty";

    private TXVideoEditer mTXVideoEditer;
    private String mInputSource;
    private String mOutputPath;
    private TXVideoEditConstants.TXVideoInfo mTXVideoInfo;
    private int mVideoResolution;
    private VideoWorkProgressFragment             mFragmentWorkLoadingProgress;     // 生成视频的等待框
    private TXVideoEditer.TXVideoGenerateListener mTXVideoGenerateListener;
    private boolean                               mCompressing;
    private int                                   mBiteRate;                                          // 码率

    @Override
    public int getLayoutId() {
        return R.layout.aty_video_compress;
    }


    @Override
    public void initView() {

    }

    @Override
    public void requestData() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusColor("#000000");
        initData();
        startCompressVideo();
    }

    private void initData() {
        mTXVideoEditer = new TXVideoEditer(getApplicationContext());
        mVideoResolution = TXVideoEditConstants.VIDEO_COMPRESSED_480P;
//        String path = getIntent().getStringExtra("path");
        String uri = getIntent().getStringExtra("uri");

        // Android 10（Q）Google官方尚未强制启用 App 沙箱运行，当且仅当 targetSDK 为 29 的时候才会在沙箱下运行
        // Google官方预计 2020 年在 Android 11（R）强制启动沙箱机制，届时所有 app 无论 targetSDK 是否为 29，都运行在沙箱机制。
        // 因此为了您的 app 保持较高兼容性，推荐您在系统版本为 Android 10或以上的设备，都使用 Google 官方推荐的 uri 统一资源定位符的方式传递给 SDK。
//        if (Build.VERSION.SDK_INT >= 29) {
//            mInputSource = uri;
//        } else {
//            mInputSource = path;
//        }
        mInputSource = uri;
        mOutputPath = MyUtils2.generateVideoPath(this);
        int ret = mTXVideoEditer.setVideoPath(mInputSource);
        if (ret != 0) {
            if (ret == TXVideoEditConstants.ERR_SOURCE_NO_TRACK) {
                showToastS("不支持的视频格式");
            } else if (ret == TXVideoEditConstants.ERR_UNSUPPORT_AUDIO_FORMAT) {
                showToastS("暂不支持非单双声道的视频格式");
            }
            return;
        }
        mTXVideoInfo = TXVideoInfoReader.getInstance(this).getVideoFileInfo(mInputSource);
        // 部分机型上会产生00:00时长的视频文件，导致此处VideoInfo为空
        if (mTXVideoInfo == null) {
            showToastS("不支持的视频格式");
        }

        initListener();
    }


    private void initListener() {
        mTXVideoGenerateListener = new TXVideoEditer.TXVideoGenerateListener() {
            @Override
            public void onGenerateProgress(final float progress) {
                TXCLog.i(TAG, "onGenerateProgress = " + progress);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mFragmentWorkLoadingProgress.setProgress((int) (progress * 100));
                    }
                });
            }

            @Override
            public void onGenerateComplete(final TXVideoEditConstants.TXGenerateResult result) {
                TXCLog.i(TAG, "onGenerateComplete result retCode = " + result.retCode);
                mCompressing = false;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mFragmentWorkLoadingProgress != null && mFragmentWorkLoadingProgress.isAdded()) {
                            mFragmentWorkLoadingProgress.dismiss();
                        }
                        if (result.retCode == TXVideoEditConstants.GENERATE_RESULT_OK) {
                            // 生成成功
                            if (!TextUtils.isEmpty(mOutputPath)) {

                                Intent intent = new Intent();
                                intent.putExtra("type", "video_compress");
                                intent.putExtra("data", mOutputPath);
                                setResult(RESULT_OK, intent);
                                finish();

//                                startPublishActivity(mOutputPath);
                            }
                        } else {

                            Toast.makeText(CompressVideoTxAty.this, result.descMsg, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        };

        mTXVideoEditer.setVideoGenerateListener(mTXVideoGenerateListener);
    }


    private void startCompressVideo() {

        if (mFragmentWorkLoadingProgress == null) {
            initWorkLoadingProgress();
        }
        mFragmentWorkLoadingProgress.setProgress(0);
        mFragmentWorkLoadingProgress.setCancelable(false);
        mFragmentWorkLoadingProgress.show(getSupportFragmentManager(), "progress_dialog");

        mBiteRate = 2400;
        mTXVideoEditer.setVideoBitrate(mBiteRate);
        mTXVideoEditer.setCutFromTime(0, mTXVideoInfo.duration);

        if (!TextUtils.isEmpty(mOutputPath)) {
            mTXVideoEditer.generateVideo(mVideoResolution, mOutputPath);
        }
        mCompressing = true;
    }

    private void initWorkLoadingProgress() {
        if (mFragmentWorkLoadingProgress == null) {
            mFragmentWorkLoadingProgress = new VideoWorkProgressFragment();
            mFragmentWorkLoadingProgress.setOnClickStopListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTXVideoEditer != null) {
                        mTXVideoEditer.cancel();
                        mFragmentWorkLoadingProgress.dismiss();
                        mFragmentWorkLoadingProgress.setProgress(0);
                        finish();
                    }
                }
            });
        }
        mFragmentWorkLoadingProgress.setProgress(0);
    }

}
