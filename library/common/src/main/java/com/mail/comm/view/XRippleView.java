package com.mail.comm.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.zhy.autolayout.utils.AutoUtils;

import java.util.ArrayList;
import java.util.List;


//com.mail.comm.view.XRippleView
public class XRippleView extends View {

    private Paint mPaint;
    private List<Circle> mRipples;
    // 圆圈扩散的速度
    private int mSpeed = AutoUtils.getPercentWidthSize(80);

    // 圆圈是否为渐变模式
    private boolean mIsAlpha = true;

    public XRippleView(Context context) {
        this(context, null);
    }

    public XRippleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public XRippleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public XRippleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
        mPaint = new Paint();
        mPaint.setColor(Color.parseColor("#ffffff"));
        mPaint.setStrokeWidth(1);
        mPaint.setStyle(Paint.Style.STROKE);
        // 添加第一个圆圈
        mRipples = new ArrayList<>();
        mRipples.add(new Circle(with_circle, 255));
        mRipples.add(new Circle(with_circle + mSpeed * 1, 255));
        mRipples.add(new Circle(with_circle + mSpeed * 2, 255));
//        mRipples.add(new Circle(with_circle+mSpeed*3, 255));
//        mRipples.add(new Circle(with_circle+mSpeed*6, 255));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(with, with);
    }

    int with = AutoUtils.getPercentWidthSize(1000);
    int with_circle = AutoUtils.getPercentWidthSize(500)/2;
//    int space = AutoUtils.getPercentWidthSize(80);

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        canvas.save();
//        for (int i=0;i<5;i++){
//            canvas.drawCircle(with / 2, with / 2, with/2-space*i, mPaint);
//        }
//        invalidate();
//        canvas.restore();
        drawInCircle(canvas);
    }




    private  int calcAlpha(int r){
//        private fun calcAlpha(r:Float):Int = ((mRadiusMax - r)/ mRadiusMax * 120).toInt()
        int mRadiusMax = with/2;
       int s = (int) ((mRadiusMax - r)/ mRadiusMax * 120);
       return s ;
    }
    /**
     * 圆到宽度
     *
     * @param canvas
     */
    private void drawInCircle(Canvas canvas) {
        canvas.save();
        // 处理每个圆的宽度和透明度
        for (int i = 0; i < mRipples.size(); i++) {
            Circle c = mRipples.get(i);
            if (mIsAlpha) {
//                double alpha = 255-c.width * (255 / ((double) with/2-with_circle));
                double alpha = (255*c.width)/with_circle;
                c.alpha = (int) alpha;
            }
            mPaint.setAlpha(c.alpha);// （透明）0~255（不透明）
            canvas.drawCircle(with / 2, with / 2, c.width, mPaint);
        }

        for (int i = 0; i < mRipples.size(); i++) {
            Circle circle = mRipples.get(i);
            circle.width = circle.width + mSpeed;
            mRipples.set(i, circle);
        }

        Circle circle = mRipples.get(0);
//        //最外层波纹超过最大值时，重新把它添加到波纹队列末尾
        if (circle.width > with/2) {
            circle.width = with_circle;
            mRipples.set(0, circle);
        }

        ArrayList<Circle> list = new ArrayList<>();

        for ( int i = 1;i<mRipples.size();i++){
            list.add(mRipples.get(i));
        }

        list.add(mRipples.get(0));
        mRipples.clear();
        mRipples.addAll(list);
//        if ();

        postInvalidateDelayed(100);
        canvas.restore();
    }


    class Circle {
        Circle(int width, int alpha) {
            this.width = width;
            this.alpha = alpha;
        }

        int width;
        int alpha;
    }

}
