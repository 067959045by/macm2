package com.mail.comm.base;

public class ConstantModel {


    public final  static  String LOADVIEW_LOADING = "loadview_Loading";
    public final  static  String LOADVIEW_FINISH = "loadview_finish";
    public final  static  String LOADVIEW_EMPTY = "loadview_empty";
    public final  static  String LOADVIEW_ERROR = "loadview_error";

}
