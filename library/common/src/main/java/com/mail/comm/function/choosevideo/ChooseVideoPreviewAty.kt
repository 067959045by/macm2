package com.mail.comm.function.choosevideo

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.MediaController
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.VideoView
import com.mail.comm.R
import com.mail.comm.base.BaseAty

class ChooseVideoPreviewAty :BaseAty(){

    var relay_top_bg:RelativeLayout?=null
    var tv_title:TextView?=null
    var tv_right:TextView?=null
    var mVideoView: VideoView?=null
    var videoPath = ""
    var mPlayingPos = 0

    override fun getLayoutId(): Int = R.layout.aty_choose_video_previdew

    override fun initView() {
        relay_top_bg =findViewById(R.id.relay_top_bg)
        tv_title =findViewById(R.id.tv_title)
        tv_right =findViewById(R.id.tv_right)
        mVideoView =findViewById(R.id.videoView)
        videoPath = intent.getStringExtra("video_path").toString()
    }

    override fun requestData() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusColor("#62d7c8")
        initTopview(relay_top_bg,"#62d7c8")
//        initTopview(relay_top_bg,"#ffffff")
        tv_title?.text= "选择视频"
        tv_right?.text = "确认"
        tv_title?.setTextColor(Color.parseColor("#000000"))
        tv_right?.setTextColor(Color.parseColor("#000000"))
        tv_right?.visibility = View.VISIBLE

        var mMediaController =  MediaController(this);
        if (!TextUtils.isEmpty(videoPath)) {
            mVideoView?.setVideoPath(videoPath);
            mVideoView?.setMediaController(mMediaController);
            mVideoView?.seekTo(0);
            mVideoView?.requestFocus();
        }
    }

    fun mainClick(v:View){

        when(v.id){

            R.id.relay_back->{
                finish()
            }

            R.id.tv_right ->{
                var intent = Intent()
                intent.putExtra("data",videoPath)
                setResult(RESULT_OK,intent)
                finish()
            }

        }
    }

    override fun onPause() {
        super.onPause()
        mPlayingPos = mVideoView!!.currentPosition
        mVideoView?.stopPlayback()
    }

    override fun onResume() {
        super.onResume()
        mVideoView?.start()
        mVideoView?.seekTo(mPlayingPos)
    }


}