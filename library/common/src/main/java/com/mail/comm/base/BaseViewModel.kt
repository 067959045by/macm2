package com.mail.comm.base

import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mail.comm.app.AppConfig
import com.mail.comm.app.AppManager
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.net.ApiListener
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils2
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.xutils.common.Callback
import org.xutils.common.util.LogUtil
import org.xutils.http.RequestParams
import kotlin.system.exitProcess

open class BaseViewModel:ViewModel(),ApiListener{

    var page = 1
    var type_load = MutableLiveData<String>()

    override fun onCancelled(var1: Callback.CancelledException?) {
    }

    override fun onComplete(var2: String?, type: String?) {
    }

    override fun onCompleteChild(var2: String?, type: String?) {
        var map = JSONUtils.parseKeyAndValueToMap(var2)

        LogUtil.e("onComplete2=========$type,,,,$var2")
        if (type == "ping") {
            return
        }

        if (!map.containsKey("code")){
            return
        }

        if (map["code"] != "200" && map["code"] != "500" && map["code"] != "0") {
            AppManager.getInstance().killAllActivity()
            exitProcess(0)
            return
        }

        if (type == "version/update") {
            return
        }

        onComplete(var2,type)
        if (map["code"] == "200"&& AppConfig.debug) {
            var str = AESCBCCrypt.aesDecrypt(map["data"])
            MyUtils2.log("onComplete3333","onComplete3=======" + type+",,,,"+ str)
        }
    }

    override fun onError(var1: Map<String?, String?>?, var2: RequestParams?) {
    }

    override fun onExceptionType(type: String?) {
        onExceptionType(type)
    }

    override fun onExceptionTypeChild(type: String?) {
    }

    override fun getCoroutineScope(): CoroutineScope {
        return  CoroutineScope(Dispatchers.Main)
    }

    override fun onLoading(type: String, total: Long, current: Long, isDownloading: Boolean) {


    }


}