package com.mail.comm.view.load

import android.app.Activity
import android.app.Dialog
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.mail.comm.R

class XLoadDialog {

    var mLoadingDialog: Dialog? = null
    var imgv_down_loading: ImageView? = null
    var tv_down_loading: TextView? = null
    var animation2: Animation? = null

    fun showDialogForLoading(context: Activity?): Dialog? {

        if (mLoadingDialog != null) {
            if (!mLoadingDialog!!.isShowing) {
                mLoadingDialog?.show()
            }
        } else {
            val view = LayoutInflater.from(context).inflate(R.layout.loading_dialog, null)
            mLoadingDialog = Dialog(context!!, R.style.CustomProgressDialog)
            mLoadingDialog?.setCancelable(true)
            mLoadingDialog?.setCanceledOnTouchOutside(false)
            mLoadingDialog?.setContentView(view, LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT))
            imgv_down_loading = view.findViewById(R.id.down_loading)
            tv_down_loading = view.findViewById(R.id.tv_loading_dialog_text)
            animation2 = AnimationUtils.loadAnimation(context, R.anim.loading_dialog_rotate)
            animation2?.interpolator = LinearInterpolator()
            mLoadingDialog?.show()
        }
        imgv_down_loading?.startAnimation(animation2)
        return mLoadingDialog
    }

    fun setText(text: String) {
        if (mLoadingDialog?.isShowing == true) {
            tv_down_loading?.text = text
            if (TextUtils.isEmpty(text)) {
                tv_down_loading?.visibility = View.GONE
            } else {
                tv_down_loading?.visibility = View.VISIBLE
            }
        }
    }

    fun setCancelable(b:Boolean){
        mLoadingDialog?.setCancelable(b)
    }

    fun setIstransparent(b: Boolean = false) {
        if (mLoadingDialog?.isShowing == true) {
//            mLoadingDialog?.set
        }
    }


    fun cancelDialogForLoading() {
        mLoadingDialog?.cancel()
        imgv_down_loading?.clearAnimation()
    }

    fun destroy() {
        mLoadingDialog?.cancel()
        imgv_down_loading?.clearAnimation()
        mLoadingDialog = null
    }
}