package com.mail.comm.base

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import com.mail.comm.app.AppConfig
import com.mail.comm.app.AppManager
import com.mail.comm.interfaces.LifeCycleListener
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.net.ApiListener
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils2
import com.zhy.autolayout.AutoLayoutActivity
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.Callback
import org.xutils.common.util.LogUtil
import org.xutils.http.RequestParams
import kotlin.collections.ArrayList

open class AbsAty : AutoLayoutActivity(), ApiListener {

    var mContext: Context? = null

    var mLifeCycleListeners: ArrayList<LifeCycleListener>? = null

    var typeNet = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        mLifeCycleListeners = ArrayList()
        for (listener in mLifeCycleListeners!!) {
            listener.onCreate()
        }
    }

    override fun onDestroy() {
        for (listener in mLifeCycleListeners!!) {
            listener.onDestroy()
        }
        mLifeCycleListeners?.clear()
        mLifeCycleListeners = null
        super.onDestroy()
    }

    override fun onStart() {
        super.onStart()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onStart()
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onReStart()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onResume()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onPause()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onStop()
            }
        }
    }

    fun addLifeCycleListener(listener: LifeCycleListener?) {
        if (mLifeCycleListeners != null && listener != null) {
            mLifeCycleListeners!!.add(listener)
        }
    }

    fun addAllLifeCycleListener(listeners: ArrayList<LifeCycleListener>?) {
        if (mLifeCycleListeners != null && listeners != null) {
            mLifeCycleListeners!!.addAll(listeners)
        }
    }

    fun removeLifeCycleListener(listener: LifeCycleListener?) {
        if (mLifeCycleListeners != null) {
            mLifeCycleListeners!!.remove(listener!!)
        }
    }

    override fun onCancelled(var1: Callback.CancelledException?) {}

    override fun onCompleteChild(var2: String?, type: String?){
        if (typeNet == "onDestroy") {
            return
        }
        onComplete(var2 ,type)
    }


    override fun onComplete(var2: String?, type: String?) {
        Log.e("onComplete2=========","$type,,,,$var2")
        var map = JSONUtils.parseKeyAndValueToMap(var2)
        if (type == "ping") {
            return
        }

        if (!map.containsKey("code")){
            return
        }

        if (map["code"] != "200" && map["code"] != "500" && map["code"] != "0") {
//            Toast.makeText(this,"服务器异常：${map["code"]}：${map["message"]}",Toast.LENGTH_SHORT).show()
//            AppManager.getInstance().killAllActivity()
//            exitProcess(0)
            AppManager.getInstance().AppExit(this)
            return
        }

        if (type == "version/update"||type=="area/json") {
            return
        }

        if (map["code"] == "200"&&AppConfig.debug) {
            var str = AESCBCCrypt.aesDecrypt(map["data"])
            MyUtils2.log("onComplete3333","onComplete3=======" + type+",,,,"+ str)
        }

    }

    override  fun onExceptionTypeChild(type: String?){
        if (typeNet == "onDestroy") {
            return
        }
        onExceptionType(type)
    }

    override fun getCoroutineScope(): LifecycleCoroutineScope {
        return  lifecycleScope
    }

    override fun onExceptionType(type: String?) {

    }

    override fun onError(var1: Map<String?, String?>?, var2: RequestParams?) {
    }


    override fun onLoading(type: String, total: Long, current: Long, isDownloading: Boolean) {
    }

    //非沉淀状态栏   背景-资源文件
    fun initTopview(relay: RelativeLayout?,bgColor:String = "#ffffff") {
        val lp = relay?.layoutParams as RelativeLayout.LayoutParams
        lp.height = AutoUtils.getPercentHeightSize(130)
        relay.layoutParams = lp
        relay.setBackgroundColor(Color.parseColor(bgColor))
    }

}


