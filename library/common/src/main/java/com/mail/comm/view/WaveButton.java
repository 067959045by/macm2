package com.mail.comm.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ICELEE on 5/1/2017.
 */

public class WaveButton extends View {

    private static final String TAG = "ICE";
    private Paint mPaint;
    private int mRadius;//里面圆圈的半径
    private int mWidth;//控件的宽度
    private int mStrokeWidth;//波浪的宽度
    private int mFillColor;//圆圈填充颜色
    private int mCircleStrokeColor;//圆圈边缘颜色
    private int gapSize;//波浪之间的距离
    private int firstRadius;//第一个圆圈的半径
    private int numberOfCircle;//显示波浪的数量
    private int mLineColor;//波浪线的颜色
    private boolean isFirstTime = true;//是否是第一次开启动画
    private OnClickListener mClickListener;//点击事件监听器
    private float mDownX,mDownY;//手指按下的坐标

    //省略了前面两个少参数的构造函数 源码里面是用前面两个调用这个
    public WaveButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        //省略了各个成员变量的初始化过程
        mPaint = new Paint();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //省略了测量的代码  在源码里面有简单的测量
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.translate(mWidth/2,mWidth/2);//平移


        //画中间的圆
        mPaint.setAlpha(255);
        mPaint.setColor(mFillColor);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(0,0,mRadius,mPaint);
        //画圆的边
        mPaint.setStrokeWidth(mStrokeWidth);
        mPaint.setColor(mCircleStrokeColor);
        mPaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(0,0,mRadius,mPaint);
        //画文字
        Rect rect = new Rect();//文字的区域
//        mTextPaint.getTextBounds(mText,0,mText.length(),rect);
        int height = rect.height();
        int width = rect.width();
//        canvas.drawText(mText,-width/2,height/2,mTextPaint);

        //画周围的波浪
        firstRadius += 3;//每次刷新半径增加3像素
        firstRadius %= (mWidth/2);//控制在控件的范围中
        if(firstRadius<mRadius) isFirstTime =false;
        firstRadius = checkRadius(firstRadius);//检查半径的范围
        mPaint.setColor(mLineColor);
        mPaint.setStyle(Paint.Style.STROKE);

        //画波浪
        for (int i = 0; i < numberOfCircle; i++) {
            int radius = (firstRadius + i*gapSize ) % (mWidth/2);
            if(isFirstTime && radius>firstRadius) continue;
            radius = checkRadius(radius);//检查半径的范围
            //用半径来计算透明度  半径越大  越透明
            double x = (mWidth/2 -radius)*1.0 /(mWidth/2 - mRadius);
            mPaint.setAlpha((int) (255*x));
            canvas.drawCircle(0,0,radius,mPaint);
        }

    }

    //检查波浪的半径  如果小于圆圈，那么加上圆圈的半径
    private int checkRadius(int radius) {
        if(radius<mRadius){
            return radius+mRadius + gapSize;
        }
        return radius;
    }

//    //dp转像素
//    public int dip2px(float dpValue) {
//        final float scale = mContext.getResources().getDisplayMetrics().density;
//        return (int) (dpValue * scale + 0.5f);
//    }

    //不断重绘  展示出波浪效果
    public void startAnimation(){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                postInvalidate();
            }
        },0,50);

    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        mClickListener = l;
    }

    //设置只有点击圆圈才有点击效果 点击波浪不能触发点击效果
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){

            case MotionEvent.ACTION_DOWN:
                mDownX = event.getX();
                mDownY = event.getY();
                return checkIsInCircle((int)mDownX,(int)mDownY);

            case MotionEvent.ACTION_UP:
                int upX = (int) event.getX(),upY = (int) event.getY();
                if(checkIsInCircle(upX,upY) && mClickListener!=null){
                    mClickListener.onClick(this);//触发点击事件
                }
                break;

        }
        return true;
    }

    /**
     * 检查点x,y是否落在圆圈内
     * @param x
     * @param y
     * @return
     */
    private boolean checkIsInCircle(int x, int y){

        int centerX = (getRight() + getLeft())/2;
        int centerY = (getTop() + getBottom())/2;
        return  Math.pow(x-centerX,2)+Math.pow(y-centerY,2) < Math.pow(mRadius,2);
    }
}

