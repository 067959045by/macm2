package com.mail.comm.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import androidx.core.content.FileProvider
import com.mail.comm.app.AppManager
import com.mail.comm.app.BaseApp
import org.xutils.common.util.LogUtil
import org.xutils.x
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class MyUtils {

    companion object {


        fun  saveMyInfo( context:Context, map_info: HashMap<String,String>){


            BaseApp.instance?.saveOneMapData("info_nick",map_info["nick"])
            BaseApp.instance?.saveOneMapData("info_code",map_info["code"])
            BaseApp.instance?.saveOneMapData("info_flow",map_info["flow"])
            BaseApp.instance?.saveOneMapData("info_fans",map_info["fans"])
            BaseApp.instance?.saveOneMapData("info_avatar",map_info["avatar"])
            BaseApp.instance?.saveOneMapData("info_intro",map_info["intro"])
            BaseApp.instance?.saveOneMapData("info_city",map_info["city"])
            BaseApp.instance?.saveOneMapData("info_years",map_info["years"])
            BaseApp.instance?.saveOneMapData("info_job",map_info["job"])
            BaseApp.instance?.saveOneMapData("info_gender",map_info["gender"])
            BaseApp.instance?.saveOneMapData("info_birth",map_info["birth"])
            BaseApp.instance?.saveOneMapData("info_habit",map_info["habit"])
            BaseApp.instance?.saveOneMapData("info_invite",map_info["invite"])
            if (map_info.containsKey("message_type")){
                BaseApp.instance?.saveOneMapData("message_type",map_info["message_type"])
            }
            if (map_info.containsKey("parent_code")){
                BaseApp.instance?.saveOneMapData("info_parent_code",map_info["parent_code"])
            }
            if (map_info.containsKey("mobile")){
                BaseApp.instance?.saveOneMapData("info_mobile",map_info["mobile"])
            }

            if (map_info.containsKey("is_creator")){
                PreferencesUtils.putString(context,"info_is_creator",map_info["is_creator"])
            }
            if (map_info.containsKey("vip_level")){
                BaseApp.instance?.saveOneMapData("info_vip_level",map_info["vip_level"])
            }
            if (map_info.containsKey("vip_expired")){
                BaseApp.instance?.saveOneMapData("info_vip_expired",map_info["vip_expired"])
            }
        }

        fun getInfoDetails(context:Context,key:String):String{
            var str =PreferencesUtils.getString(context,key,"")
            return str
        }
        fun saveInfoDetails(context:Context,key:String,value:String){
//            PreferencesUtils.putString(context,key,value)
            BaseApp.instance?.saveOneMapData(key,value)
        }

        fun saveInfoDetailsSp(context:Context,key:String,value:String){
            PreferencesUtils.putString(context,key,value)
//          BaseApp.instance?.saveOneMapData(key,value)
        }

        fun getVersionName(context: Context): String {
            try {
                val packageManager = context.packageManager
                val packageInfo = packageManager.getPackageInfo(context.packageName, 0)
                return packageInfo.versionName
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return ""
        }

        fun browerUpdate(url: String) {
            val intent = Intent()
            intent.action = "android.intent.action.VIEW"
            val content_url = Uri.parse(url)
            intent.data = content_url
            AppManager.getInstance().topActivity.startActivity(intent)
        }

        fun getVersionCode(context: Context): Int {
            try {
                val packageManager = context.packageManager
                val packageInfo = packageManager.getPackageInfo(context.packageName, 0)
                return packageInfo.versionCode
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return 0
        }

        fun installApk(file: File, fileProvider: String, context: Context) {
            val info = x.app().applicationInfo
            val apkUri: Uri = if (info.targetSdkVersion >= 24 && Build.VERSION.SDK_INT >= 24) {
                FileProvider.getUriForFile(context, fileProvider, file)
            } else {
                Uri.fromFile(file)
            }
            val intent = Intent(Intent.ACTION_VIEW)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive")
            context.startActivity(intent)
        }


        fun fileIsExists(filename: String?): Boolean {
            val s = Environment.getExternalStorageDirectory().absolutePath + "/分享赚/" + filename + ".mp4"
            return File(s).exists()
        }

        fun getPath():String= Environment.getExternalStorageDirectory().absolutePath + "/分享赚/"  + "icon.zip"

        fun getVideoPath(tag: String?): String = Environment.getExternalStorageDirectory().absolutePath + "/分享赚/" + tag + ".mp4"

        fun getCurrentYear():Int= Integer.parseInt( SimpleDateFormat("yyyy").format(Date()))

        fun getCurrentMonth():Int= Integer.parseInt( SimpleDateFormat("MM").format(Date()))

        fun getCurrentDay():Int= Integer.parseInt( SimpleDateFormat("dd").format(Date()))

        fun getMonthOfDays(year:Int,month:Int):Int{
            val a = Calendar.getInstance()
            a.set(Calendar.YEAR, year)
            a.set(Calendar.MONTH, month - 1)
            a.set(Calendar.DATE, 1)//把日期设置为当月第一天
            a.roll(Calendar.DATE, -1)//日期回滚一天，也就是最后一天
            return a.get(Calendar.DATE)
        }

        fun getFirstDayOfMonth(year:Int,month:Int):Int{
            val a = Calendar.getInstance()
            a.set(Calendar.DAY_OF_MONTH, 1)
            a.set(Calendar.YEAR, year)
            a.set(Calendar.MONTH, month-1)
            return a.get(Calendar.DAY_OF_WEEK)
        }

        fun getTimestampDay(year:Int,month:Int,day:Int,hour:Int):Long{
            var cal = Calendar.getInstance()
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH,month-1)
            cal.set(Calendar.DATE, day)//把日期设置为当月第一天
            cal.set(Calendar.HOUR_OF_DAY,hour)
            cal.set(Calendar.SECOND, 0)
            cal.set(Calendar.MINUTE, 0)
            cal.set(Calendar.MILLISECOND, 0)
            return cal.timeInMillis/1000
        }

        /**
         * 根据毫秒返回时分秒
         */
        fun getFormatHMS(time: Int): String {
            var s = (time % 60)//秒
            var m = (time / 60)//分
            var h= (time/3600)//秒
            return String.format("%02d:%02d", m, s)
        }

        fun dpTopx(dpVal: Int): Int {
            var scale = x.app().getResources().getDisplayMetrics().density;
            return (scale * dpVal + 0.5f).toInt()
        }
    }

}





