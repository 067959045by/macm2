package com.mail.myapplication.ui.msg.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ChatpanelList {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "user_code")
    public String user_code;

    @ColumnInfo(name = "to_user_code")
    public String to_user_code;

    @ColumnInfo(name = "time")
    public String time;

    @ColumnInfo(name = "msg")
    public String msg;

    @ColumnInfo(name = "message_id")
    public String message_id;


    public ChatpanelList(String user_code, String to_user_code, String time, String msg, String message_id) {
        this.user_code = user_code;
        this.to_user_code = to_user_code;
        this.time = time;
        this.msg = msg;
        this.message_id = message_id;
    }

    // public Chatpanel(String user_code, String to_user_code,
//                     String user_nick, String to_user_nick,
//                     String user_avatar, String to_user_avatar,
//                     String type,
//                     String text,
//                     String img, String thumbImage, String img_name, String send_time) {
//        this.user_code = user_code;
//        this.to_user_code = to_user_code;
//        this.user_nick = user_nick;
//        this.to_user_nick = to_user_nick;
//        this.user_avatar = user_avatar;
//        this.to_user_avatar = to_user_avatar;
//        this.type = type;
//        this.text = text;
//        this.img = img;
//        this.thumbImage = thumbImage;
//        this.img_name = img_name;
//        this.send_time = send_time;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
