package com.mail.myapplication.ui.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.core.content.PermissionChecker;

import com.google.gson.Gson;
import com.mail.comm.base.BaseAty;
import com.mail.comm.net.DataManager;
import com.mail.comm.utils.PreferencesUtils;
import com.mail.comm.utils.ToastUitl;
import com.mail.myapplication.R;
import com.snail.antifake.deviceid.emulator.EmuCheckUtil;
import com.snail.antifake.deviceid.macaddress.MacAddressUtils;

import org.json.JSONObject;
import org.xutils.common.util.LogUtil;
import org.xutils.x;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.UUID;

import master.flame.danmaku.danmaku.model.BaseDanmaku;

public class MyUtils3 {



    // 两次点击按钮之间的点击间隔不能少于1000毫秒
    private static final int MIN_CLICK_DELAY_TIME = 1000;
    private static long lastClickTime;

    public static boolean isFastClick() {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }


    //    {
//        "phone": "+855962089283",
//            "mac_address": "6A:B7:07:FB:94:9D",
//            "cpu_type": "armeabi-v7a",
//            "jg_id": "jg_1507bfd3f71d91e29c3",
//            "sys_v": "10",
//            "network_type": "wifi",
//            "phone_model": "ANA-NX9-HUAWEI",
//            "is_mulator": "false"
//    }
//    {"type":1, "text":"", "form_user":{ "user_code": "用户code", "avatar": "用户头像", "nick": "用户昵称"},
//        "to_user":{ "user_code": "用户code", "avatar": "用户头像路径，非完整URL", "nick": "用户昵称"}}

    //vip等级（1月度会员3季度会员6半年会员12年度会员13超级会员20体验会员
    public static void setVipLevel(String level, ImageView imageView, Integer index) {

        switch (level) {
            case "0":
                imageView.setVisibility(View.GONE);
                break;
            case "1":
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_71);
                break;
            case "3":
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_91);
                break;
            case "12":
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_92);
                break;
            case "13":
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_93);
                break;
            case "20":
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_94);
                break;
            default:
                imageView.setVisibility(View.GONE);
                break;
        }
    }

    public static void setVipLevel2(String level, ImageView imageView, ViewGroup v, Integer index) {
        switch (level) {
            case "0":
                imageView.setVisibility(View.GONE);
                v.setVisibility(View.GONE);
                break;
            case "1":
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_71);
                v.setVisibility(View.VISIBLE);
                break;
            case "3":
                imageView.setVisibility(View.VISIBLE);
                v.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_91);
                break;
            case "12":
                imageView.setVisibility(View.VISIBLE);
                v.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_92);
                break;
            case "13":
                imageView.setVisibility(View.VISIBLE);
                v.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_93);
                break;
            case "20":
                imageView.setVisibility(View.VISIBLE);
                v.setVisibility(View.VISIBLE);
                imageView.setImageResource(R.mipmap.ic_94);
                break;
            default:
                imageView.setVisibility(View.GONE);
                v.setVisibility(View.GONE);
                break;
        }
    }

    public void test01() {
        int colors[]={R.color.black};
    }

    public static String getMD5Str(String str) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            System.exit(-1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte[] byteArray = messageDigest.digest();
        StringBuffer md5StrBuff = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
        }
        return md5StrBuff.toString().toLowerCase();
    }


    public static String getTextMsg(String to_user_code, String to_avatar, String to_nick
            , String user_code, String avatar, String nick, String text, String time, String message_id) {

        String resource_cdn = PreferencesUtils.getString(x.app(), "resource_cdn", "");

        HashMap<String, String> map_to_user = new HashMap<>();
        map_to_user.put("user_code", to_user_code);
        map_to_user.put("avatar", to_avatar.replace(resource_cdn, ""));
        map_to_user.put("nick", to_nick);
        HashMap<String, String> map_user = new HashMap<>();
        map_user.put("user_code", user_code);
        map_user.put("avatar", avatar.replace(resource_cdn, ""));
        map_user.put("nick", nick);

//        String json_to_user = new JSONObject(map_to_user).toString();
//        String json_form_user = new JSONObject(map_user).toString();

        HashMap<String, Object> map = new HashMap<>();


        map.put("type", "1");
        map.put("time", time);
        map.put("message_id", message_id);
        map.put("text", text);
//        map.put("from_user", json_form_user);
//        map.put("to_user", json_to_user);
        map.put("from_user", map_user);
        map.put("to_user", map_to_user);
        map.put("send_time", System.currentTimeMillis() + "");

        //new JSONObject(map)
//        String json = DataManager.singPostBody(new JSONObject(map));
//        String encoded = Base64.encodeToString(new Gson().toJson(map).getBytes(), Base64.NO_WRAP);
        String json = DataManager.singPostBody2(new Gson().toJson(map));
//        String json = DataManager.singPostBody2(encoded);
        LogUtil.e("getTextMsg=======" + json);
        return json;

    }


    public static String getImgMsg(String to_user_code, String to_avatar, String to_nick
            , String user_code, String avatar, String nick, String img, String thumbImage, String name
            , String time, String message_id) {

        String resource_cdn = PreferencesUtils.getString(x.app(), "resource_cdn", "");


        HashMap<String, String> map_to_user = new HashMap<>();
        map_to_user.put("user_code", to_user_code);
        map_to_user.put("avatar", to_avatar.replace(resource_cdn, ""));
        map_to_user.put("nick", to_nick);
        HashMap<String, String> map_user = new HashMap<>();
        map_user.put("user_code", user_code);
        map_user.put("avatar", avatar.replace(resource_cdn, ""));
        map_user.put("nick", nick);

//        String json_to_user = new JSONObject(map_to_user).toString();
//        String json_form_user = new JSONObject(map_user).toString();

        HashMap<String, Object> map = new HashMap<>();

        map.put("type", "2");
        map.put("time", time);
        map.put("message_id", message_id);
        map.put("image", img);
        map.put("thumb_image", thumbImage);
        map.put("name", name);
        map.put("from_user", map_user);
        map.put("to_user", map_to_user);
        map.put("send_time", System.currentTimeMillis() + "");

//        String json = DataManager.singPostBody(new JSONObject(map));
        String json = DataManager.singPostBody2(new Gson().toJson(map));

        return json;

    }

    public static void installApk(File file, String fileProvider, Context context) {
        try {
            ApplicationInfo info = x.app().getApplicationInfo();
            Uri apkUri;
            if (info.targetSdkVersion >= 24 && Build.VERSION.SDK_INT >= 24) {
                apkUri = FileProvider.getUriForFile(context, fileProvider, file);
            } else {
                apkUri = Uri.fromFile(file);
            }

//          ACTION_INSTALL_PACKAGE
//          Intent intent = new Intent(Intent.ACTION_VIEW);
            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);

            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
            context.startActivity(intent);
        } catch (Exception e) {

        } finally {
            try {
                Thread.sleep(5000);
                android.os.Process.killProcess(android.os.Process.myPid());

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    public static String getDeviceInfoJson(Context context) {
        String jsonString = "";
        try {
            HashMap<String, String> map = new HashMap<>();
            map.put("sys_v", "Android-" + android.os.Build.VERSION.RELEASE);
            map.put("cpu_type", android.os.Build.CPU_ABI);
            map.put("phone_model", android.os.Build.BRAND + "-" + android.os.Build.MODEL);
            map.put("network_type", getNetType(context));
            map.put("mac_address", getMacAddress(context));
            map.put("jg_id", PreferencesUtils.getString(context, "registrationId", ""));
            map.put("phone", "");
            map.put("device_id", getDeviceId());
            map.put("is_mulator", String.valueOf(mayOnEmulator(context)));
            jsonString = new Gson().toJson(map);
        } catch (Exception e) {
            jsonString = "";
        }

        LogUtil.e("djfkdjsfksdfjkfdsf===" + jsonString);
        return jsonString;
    }


//    private void addDanmaku(boolean islive,Context mContext) {
//        BaseDanmaku danmaku = mContext.mDanmakuFactory.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL);
//        if (danmaku == null || mDanmakuView == null) {
//            return;
//        }
//        // for(int i=0;i<100;i++){
//        // }
//        danmaku.text = "这是一条弹幕" + System.nanoTime();
//        danmaku.padding = 5;
//        danmaku.priority = 0;  // 可能会被各种过滤器过滤并隐藏显示
//        danmaku.isLive = islive;
//        danmaku.setTime(mDanmakuView.getCurrentTime() + 1200);
//        danmaku.textSize = 25f * (mParser.getDisplayer().getDensity() - 0.6f);
//        danmaku.textColor = Color.RED;
//        danmaku.textShadowColor = Color.WHITE;
//        // danmaku.underlineColor = Color.GREEN;
//        danmaku.borderColor = Color.GREEN;
//        mDanmakuView.addDanmaku(danmaku);
//
//    }


    public static boolean mayOnEmulator(Context context) {
        boolean str = false;
        try {
            str = EmuCheckUtil.mayOnEmulator(context);
        } catch (Exception e) {
            str = false;
        }
        return str;
    }

    public static String getMacAddress(Context context) {
        String str = "";
        try {
            str = MacAddressUtils.getMacAddress(context);
        } catch (Exception e) {
            str = "";
        }
        return str;
    }

    public static String getDeviceId() {
        String number = "";
        try {
            TelephonyManager tm = (TelephonyManager) x.app().getSystemService(Context.TELEPHONY_SERVICE);
            number = tm.getDeviceId();
            if (!TextUtils.isEmpty(number)) {
                number = "id_" + number;
            }

        } catch (Exception ex) {
            number = "";
        }
        return number;
    }


    @SuppressLint("WrongConstant")
    public static String getPhone2(Context context) {
        String number = "";
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            if (PermissionChecker.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                number = tm.getLine1Number();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return number;
    }


    public static String getNetType(Context context) {
        String type2 = "no network";

        try {
            int type = NetworkUtils.getNetWorkType(context);
            switch (type) {
                case -1:
                    type2 = "no network";
                    break;
                case 1:
                    type2 = "wifi";
                    break;
                case 2:
                    type2 = "2G";
                    break;
                case 3:
                    type2 = "3G";
                    break;
                case 4:
                    type2 = "4G";
                    break;
                case 5:
                    type2 = " unknown network";
                    break;
                default:
                    type2 = " unknown network";
                    break;
            }
        } catch (Exception e) {
            type2 = "";
        }
        return type2;
    }

    public static int getAPNType(Context context) {
        int netType = 0;
        ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo == null) {
            return netType;
        }
        int nType = networkInfo.getSubtype();
        if (nType == ConnectivityManager.TYPE_WIFI) {
            netType = 1;// wifi
        } else if (nType == ConnectivityManager.TYPE_MOBILE) {
            int nSubType = networkInfo.getSubtype();
            TelephonyManager mTelephony = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            if (nSubType == TelephonyManager.NETWORK_TYPE_LTE) {
                netType = 4;// 4G
            } else if (nSubType == TelephonyManager.NETWORK_TYPE_UMTS
                    && !mTelephony.isNetworkRoaming()) {
                netType = 3;// 3G
            } else {
                netType = 2;// 2G
            }
        }
        return netType;
    }

    public static void openBrowser(BaseAty context, String url) {
        final Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
        // 官方解释 : Name of the component implementing an activity that can display the intent
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            final ComponentName componentName = intent.resolveActivity(context.getPackageManager());
            context.startActivity(Intent.createChooser(intent, "请选择浏览器"));
        } else {
            context.showToastS("链接错误或无浏览器");
        }
    }


    public static void openTowPage(BaseAty context, String url) {

        if (TextUtils.isEmpty(url)) {
            return;
        }

        if (url.startsWith("url")) {
            int index = url.indexOf("//");
            openBrowser(context, url.substring(index + 2));
        }
    }


    public static void setText(Context context, TextView tv,
                               ArrayList<String> str, ArrayList<Integer> color,
                               ArrayList<Float> text_size_list, ClickListener clickListener) {
// 累加数组所有的字符串为一个字符串
        StringBuffer long_str = new StringBuffer();
        for (int i = 0; i < str.size(); i++) {
            long_str.append(str.get(i));
        }
        SpannableString builder = new SpannableString(long_str.toString());
// 设置不同字符串的点击事件
        for (int i = 0; i < str.size(); i++) {
            int p = i;
            int star = long_str.toString().indexOf(str.get(i));
            int end = star + str.get(i).length();
            builder.setSpan(new Clickable(clickListener, p), star, end,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

// 设置不同字符串的颜色

        ArrayList<ForegroundColorSpan> foregroundColorSpans = new ArrayList<ForegroundColorSpan>();
        for (int i = 0; i < color.size(); i++) {
            foregroundColorSpans.add(new ForegroundColorSpan(color.get(i)));
        }
        for (int i = 0; i < str.size(); i++) {
            int star = long_str.toString().indexOf(str.get(i));
            int end = star + str.get(i).length();
            builder.setSpan(foregroundColorSpans.get(i), star, end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
// 设置不同字符串的字号
        ArrayList<AbsoluteSizeSpan> absoluteSizeSpans = new ArrayList<AbsoluteSizeSpan>();
        for (int i = 0; i < color.size(); i++) {
            absoluteSizeSpans.add(new AbsoluteSizeSpan(sp2px(context,
                    text_size_list.get(i))));
        }
        for (int i = 0; i < str.size(); i++) {
            int star = long_str.toString().indexOf(str.get(i));
            int end = star + str.get(i).length();
            builder.setSpan(absoluteSizeSpans.get(i), star, end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
// 设置点击后的颜色为透明，否则会一直出现高亮
        tv.setHighlightColor(Color.TRANSPARENT);
        tv.setClickable(true);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(builder);
    }

    public static int sp2px(Context context, float spValue) {
//        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
//        return (int) (spValue * fontScale + 0.5f);
        return (int) spValue;
    }

    /**
     * 获取系统相册目录
     *
     * @return
     */
    public static String getGalleryPath() {
        String galleryPath = Environment.getExternalStorageDirectory()
                + File.separator + Environment.DIRECTORY_DCIM
                + File.separator + "Camera" + File.separator;
        return galleryPath;
    }


    /**
     * view转bitmap
     */
    public static Bitmap viewConversionBitmap(View v) {
        int w = v.getWidth();
        int h = v.getHeight();

        Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmp);

        c.drawColor(Color.WHITE);
        /** 如果不设置canvas画布为白色，则生成透明 */

        v.layout((int) v.getX(), (int) v.getY(), w + (int) v.getX(), h + (int) v.getY());
        v.draw(c);

        return bmp;
    }

    /**
     * @param rootView 获取的bitmap数据
     * @param picName  自定义的图片名
     */
    public static void saveBmpGallery(Context context, View rootView, String picName) {
        Bitmap bitmap = viewConversionBitmap(rootView);
        if (picName.contains(".jpg")) {
            picName.replace(".jpg", "");
        }
        //系统相册目录
        String galleryPath = getGalleryPath();
        // 声明文件对象
        File file = null;
        // 声明输出流
        FileOutputStream outStream = null;
        try {
            // 如果有目标文件，直接获得文件对象，否则创建一个以filename为名称的文件
            file = new File(galleryPath, picName);
            // 获得输出流，如果文件中有内容，追加内容
            outStream = new FileOutputStream(picName);
            if (null != outStream) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
            }

        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            try {
                if (outStream != null) {
                    outStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//通知相册更新
        MediaStore.Images.Media.insertImage(context.getContentResolver(),
                bitmap, picName, null);
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri uri = Uri.fromFile(file);
        intent.setData(uri);
        context.sendBroadcast(intent);
        ToastUitl.showShort( "已保存");
    }


    /**
     * @param picName  自定义的图片名
     */
    public static void saveBimap(Context context,Bitmap bitmap, String picName) {
        if (picName.contains(".jpg")) {
            picName.replace(".jpg", "");
        }
        //系统相册目录
        String galleryPath = getGalleryPath();
        // 声明文件对象
        File file = null;
        // 声明输出流
        FileOutputStream outStream = null;
        try {
            // 如果有目标文件，直接获得文件对象，否则创建一个以filename为名称的文件
            file = new File(galleryPath, picName);
            // 获得输出流，如果文件中有内容，追加内容
            outStream = new FileOutputStream(picName);
            if (null != outStream) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
            }

        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            try {
                if (outStream != null) {
                    outStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//通知相册更新
        MediaStore.Images.Media.insertImage(context.getContentResolver(),
                bitmap, picName, null);
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri uri = Uri.fromFile(file);
        intent.setData(uri);
        context.sendBroadcast(intent);
        ToastUitl.showShort( "已保存");
    }

    public static void copyString(String string, Context context) {
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        cm.setText(string);
    }

    /**
     * 显示键盘
     *
     * @param et 输入焦点
     */
    public static void showInput(final EditText et, BaseAty activity) {
//        et.requestFocus();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }


    /**
     * 隐藏键盘
     */
    public static void hideInput(BaseAty activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
        View v = activity.getWindow().peekDecorView();
        if (null != v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public static void openBrowser(Context context, String url) {
        final Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
        // 官方解释 : Name of the component implementing an activity that can display the intent
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            final ComponentName componentName = intent.resolveActivity(context.getPackageManager());
            context.startActivity(Intent.createChooser(intent, "请选择浏览器"));
        } else {
            ToastUitl.showToast( "链接错误或无浏览器", Toast.LENGTH_SHORT);
        }
    }

    public static String getJson(String fileName, Context context) {
        //将json数据变成字符串
        StringBuilder stringBuilder = new StringBuilder();
        try {
            //获取assets资源管理器
            AssetManager assetManager = context.getAssets();
            //通过管理器打开文件并读取
            BufferedReader bf = new BufferedReader(new InputStreamReader(
                    assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    /**
     * @param bitmap     需要打马赛克的图片
     * @param BLOCK_SIZE 马赛克方块
     * @description 图片打马赛克
     */


    public static Bitmap BitmapMosaic(Bitmap bitmap, int BLOCK_SIZE) {

        if (bitmap == null || bitmap.getWidth() == 0 || bitmap.getHeight() == 0
                || bitmap.isRecycled()) {
            return null;
        }
        int mBitmapWidth = bitmap.getWidth();
        int mBitmapHeight = bitmap.getHeight();
        Bitmap mBitmap = Bitmap.createBitmap(mBitmapWidth, mBitmapHeight,
                Bitmap.Config.ARGB_8888);//创建画布
        int row = mBitmapWidth / BLOCK_SIZE;// 获得列的切线
        int col = mBitmapHeight / BLOCK_SIZE;// 获得行的切线
        int[] block = new int[BLOCK_SIZE * BLOCK_SIZE];
        for (int i = 0; i <= row; i++) {
            for (int j = 0; j <= col; j++) {
                int length = block.length;
                int flag = 0;// 是否到边界标志
                if (i == row && j != col) {
                    length = (mBitmapWidth - i * BLOCK_SIZE) * BLOCK_SIZE;
                    if (length == 0) {
                        break;// 边界外已经没有像素
                    }
                    bitmap.getPixels(block, 0, BLOCK_SIZE, i * BLOCK_SIZE, j
                                    * BLOCK_SIZE, mBitmapWidth - i * BLOCK_SIZE,
                            BLOCK_SIZE);

                    flag = 1;
                } else if (i != row && j == col) {
                    length = (mBitmapHeight - j * BLOCK_SIZE) * BLOCK_SIZE;
                    if (length == 0) {
                        break;// 边界外已经没有像素
                    }
                    bitmap.getPixels(block, 0, BLOCK_SIZE, i * BLOCK_SIZE, j
                            * BLOCK_SIZE, BLOCK_SIZE, mBitmapHeight - j
                            * BLOCK_SIZE);
                    flag = 2;
                } else if (i == row && j == col) {
                    length = (mBitmapWidth - i * BLOCK_SIZE)
                            * (mBitmapHeight - j * BLOCK_SIZE);
                    if (length == 0) {
                        break;// 边界外已经没有像素
                    }
                    bitmap.getPixels(block, 0, BLOCK_SIZE, i * BLOCK_SIZE, j
                                    * BLOCK_SIZE, mBitmapWidth - i * BLOCK_SIZE,
                            mBitmapHeight - j * BLOCK_SIZE);

                    flag = 3;
                } else {
                    bitmap.getPixels(block, 0, BLOCK_SIZE, i * BLOCK_SIZE, j
                            * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);//取出像素数组
                }

                int r = 0, g = 0, b = 0, a = 0;
                for (int k = 0; k < length; k++) {
                    r += Color.red(block[k]);
                    g += Color.green(block[k]);
                    b += Color.blue(block[k]);
                    a += Color.alpha(block[k]);
                }
                int color = Color.argb(a / length, r / length, g / length, b
                        / length);//求块内所有颜色的平均值
                for (int k = 0; k < length; k++) {
                    block[k] = color;
                }
                if (flag == 1) {
                    mBitmap.setPixels(block, 0, mBitmapWidth - i * BLOCK_SIZE,
                            i * BLOCK_SIZE, j
                                    * BLOCK_SIZE, mBitmapWidth - i * BLOCK_SIZE,
                            BLOCK_SIZE);
                } else if (flag == 2) {
                    mBitmap.setPixels(block, 0, BLOCK_SIZE, i * BLOCK_SIZE, j
                            * BLOCK_SIZE, BLOCK_SIZE, mBitmapHeight - j
                            * BLOCK_SIZE);
                } else if (flag == 3) {
                    mBitmap.setPixels(block, 0, BLOCK_SIZE, i * BLOCK_SIZE, j
                                    * BLOCK_SIZE, mBitmapWidth - i * BLOCK_SIZE,
                            mBitmapHeight - j * BLOCK_SIZE);
                } else {
                    mBitmap.setPixels(block, 0, BLOCK_SIZE, i * BLOCK_SIZE, j
                            * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
                }

            }
        }
        //并没有回收传进来的bitmap  原因是JAVA传值默认是引用,如果回收了之后,其他地方用到bitmap的位置可能报NULL指针异常,请根据实际情况决定是否回收.
        return mBitmap;
    }


    public static Bitmap rsBlur(Context context, Bitmap source, int radius, float scale) {

//        Log.i(TAG,"origin size:"+source.getWidth()+"*"+source.getHeight());
        int width = Math.round(source.getWidth() * scale);
        int height = Math.round(source.getHeight() * scale);

        Bitmap inputBmp = Bitmap.createScaledBitmap(source, width, height, false);

        RenderScript renderScript = RenderScript.create(context);

//        Log.i(TAG,"scale size:"+inputBmp.getWidth()+"*"+inputBmp.getHeight());

        // Allocate memory for Renderscript to work with

        final Allocation input = Allocation.createFromBitmap(renderScript, inputBmp);
        final Allocation output = Allocation.createTyped(renderScript, input.getType());

        // Load up an instance of the specific script that we want to use.
        ScriptIntrinsicBlur scriptIntrinsicBlur = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        scriptIntrinsicBlur.setInput(input);

        // Set the blur radius
        scriptIntrinsicBlur.setRadius(radius);

        // Start the ScriptIntrinisicBlur
        scriptIntrinsicBlur.forEach(output);

        // Copy the output to the blurred bitmap
        output.copyTo(inputBmp);


        renderScript.destroy();
        return inputBmp;
    }


    public static Bitmap fastBlur(Bitmap sentBitmap, float scale, int radius) {

        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
//        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

//        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }


}


