package com.mail.myapplication.ui.dg

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.mail.myapplication.R
import com.mail.myapplication.databinding.DgPayBinding
import com.mail.myapplication.databinding.DialogLoadBinding
import com.mail.myapplication.databinding.ItemDgPayBinding
import com.mail.myapplication.interfaces.Lar
import com.zhy.autolayout.utils.AutoUtils

class UploadDialog(context: BaseAty) : Dialog(context) {

    private var baseAty = context
    lateinit var mBinding: DialogLoadBinding
    var animation2: Animation? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DialogLoadBinding.inflate(layoutInflater);
        setContentView(mBinding.root)
        window?.setWindowAnimations(R.style.dialogFullscreen3)
        val dialogWindow = window
        dialogWindow!!.setBackgroundDrawable(null)
        dialogWindow.setGravity(Gravity.CENTER)
        setCanceledOnTouchOutside(false)
        setCancelable(false)
        val m = baseAty.windowManager
        val d = m.defaultDisplay// 获取屏幕宽、高
        val p = dialogWindow.attributes
        window?.decorView?.setPadding(0, 0, 0, 0)
//        p.height =  (d.height * 1) // 高度设置为屏幕的0.6
//        p.width = (d.width * 1) // 宽度设置为屏幕的0.85
        dialogWindow.attributes = p
        animation2 = AnimationUtils.loadAnimation(context, com.mail.comm.R.anim.loading_dialog_rotate)
        animation2?.interpolator = LinearInterpolator()
        mBinding.downLoading?.startAnimation(animation2)

    }

    override fun dismiss() {
        super.dismiss()
//        mBinding.downLoading.clearAnimation()
    }

    fun setData(info :String,info2: String){
        if (isShowing){
            mBinding.tvLoadingDialogText.text = info
            mBinding.tv01.text = info2
        }

    }


}