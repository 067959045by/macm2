package com.mail.myapplication.ui.msg

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.app.AppConfig
import com.mail.comm.function.chooseimg.ChooseImgAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.net.DataManager
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.utils.TimeUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.MyApp
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.msg.database.AppDatabase
import com.mail.myapplication.ui.msg.database.ChatUtils
import com.mail.myapplication.ui.msg.database.Chatpanel
import com.mail.myapplication.ui.msg.model.MessageBean
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils
import io.agora.rtm.*
import org.json.JSONObject
import org.xutils.common.util.LogUtil

class ChatAty : BaseXAty(), RtmClientListener {

    lateinit var mBinding: AtyChatBinding
    lateinit var mAdapter: GoldRecyclerAdapter
    lateinit var mRtmClient: RtmClient
    lateinit var mChatManager: ChatManager
    var map_service_info:MutableMap<String,String>? = null
    var list = ArrayList<MutableMap<String,String>>()

    var to_user_code = ""
    var to_nick = ""
    var to_avatar = ""

    var user_code = ""
    var nick = ""
    var avatar = ""

    var lar = Lar()
    var resource_cdn =""
    var home = Home()

    override fun getLayoutId(): Int = 0

    override fun initView() {
        resource_cdn = PreferencesUtils.getString(this,"resource_cdn")
        user_code = PreferencesUtils.getString(this,"info_code")
        nick = PreferencesUtils.getString(this,"info_nick")
        avatar = PreferencesUtils.getString(this,"info_avatar")

        to_user_code = intent.getStringExtra("code").toString()
        to_nick = intent.getStringExtra("nick").toString()
        to_avatar = intent.getStringExtra("avatar").toString()
        setChatPannelIsRead()

        var service_info = PreferencesUtils.getString(this,"service_info")
        map_service_info = JSONUtils.parseKeyAndValueToMap(service_info)

    }

    fun setChatPannelIsRead() {
        Thread {
            ChatUtils.setChatPannelIsRead(user_code,to_user_code)
        }.start()
    }

    var handler : Handler =  @SuppressLint("HandlerLeak") object : Handler(){

        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            when(msg.what){
                1->{
                    mAdapter.notifyDataSetChanged()
                    mBinding.swipeRefreshLayout.finishLoadmore()
                    mBinding.swipeRefreshLayout.finishRefreshing()
                }
                2->{
                     mBinding.swipeRefreshLayout.finishRefreshing()
                }
                3->{
                    mAdapter.notifyItemRangeChanged(list.size, 1)
                    mBinding.recyclerview.scrollToPosition(list.size - 1)
                }
            }
        }
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        home?.a45(1,to_user_code,"",this)
//        Thread{
//            var mList = ChatUtils.getOnePannelList(user_code,to_user_code)
//
//            if (mList!=null&&mList.size>0){
//                this.list.clear()
//                this.list.addAll(mList)
//                handler.sendEmptyMessage(1)
//            }else{
//                handler.sendEmptyMessage(2)
//            }
//        }.start()
    }

     fun requestDataMore(){
         var ms = ""
         if (list!=null&&list.size>0){
              ms = list[0]["ms"]!!
         }
         home?.a45(1,to_user_code,ms!!,this)
     }

    override fun getLayoutView(): View {
        mBinding = AtyChatBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var myApp = application  as MyApp
        mChatManager = myApp.getChatManager()
        mChatManager.enableOfflineMessage(true)
        mChatManager.enableHistoricalMessage(true)
        mRtmClient = mChatManager.rtmClient
        mChatManager.registerListener(this)
        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = to_nick
            include.relayRight.visibility = View.VISIBLE
            include.imgvRight.setImageResource(R.mipmap.ic_56)

            var mLayoutManager = GridLayoutManager(this@ChatAty,1)
            mLayoutManager.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager =mLayoutManager
            mAdapter= GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter

            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    requestDataMore()
                }

                override fun loadMoreStart() {
                }

            })

            if (map_service_info!!["code"]!! == to_user_code){
                include.relayRight.visibility = View.GONE
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        (application  as MyApp).getChatManager().unregisterListener(this)
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.relay_right -> {
                var bundle = Bundle()
                bundle.putString("to_user_code",to_user_code)
                startActivity(ChatSetAty::class.java,bundle)
            }

            R.id.imgv_send ->{

                if (TextUtils.isEmpty(mBinding.editChat.text.toString())){
                    showToastS("请输入！")
                    return
                }

                var time = TimeUtils.getBeijinTime()
                var message_id = MyUtils3.getMD5Str(time+user_code)

                var json = MyUtils3.getTextMsg(to_user_code,to_avatar,to_nick,user_code,avatar,nick,
                    mBinding.editChat.text.toString(),time,message_id)
                val message = mRtmClient.createMessage()
                message.text = json


                LogUtil.e(" message.serverReceivedTs="+json)
//                val messageBean = MessageBean(user_code, message, true)
                var map = HashMap<String,String>()
                map["user_code"] = user_code
                map["to_user_code"] = to_user_code
                map["time"] = time
                map["msg"] = json
                map["message_id"] = message_id
                list.add(map)

                mAdapter.notifyItemRangeChanged(list.size, 1)
                mBinding.recyclerview.scrollToPosition(list.size - 1)
                sendPeerMessage(message)
                mBinding.editChat.setText("")

                mBinding.imgvSend.visibility = View.GONE
                mBinding.progressP.visibility = View.VISIBLE
            }

            R.id.imgv_add ->{
                if (mBinding.progressP.visibility == View.VISIBLE){
                    return
                }

                var bundle = Bundle()
                bundle.putInt("max_num",1)
                bundle.putString("ratio","16,16")
                startActivityForResult(ChooseImgAty::class.java,bundle,100)
            }
        }
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "message/create"){
            stopProgressDialog()
            mBinding.imgvSend.visibility = View.VISIBLE
            mBinding.progressP.visibility = View.GONE
//            MyUtils3.showInput(mBinding.editChat,this@ChatAty)
        }

        if (type =="/app/api/message/user/list"){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            mBinding.swipeRefreshLayout.finishRefreshing()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)

                if (mList==null||mList.size==0){
                    return
                }
                list.addAll(0, mList)
                mAdapter?.notifyDataSetChanged()
//                if (TextUtils.isEmpty(ms)){
//                    mBinding.recyclerview.scrollToPosition(mAdapter?.itemCount!! - 1)
//                }else{
//                  mBinding.recyclerview.scrollToPosition(mList.size-1)
//                }

                if (list.size>mList.size){
                    mBinding.recyclerview.scrollToPosition(mList.size)
                }else{
                    mBinding.recyclerview.scrollToPosition(mList.size-1)
                }
//            MyUtils2.log("onComplete3333","onComplete3=======" + type+",,,,"+ str)
            }else{

            }
        }

        if (type == "file/upload") {
            stopProgressDialog()

            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "0") {
                var dataMap = JSONUtils.parseDataToMap(var2)
                var img = dataMap["file_path"]!!
                var thumbImage = dataMap["file_path"]!!
                var name = dataMap["file_path"]!!

                var time = TimeUtils.getBeijinTime()
                var message_id = MyUtils3.getMD5Str(time+user_code)

                var json = MyUtils3.getImgMsg(to_user_code,to_avatar,to_nick,user_code,avatar,nick,img,thumbImage,name
                ,time,message_id)

                val message = mRtmClient.createMessage()

//                LogUtil.e(" message.serverReceivedTs="+ message.serverReceivedTs)
                message.text = json
//                val messageBean = MessageBean(user_code, message, true)

                var map = HashMap<String,String>()
                map["user_code"] = user_code
                map["to_user_code"] = to_user_code
                map["time"] = time
                map["msg"] = json
                map["message_id"] =message_id
                list.add(map)

                mAdapter.notifyItemRangeChanged(list.size, 1)
                mBinding.recyclerview.scrollToPosition(list.size - 1)
                sendPeerMessage(message)

            } else {
                showToastS(map["message"])
                mBinding.imgvSend.visibility = View.VISIBLE
                mBinding.progressP.visibility = View.GONE
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        mBinding.swipeRefreshLayout.finishRefreshing()

        if (type == "message/create"){
            showToastS("发送失败n")
            mBinding.imgvSend.visibility = View.VISIBLE
            mBinding.progressP.visibility = View.GONE
        }

        if (type =="file/upload" ){
            mBinding.imgvSend.visibility = View.VISIBLE
            mBinding.progressP.visibility = View.GONE
        }
    }

    /**
     * API CALL: send message to peer
     */
    private fun sendPeerMessage(message: RtmMessage) {
        mRtmClient.sendMessageToPeer(to_user_code, message, mChatManager.getSendMessageOptions(),
            object : ResultCallback<Void?> {
                override fun onSuccess(aVoid: Void?) {
                    LogUtil.e("sendPeerMessage=======发送成功")
                    ChatUtils.saveChatpannel(message.text)

                    runOnUiThread {
                        var str = AESCBCCrypt.aesDecrypt(message.text)
                        var map = JSONUtils.parseKeyAndValueToMap(str)
                        var map_f = JSONUtils.parseKeyAndValueToMap(map["from_user"])
                        var map_t = JSONUtils.parseKeyAndValueToMap(map["to_user"])

                        home.a43(map_t["user_code"]!!,map_f["user_code"]!!,message.text,map["time"]!!,map["message_id"]!!,this@ChatAty)

                    }

                }

                override fun onFailure(errorInfo: ErrorInfo) {
                    LogUtil.e("sendPeerMessage=======发送失败"+errorInfo.errorCode)
                    val errorCode = errorInfo.errorCode
//                    runOnUiThread {
                        when (errorCode) {
                            RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_TIMEOUT, RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_FAILURE -> {
//                                showToastS("消息发送失败")
                                runOnUiThread {
                                    mBinding.imgvSend.visibility = View.VISIBLE
                                    mBinding.progressP.visibility = View.GONE
                                }

                            }
                            RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_PEER_UNREACHABLE -> {
                                runOnUiThread {
                                    mBinding.imgvSend.visibility = View.VISIBLE
                                    mBinding.progressP.visibility = View.GONE
                                }
//                                showToastS("对方不在线")
                            }
                            RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_CACHED_BY_SERVER -> {
                                ChatUtils.saveChatpannel(message.text)

                                runOnUiThread {
                                    var str = AESCBCCrypt.aesDecrypt(message.text)
                                    var map = JSONUtils.parseKeyAndValueToMap(str)
                                    var map_f = JSONUtils.parseKeyAndValueToMap(map["from_user"])
                                    var map_t = JSONUtils.parseKeyAndValueToMap(map["to_user"])

                                    home.a43(map_t["user_code"]!!,map_f["user_code"]!!,message.text,map["time"]!!,map["message_id"]!!,this@ChatAty)
//                                showToastS("消息已被服务器缓存")
                                }

                            }
                            else -> {
                                runOnUiThread {
                                    //4EFYTC
                                    mBinding.imgvSend.visibility = View.VISIBLE
                                    mBinding.progressP.visibility = View.GONE
                                }
                            }
                        }
//                    }
                }
            })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK){
            when(requestCode){

                100 ->{
                    var type =data?.getStringExtra("type")

                    when (type) {
                        "img_photo" -> {
                            var list = data?.getStringArrayListExtra("data")
//                            sendPeerImgMessage(list!![0])
                            mBinding.imgvSend.visibility = View.GONE
                            mBinding.progressP.visibility = View.VISIBLE
                            lar.b7(list!![0], this)

                        }
                    }

                }
            }
        }
    }

    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(ItemChatBinding.inflate(LayoutInflater.from(this@ChatAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        var msg = ""

                        if (list[position].containsKey("msg")){
                            msg = AESCBCCrypt.aesDecrypt(list[position]["msg"])
                        }else{
                            msg = AESCBCCrypt.aesDecrypt(list[position]["payload"])
                        }
                        LogUtil.e("AESCBCCrypt msg==="+msg)
                        var  map =JSONUtils.parseKeyAndValueToMap(msg)
                        var  map_form_user =JSONUtils.parseKeyAndValueToMap(map["from_user"])
                        var  map_to_user =JSONUtils.parseKeyAndValueToMap(map["to_user"])

//                        if (position > 0) {
//
//                            var  msgPre = ""
//                            if (list[position-1].containsKey("msg")){
//                                msgPre = AESCBCCrypt.aesDecrypt(list[position-1]["msg"])
//                            }else{
//                                msgPre = AESCBCCrypt.aesDecrypt(list[position-1]["payload"])
//                            }
//                            var  mapPre =JSONUtils.parseKeyAndValueToMap(msgPre)
//                            val preMsgTime = java.lang.Long.parseLong(mapPre["time"])
//                            val cruMsgTime = java.lang.Long.parseLong(map["time"])
//
//                            LogUtil.e("cruMsgTime=======${cruMsgTime - preMsgTime}")
//                            if (cruMsgTime - preMsgTime > 1000 * 4 ) {
//                                tvTime.visibility = View.VISIBLE
//                                var time = (TimeUtils.getBeijinTime().toDouble() - map["time"]!!.toDouble())/1000
//
//                                LogUtil.e("cruMsgTime=======${TimeUtils.getBeijinTime().toDouble()},${map["time"]!!.toDouble()}")
//
//                                tvTime.text = TimeUtils.formatSecond3(time)
//
//                            } else {
//                                tvTime.visibility = View.GONE
//                            }
//                        }

                        when (map["type"]) {
                            "2" -> {
                                if (map_form_user["user_code"]==user_code) {
                                    relayMy.visibility = View.VISIBLE
                                    relayOther.visibility = View.GONE
                                    tvContentMy.visibility = View.GONE
                                    framlayImgvMy.visibility = View.VISIBLE

                                    imgvMy.setImageResource(R.drawable.ib_01)

                                    var maxW = AutoUtils.getPercentWidthSizeBigger(500)
                                    ImageLoader.loadImageAes(this@ChatAty,resource_cdn+map["thumb_image"],imgvMy,maxW,maxW)
                                    var avatar = map_form_user["avatar"]

                                    var maxW_head = AutoUtils.getPercentWidthSizeBigger(200)
                                    ImageLoader.loadImageAes(this@ChatAty, avatar,ivHeadMy,maxW_head,maxW_head)

                                    imgvMy.setOnClickListener {
                                        var list = ArrayList<String>()
                                        list.add(resource_cdn+map["thumb_image"])
                                        var bundle = Bundle()
                                        bundle.putStringArrayList("data",list)
                                        startActivity(BigImgListAty::class.java,bundle)
                                    }
//                                    builder.into(imgvMy)
                                } else {
                                    relayMy.visibility = View.GONE
                                    relayOther.visibility = View.VISIBLE
                                    tvContent.visibility = View.GONE
                                    framlayImgv.visibility = View.VISIBLE

                                    imgv.setImageResource(R.drawable.ib_01)

                                    var maxW = AutoUtils.getPercentWidthSizeBigger(500)

                                    ImageLoader.loadImageAes(this@ChatAty,resource_cdn+map["thumb_image"],imgv,maxW,maxW)

                                    var avatar = map_form_user["avatar"]

                                    var maxW_head = AutoUtils.getPercentWidthSizeBigger(200)
                                    ImageLoader.loadImageAes(this@ChatAty, avatar,ivHead,maxW_head,maxW_head)
//                                    builder.into(imgv)
//                                    holder.imageViewOtherImg.setVisibility(View.VISIBLE)
//                                    builder.into(holder.imageViewOtherImg)

                                    imgv.setOnClickListener {
                                        var list = ArrayList<String>()
                                        list.add(resource_cdn+map["thumb_image"])
                                        var bundle = Bundle()
                                        bundle.putStringArrayList("data",list)
                                        startActivity(BigImgListAty::class.java,bundle)
                                    }
                                }
                            }

                           "1" -> {

                               if (map_form_user["user_code"] == user_code) {
                                   relayMy.visibility = View.VISIBLE
                                   relayOther.visibility = View.GONE
                                   tvContentMy.text = map["text"]
                                   var avatar = map_form_user["avatar"]


                                   var maxW_head = AutoUtils.getPercentWidthSizeBigger(200)
                                   ImageLoader.loadImageAes(this@ChatAty, avatar,ivHeadMy,maxW_head,maxW_head)

                               } else {
                                   relayMy.visibility = View.GONE
                                   relayOther.visibility = View.VISIBLE
                                   tvContent.text = map["text"]
                                   var avatar = map_form_user["avatar"]


                                   var maxW_head = AutoUtils.getPercentWidthSizeBigger(200)
                                   ImageLoader.loadImageAes(this@ChatAty, avatar,ivHead,maxW_head,maxW_head)
                               }

                            }

                            else->{

                            }
                        }
                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemChatBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemChatBinding = binding
            init {
                AutoUtils.autoSize(binding.root)
            }
        }

    }

    override fun onConnectionStateChanged(p0: Int, p1: Int) {
    }

    override fun onMessageReceived(message: RtmMessage?, peerId: String?) {

        LogUtil.e("AESCBCCrypt=="+to_user_code+",,,"+peerId)

        if (message != null) {
            LogUtil.e("AESCBCCrypt=="+message.text)
        }

        var str = AESCBCCrypt.aesDecrypt(message!!.text)

        LogUtil.e("AESCBCCrypt=="+str)

//        runOnUiThread {
            if (peerId == to_user_code) {

                var map = HashMap<String,String>()
                map["user_code"] = user_code
                map["to_user_code"] = to_user_code
                map["msg"] = message!!.text
                list.add(map)
//              val messageBean = MessageBean(peerId, message, false)
//              list.add(messageBean)
                handler.sendEmptyMessage(3)
                ChatUtils.saveChatpannel(message!!.text,true)

            } else {
                ChatUtils.saveChatpannel(message!!.text,false)

                //
//              MessageUtil.addMessageBean(peerId, message)
            }
//        }
    }

    override fun onImageMessageReceivedFromPeer(rtmImageMessage: RtmImageMessage?, peerId: String?) {

//        runOnUiThread {
//            if (peerId == mPeerId) {
//                val messageBean = MessageBean(peerId, rtmImageMessage, false)
//                list.add(messageBean)
//                mAdapter.notifyItemRangeChanged(list.size, 1)
//                mBinding.recyclerview.scrollToPosition(list.size - 1)
//            } else {
////                MessageUtil.addMessageBean(peerId, rtmImageMessage)
//            }
//        }
    }

    override fun onFileMessageReceivedFromPeer(p0: RtmFileMessage?, p1: String?) {
    }

    override fun onMediaUploadingProgress(p0: RtmMediaOperationProgress?, p1: Long) {
    }

    override fun onMediaDownloadingProgress(p0: RtmMediaOperationProgress?, p1: Long) {
    }

    override fun onTokenExpired() {
    }

    override fun onPeersOnlineStatusChanged(p0: MutableMap<String, Int>?) {
    }


}