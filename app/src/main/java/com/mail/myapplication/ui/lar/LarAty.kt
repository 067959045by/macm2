package com.mail.myapplication.ui.lar

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.mail.comm.app.AppConfig
import com.mail.comm.app.BaseApp
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyLarBinding
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.MainAty
import com.mail.myapplication.ui.utils.MyUtils3
import org.xutils.common.util.LogUtil
import org.xutils.x

class LarAty : BaseXAty() {

    var lar = Lar()
    var pastCode = ""
    var pastChannel = ""

    lateinit var mBinding: AtyLarBinding

    override fun getLayoutId(): Int = 0

    override fun initView() {
        if (intent.hasExtra("pastCode")){
            pastCode = intent.getStringExtra("pastCode")!!
        }
        if (intent.hasExtra("pastChannel")){
            pastChannel = intent.getStringExtra("pastChannel")!!
        }
    }

    override fun requestData() {
    }


    override fun getLayoutView(): View {
        mBinding = AtyLarBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTranslanteBar()
        setAndroidNativeLightStatusBar(true)

        with(mBinding) {

        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type == "login/type") {
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map = JSONUtils.parseKeyAndValueToMap(str)
                var map_auth = JSONUtils.parseKeyAndValueToMap(map["auth"])
                var map_config = JSONUtils.parseKeyAndValueToMap(map["config"])
                var map_config_sys = JSONUtils.parseKeyAndValueToMap(map_config["sys"])
                var map_user_info = JSONUtils.parseKeyAndValueToMap(map["user_info"])

                if (TextUtils.isEmpty(map_auth["token"])) {
                    showToastS("token is null！")
                    return
                }

                if (TextUtils.isEmpty(map_user_info["code"])) {
                    showToastS("code is null！")
                    return
                }

                setLoginStatus(true)
//                AppConfig.TOKENx share_url
                BaseApp.instance?.saveOneMapData("info_code",map_user_info["code"])
                BaseApp.instance?.saveOneMapData("info_vip_level",map_user_info["vip_level"])
                BaseApp.instance?.saveOneMapData("info_mobile",map_user_info["mobile"])

                PreferencesUtils.putString(x.app(), "share_url", map_config_sys["share_url"])
                PreferencesUtils.putString(x.app(), "info_code", map_user_info["code"])

                AppConfig.TOKEN = map_auth["token"]
                AppConfig.upload_url =map_config_sys["upload_url"]
                startActivity(MainAty::class.java)
                finshPage("2")
            } else {
                showToastS(map["message"])
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type == "login/type") {
            stopProgressDialog()
            showToastS("网络异常,请重试")
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.tv_register -> {
                startActivity(RegisterAty::class.java)
//              startActivity(RegisterHeadAty::class.java)
            }

            R.id.tv_ok -> {

            }

            R.id.tv_youke ->{
                startProgressDialog()
                lar.b14("","","","1",this)
            }
        }
    }


}