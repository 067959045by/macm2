package com.mail.myapplication.ui.home

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.TextUtils
import android.util.TypedValue
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils2
import com.mail.comm.view.load.XLoadTip
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.mine.PostSendAty
import com.zhy.autolayout.utils.AutoUtils

class HistoryActiveListAty : BaseXAty() {

    lateinit var mBinding: AtyHistoryActiveListBinding
    lateinit var list_tv: Array<TextView>
    lateinit var list_v: Array<View>
    override fun getLayoutId(): Int = 0
    var home = Home()
    var id = ""
    var topic = ""

    override fun initView() {
        id =intent.getStringExtra("id").toString()

        if (intent.hasExtra("topic")){
            topic =intent.getStringExtra("topic").toString()
        }

//      initLayout()
        with(mBinding){
            list_tv = arrayOf(tv01,tv02)
            list_v = arrayOf(v01,v02)
        }
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData2()
    }

     fun requestData2() {
        home.a25(id,this)
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type=="topic/info"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map = JSONUtils.parseKeyAndValueToMap(str)
                if (map["from"]=="1"){

                    var maxW = AutoUtils.getPercentWidthSizeBigger(300)
                    ImageLoader.loadImageAes(this,map["logo"],mBinding.imgv,maxW,maxW)
                    mBinding.tvTitle.text = map["title"]
                    topic = map["title"]!!
                    mBinding.tvContent.text = map["intro"]
                    mBinding.tvZanNum.text = map["total_like"]
                    mBinding.tvCommentNum.text = map["total_comment"]
                    mBinding.tvShareNum.text = map["total_share"]
                    mBinding.tvTime.text ="活動截止："+map["end_time"]
                    mBinding.linlayTopic.visibility = View.VISIBLE
                    setStatusColor("#000000")
                    setAndroidNativeLightStatusBar(false)
                }else{
                    mBinding.linlayTopic.visibility = View.GONE
                    setStatusColor("#ffffff")
                    setAndroidNativeLightStatusBar(true)
                    mBinding.tv01.text = "最热"
                    mBinding.linlay003.setBackgroundColor(Color.parseColor("#ffffff"))

                }

            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)

            }

        }
    }


    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type=="topic/info"){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }
    }


//    fun initLayout2(){
//        with(mBinding){
//            var params =imgv02.layoutParams  as RelativeLayout.LayoutParams
//            var StatusBarHeight = MyUtils2.getStateBar(this@HistoryActiveListAty)
//            if (StatusBarHeight <= 0) {
//                StatusBarHeight = MyUtils2.dip2px(this@HistoryActiveListAty, 20F)
//            }
//            params.height = StatusBarHeight+AutoUtils.getPercentHeightSizeBigger(510)
//            imgv02.layoutParams =params
//        }
//    }

    private fun setSelector(tv: TextView) {
        for (i in list_tv.indices) {
            if (tv === list_tv[i]) {
                list_tv[i].setTextColor(Color.parseColor("#333438"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗
                list_tv[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentWidthSizeBigger(55).toFloat())
                list_v[i].visibility = View.VISIBLE
            } else {
                list_tv[i].setTextColor(Color.parseColor("#65666D"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));//加粗
                list_tv[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentWidthSizeBigger(45).toFloat())
                list_v[i].visibility = View.INVISIBLE
            }
        }
    }


    override fun getLayoutView(): View {
        mBinding = AtyHistoryActiveListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusColor("#000000")
        setAndroidNativeLightStatusBar(false)

        with(mBinding) {
            editSearch.setText(topic)
            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }
            })

            var list =  ArrayList<String>()
            list.add("topic_hot")
            list.add("topic_new")
            viewPager.adapter = HomeListAdataper(supportFragmentManager, this@HistoryActiveListAty.lifecycle, list)
            viewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    when(position){
                        0 -> {
                            setSelector(tv01)
                        }
                        1 -> {
                            setSelector(tv02)
                        }

                    }
                }
            })

        }
    }


    inner class HomeListAdataper(fa: FragmentManager, lifecycle: Lifecycle, val list : MutableList<String>) :

        FragmentStateAdapter(fa, lifecycle) {

        override fun getItemCount(): Int =list.size

        override fun createFragment(position: Int): Fragment =HomeListFrg.create4(list[position],id)

    }


    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_search -> {
                if (TextUtils.isEmpty(mBinding.editSearch.text.toString())){
                    showToastS("搜索内容不能为空")
                    return
                }
                var bundle = Bundle()
                bundle.putString("id",id)
                bundle.putString("keywords",mBinding.editSearch.text.toString())
                startActivity(HistoryActiveListSearchAty::class.java,bundle)
//                mBinding.editSearch.setText("")

            }

            R.id.linlay_01 -> {
                mBinding.viewPager.setCurrentItem(0,true)
            }
            R.id.linlay_02 -> {
                mBinding.viewPager.setCurrentItem(1,true)
            }
            R.id.imgv_post -> {
                var bundle = Bundle()
                bundle.putString("topic",topic)
                bundle.putString("topic_id",id)
                startActivity(PostSendAty::class.java,bundle)
            }
        }
    }


}