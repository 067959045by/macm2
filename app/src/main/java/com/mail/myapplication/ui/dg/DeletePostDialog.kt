package com.mail.myapplication.ui.dg

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.base.BaseAty
import com.mail.comm.base.BaseDgFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.ui.mine.Set2Aty

class DeletePostDialog (context: BaseAty) : Dialog(context)  {

    lateinit var mBinding: DgDeletePostBinding
    private var baseAty = context
    var id =""
    var listener : RechargeListen? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DgDeletePostBinding.inflate(layoutInflater);
        setContentView(mBinding.root)
        window?.setWindowAnimations(R.style.dialogFullscreen3)
        val dialogWindow = window
        dialogWindow!!.setBackgroundDrawable(null)
        dialogWindow.setGravity(Gravity.BOTTOM)
        setCanceledOnTouchOutside(true)
        val m = baseAty.windowManager
        val d = m.defaultDisplay// 获取屏幕宽、高
        val p = dialogWindow.attributes
        window?.getDecorView()?.setPadding(0, 0, 0, 0)
//        p.height =  (d.height * 1) // 高度设置为屏幕的0.6
        p.width = (d.width * 1) // 宽度设置为屏幕的0.85
        dialogWindow.attributes = p

        with(mBinding) {



            tvCancel.setOnClickListener {
                dismiss()
            }

            tv01.setOnClickListener {
                listener?.onclik01()
            }


        }
    }


    interface  RechargeListen{
        fun onclik01()
    }

    fun setRechargeListen(listener:RechargeListen){
        this.listener =listener
    }





}