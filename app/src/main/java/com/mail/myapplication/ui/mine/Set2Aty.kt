package com.mail.myapplication.ui.mine


import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import com.mail.comm.app.AppConfig
import com.mail.comm.app.AppManager
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.*
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtySetBinding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.dg.ClearCacheDgFrg
import com.mail.myapplication.ui.dg.QuitLoginDgFrg
import com.mail.myapplication.ui.dg.UpdateDialog
import com.mail.myapplication.ui.lar.LarAty
import com.mail.myapplication.ui.lar.PatternAty
import java.util.HashMap

class Set2Aty:BaseXAty() {

    lateinit var mBinding: AtySetBinding

    var home = Home()
    var lar = Lar()
    var updateDialog: UpdateDialog?=null

    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtySetBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "設置"

            var v_code = MyUtils2.getAppVersionCode()
            var v_name = MyUtils2.getAppVersionName()
            mBinding.tvCode.text = "v${v_name}/${v_code}"

        }
    }

    fun clearFinish(){
        ToastUitl.showToast("清除緩存成功",Toast.LENGTH_SHORT)
    }

    fun quitFinish(){
        startProgressDialog()
        lar.b17(this)
//        AppManager.getInstance().killAllActivity()
//        startActivity(LarAty::class.java)
    }


    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
            R.id.relay_black -> {
                startActivity(BlackListAty::class.java)
            }

            R.id.relay_clear -> {
                var dg = ClearCacheDgFrg()
                dg.show(supportFragmentManager,"ClearCacheDgFrg")
            }

            R.id.relay_quit -> {
                var dg = QuitLoginDgFrg()
                dg.show(supportFragmentManager,"QuitLoginDgFrg")
            }

            R.id.relay_private -> {
                startActivity(PrivateSetAty::class.java)
            }

            R.id.relay_update ->{
                startProgressDialog()
                home.a42(this)
            }

            R.id.relay_ss -> {
//                showToastS("暂未开放")
                startActivity(PatternAty::class.java)
            }
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        stopProgressDialog()

        if (type == "login/out"){
//            AppConfig.TOKEN=""
//            PreferencesUtils.remove(this,"token")
            AppManager.getInstance().killAllActivity()
            startActivity(LarAty::class.java)
        }

        if (type == "version/update"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"]=="200"){
                var map = JSONUtils.parseKeyAndValueToMap(map["data"])
                if (updateDialog == null){
                    updateDialog = UpdateDialog(this)
                }
                updateDialog?.show()
                updateDialog?.setData(map["remark"]!!,map["force"]!!,map["package_path"]!!)
//                showBuilder(map["remark"]!!, map["package_p
                //                ath"]!!,map["force"]!!)
            }else{
                showToastS(map["message"])
            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
         stopProgressDialog()
        if (type == "login/out"){
//            AppManager.getInstance().killAllActivity()
//            startActivity(LarAty::class.java)
        }
    }

}