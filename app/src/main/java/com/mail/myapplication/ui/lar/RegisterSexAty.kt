package com.mail.myapplication.ui.lar

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*

class RegisterSexAty : BaseXAty() {

    lateinit var mBinding: AtyRegisterSexBinding

    var type = ""
    var info_gender = ""

    var login_job = ""

    override fun getLayoutId(): Int = 0

    override fun initView() {
        type = intent.getStringExtra("type")!!
        if (type == "edit") {
            info_gender = intent.getStringExtra("info_gender")!!
        }else{

            if (intent.hasExtra("login_job")){
                login_job = intent.getStringExtra("login_job")!!
            }
        }
    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtyRegisterSexBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvRight.text = "跳過"
            if (type == "edit") {
                include.tvRight.visibility = View.GONE
            } else {
                include.tvRight.visibility = View.VISIBLE
            }
        }

        initSex()
    }


    fun initSex() {
        when (info_gender) {
            "1" -> {
                mBinding.tvGender01.setBackgroundResource(R.drawable.shape_33)
                mBinding.tvGender02.setBackgroundResource(R.drawable.shape_9)
                mBinding.tvGender03.setBackgroundResource(R.drawable.shape_9)
            }
            "0" -> {
                mBinding.tvGender01.setBackgroundResource(R.drawable.shape_9)
                mBinding.tvGender02.setBackgroundResource(R.drawable.shape_33)
                mBinding.tvGender03.setBackgroundResource(R.drawable.shape_9)
            }
            "10" -> {
                mBinding.tvGender01.setBackgroundResource(R.drawable.shape_9)
                mBinding.tvGender02.setBackgroundResource(R.drawable.shape_9)
                mBinding.tvGender03.setBackgroundResource(R.drawable.shape_33)
            }
            else -> {
                mBinding.tvGender01.setBackgroundResource(R.drawable.shape_9)
                mBinding.tvGender02.setBackgroundResource(R.drawable.shape_9)
                mBinding.tvGender03.setBackgroundResource(R.drawable.shape_9)
            }

        }

    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_ok -> {
                if (type == "edit") {
                    if (TextUtils.isEmpty(info_gender)) {
                        showToastS("请选择你的性别")
                        return
                    }
                    var intent = Intent()
                    intent.putExtra("data", info_gender)
                    setResult(RESULT_OK, intent)
                    finshPage("2")
                }else{
                    if (TextUtils.isEmpty(info_gender)) {
                        showToastS("请选择你的性别")
                        return
                    }
                    var bundle = Bundle()
                    bundle.putString("type","login")
                    bundle.putString("login_job",login_job)
                    bundle.putString("login_gender",info_gender)
                    startActivity(RegisterLikeAty::class.java,bundle)
                }
//                startActivity(RegisterLikeAty::class.java)
            }

            R.id.tv_right -> {
                var bundle = Bundle()
                bundle.putString("type","login")
                bundle.putString("login_job",login_job)
                bundle.putString("login_gender","")
                startActivity(RegisterLikeAty::class.java,bundle)
//                startActivity(RegisterCodeAty::class.java)
            }
            R.id.tv_gender_01 -> {
                info_gender = "1"
                initSex()
            }
            R.id.tv_gender_02 -> {
                info_gender = "0"
                initSex()
            }
            R.id.tv_gender_03 -> {
                info_gender = "10"
                initSex()
            }
        }
    }


}