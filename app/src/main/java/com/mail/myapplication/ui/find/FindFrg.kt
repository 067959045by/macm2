package com.mail.myapplication.ui.find

import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.View
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.databinding.FrgFindBinding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.utils.MyUtils3
import com.wkp.randomlayout.RandomLayout
import com.zhy.autolayout.utils.AutoUtils
import master.flame.danmaku.controller.DrawHandler
import master.flame.danmaku.danmaku.model.BaseDanmaku
import master.flame.danmaku.danmaku.model.DanmakuTimer
import master.flame.danmaku.danmaku.model.IDisplayer
import master.flame.danmaku.danmaku.model.android.BaseCacheStuffer
import master.flame.danmaku.danmaku.model.android.DanmakuContext
import master.flame.danmaku.danmaku.model.android.Danmakus
import master.flame.danmaku.danmaku.model.android.SpannedCacheStuffer
import master.flame.danmaku.danmaku.parser.BaseDanmakuParser
import master.flame.danmaku.danmaku.util.IOUtils
import org.xutils.common.util.LogUtil
import java.io.IOException
import java.io.InputStream
import java.net.MalformedURLException
import java.net.URL
import java.util.*


class FindFrg : BaseXFrg() {

    var index = 0
    var mContext: DanmakuContext? = null
    var mParser: BaseDanmakuParser? = null

    lateinit var mBinding: FrgFindBinding

    var home = Home()

    override fun getLayoutView(): View {
        mBinding = FrgFindBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onResume() {
        super.onResume()
        home.a50(this)
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type == "random/people") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var maps = JSONUtils.parseKeyAndValueToMap(str)
                mBinding.tvPerNum.text = maps["people"]
            }
        }
    }

    override fun getLayoutId(): Int = 0

    override fun initView() {}

    override fun requestData() {}

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(mBinding) {
            soulPlanetView.setAdapter(TestAdapter())

            linlay03.setOnClickListener {
//                if (!MyUtils3.isFastClick()){
//                    return@setOnClickListener
//                }
//                index++ny
//              ToastUtils.showShort(index.toString())
//                showToastS("暂未开放")
                LogUtil.e("addDanmaku00")
                addDanmaku(true)
            }

            linlay04.setOnClickListener {
                if (!MyUtils3.isFastClick()) {
                    return@setOnClickListener
                }
                startActivity(MatchAty::class.java)
//                showToastS("暂未开放")
            }
        }
//        addrandData()
        initDanmu()
    }


    private val mCacheStufferAdapter: BaseCacheStuffer.Proxy = object : BaseCacheStuffer.Proxy() {
        private var mDrawable: Drawable? = null
        override fun prepareDrawing(danmaku: BaseDanmaku, fromWorkerThread: Boolean) {
//            if (danmaku.text is Spanned) { // 根据你的条件检查是否需要需要更新弹幕
//                // FIXME 这里只是简单启个线程来加载远程url图片，请使用你自己的异步线程池，最好加上你的缓存池
//                object : Thread() {
//                    override fun run() {
//                        val url = "http://www.bilibili.com/favicon.ico"
//                        var inputStream: InputStream? = null
//                        var drawable = mDrawable
//                        if (drawable == null) {
//                            try {
//                                val urlConnection = URL(url).openConnection()
//                                inputStream = urlConnection.getInputStream()
//                                drawable = BitmapDrawable.createFromStream(inputStream, "bitmap")
//                                mDrawable = drawable
//                            } catch (e: MalformedURLException) {
//                                e.printStackTrace()
//                            } catch (e: IOException) {
//                                e.printStackTrace()
//                            } finally {
//                                IOUtils.closeQuietly(inputStream)
//                            }
//                        }
//                        if (drawable != null) {
//                            drawable.setBounds(0, 0, 100, 100)
//                            val spannable: SpannableStringBuilder = createSpannable(drawable)
//                            danmaku.text = spannable
//                            if (mDanmakuView != null) {
//                                mDanmakuView.invalidateDanmaku(danmaku, false)
//                            }
//                            return
//                        }
//                    }
//                }.start()
//            }
        }

        override fun releaseResource(danmaku: BaseDanmaku) {
        }
    }

    fun initDanmu() {

        // 设置最大显示行数
        val maxLinesPair = HashMap<Int, Int>()
        maxLinesPair[BaseDanmaku.TYPE_SCROLL_RL] = 5 // 滚动弹幕最大显示5行

        // 设置是否禁止重叠
        // 设置是否禁止重叠
        val overlappingEnablePair = HashMap<Int, Boolean>()
        overlappingEnablePair[BaseDanmaku.TYPE_SCROLL_RL] = true
        overlappingEnablePair[BaseDanmaku.TYPE_FIX_TOP] = true

        mContext = DanmakuContext.create()
        mContext?.setDanmakuStyle(IDisplayer.DANMAKU_STYLE_STROKEN, 3f)
            ?.setDuplicateMergingEnabled(false)
            ?.setScrollSpeedFactor(1.2f)
            ?.setScaleTextSize(1.2f)
            ?.setCacheStuffer(BackgroundCacheStuffer(), mCacheStufferAdapter)
            // 图文混排使用SpannedCacheStuffer
//            ?.setCacheStuffer(BackgroundCacheStuffer())  // 绘制背景使用BackgroundCacheStuffer
            ?.setMaximumLines(maxLinesPair)
            ?.preventOverlapping(overlappingEnablePair)
            ?.setDanmakuMargin(40)
        mParser = createParser(null)
        mBinding.svDanmaku.prepare(mParser, mContext)
        mBinding.svDanmaku.showFPS(false)
        mBinding.svDanmaku.enableDanmakuDrawingCache(true)

        mBinding.svDanmaku.setCallback(object : DrawHandler.Callback {
            override fun updateTimer(timer: DanmakuTimer) {}
            override fun drawingFinished() {}
            override fun danmakuShown(danmaku: BaseDanmaku) {
//                    Log.d("DFM", "danmakuShown(): text=" + danmaku.text);
            }

            override fun prepared() {
                mBinding.svDanmaku.start()
            }
        })
//        mDanmakuView.prepare(mParser, mContext)
//        mDanmakuView.showFPS(true)
//        mDanmakuView.enableDanmakuDrawingCache(true)
    }

    fun createParser(stream: InputStream?): BaseDanmakuParser? {
        if (stream == null) {
            return object : BaseDanmakuParser() {
                override fun parse(): Danmakus {
                    return Danmakus()
                }
            }
        }
        return null
//        val loader = DanmakuLoaderFactory.create(DanmakuLoaderFactory.TAG_BILI)
//        try {
//            loader.load(stream)
//        } catch (e: IllegalDataException) {
//            e.printStackTrace()
//        }
//
//        val parser: BaseDanmakuParser = BiliDanmukuParser()
//        val dataSource = loader.dataSource
//        parser.load(dataSource)
//        return parser
    }

    private fun addDanmaku(islive: Boolean) {
        val danmaku = mContext?.mDanmakuFactory?.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL)
        if (danmaku == null || mBinding.svDanmaku == null) {
            LogUtil.e("addDanmaku0")
            return
        }
        // for(int i=0;i<100;i++){
        // }
        danmaku.text = "这是一条弹幕" + System.nanoTime()
//        danmaku.padding = 5
        danmaku.priority = 0 // 可能会被各种过滤器过滤并隐藏显示
        danmaku.isLive = true
        danmaku.time = mBinding.svDanmaku.getCurrentTime() + 1200
//        danmaku.textSize = 25f * (mParser.getDisplayer().getDensity() - 0.6f)
        danmaku.textColor = Color.WHITE
        danmaku.textSize = AutoUtils.getPercentWidthSizeBigger(40).toFloat()
        danmaku.textShadowColor = Color.WHITE
        // danmaku.underlineColor = Color.GREEN;
        danmaku.borderColor =0
        mBinding.svDanmaku.addDanmaku(danmaku)
        LogUtil.e("addDanmaku1")

    }

    fun addrandData() {
//        val randomLayout = RandomLayout(mContext)
        var list = arrayOf(
            "ArrayArray1",
            "ArrayArray2",
            "ArrayArray3",
            "ArrayArray4",
            "ArrayArray5",
            "ArrayArray6",
            "ArrayArray8",
            "ArrayArray9",
            "ArrayArray10",
            "ArrayArray11",
            "ArrayArray12",
            "ArrayArray13",
            "ArrayArray14",
            "ArrayArray15",
            "ArrayArray16",
            "ArrayArray17",
            "ArrayArray18",
            "ArrayArray19",
            "ArrayArray20",
        )
        mBinding.randomLayout.setData(list)
        mBinding.randomLayout.setOnItemClickListener(object : RandomLayout.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int, text: String?) {
            }

        })
        mBinding.randomLayout.setOnAnimationEndListener(object : RandomLayout.OnAnimationEndListener {

            override fun onAnimationEnd(randomLayout: RandomLayout?, animationCount: Int) {

            }

        })
//        mBinding.randomLayout.startAnimation()

    }

}