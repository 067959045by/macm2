package com.mail.myapplication.ui.dg

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lzy.okgo.utils.HttpUtils.runOnUiThread
import com.mail.comm.app.AppManager
import com.mail.comm.base.BaseAty
import com.mail.comm.base.BaseDgFrg
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.WheelView.OnWheelViewListener
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.ui.lar.RegisterBirthdayAty
import com.mail.myapplication.ui.utils.NetworkUtils
import com.mail.myapplication.ui.wallet.RechargeAty
import com.rey.material.widget.TextView
import org.xutils.common.util.LogUtil


class SplashIsAllowDialog(context: BaseAty) : Dialog(context) {

    var baseAty = context
    lateinit var mBinding: DgSplash2Binding
    var info = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DgSplash2Binding.inflate(layoutInflater);
        setContentView(mBinding.root)
        window?.setWindowAnimations(R.style.dialogFullscreen2)

        window!!.setBackgroundDrawable(null)
        window!!.setGravity(Gravity.CENTER)
        setCanceledOnTouchOutside(false)
        setCancelable(false)
        val m = baseAty.windowManager
        val d = m.defaultDisplay// 获取屏幕宽、高
        val p = window!!.attributes
        window?.getDecorView()?.setPadding(0, 0, 0, 0)
//        p.height =  (d.height * 1) // 高度设置为屏幕的0.6
        p.width = (d.width * 1) // 宽度设置为屏幕的0.85
        window!!.attributes = p

        if (NetworkUtils.isConnected(baseAty)){
            mBinding.tvContent.text = "服务器正在维护中，请耐心等待！"
        }else{
            mBinding.tvContent.text = "请检查您的网络是否可用！"
        }

        mBinding.tv02.setOnClickListener {
            dismiss()
            AppManager.getInstance().AppExit(context)
        }
    }


    fun setData(type:String){

        if (isShowing){
            when(type){

                "1"->{
                    mBinding.tvContent.text ="系统检测到您的设备是模拟器！暂不支持模拟器,给您带来的不便请谅解"
                }
                "2"->{
                    mBinding.tvContent.text ="系统检测到您的设备Android系统版本过低，暂不支持低版本设备,给您带来的不便请谅解！请使用Android6.0以上设备"
                }
            }

        }
    }






}