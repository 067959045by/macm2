package com.mail.myapplication.ui.video

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter


class PlayListAdataper(fa: FragmentManager, lifecycle: Lifecycle,  val list : ArrayList<MutableMap<String, String>>) :

    FragmentStateAdapter(fa, lifecycle) {

    override fun getItemCount(): Int =list.size

    override fun createFragment(position: Int): Fragment =
        PlayList2Frg.create(list[position]["img"]!!, list[position]["m3u"]!!)

}