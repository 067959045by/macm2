package com.mail.myapplication.ui.dg

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lzy.okgo.utils.HttpUtils.runOnUiThread
import com.mail.comm.base.BaseAty
import com.mail.comm.base.BaseDgFrg
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.WheelView.OnWheelViewListener
import com.mail.myapplication.R
import com.mail.myapplication.databinding.DgBindPhoneBinding
import com.mail.myapplication.databinding.DgDateSelectBinding
import com.mail.myapplication.databinding.DgPayBinding
import com.mail.myapplication.databinding.DgRechargeBinding
import com.mail.myapplication.ui.lar.RegisterBirthdayAty
import com.mail.myapplication.ui.wallet.RechargeAty
import com.rey.material.widget.TextView
import org.xutils.common.util.LogUtil


class BindPhoneDialog(context: BaseAty) : Dialog(context) {

    var baseAty = context
    var listener :RechargeListen? =null
    lateinit var mBinding: DgBindPhoneBinding
    var info = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DgBindPhoneBinding.inflate(layoutInflater);
        setContentView(mBinding.root)
        window?.setWindowAnimations(R.style.dialogFullscreen2)

        window!!.setBackgroundDrawable(null)
        window!!.setGravity(Gravity.CENTER)
        setCanceledOnTouchOutside(false)
        val m = baseAty.windowManager
        val d = m.defaultDisplay// 获取屏幕宽、高
        val p = window!!.attributes
        window?.getDecorView()?.setPadding(0, 0, 0, 0)
//        p.height =  (d.height * 1) // 高度设置为屏幕的0.6
        p.width = (d.width * 1) // 宽度设置为屏幕的0.85
        window!!.attributes = p

        mBinding.tv01.setOnClickListener {

            listener?.onclik01()
        }
        mBinding.tv02.setOnClickListener {
            listener?.onclik02()
        }
    }

    interface  RechargeListen{
        fun onclik01()
        fun onclik02()
    }

    fun setRechargeListen(listener:RechargeListen){
        this.listener =listener
    }




}