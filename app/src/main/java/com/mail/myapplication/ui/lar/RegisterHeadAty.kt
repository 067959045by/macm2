package com.mail.myapplication.ui.lar

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.app.AppManager
import com.mail.comm.function.chooseimg.ChooseImgAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.utils.JSONUtils
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.MainAty
import com.zhy.autolayout.utils.AutoUtils

class RegisterHeadAty : BaseXAty() {


    var login_job = ""
    var login_gender = ""
    var login_like = ""
    var login_birth = ""
    var login_city = ""
    var login_head = ""
    var lar = Lar()

    lateinit var mBinding: AtyRegisterHeadBinding

    override fun getLayoutId(): Int = 0

    override fun initView() {
        if (intent.hasExtra("login_job")){
            login_job = intent.getStringExtra("login_job")!!
        }
        if (intent.hasExtra("login_gender")){
            login_gender = intent.getStringExtra("login_gender")!!
        }
        if (intent.hasExtra("login_like")){
            login_like = intent.getStringExtra("login_like")!!
        }
        if (intent.hasExtra("login_birth")){
            login_birth = intent.getStringExtra("login_birth")!!
            login_birth =login_birth.replace("\\s".toRegex(), "")
        }
        if (intent.hasExtra("login_city")){
            login_city = intent.getStringExtra("login_city")!!
        }
    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtyRegisterHeadBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvRight.text = "跳過"
            include.tvRight.visibility = View.VISIBLE
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_ok -> {
                if (TextUtils.isEmpty(login_head)){
                    showToastS("请选择头像")
                    return
                }
                startProgressDialog()
                lar.b7(login_head, this)
            }

            R.id.tv_right -> {
                startProgressDialog()
                lar.b4(
                    "",
                    "",
                    login_gender,
                    login_birth,
                    "",
                    login_city,
                    login_job,
                    login_like,
                    "",
                    this
                )
//                startActivity(RegisterCodeAty::class.java)
            }

            R.id.relay_head -> {
                var bundle = Bundle()
                bundle.putInt("max_num",1)
                bundle.putString("ratio","16,16")
                startActivityForResult(ChooseImgAty::class.java,bundle,100)
            }
        }
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type == "update") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                AppManager.getInstance().killActivity(RegisterJobAty::class.java)
                AppManager.getInstance().killActivity(RegisterLikeAty::class.java)
                AppManager.getInstance().killActivity(RegisterSexAty::class.java)
                AppManager.getInstance().killActivity(CityListAty::class.java)
                AppManager.getInstance().killActivity(RegisterBirthdayAty::class.java)
                startActivity(MainAty::class.java)
                finshPage("2")
            } else {
                showToastS(map["message"])
            }
        }
        if (type == "file/upload") {
            stopProgressDialog()

            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "0") {
                var dataMap = JSONUtils.parseKeyAndValueToMap(map["data"])
                var info_avatar2 = dataMap["file_path"]!!
//                lar.b4(
//                    info_nick,
//                    info_intro,
//                    info_gender,
//                    info_birth,
//                    info_avatar2,
//                    info_city,
//                    info_job,
//                    info_habit,
//                    "",
//                    this
//                )
                lar.b4(
                    "",
                    "",
                    login_gender,
                    login_birth,
                    info_avatar2,
                    login_city,
                    login_job,
                    login_like,
                    "",
                    this
                )
            } else {
                showToastS(map["message"])
            }
        }

    }


    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type == "update") {
            stopProgressDialog()
            showToastS("网络异常")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK){
            when(requestCode){

                100 ->{
                    var type =data?.getStringExtra("type")

                    when (type) {
                        "img_photo" -> {
                            var list = data?.getStringArrayListExtra("data")
                            login_head = list!![0]
                            ImageLoader.loadImage(this, list!![0],mBinding.ivHead,false)
                            mBinding.viewHead.visibility = View.GONE

                        }
                    }

                }
            }

        }
    }

    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemRegisterLikeBinding.inflate(LayoutInflater.from(this@RegisterHeadAty)))
        }

        override fun getItemCount(): Int = 9

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {



            }

        }

        inner class fGoldViewHolder(binding: ItemRegisterLikeBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemRegisterLikeBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }



}