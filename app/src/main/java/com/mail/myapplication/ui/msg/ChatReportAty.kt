package com.mail.myapplication.ui.msg

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.function.chooseimg.ChooseImgAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil

class ChatReportAty : BaseXAty() {

    var lar = Lar()
    var home = Home()

    var list = ArrayList<String>()
    var list_check = ArrayList<String>()
    var pathImg =""
    var to_user_code=""

    lateinit var mBinding: AtyChatReportBinding
    lateinit var mAdapter: GoldRecyclerAdapter

    override fun getLayoutId(): Int = 0

    override fun initView() {
        to_user_code = intent.getStringExtra("to_user_code").toString()
    }

    override fun requestData() {
        lar.b5(this)
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type == "data") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                var str = AESCBCCrypt.aesDecrypt(map["data"])

                var map2 = JSONUtils.parseKeyAndValueToMap(str)

                list = JSONUtils.parseKeyAndValueToListString(map2["complaint_data"])
                mAdapter.notifyDataSetChanged()

            } else {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }
        }


        if (type == "user/report"){

            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                showToastS("举报成功")
                finish()
            }else{
                showToastS(map["message"])
            }
        }

        if (type == "file/upload") {
//            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "0") {
                var dataMap = JSONUtils.parseDataToMap(var2)
                var path = dataMap["file_path"]!!
//
                var stringBuffer = StringBuffer()
                for (i in list_check.indices) {
                    stringBuffer.append(list_check[i])
                    if (i!=list_check.size-1){
                        stringBuffer.append(",")
                    }
                }
                home.a46(to_user_code,stringBuffer.toString(),path,this)

            } else {
                stopProgressDialog()
                showToastS(map["message"])
            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type == "data") {
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }
    }

    override fun getLayoutView(): View {
        mBinding = AtyChatReportBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "舉報"

            var mLayoutManager = GridLayoutManager(this@ChatReportAty, 1)
            mLayoutManager.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager = mLayoutManager
            mAdapter = GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {

                override fun reload() {
                    requestData()
                }

            })

        }
    }


    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_ok -> {
                if (list_check.size==0){
                    showToastS("请选择")
                    return
                }

                var stringBuffer = StringBuffer()
                for (i in list_check.indices) {
                    stringBuffer.append(list_check[i])
                    if (i!=list_check.size-1){
                        stringBuffer.append(",")
                    }
                }

                startProgressDialog()
                if (!TextUtils.isEmpty(pathImg)){
                    lar.b7(pathImg, this)
                }else{
                    home.a46(to_user_code,stringBuffer.toString(),"",this)
                }
//                ToastUitl.showToast(this, "举报成功", Toast.LENGTH_SHORT)
            }

            R.id.relay_head ->{
                var bundle = Bundle()
                bundle.putInt("max_num",1)
                bundle.putString("ratio","0,0")
                startActivityForResult(ChooseImgAty::class.java,bundle,100)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK){
            when(requestCode){

                100 ->{
                    var type =data?.getStringExtra("type")

                    when (type) {
                        "img_photo" -> {
                            var list = data?.getStringArrayListExtra("data")
                            pathImg = list!![0]
                            ImageLoader.loadImage(this, pathImg,mBinding.ivHead,false)
                            mBinding.viewHead.visibility = View.GONE

                        }
                    }

                }
            }

        }
    }




    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemChatReportBinding.inflate(LayoutInflater.from(this@ChatReportAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        tvName.text = "${position + 1}、${list[position]}"

                        if (list_check.contains(list[position])) {
                            imgvCheck?.setImageResource(com.mail.comm.R.drawable.icon_select_1)
                        } else {
                            imgvCheck?.setImageResource(com.mail.comm.R.drawable.icon_select_0)
                        }

                        itemView.setOnClickListener {

                            if (list_check.contains(list[position])) {
                                list_check.remove(list[position])
                            }else{
                                list_check.add(list[position])
                            }

                            notifyItemChanged(position)

                        }
                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemChatReportBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemChatReportBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}