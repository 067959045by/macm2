package com.mail.myapplication.ui.lar

import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.mail.comm.app.AppConfig
import com.mail.comm.app.AppManager
import com.mail.comm.app.BaseApp
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.view.AppCountdown
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyRegisterCodeBinding
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.MainAty
import com.mail.myapplication.ui.utils.MyUtils3
import org.xutils.common.util.LogUtil
import org.xutils.http.RequestParams
import org.xutils.x

class RegisterCodeAty : BaseXAty() {

    lateinit var mBinding: AtyRegisterCodeBinding

    lateinit var apCountdown: AppCountdown

    var lar = Lar()

    var phone =""
    var area_code =""

    var type_login = ""

    override fun getLayoutId(): Int = 0

    override fun initView() {
        phone = intent.getStringExtra("phone")!!
        area_code = intent.getStringExtra("area_code")!!
    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtyRegisterCodeBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding){
            initTopview2(include.relayTopBg)

            apCountdown = AppCountdown.getInstance()
            editCode.requestFocus()

            var mInputManager = this@RegisterCodeAty.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager

            editCode.post(Runnable {
                mInputManager.showSoftInput(editCode, 0)
              })


            Handler().postDelayed({
                run {
//                showToastS(registrationId)
                    MyUtils3.showInput(editCode,this@RegisterCodeAty)
//                    mInputManager.showSoftInput(editCode, 0)
                }
            }, 500)

            editCode.setOnTextChangeListeven { pwd ->

                if (editCode.text.toString().length==6){
                    tvOk.isEnabled  = true
                    tvOk.setBackgroundResource(R.drawable.shape_8)
                }else{
                    tvOk.isEnabled  = false
                    tvOk.setBackgroundResource(R.drawable.shape_6)
                }

            }
        }

    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_ok -> {
                startProgressDialog()
                lar.b14(phone,mBinding.editCode.text.toString(),area_code,"2",this)
            }
        }
    }

    override fun onComplete( var2: String?, type: String?) {
        super.onComplete( var2, type)

        if (type == "login/type") {
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map = JSONUtils.parseKeyAndValueToMap(str)
                var map_auth = JSONUtils.parseKeyAndValueToMap(map["auth"])
                var map_config = JSONUtils.parseKeyAndValueToMap(map["config"])
                var map_config_sys = JSONUtils.parseKeyAndValueToMap(map_config["sys"])
                var map_user_info = JSONUtils.parseKeyAndValueToMap(map["user_info"])

                if (TextUtils.isEmpty(map_auth["token"])) {
                    showToastS("token is null！")
                    return
                }

                type_login = "login"
                LogUtil.e("login token=="+map_auth["token"])
                setLoginStatus(true)
                PreferencesUtils.putString(x.app(), "token", map_auth["token"])
                BaseApp.instance?.saveOneMapData("info_code",map_user_info["code"])
                BaseApp.instance?.saveOneMapData("info_vip_level",map_user_info["vip_level"])
                BaseApp.instance?.saveOneMapData("info_mobile",map_user_info["mobile"])
                PreferencesUtils.putString(x.app(), "share_url", map_config_sys["share_url"])
                AppConfig.TOKEN = map_auth["token"]
                AppConfig.upload_url =map_config_sys["upload_url"]
                apCountdown.reSet()
                AppManager.getInstance().killActivity(LarAty::class.java)
                AppManager.getInstance().killActivity(RegisterAty::class.java)

                if (map_user_info!!["is_fresh"]=="1"){
                    var bundle = Bundle()
                    bundle.putString("type","login")
                    startActivity(RegisterJobAty::class.java,bundle)
                }else{
                    finish()
                    startActivity(MainAty::class.java)
                }

            } else {
                showToastS(map["message"])
            }
        }
    }


    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type == "bindphone") {
            stopProgressDialog()
            showToastS("网络异常")
        }
    }



}