package com.mail.myapplication.ui.viewmodel

import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.base.ConstantModel
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.FrgWindrawLogBinding
import com.mail.myapplication.databinding.ItemFrgHome02Binding
import com.mail.myapplication.databinding.ItemWindrawBinding
import com.mail.myapplication.interfaces.Home
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil

class WithdrawLog2Frg : BaseXFrg() {

    lateinit var mBinding: FrgWindrawLogBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2
    var list = ArrayList<MutableMap<String, String>>()
    var type = ""
    lateinit var mWithdrawViewModel:WithdrawViewModel
    lateinit var mWithdrawViewModel2:WithdrawViewModel2
    lateinit var mWithdrawViewModel3:WithdrawViewModel3

    override fun getLayoutId(): Int = 0

    override fun initView() {
        type = arguments?.getString("type").toString()
        mWithdrawViewModel =  ViewModelProvider(this).get(WithdrawViewModel::class.java)
        mWithdrawViewModel2 =  ViewModelProvider(this).get(WithdrawViewModel2::class.java)
        mWithdrawViewModel3 =  ViewModelProvider(this).get(WithdrawViewModel3::class.java)
        subscriptionViewModel()
    }

    fun subscriptionViewModel() {

        mWithdrawViewModel.type_load.observe(this, { type_load->

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()

            when(type_load){

                ConstantModel.LOADVIEW_LOADING ->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
                }

                ConstantModel.LOADVIEW_EMPTY->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                }

                ConstantModel.LOADVIEW_ERROR->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }

                ConstantModel.LOADVIEW_FINISH->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                }
            }
        })

        mWithdrawViewModel2.type_load.observe(this, { type_load->

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()

            when(type_load){

                ConstantModel.LOADVIEW_LOADING ->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
                }

                ConstantModel.LOADVIEW_EMPTY->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                }

                ConstantModel.LOADVIEW_ERROR->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }

                ConstantModel.LOADVIEW_FINISH->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                }
            }
        })

        mWithdrawViewModel3.type_load.observe(this, { type_load->

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()

            when(type_load){

                ConstantModel.LOADVIEW_LOADING ->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
                }

                ConstantModel.LOADVIEW_EMPTY->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                }

                ConstantModel.LOADVIEW_ERROR->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }

                ConstantModel.LOADVIEW_FINISH->{
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                }
            }
        })

        mWithdrawViewModel.list.observe(this,
            { t ->
                this@WithdrawLog2Frg.list.clear()
                this@WithdrawLog2Frg.list.addAll(t!!)
                mAdapter2.notifyDataSetChanged()
            })

        mWithdrawViewModel2.list.observe(this,
            { t ->
                this@WithdrawLog2Frg.list.clear()
                this@WithdrawLog2Frg.list.addAll(t!!)
                mAdapter2.notifyDataSetChanged()
            })

        mWithdrawViewModel3.list.observe(this,
            { t ->
                this@WithdrawLog2Frg.list.clear()
                this@WithdrawLog2Frg.list.addAll(t!!)
                mAdapter2.notifyDataSetChanged()
            })

    }

    override fun requestData() {
        when (type) {
            "1"->{
                mWithdrawViewModel.requestData(isLoadMore = false, isShowLoadVew = true)
            }
            "2"->{
                mWithdrawViewModel2.requestData(isLoadMore = false, isShowLoadVew = true)
            }
            "3"->{
                mWithdrawViewModel3.requestData(isLoadMore = false, isShowLoadVew = true)
            }
        }
    }


    companion object {

        fun create(type: String): WithdrawLog2Frg {
            val fragment = WithdrawLog2Frg()
            val bundle = Bundle()
            bundle.putString("type", type)
            fragment.arguments = bundle
            return fragment
        }

    }


    override fun getLayoutView(): View {
        mBinding = FrgWindrawLogBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(mBinding) {

            var mLayoutManager2 = GridLayoutManager(activity, 1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager = mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.setItemViewCacheSize(200)
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)

            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    mWithdrawViewModel.requestData(isLoadMore = false, isShowLoadVew = false)
                }

                override fun loadMoreStart() {
                    mWithdrawViewModel.requestData(isLoadMore = true, isShowLoadVew = false)

                }

            })

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    mWithdrawViewModel.requestData(isLoadMore = false, isShowLoadVew = true)
                }

            })
        }

    }


    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            return fGoldViewHolder(ItemWindrawBinding.inflate(LayoutInflater.from(activity)))
        }

        override fun getItemCount(): Int = list.size


        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {

                    with(mBinding){


                       when(this@WithdrawLog2Frg.type){
                           "1"->{
                               tvName.text = list[position]["pay_gate_type"]
                               tvTime.text = list[position]["created_at"]
                               tvNum.text = "+"+list[position]["real_amount"]

                               tvStatus.visibility = View.VISIBLE
                               when(list[position]["status"]){
                                   "0"->{
                                       tvStatus.text = "待支付"
                                       tvStatus.setTextColor(Color.parseColor("#FFA4A4A4"))

                                   }
                                   "1"->{
                                       tvStatus.text = "已支付"
                                   }
                                   "2"->{
                                       tvStatus.text = "已取消"
                                       tvStatus.setTextColor(Color.parseColor("#FFA4A4A4"))

                                   }
                               }
                           }
                           "2"->{
                               tvStatus.visibility = View.GONE
                               tvName.text = "钻石收益"
                               tvTime.text = list[position]["created_at"]
                               tvNum.text = "+"+list[position]["coins"]
                           }
                           "3"->{
                               if (TextUtils.isEmpty(list[position]["bank_name"])){
                                   tvName.text = "提现到USDT"

                               }else{
                                   tvName.text = "提现到银行卡"
                               }
                               tvStatus.visibility = View.VISIBLE

                               //状态1成功2失败3提现中
                               when(list[position]["status"]){
                                   "1"->{
                                       tvStatus.text = "提现成功"
                                       tvStatus.setTextColor(resources.getColor(R.color.main_03))
                                   }
                                   "2"->{
                                       tvStatus.text ="（"+list[position]["message"]+"） 提现失败"
                                       tvStatus.setTextColor(Color.parseColor("#FFA4A4A4"))

                                   }
                                   "3"->{
                                       tvStatus.text = "审核中"
                                       tvStatus.setTextColor(Color.parseColor("#FFFF3D77"))

                                   }
                               }

                               tvTime.text = list[position]["created_at"]
                               tvNum.text = "-"+list[position]["withdraw_num"]
                           }
                       }

                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemWindrawBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemWindrawBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }

}