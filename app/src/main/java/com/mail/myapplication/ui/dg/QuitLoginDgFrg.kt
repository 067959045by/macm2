package com.mail.myapplication.ui.dg

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import com.mail.comm.base.BaseDgFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.DgClearCacheBinding
import com.mail.myapplication.databinding.DgQuitLoginBinding
import com.mail.myapplication.databinding.FrgHomeBinding
import com.mail.myapplication.ui.mine.Set2Aty

class QuitLoginDgFrg : BaseDgFrg() {

    lateinit var mBinding: DgQuitLoginBinding

    var mQuitLoginDgFrgListen: QuitLoginDgFrgListen?=null

    override fun getLayoutId(): Int = 0

    override fun getDialogStyle(): Int = R.style.dialogFullscreen3


    override fun getLayoutView(): View {
        mBinding = DgQuitLoginBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun canCancel(): Boolean = true

    override fun setWindowAttributes(window: Window?) {}

    @SuppressLint("MissingSuperCall")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(mBinding){

            tv01.setOnClickListener {
                var setAty = activity as Set2Aty
                setAty.quitFinish()
                dismiss()
            }
            tvCancel.setOnClickListener {
                dismiss()
            }
        }
    }

    interface  QuitLoginDgFrgListen{
         fun onClick01()
    }

    fun setQuitLoginDgFrgListen(mQuitLoginDgFrgListen :QuitLoginDgFrgListen){
        this.mQuitLoginDgFrgListen = mQuitLoginDgFrgListen
    }

}