package com.mail.myapplication.ui.dg

import android.os.Bundle
import android.view.*
import com.mail.comm.base.BaseDgFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.DgChatLaHeiBinding
import com.mail.myapplication.databinding.DgClearCacheBinding
import com.mail.myapplication.databinding.FrgHomeBinding
import com.mail.myapplication.ui.mine.Set2Aty
import com.mail.myapplication.ui.msg.ChatSetAty

class ChatLaHeiDgFrg : BaseDgFrg() {

    lateinit var mBinding: DgChatLaHeiBinding

    override fun getLayoutId(): Int = 0

    override fun getDialogStyle(): Int = R.style.dialogFullscreen3


    override fun getLayoutView(): View {
        mBinding = DgChatLaHeiBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun canCancel(): Boolean = true

    override fun setWindowAttributes(window: Window?) {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(mBinding){

            tv01.setOnClickListener {
                var setAty = activity as ChatSetAty
                setAty.blakFinish()
                dismiss()
            }
            tvCancel.setOnClickListener {
                dismiss()
            }
        }
    }




}