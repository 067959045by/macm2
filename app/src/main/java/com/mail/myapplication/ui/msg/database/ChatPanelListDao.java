package com.mail.myapplication.ui.msg.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

// 定义数据访问对象的接口
@Dao
public interface ChatPanelListDao {  // 定义成接口
    // Query注解定义查询, 参数是sql语句
    @Query("SELECT * FROM ChatpanelList WHERE (user_code =:user_code And to_user_code = :to_user_code)OR(user_code =:to_user_code And to_user_code = :user_code)")
    List<ChatpanelList> getAll(String user_code,String to_user_code);

    // 根据id查询user, :id这里意思是引用findById方法里参数id。是room定义固定写法：冒号+参数名称
    @Query("SELECT * FROM ChatpanelList WHERE (user_code =:user_code And to_user_code = :to_user_code)OR(user_code =:to_user_code And to_user_code = :user_code)")
    ChatpanelList findById(String user_code,String to_user_code);
    //Update注解定义更新User
    @Update
    void udapte(ChatpanelList user);
    //Insert注解定义插入User
    @Insert
    void insert(ChatpanelList user);
    //Delete注解定义删除User
    @Delete
    void delete(ChatpanelList user);
}
