package com.mail.myapplication.ui.mine.apply

import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyApplyBinding
import com.mail.myapplication.databinding.AtyApplyFinishBinding
import com.mail.myapplication.databinding.AtyApplyImgVideoBinding
import com.mail.myapplication.databinding.AtySetBinding

class ApplyFinishAty : BaseXAty() {

    lateinit var mBinding: AtyApplyFinishBinding

    override fun getLayoutId(): Int = 0

    override fun initView() {}

    override fun requestData() {}

    override fun getLayoutView(): View {
        mBinding = AtyApplyFinishBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = ""
        }

    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()

            }

            R.id.tv_ok -> {
              finish()
            }

        }
    }


}