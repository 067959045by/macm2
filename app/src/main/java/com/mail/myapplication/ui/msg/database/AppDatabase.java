package com.mail.myapplication.ui.msg.database;

import android.app.Application;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Chatpanel.class,ChatpanelList.class}, version = 2,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract ChatPanelListDao chatPanelListDao();

    private static final String DB_NAME = "room_chat";
    private static volatile AppDatabase sInstance;

    //通常定义一个单例持有AppDatabase引用
    //DB_NAME是数据库文件名称
    public static AppDatabase getInstance(Application app){
        if(sInstance == null){
            synchronized (AppDatabase.class){
                if(sInstance == null){
                    sInstance = Room.databaseBuilder(app,
                            AppDatabase.class, DB_NAME).build();
                }
            }

        }
        return sInstance;
    }
}
