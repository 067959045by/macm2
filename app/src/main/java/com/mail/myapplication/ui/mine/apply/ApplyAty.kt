package com.mail.myapplication.ui.mine.apply

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyApplyBinding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.utils.MyUtils3

class ApplyAty : BaseXAty() {

    lateinit var mBinding: AtyApplyBinding
    var home = Home()
    var creator_status = ""
    var str =""
    var potato_url =""
    var tg_url =""

    override fun getLayoutId(): Int = 0

    override fun initView() {
        creator_status = intent.getStringExtra("creator_status").toString()
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData2()
    }

    fun requestData2() {
        home.a32(this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyApplyBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        mBinding.swipeRefreshLayout.finishLoadmore()
        mBinding.swipeRefreshLayout.finishRefreshing()
        stopProgressDialog()
        if (type == "creator/info") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                 str = AESCBCCrypt.aesDecrypt(map["data"])
                var maps = JSONUtils.parseKeyAndValueToMap(str)
                potato_url = maps!!["potato_url"]!!
                tg_url = maps["tg_url"]!!

            } else {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)

            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        mBinding.swipeRefreshLayout.finishLoadmore()
        mBinding.swipeRefreshLayout.finishRefreshing()
        stopProgressDialog()
        if (type == "creator/info"){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvTitle.text = ""

            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setIsPinContentView(true)

            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    requestData2()
                }

                override fun loadMoreStart() {
                }

            })

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    requestData()
                }

            })

            when(creator_status){
                "1"->{
                    mBinding.tvOk.text = "已認證"
                }
                "2"->{
                    mBinding.tvOk.text = "認證中"
                }
                "0"->{
                    mBinding.tvOk.text = "已拒絕"
                }
                "10"->{
                    mBinding.tvOk.text = "去認證"
                }
                else ->{
                    mBinding.tvOk.text = "去認證"
                }
            }

        }

    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.imv_01 -> {
                MyUtils3.openBrowser(this,potato_url)
            }

            R.id.imgv_02 -> {
                MyUtils3.openBrowser(this,tg_url)
            }

            R.id.tv_ok -> {
                var bundle = Bundle()
                bundle.putString("creator_status",creator_status)
                bundle.putString("data",str)
                startActivity(ApplyImgVideoAty::class.java,bundle)
            }
        }
    }


}