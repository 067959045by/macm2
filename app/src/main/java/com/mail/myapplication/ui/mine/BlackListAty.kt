package com.mail.myapplication.ui.mine

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils

class BlackListAty : BaseXAty() {

    var home = Home()
    var page = 1
    var list = ArrayList<MutableMap<String, String>>()

    var index_check = -1
    lateinit var mBinding: AtyBlackListBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2

    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        home.a29(page,this)
    }

    fun requestData2() {
        home.a29(page,this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyBlackListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "黑名单"

            var mLayoutManager2 = GridLayoutManager(this@BlackListAty,1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager =mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page =1
                    requestData2()
                }

                override fun loadMoreStart() {
                    page++
                    requestData2()
                }

            })

            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }

            })


        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)


        if (type == "user/delete") {
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                if (index_check==-1||index_check>=list.size){
                    return
                }
                list.removeAt(index_check)
                mAdapter2.notifyDataSetChanged()
            }else{
                showToastS(map["message"])
            }
        }

        if (type== "block/list"){

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if(map["code"]=="200"){
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                if (page == 1){
                    list.clear()
                    list.addAll(mList)

                }else{
                    list.addAll(mList)
                }

                if (page==1&&mList.size==0){
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                }else{
                    if (mList!=null&&mList.size>0){
                        mAdapter2?.notifyDataSetChanged()
                    }
                }
            }else{
                if (page==1){
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type== "block/list"){
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            if (page==1&&list.size==0){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
    }


    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemBlackListBinding.inflate(LayoutInflater.from(this@BlackListAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        var map = list[position]
                        var map_user = map

                        var maxW = AutoUtils.getPercentWidthSizeBigger(200)
                        ImageLoader.loadImageAes(this@BlackListAty, map_user["avatar"], ivHead,maxW,maxW)
                        tvName.text = map_user["nick"]

                        tvContent.text = list[position]["intro"]

                        ivHead.setOnClickListener {
                            var bundle = Bundle()
                            bundle.putString("user_id",map_user["uid"])
                            startActivity(PersonOtherDetailsAty::class.java,bundle)
                        }


                        MyUtils3.setVipLevel(map_user["vip_level"],mBinding.imgvVipLevel,0)

                        when (map_user["gender"]) {
                            "1" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
                            }
                            "0" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
                            }
                            "10" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
                            }
                            else -> {
                                mBinding.imgvGender.visibility = View.GONE
                            }
                        }

                        if (map_user["is_creator"]=="1"){
                            mBinding.imgvCreator.visibility = View.VISIBLE
                        }else{
                            mBinding.imgvCreator.visibility = View.GONE
                        }


                        imgvAtten.setOnClickListener {
                            index_check = position
                            startProgressDialog()
                            home.a30(map_user["to_user_id"]!!,this@BlackListAty)

                        }

                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemBlackListBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemBlackListBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }




}