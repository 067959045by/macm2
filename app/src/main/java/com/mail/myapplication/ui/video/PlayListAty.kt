package com.mail.myapplication.ui.video

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.viewpager2.widget.ViewPager2
import com.mail.comm.utils.JSONUtils
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyPlayListBinding
import com.mail.myapplication.ui.video.PlayListAdataper

class PlayListAty : BaseXAty() {

    lateinit var mBinding: AtyPlayListBinding

    var index = 0

    var list = ArrayList<MutableMap<String, String>>()

    override fun getLayoutId(): Int = 0

    override fun initView() {

        var data = intent.getStringExtra("data")
        var map = JSONUtils.parseKeyAndValueToMap(data)
        list = JSONUtils.parseKeyAndValueToMapList(map!!["img_data"])

//        list.addAll(list)
//        list.addAll(list)
        if (intent.hasExtra("index")){
            index = intent.getStringExtra("index")!!.toInt()
        }
    }

    override fun requestData() {
    }


    override fun getLayoutView(): View {
        mBinding = AtyPlayListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        initTopview2(mBinding.include.relayTopBg)

        with(mBinding) {
            viewPager.adapter = PlayListAdataper(supportFragmentManager, this@PlayListAty.lifecycle, list)
//            viewPager.orientation = ViewPager2.ORIENTATION_VERTICAL
            viewPager.setCurrentItem(index,false)
            include.tvTitle.text = "${index+1}/${list.size}"
            viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    include.tvTitle.text = "${position+1}/${list.size}"
                }
            })

//            swipeRefreshLayout.setEnableLoadmore(true)
//            swipeRefreshLayout.setEnableRefresh(true)
////            swipeRefreshLayout.setIsPinContentView(true)
//            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
//                override fun refreshStart() {
//                    swipeRefreshLayout.finishRefreshing()
//                }
//
//                override fun loadMoreStart() {
//                    swipeRefreshLayout.finishLoadmore()
//                }
//            })

        }
    }

    fun mainClick(v: View) {
        when (v.id) {
            R.id.relay_back -> {
                finish()
            }
        }
    }

}