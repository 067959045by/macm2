package com.mail.myapplication.ui.wallet

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mail.comm.image.ImageLoader
import com.mail.myapplication.R
import com.youth.banner.adapter.BannerAdapter
import com.youth.banner.util.BannerUtils
import com.zhy.autolayout.config.AutoLayoutConifg
import com.zhy.autolayout.utils.AutoUtils

class ImageAdapter(imageUrls: ArrayList<MutableMap<String,String>>) : BannerAdapter<MutableMap<String,String>, ImageAdapter.ImageHolder>(imageUrls) {


    override fun onCreateHolder(parent: ViewGroup?, viewType: Int): ImageHolder {
//        val imageView = ImageView(parent!!.context)
//        val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
//        params.height = AutoUtils.getPercentWidthSizeBigger(672)
//        params.width = AutoUtils.getPercentHeightSizeBigger(315)
//        imageView.layoutParams = params
//        imageView.scaleType = ImageView.ScaleType.FIT_XY
        //通过裁剪实现圆角

        val imageView = BannerUtils.getView(parent!!, R.layout.item_frg_wallet2) as RelativeLayout
//        BannerUtils.setBannerRound(imageView, 20f)
        return ImageHolder(imageView)
    }


    override fun onBindView(holder: ImageHolder, data: MutableMap<String,String>, position: Int, size: Int) {
//                .load(data)
//                .into(holder.imageView)
//        holder.imageView?.setImageResource(R.mipmap.ic_50)
        var maxW = AutoLayoutConifg.getInstance().screenWidth
        ImageLoader.loadImageAes(context = null,data["cover"],holder.imageView,maxW,maxW)
    }

    class ImageHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imageView: ImageView?=null

        init {
            AutoUtils.autoSize(view)
            imageView = view.findViewById(R.id.imgv_01)
        }
//        var imageView: ImageView = view as ImageView
    }

}

