package com.mail.myapplication.ui.mine

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.TextUtils
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils2
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyPerDetailsBinding
import com.mail.myapplication.databinding.AtyPerOtherDetailsBinding
import com.mail.myapplication.databinding.ItemFrgHome02Binding
import com.mail.myapplication.databinding.ItemFrgHome03Binding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.MainWalletAty
import com.mail.myapplication.ui.dg.DeletePostDialog
import com.mail.myapplication.ui.dg.RechargeDgFrg
import com.mail.myapplication.ui.dg.ReportDgFrg
import com.mail.myapplication.ui.home.HomeListFrg
import com.mail.myapplication.ui.msg.ChatAty
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil
import kotlin.math.abs

class PersonOtherDetailsAty : BaseXAty() {

    lateinit var mBinding: AtyPerOtherDetailsBinding
    lateinit var list_tv: Array<TextView>
    lateinit var list_v: Array<View>

    var user_id = ""
    var home = Home()
    var lar = Lar()
    var map_info: MutableMap<String, String>? = null
    var rechargeDg2:RechargeDgFrg?=null
    var reportDgFrg:ReportDgFrg?=null


    override fun getLayoutId(): Int = 0


    override fun getLayoutView(): View {
        mBinding = AtyPerOtherDetailsBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun initView() {
        user_id = intent.getStringExtra("user_id").toString()
        with(mBinding) {
            list_tv = arrayOf(tv01, tv02, tv03)
            list_v = arrayOf(v01, v02, v03)
        }
        initLayout()

    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        home.a17(user_id, this)
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "message/auth") {
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data = JSONUtils.parseKeyAndValueToMap(str)

                if (map_data["status"] != "0") {
//                    showToastS(map_data["message"])
                }

                when (map_data["status"]) {
                    "0" -> {
                        var bundle = Bundle()
                        bundle.putString("code", map_info!!["code"]!!)
                        bundle.putString("nick", map_info!!["nick"]!!)
                        bundle.putString("avatar", map_info!!["avatar"]!!)
                        startActivity(ChatAty::class.java, bundle)
                    }

                    "1"->{

                        if (rechargeDg2 == null) {
                            rechargeDg2 = RechargeDgFrg(this)
                            rechargeDg2?.setRechargeListen(object : RechargeDgFrg.RechargeListen{
                                override fun onclik01() {
                                    rechargeDg2?.dismiss()
                                }

                                override fun onclik02() {
                                    rechargeDg2?.dismiss()
                                    var bundle = Bundle()
                                    bundle.putString("page_type","pay")
                                    startActivity(MainWalletAty::class.java,bundle)
                                }

                            })
                        }
                        rechargeDg2?.show()
                        rechargeDg2?.setData(map_data["message"]!!,"2")
                    }

                    "2"->{
                        startActivity(BlackListAty::class.java)
                        showToastS(map_data["message"])
                    }

                    "3"->{
                        showToastS(map_data["message"])

                    }

                    "4"->{
                        showToastS(map_data["message"])
                    }

                }

            } else {
                showToastS(map["message"])
            }
        }

        if (type!!.startsWith("attention")) {
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data = JSONUtils.parseKeyAndValueToMap(str)
                if (map_data["status"] == "1") {
                    showToastS("已关注")
                    mBinding.imgvAtten.setImageResource(R.mipmap.ic_66)
                    mBinding.imgvAtten2.setImageResource(R.mipmap.ic_66)
                } else {
                    mBinding.imgvAtten.setImageResource(R.mipmap.ic_10)
                    mBinding.imgvAtten2.setImageResource(R.mipmap.ic_10)
                    showToastS("取消关注")
                }
//                refreshFrgAllAttentionData(map_data["status"]!!)

            } else {
                showToastS(map["message"])
            }
        }


        if (type == "other/info") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)

                mBinding.relayBack2.visibility = View.GONE
                var strs = AESCBCCrypt.aesDecrypt(map["data"])
                var map = JSONUtils.parseKeyAndValueToMap(strs)
                map_info = JSONUtils.parseKeyAndValueToMap(strs)

                if (map!!["is_follow"] == "1") {
                    mBinding.imgvAtten.setImageResource(R.mipmap.ic_66)
                    mBinding.imgvAtten2.setImageResource(R.mipmap.ic_66)
                } else {
                    mBinding.imgvAtten.setImageResource(R.mipmap.ic_10)
                    mBinding.imgvAtten2.setImageResource(R.mipmap.ic_10)
                }

                with(mBinding) {

                    var info_nick = map["nick"]
                    var info_flow = map["flow"]
                    var info_fans = map["fans"]
                    var info_avatar = map["avatar"]
                    var info_intro = map["intro"]
                    var info_city = map["city"]
                    var info_years = map["years"]
                    var info_job = map["job"]
                    var info_gender = map["gender"]
                    var info_is_creator = map["is_creator"]
                    var info_vip_level = map["vip_level"]

                    var maxW = AutoUtils.getPercentWidthSizeBigger(300)

                    ImageLoader.loadImageAes(this@PersonOtherDetailsAty, info_avatar, ivHead,maxW,maxW)
                    mBinding.tvName.text = info_nick
                    mBinding.tvName2.text = info_nick
                    mBinding.tvIntro.text = info_intro
                    mBinding.tvAttenNum.text = info_flow
                    mBinding.tvFansNum.text = info_fans
                    mBinding.tvCity.text = info_city
                    mBinding.tvJob.text = info_years + "岁  " + info_job

                    mBinding.tvId.text = "ID: ${map["code"]}"

                    if (TextUtils.isEmpty(info_intro)) {
                        mBinding.tvIntro.text = "暂无签名"
                    }

                    if (info_is_creator== "1") {
                        mBinding.imgvCreator.visibility = View.VISIBLE
                    } else {
                        mBinding.imgvCreator.visibility = View.GONE
                    }
                    MyUtils3.setVipLevel(info_vip_level,  mBinding.imgvVipLevel,0)


                    when (info_gender) {
                        "1" -> {
                            imgvGender.visibility = View.VISIBLE
                            imgvGender.setImageResource(R.mipmap.ic_60)
                        }
                        "0" -> {
                            imgvGender.visibility = View.VISIBLE
                            imgvGender.setImageResource(R.mipmap.ic_61)
                        }
                        "10" -> {
                            imgvGender.visibility = View.VISIBLE
                            imgvGender.setImageResource(R.mipmap.ic_18)
                        }
                        else -> {
                            imgvGender.visibility = View.GONE
                        }

                    }
                }

                var info_habit =
                    PreferencesUtils.getString(this@PersonOtherDetailsAty, "info_habit")
                var str = info_habit.split(",")

                if (str == null || str.size == 0 || info_habit.length == 0 || TextUtils.isEmpty(
                        info_habit
                    )
                ) {
                    mBinding.tvLike01.visibility = View.GONE
                    mBinding.tvLike02.visibility = View.GONE
                    mBinding.tvLike03.visibility = View.GONE
                } else if (str.size == 1) {
                    mBinding.tvLike01.visibility = View.VISIBLE
                    mBinding.tvLike02.visibility = View.GONE
                    mBinding.tvLike03.visibility = View.GONE
                    mBinding.tvLike01.text = str[0]
                } else if (str.size == 2) {
                    mBinding.tvLike01.visibility = View.VISIBLE
                    mBinding.tvLike02.visibility = View.VISIBLE
                    mBinding.tvLike03.visibility = View.GONE
                    mBinding.tvLike01.text = str[0]
                    mBinding.tvLike02.text = str[1]
                } else if (str.size == 3) {
                    mBinding.tvLike01.visibility = View.VISIBLE
                    mBinding.tvLike02.visibility = View.VISIBLE
                    mBinding.tvLike03.visibility = View.VISIBLE
                    mBinding.tvLike01.text = str[0]
                    mBinding.tvLike02.text = str[1]
                    mBinding.tvLike03.text = str[2]
                }
            } else {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
    }


    fun refreshFrgAllAttentionData(status:String) {
        var frg = list_frg[1]
        frg.refreshFrgAllAttentionData(status,user_id)
    }

    var list_frg =  ArrayList<HomeListFrg>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTranslanteBar()
        setAndroidNativeLightStatusBar(true)
        with(mBinding) {

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    requestData()
                }
            })

            var mLayoutManager2 = GridLayoutManager(this@PersonOtherDetailsAty, 1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL

//            var list = ArrayList<String>()

            list_frg.add(HomeListFrg.create2("mine_like", user_id))
            list_frg.add(HomeListFrg.create2("mine_my", user_id))
            list_frg.add(HomeListFrg.create2("mine_buy", user_id))

//            HomeListFrg.create2("mine_my", user_id)
//            HomeListFrg.create2("mine_buy", user_id)
//            list.add("mine_like")
//            list.add("mine_my")
//            list.add("mine_buy")

            viewPager.adapter = PersonListAdataper(
                supportFragmentManager,
                this@PersonOtherDetailsAty.lifecycle,
                list_frg
            )

            appbar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                val Offset = abs(verticalOffset).toFloat()
                val totalScrollRange = appBarLayout?.totalScrollRange
                var a = (Offset / totalScrollRange!!)
                linlayName.alpha = a
                relayReport.alpha = a
                imgvAtten2.alpha = a
            })

            linlay01.setOnClickListener {
                viewPager.setCurrentItem(0, true)
            }
            linlay02.setOnClickListener {
                viewPager.setCurrentItem(1, true)

            }
            linlay03.setOnClickListener {
                viewPager.setCurrentItem(2, true)
            }

            viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    when (position) {
                        0 -> {
                            setSelector(tv01)
                        }
                        1 -> {
                            setSelector(tv02)
                        }
                        2 -> {
                            setSelector(tv03)
                        }

                    }
                }
            })

            viewPager.setCurrentItem(1, false)


//            relayReport.setOnClickListener {
//
//                if (reportDgFrg==null){
//                    reportDgFrg = ReportDgFrg(this@PersonOtherDetailsAty)
//                }
//                reportDgFrg?.setRechargeListen(object : ReportDgFrg.RechargeListen{
//                    override fun onclik02(type: String, id: String) {
//                        if (type =="1"){
//                            startProgressDialog()
//                            home.a27(user_id,this@PersonOtherDetailsAty)
//                        }else{
//                            startProgressDialog()
//                            home.a28("1",user_id,this@HomeListFrg)
//                        }
//                    }
//
//                })
//                reportDgFrg?.show()
//                reportDgFrg?.setData("")
//            }

        }
    }

    inner class PersonListAdataper(fa: FragmentManager, lifecycle: Lifecycle, val docs:  ArrayList<HomeListFrg>) :

        FragmentStateAdapter(fa, lifecycle) {

        override fun getItemCount(): Int = docs.size

        override fun createFragment(position: Int): Fragment = docs[position]

    }

    private fun setSelector(tv: TextView) {
        for (i in list_tv.indices) {
            if (tv === list_tv[i]) {
                list_tv[i].setTextColor(Color.parseColor("#333438"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗


                list_tv[i].setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    AutoUtils.getPercentWidthSizeBigger(55).toFloat()
                )
                list_v[i].visibility = View.VISIBLE
            } else {
                list_tv[i].setTextColor(Color.parseColor("#65666D"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));//加粗
                list_tv[i].setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    AutoUtils.getPercentWidthSizeBigger(45).toFloat()
                )
                list_v[i].visibility = View.INVISIBLE
            }
        }
    }

    fun initLayout() {
        with(mBinding) {
            var params = toolbar.layoutParams as CollapsingToolbarLayout.LayoutParams
            var StatusBarHeight = MyUtils2.getStateBar(this@PersonOtherDetailsAty)
            if (StatusBarHeight <= 0) {
                StatusBarHeight = MyUtils2.dip2px(this@PersonOtherDetailsAty, 20F)
            }
            params.topMargin = StatusBarHeight
            params.height = AutoUtils.getPercentHeightSizeBigger(120)
            toolbar.setPadding(0, 0, 0, 0)
            toolbar.layoutParams = params
        }
        initLayout2()
    }

    fun initLayout2() {
        with(mBinding) {
            var params = linlayTop.layoutParams as LinearLayout.LayoutParams
            var StatusBarHeight = MyUtils2.getStateBar(this@PersonOtherDetailsAty)
            if (StatusBarHeight <= 0) {
                StatusBarHeight = MyUtils2.dip2px(this@PersonOtherDetailsAty, 20F)
            }
            params.topMargin = StatusBarHeight + AutoUtils.getPercentHeightSizeBigger(150)
            linlayTop.layoutParams = params
        }
    }


    fun mainClick(v: View) {

        when (v.id) {

            R.id.imgv_chat -> {
//                var bundle = Bundle()
//                bundle.putString("code",map_info!!["code"]!!)
//                bundle.putString("nick",map_info!!["nick"]!!)
//                bundle.putString("avatar",map_info!!["avatar"]!!)
//                startActivity(ChatAty::class.java,bundle)
                startProgressDialog()
                home.a47(map_info!!["code"]!!, this)
            }

            R.id.relay_back, R.id.relay_back2 -> {
                finish()
            }

            R.id.relay_edit -> {
                startActivity(EditInfoAty::class.java)
            }

            R.id.framlay_head -> {
//                startActivity(MyHeadAty::class.java)
            }

            R.id.imgv_atten, R.id.imgv_atten2 -> {
                startProgressDialog()
                lar.b9(user_id, this)
            }
        }
    }


}