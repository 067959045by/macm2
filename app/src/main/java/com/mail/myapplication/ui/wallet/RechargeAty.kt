package com.mail.myapplication.ui.wallet

import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.utils.TimeUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.dg.PayDialog
import com.mail.myapplication.ui.dg.PaySure2DgFrg
import com.mail.myapplication.ui.dg.PaySureDgFrg
import com.mail.myapplication.ui.home.HistoryActiveListAty
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.msg.ChatAty
import com.mail.myapplication.ui.msg.CustomerServiceAty
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils

class RechargeAty : BaseXAty() {

    lateinit var mBinding: AtyRechargeBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2
    var lar = Lar()
    var str_wallet = ""
    var coin_id = ""
    var payment_id = ""
    var dgPaySureDgFrg: PaySureDgFrg? = null
    var dgPaySure2DgFrg: PaySure2DgFrg? = null
    var dgPayDgFrg: PayDialog? = null
    var order_sn = ""
    var pay_time = 0L
    var map_service_info:MutableMap<String,String>? = null

    var list = ArrayList<MutableMap<String,String>>()
    var index_check = -1

    override fun getLayoutId(): Int = 0

    override fun initView() {
        var service_info = PreferencesUtils.getString(this,"service_info")
        map_service_info = JSONUtils.parseKeyAndValueToMap(service_info)
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData2()
    }

    override fun onRestart() {
        super.onRestart()
        if (dgPayDgFrg?.isShowing == true) {
            dgPayDgFrg?.dismiss()
            if (dgPaySureDgFrg == null) {
                dgPaySureDgFrg = PaySureDgFrg( this)
                dgPaySureDgFrg?.setPaySureListen(object : PaySureDgFrg.PaySureListen {
                    override fun surePay() {
                        if (dgPaySure2DgFrg == null) {
                            dgPaySure2DgFrg = PaySure2DgFrg(this@RechargeAty)
                            dgPaySure2DgFrg?.setRechargeListen(object : PaySure2DgFrg.RechargeListen{
                                override fun onclick01() {
                                    mBinding.swipeRefreshLayout.startRefresh()
                                }

                            })
                        }
                        dgPaySure2DgFrg?.show()
                        dgPaySure2DgFrg?.startLoor(order_sn,"2")
                    }
                })
            }
            dgPaySureDgFrg?.show()
        }
    }


     fun requestData2() {
        lar.b10(this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyRechargeBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTranslanteBar()
        setAndroidNativeLightStatusBar(true)

        with(mBinding) {
            var mLayoutManager2 = GridLayoutManager(this@RechargeAty, 3)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager = mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setIsPinContentView(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    requestData2()

                }

                override fun loadMoreStart() {
                }

            })

            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }
            })

            var str_list = ArrayList<String>();
            var color_list = ArrayList<Int>()
            var text_size_list = ArrayList<Float>()
            str_list.add(this@RechargeAty.resources.getString(R.string.s_12))
            str_list.add("在線客服")
            color_list.add(this@RechargeAty.resources.getColor(R.color.main_04))
            color_list.add(this@RechargeAty.resources.getColor(R.color.main_03))
            text_size_list.add(AutoUtils.getPercentHeightSize(38).toFloat())
            text_size_list.add(AutoUtils.getPercentHeightSize(42).toFloat())

            MyUtils3.setText(this@RechargeAty, tv001, str_list, color_list, text_size_list) { position->

               if (position==1){
                   var bundle = Bundle()
                   bundle.putString("code",map_service_info!!["code"])
                   bundle.putString("nick",map_service_info!!["nick"])
                   bundle.putString("avatar",map_service_info!!["avatar"])
                   startActivity(CustomerServiceAty::class.java,bundle)
               }

            }
        }
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        mBinding.swipeRefreshLayout.finishRefreshing()
        mBinding.swipeRefreshLayout.finishLoadmore()
        stopProgressDialog()
        if (type == "wallet/info") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                str_wallet = AESCBCCrypt.aesDecrypt(map["data"])
                refreshInfo()
            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)

            }
        }

        if (type == "pay"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map1 =JSONUtils.parseKeyAndValueToMap(str)
                order_sn = map1["order_sn"]!!
                pay_time = System.currentTimeMillis()
                MyUtils3.openBrowser(this,map1["link"])
            }else{
                showToastS(map["message"])
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type == "wallet/info"){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }
        mBinding.swipeRefreshLayout.finishRefreshing()
        mBinding.swipeRefreshLayout.finishLoadmore()
    }

    fun refreshInfo() {
        index_check = -1
        coin_id =""
        payment_id =""
        var map_info = JSONUtils.parseKeyAndValueToMap(str_wallet)
        list = JSONUtils.parseKeyAndValueToMapList(map_info["coin_list"])
        mAdapter2.notifyDataSetChanged()

        var maxW = AutoUtils.getPercentWidthSizeBigger(200)
        ImageLoader.loadImageAes(this, map_info["avatar"], mBinding.ivHead,maxW,maxW)

        mBinding.tvName.text = map_info["nick"]
        val colors = intArrayOf(
            Color.parseColor("#FFE9C5"),
            Color.parseColor("#FFFFFF"),
            Color.parseColor("#FFE9C5"),
            Color.parseColor("#ffffff"),
            Color.parseColor("#FFE9C5"))
        var positions = floatArrayOf(0f,0.2f,0.6f,0.8f,1f)
        var linearGradient = LinearGradient(0f,0f,(mBinding.tvName.paint.textSize*mBinding.tvName.text.length).toFloat(), 0f,colors,positions,
            Shader.TileMode.CLAMP)
        mBinding.tvName.paint.shader = linearGradient
        var info_code = PreferencesUtils.getString(this, "info_code")
        mBinding.tvId.text = "ID: ${info_code}"

        when (map_info["gender"]) {
            "1" -> {
                mBinding.imgvGender.visibility = View.VISIBLE
                mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
            }
            "0" -> {
                mBinding.imgvGender.visibility = View.VISIBLE
                mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
            }
            "10" -> {
                mBinding.imgvGender.visibility = View.VISIBLE
                mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
            }
            else -> {
                mBinding.imgvGender.visibility = View.GONE
            }
        }
        mBinding.tvMoney01.text = map_info["coins"]
        mBinding.tvMoney02.text = map_info["diamonds"]
        mBinding.tvMoney03.text = map_info["withdraw_diamonds"]

        mBinding.tvFreeNum.text = map_info["play_num_notice"]

        MyUtils3.setVipLevel2(map_info["vip_level"], mBinding.imgvVipLevel,mBinding.linlayVip,0)
        if (mBinding.linlayVip.visibility == View.GONE){
            mBinding.linlayNoVip.visibility = View.VISIBLE
        }else{
            mBinding.linlayNoVip.visibility = View.GONE
            var time =  (TimeUtils.getTime(map_info["vip_expired"]).toDouble()
                    - TimeUtils.getBeijinTime().toDouble())/1000
            mBinding.tvVipTime.text = TimeUtils.formatSecond3(time).replace("前","后")+"到期"
        }

//        mBinding.tvVipTime.text = map_info["vip_expired"] + "到期"



        if (map_info["is_creator"] == "1") {
            mBinding.imgvCreator.visibility = View.VISIBLE
        } else {
            mBinding.imgvCreator.visibility = View.GONE
        }

    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_02 ->{
                var bundle = Bundle()
                bundle.putString("typePage", "my")
                startActivity(PersonDetailsAty::class.java, bundle)
            }

            R.id.linlay_rlog ->{
                var bundle = Bundle()
                startActivity(WithdrawLogAty::class.java, bundle)
            }

            R.id.linlay_log ->{
                var bundle = Bundle()
                bundle.putInt("type_index", 2)
                startActivity(WithdrawLogAty::class.java, bundle)
            }

            R.id.linlay_withdraw->{
                startActivity(WithdrawAty::class.java)
            }

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_ok ->{

                if (TextUtils.isEmpty(coin_id)){
                    showToastS("请选择套餐")
                    return
                }

                if (dgPayDgFrg == null) {
                    dgPayDgFrg = PayDialog(this)
                    dgPayDgFrg?.setPayListen(object: PayDialog.PayListen{

                        override fun toPay(payment_id: String) {
                            startProgressDialog()
                            lar.b11(coin_id,payment_id, "",this@RechargeAty)
                        }

                    })
                }

                var list2 = JSONUtils.parseKeyAndValueToMapList(list[index_check]["payment"])
                dgPayDgFrg?.show()
                dgPayDgFrg?.setData(list2)

            }
        }
    }

    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemPayBinding.inflate(LayoutInflater.from(this@RechargeAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {
                        if (position == index_check){
                            linlay01.setBackgroundResource(R.mipmap.ic_78)
                        }else{
                            linlay01.setBackgroundResource(R.mipmap.ic_54)
                        }

                        itemView.setOnClickListener {
                            if (index_check == position){
                                return@setOnClickListener
                            }
                            index_check = position
                            notifyDataSetChanged()
                            this@RechargeAty.mBinding.tvPayNum.text =  list[position]["price"]+"圓"
                            coin_id = list[position]["id"]!!
                        }

                        tv01.text = list[position]["coins"]+"金幣"
                        tv02.text = list[position]["price"]+"圓"

                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemPayBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemPayBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}