package com.mail.myapplication.ui.msg

import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.home.HistoryActiveListAty
import com.mail.myapplication.ui.home.PostDetailsAty
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.utils.ClickListener
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils

class CommentListAty : BaseXAty() {

    lateinit var mBinding: AtyCommentListBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2

    var home = Home()
    var page = 1
    var list = ArrayList<MutableMap<String, String>>()
    var info_code = ""

    override fun getLayoutId(): Int = 0

    override fun initView() {
        info_code = MyUtils.getInfoDetails(this,"info_code")
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        home.a3(page,this)
    }

    fun requestData2() {
        home.a3(page,this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyCommentListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "評論"

            var mLayoutManager2 = GridLayoutManager(this@CommentListAty,1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager =mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page =1
                    requestData2()
                }

                override fun loadMoreStart() {
                    page++
                    requestData2()
                }
            })

            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }

            })
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type== "comment/lists"){
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if(map["code"]=="200"){
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                if (page == 1){
                    list.clear()
                    list.addAll(mList)

                }else{
                    list.addAll(mList)
                }

                if (page==1&&mList.size==0){

                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                }else{
                    mAdapter2?.notifyDataSetChanged()
                }
            }else{
                if (page==1){
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type== "comment/lists"){
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            if (page==1&&list.size==0){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
        }
    }


    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemCommentListBinding.inflate(LayoutInflater.from(this@CommentListAty)))
        }

        override fun getItemCount(): Int =  list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        var map = list[position]
                        var map_user = map
                        var index = position

                        var maxW_head = AutoUtils.getPercentWidthSizeBigger(200)

                        ImageLoader.loadImageAes(this@CommentListAty, map_user["avatar"], ivHead,maxW_head,maxW_head)
                        if (!TextUtils.isEmpty(map_user["forum_user_avatar"])){
                            ImageLoader.loadImageAes(this@CommentListAty, map_user["forum_user_avatar"], ivHead2,maxW_head,maxW_head)
                        }else{
                            ImageLoader.loadImageAes(this@CommentListAty, map_user["forum_cover"], ivHead2,maxW_head,maxW_head)
                        }

                        tvName.text = map_user["nick"]


                        ivHead.setOnClickListener {
                            var bundle = Bundle()
                            bundle.putString("user_id",map_user["user_id"])
                            startActivity(PersonOtherDetailsAty::class.java,bundle)
                        }
//                        ivHead2.setOnClickListener {
//                            var bundle = Bundle()
//                            bundle.putString("user_id",map_user["to_user_id"])
//                            startActivity(PersonOtherDetailsAty::class.java,bundle)
//                        }

                        itemView.setOnClickListener {
                            var bundle = Bundle()
                            bundle.putString("id",map_user["svalue"])
                            startActivity(PostDetailsAty::class.java,bundle)
                        }

                        if (list[position]["content"]!!.contains("[")){
                            var listContent = JSONUtils.parseKeyAndValueToMapList(list[position]["content"])
                            var str_list =  ArrayList<String>();
                            var color_list =  ArrayList<Int>()
                            var text_size_list =  ArrayList<Float>()
                            for ( mapf in listContent){
                                text_size_list.add(AutoUtils.getPercentHeightSize(40).toFloat())
                                if (TextUtils.isEmpty(mapf["code"])){
                                    color_list.add(Color.BLACK)
                                    str_list.add("  " + mapf["content"]!! + " ")
                                }else{
                                    color_list.add(Color.parseColor(resources.getString(R.string.main_05)))
                                    str_list.add("  @" + mapf["content"]!!)
                                }
                            }

                            MyUtils3.setText(this@CommentListAty, tvContent, str_list, color_list, text_size_list, object :
                                ClickListener {
                                override fun click(position: Int) {

                                    if (TextUtils.isEmpty(listContent[position]["code"])) {
                                        return
                                    }

                                    var bundle = Bundle()
                                    if (listContent[position]["code"]==info_code){
                                        bundle.putString("typePage","my")
                                        startActivity(PersonDetailsAty::class.java,bundle)
                                    }else{
                                        bundle.putString("user_id",listContent[position]["code"])
                                        startActivity(PersonOtherDetailsAty::class.java,bundle)
                                    }
                                }
                            })

                        }else{
                            tvContent?.text = list[position]["content"]
                        }

                        MyUtils3.setVipLevel(map_user["vip_level"], mBinding.imgvVipLevel,0)

                        when (map_user["gender"]) {
                            "1" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
                            }
                            "0" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
                            }
                            "10" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
                            }
                            else -> {
                                mBinding.imgvGender.visibility = View.GONE
                            }
                        }

                        if (map_user["is_creator"]=="1"){
                            mBinding.imgvCreator.visibility = View.VISIBLE
                        }else{
                            mBinding.imgvCreator.visibility = View.GONE
                        }

                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemCommentListBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemCommentListBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }




}