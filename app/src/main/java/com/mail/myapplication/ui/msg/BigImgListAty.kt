package com.mail.myapplication.ui.msg

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.mail.comm.utils.JSONUtils
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyPlayListBinding
import com.mail.myapplication.ui.home.HomeListFrg
import com.mail.myapplication.ui.video.PlayListAdataper

class BigImgListAty : BaseXAty() {

    lateinit var mBinding: AtyPlayListBinding

    var index = 0

    var list = ArrayList<String>()

    override fun getLayoutId(): Int = 0

    override fun initView() {

        list = intent.getStringArrayListExtra("data")!!
        if (intent.hasExtra("index")){
            index = intent.getStringExtra("index")!!.toInt()
        }
    }

    override fun requestData() {
    }


    override fun getLayoutView(): View {
        mBinding = AtyPlayListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        initTopview2(mBinding.include.relayTopBg)

        with(mBinding) {
            viewPager.adapter =
                HomeListAdataper(supportFragmentManager, this@BigImgListAty.lifecycle, list)
            viewPager.setCurrentItem(index,false)
            include.tvTitle.text = "${index+1}/${list.size}"
            viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    include.tvTitle.text = "${position+1}/${list.size}"
                }
            })
        }
    }


    inner class HomeListAdataper(fa: FragmentManager, lifecycle: Lifecycle, val list : MutableList<String>) :

        FragmentStateAdapter(fa, lifecycle) {

        override fun getItemCount(): Int =list.size

        override fun createFragment(position: Int): Fragment = BigImgFrg.create(list[position])

    }

    fun mainClick(v: View) {
        when (v.id) {
            R.id.relay_back -> {
                finish()
            }
        }
    }

}