package com.mail.myapplication.ui.dg

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.ProgressBar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lzy.okgo.utils.HttpUtils.runOnUiThread
import com.mail.comm.base.BaseAty
import com.mail.comm.base.BaseDgFrg
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.WheelView.OnWheelViewListener
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.ui.download.DownApkUtils
import com.mail.myapplication.ui.lar.RegisterBirthdayAty
import com.mail.myapplication.ui.utils.MyUtils3
import com.mail.myapplication.ui.wallet.RechargeAty
import com.rey.material.widget.TextView
import org.xutils.common.util.LogUtil


class UpdateDialog(context: BaseAty) : Dialog(context) {

    var baseAty = context
    lateinit var mBinding: DlgUpdateBinding
    var info = ""
    var foce = ""
    var url = ""
    var downApkUtils: DownApkUtils?=null

    fun downApk(url: String?, down_lb: android.widget.TextView?, down_pb: ProgressBar) {
        downApkUtils?.onPause()
        downApkUtils?.onDestroy()
        downApkUtils = DownApkUtils(url, down_lb, down_pb,baseAty)
        downApkUtils?.startDownload()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DlgUpdateBinding.inflate(layoutInflater);
        setContentView(mBinding.root)
//        setContentView(R.layout.dg_recharge)
        window?.setWindowAnimations(R.style.dialogFullscreen2)

        window!!.setBackgroundDrawable(null)
        window!!.setGravity(Gravity.CENTER)
        setCanceledOnTouchOutside(false)
        setCancelable(false)
        val m = baseAty.windowManager
        val d = m.defaultDisplay// 获取屏幕宽、高
        val p = window!!.attributes
        window?.getDecorView()?.setPadding(0, 0, 0, 0)
//        p.height =  (d.height * 1) // 高度设置为屏幕的0.6
        p.width = (d.width * 1) // 宽度设置为屏幕的0.85
        window!!.attributes = p

        mBinding.imgvCancel.setOnClickListener {
            dismiss()
        }

        mBinding.tv01.setOnClickListener {
            dismiss()
        }

        mBinding.tv02.setOnClickListener {

            if (url.contains(".apk")){
                downApk(url,mBinding.tvPro,mBinding.downPb)
                mBinding.linlay01.visibility = View.GONE
                mBinding.relay01.visibility = View.VISIBLE
            }else{
                MyUtils3.openBrowser(context,url)
            }
        }
    }

    fun setData(info: String,foce:String,url:String) {

        if (isShowing){
            mBinding.tvContent.text = info
           this.url= url
           this.foce= foce

            if (foce == "1"){
                mBinding.tv01.visibility = View.GONE
            }
        }
    }


}