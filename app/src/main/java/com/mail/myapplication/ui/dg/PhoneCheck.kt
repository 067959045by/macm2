package com.mail.myapplication.ui.dg

import com.mail.comm.app.AppManager
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.ui.lar.BindPhoneAty

class PhoneCheck {

    var index = 0

    var mBindPhoneDialog: BindPhoneDialog? = null

    companion object {

        var instance: PhoneCheck? = null

        fun getInstanceApp(): PhoneCheck? {
            if (instance == null) {
                instance = PhoneCheck()
            }
            return instance
        }
    }

    fun showBindPhoneDialog() {
        index = 0
        if (mBindPhoneDialog == null) {
            var aty = (AppManager.getInstance().topActivity) as BaseXAty
             mBindPhoneDialog = BindPhoneDialog(aty)
            mBindPhoneDialog?.setRechargeListen(object : BindPhoneDialog.RechargeListen {
                override fun onclik01() {
                    mBindPhoneDialog?.dismiss()
                }

                override fun onclik02() {
                    mBindPhoneDialog?.dismiss()
                    aty.startActivity(BindPhoneAty::class.java)
                }

            })
        }
        mBindPhoneDialog?.show()
    }

    fun getIndexs() = index

    fun setIndex() {
        if (index == 5) {
            showBindPhoneDialog()
        } else {
            index++
        }
    }


}