package com.mail.myapplication.ui.mine

import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import android.widget.RelativeLayout
import android.widget.Toast
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.ToastUitl
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyPrivateSetBinding
import com.mail.myapplication.databinding.AtySetBinding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.dg.ClearCacheDgFrg
import com.mail.myapplication.ui.dg.QuitLoginDgFrg
import org.xutils.common.util.LogUtil

class PrivateSetAty : BaseXAty() {

    var home = Home()

    lateinit var mBinding: AtyPrivateSetBinding

    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
    }

    var message_type =""

    override fun getLayoutView(): View {
        mBinding = AtyPrivateSetBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "私信設置"
            include.tvRight.text = "保存"
            include.tvRight.visibility = View.VISIBLE

            message_type =  MyUtils.getInfoDetails(this@PrivateSetAty,"message_type")

            //私信设置0不允许任何人私信,1只允许相互关注好友私信,2允许粉丝私信
            when(message_type){

                "0"->{
                    switch1.isChecked = false
                    switch2.isChecked = false
                    switch3.isChecked = true
                }
                "1"->{
                    switch1.isChecked = true
                    switch2.isChecked = false
                    switch3.isChecked = false
                }
                "2"->{
                    switch1.isChecked = false
                    switch2.isChecked = true
                    switch3.isChecked = false
                }
            }

            switch1.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener{
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    if (isChecked){
                        message_type = "1"
                        switch2.isChecked = false
                        switch3.isChecked = false
                    }
                }

            })
            switch2.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener{
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    if (isChecked){
                        message_type = "2"
                        switch1.isChecked = false
                        switch3.isChecked = false
                    }
                }

            })
            switch3.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener{
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    if (isChecked){
                        message_type = "0"
                        switch1.isChecked = false
                        switch2.isChecked = false
                    }
                }

            })
        }
    }



    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }


            R.id.tv_right ->{
                startProgressDialog()
                home.a38(message_type,this)
            }

        }
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type== "message/update"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                MyUtils.saveInfoDetails(this,"message_type",message_type)
                finish()
            }else{
                showToastS(map["message"])
            }
        }

    }


    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()

    }

}