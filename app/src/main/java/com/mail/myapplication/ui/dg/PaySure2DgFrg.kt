package com.mail.myapplication.ui.dg

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.view.*
import androidx.lifecycle.lifecycleScope
import com.lzy.okgo.utils.HttpUtils.runOnUiThread
import com.mail.comm.base.BaseAty
import com.mail.comm.base.BaseDgFrg
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.net.ApiListener
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.WheelView.OnWheelViewListener
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.lar.RegisterBirthdayAty
import kotlinx.coroutines.CoroutineScope
import org.xutils.common.Callback
import org.xutils.common.util.LogUtil
import org.xutils.http.RequestParams




class PaySure2DgFrg(context: BaseAty) : Dialog(context), ApiListener {

    private var baseAty = context
    lateinit var mBinding: DgPaySure2Binding
    var listener : RechargeListen? =null
    var countDownTimer: CountDownTimer? = null
    var lar = Lar()
    var order_sn =""
    var order_type =""
    var isPaySuccess = false
    var isEnable = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DgPaySure2Binding.inflate(layoutInflater);
        setContentView(mBinding.root)
        window?.setWindowAnimations(R.style.dialogFullscreen3)
        val dialogWindow = window
        dialogWindow!!.setBackgroundDrawable(null)
        dialogWindow.setGravity(Gravity.BOTTOM)
        setCanceledOnTouchOutside(false)
        setCancelable(false)
        window?.decorView?.setPadding(0, 0, 0, 0)

        val m = baseAty.windowManager
        val d = m.defaultDisplay// 获取屏幕宽、高
        val p = dialogWindow.attributes
//        p.height =  (d.height * 1) // 高度设置为屏幕的0.6
        p.width = (d.width * 1) // 宽度设置为屏幕的0.85
        dialogWindow.attributes = p
        with(mBinding) {
            tvOk.setOnClickListener {
                listener?.onclick01()
                dismiss()
            }
        }
    }

    fun loopCheckOrder() {
        mBinding.imgv01.visibility = View.GONE
        mBinding.tv01.visibility = View.GONE
        mBinding.tv02.visibility = View.VISIBLE
        mBinding.progressP.visibility = View.VISIBLE
        isEnable = false
        lar.b15(order_sn, order_type, this)
    }


    fun startLoor(order_sn:String,order_type:String) {
        this.isPaySuccess = false
        this.order_sn = order_sn
        this.order_type = order_type
        var millisInFuture = 60000
        var countDownInterval = 1000
        countDownTimer?.cancel()
        countDownTimer = object : CountDownTimer(millisInFuture.toLong(), countDownInterval.toLong()) {
                override fun onTick(millisUntilFinished: Long) {

                        if ((millisUntilFinished/1000).toString()!="0"){
                            mBinding.tv02.text ="支付確認中 (${(millisUntilFinished/1000)}s).."
                        }

                        if (isEnable){
                            loopCheckOrder()
                        }
                }

                override fun onFinish() {
                    countDownTimer?.cancel()
                    mBinding.imgv01.visibility = View.VISIBLE
                    mBinding.tv01.visibility = View.VISIBLE
                    mBinding.tv02.visibility = View.GONE
                    mBinding.progressP.visibility = View.GONE
                    if (isPaySuccess) {
                        mBinding.tv01.text = "支付成功"
                        mBinding.tv01.setTextColor(Color.parseColor("#000000"))
                        mBinding.imgv01.setImageResource(R.mipmap.ic_80)
                    } else {
                        mBinding.tv01.text = "订单检测超时"
                        mBinding.tv01.setTextColor(Color.parseColor("#ff0000"))
                        mBinding.imgv01.setImageResource(R.mipmap.ic_81)
                    }
                }

            }
        countDownTimer?.start()
    }


    override fun dismiss() {
        super.dismiss()
        destroy()
    }

   fun destroy(){
       countDownTimer?.cancel()
   }


    interface  RechargeListen{
        fun onclick01()
    }

    fun setRechargeListen(listener: RechargeListen){
        this.listener =listener
    }

    override fun onCancelled(var1: Callback.CancelledException?) {
    }



    override fun onComplete(var2: String?, type: String?) {
        isEnable = true
        //状态 0 待支付 1已支付 2 已取消
        if (type =="pay/result") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map1 = JSONUtils.parseKeyAndValueToMap(str)
                var status = map1["status"]

                LogUtil.e("pay==="+str)
                if (status == "1"){
                    this.isPaySuccess = true
                    countDownTimer?.cancel()
                    mBinding.imgv01.visibility = View.VISIBLE
                    mBinding.tv01.visibility = View.VISIBLE
                    mBinding.tv02.visibility = View.GONE
                    mBinding.progressP.visibility = View.GONE
                    mBinding.tv01.text = "支付成功"
                    mBinding.imgv01.setImageResource(R.mipmap.ic_80)
                }

            }else{

            }
        }
    }

    override fun onCompleteChild(var2: String?, type: String?) {
        onComplete(var2, type)
    }


    override fun onError(var1: Map<String?, String?>?, var2: RequestParams?) {
    }

    override fun onExceptionType(type: String?) {
        isEnable = true
    }

    override fun onExceptionTypeChild(type: String?) {
        onExceptionType(type)
    }

    override fun getCoroutineScope(): CoroutineScope? {
       return  baseAty.lifecycleScope
    }

    override fun onLoading(type: String, total: Long, current: Long, isDownloading: Boolean) {
    }


}