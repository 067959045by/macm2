package com.mail.myapplication.ui.msg.database

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import androidx.core.content.FileProvider
import com.mail.comm.app.AppManager
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import org.xutils.common.util.LogUtil
import org.xutils.x
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ChatUtils {

    companion object {

        fun getAllPannel(user_code:String): ArrayList<MutableMap<String,String>>{
            var mList = AppDatabase.getInstance(x.app())?.userDao()?.getAll(user_code)
            var list = ArrayList<MutableMap<String,String>>()

            if (mList!=null&&mList.size>0){

                for (data in mList!!){
                    var map = HashMap<String,String>()
                    map["user_code"]=data.user_code
                    map["to_user_code"]=data.to_user_code
                    map["time"]=data.time
                    map["message_id"]=data.message_id
                    map["msg"]=data.msg
                    map["unread_num"]=data.getUnread_num()
                    list.add(map)
                }
            }
            return  list
        }

        fun getOnePannelList(user_code:String,to_user_code:String): ArrayList<MutableMap<String,String>>{
            var mList = AppDatabase.getInstance(x.app())?.chatPanelListDao()?.getAll(user_code,to_user_code)
            var list = ArrayList<MutableMap<String,String>>()

            if (mList!=null&&mList.size>0){

                for (data in mList!!){
                    var map = HashMap<String,String>()
                    map["user_code"]=data.user_code
                    map["to_user_code"]=data.to_user_code
                    map["time"]=data.time
                    map["message_id"]=data.message_id
                    map["msg"]=data.msg
                    list.add(map)
                }
            }
            return  list
        }

        //默认已读
        fun  saveChatpannel(msg:String,isRead:Boolean = true){
            var str = AESCBCCrypt.aesDecrypt(msg)
            var map = JSONUtils.parseKeyAndValueToMap(str)
            var map_f = JSONUtils.parseKeyAndValueToMap(map["from_user"])
            var map_t = JSONUtils.parseKeyAndValueToMap(map["to_user"])
            var chatpanel = Chatpanel(map_f["user_code"],map_t["user_code"],map["time"],msg,map["message_id"])
            var chatpanelList = ChatpanelList(map_f["user_code"],map_t["user_code"],map["time"],msg,map["message_id"])
            var bean = AppDatabase.getInstance(x.app())?.userDao()?.findById(map_f["user_code"],map_t["user_code"])

            if(isRead){
                chatpanel.unread_num = "0"
            }else{
                if (bean==null){
                    chatpanel.unread_num = "1"
                }else{
                    var n = bean!!.unread_num.toInt()
                    chatpanel.unread_num = (n+1).toString()
                }
            }

            if (bean == null){
                LogUtil.e("AppDatabasess ==null")
                AppDatabase.getInstance(x.app())?.userDao()?.insert(chatpanel)
            }else{
                chatpanel.id = bean.id
                LogUtil.e("AppDatabasess !=null")
                AppDatabase.getInstance(x.app())?.userDao()?.udapte(chatpanel)
            }
//            AppDatabase.getInstance(x.app())?.chatPanelListDao()?.insert(chatpanelList)
        }

        //标记为已读
        fun  setChatPannelIsRead(user_code:String,to_user_code:String){
            var bean = AppDatabase.getInstance(x.app())?.userDao()?.findById(user_code,to_user_code)
            if (bean!=null){
                bean.setUnread_num("0")
                AppDatabase.getInstance(x.app())?.userDao()?.udapte(bean)
            }
        }

    }

}





