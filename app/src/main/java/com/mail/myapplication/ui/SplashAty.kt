package com.mail.myapplication.ui

import android.Manifest
import android.app.AlertDialog
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.TextUtils
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.exoplayer2.util.Log
import com.mail.comm.app.AppConfig
import com.mail.comm.app.AppStatusManager
import com.mail.comm.app.BaseApp
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.netretrofit.RetrofitFactory
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils2
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.utils.ToastUitl
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.MyApp
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtySplashBinding
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.dg.SplashDialog
import com.mail.myapplication.ui.dg.SplashIsAllowDialog
import com.mail.myapplication.ui.lar.LarAty
import com.mail.myapplication.ui.utils.MyUtils3
import com.mail.myapplication.ui.utils.VerificationCountDownTimer
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.xutils.common.util.LogUtil
import org.xutils.x

class SplashAty : BaseXAty() {

    val REQUESTCODE = 1001
    var isEndbleOpen = false
    var mPermissionList = ArrayList<String>()
    val PERMISSIONS_STORAGE = arrayOf(
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )

    var pastCode = ""
    var pastChannel = ""
    var link = ""
    var listIndex = 0
    var listIndex2 = 0
    var listLoginIndex = 0
    var lar = Lar()
    var loginType = ""
    var id_ad = ""
    var list = ArrayList<String>()
    lateinit var mBinding: AtySplashBinding
    var verificationCountDownTimer: VerificationCountDownTimer? = null
    var splashDialog: SplashDialog?=null
    var splashIsAllowDialog: SplashIsAllowDialog?=null

    override fun getLayoutId(): Int = 0

    override fun getLayoutView(): View {
        mBinding = AtySplashBinding.inflate(layoutInflater)
        return mBinding.root
    }

    override fun initView() {
        var typeAllow = isAllowEnter()
        if (!TextUtils.isEmpty(typeAllow)){
            if (splashIsAllowDialog == null){
                splashIsAllowDialog = SplashIsAllowDialog(this)
            }
            splashIsAllowDialog?.show()
            splashIsAllowDialog?.setData(typeAllow)
            return
        }
        startProgressDialog("線路選擇中..",false)
        initUrl()
        requestPermission()
    }

    fun getJpushId() {
        var time = 1000
        var registrationId = PreferencesUtils.getString(this, "registrationId", "")
        if (TextUtils.isEmpty(registrationId)) {
            time = 5000
        }

        Handler().postDelayed({
            run {
                getCoptyText()
            }
        }, time.toLong())

    }

    override fun requestData() {}

    fun requestDataPing() {
        requestDataPing2()
    }

    fun requestDataPing2() {
        lar.a(this)
    }

    fun requestDataLogin2() {
        lar.b(pastCode, pastChannel, this, MyUtils3.getDeviceInfoJson(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppStatusManager.getInstance().appStatus = 1
        super.onCreate(savedInstanceState)
        setStatusColor("#000000")
        setAndroidNativeLightStatusBar(false)
        var v_code = MyUtils2.getAppVersionCode()
        var v_name = MyUtils2.getAppVersionName()
        mBinding.tvV.text = "v${v_name}/${v_code}"

    }


    fun isAllowEnter():String{

        //禁止模拟器登陆
        if (MyUtils3.mayOnEmulator(this)){
            return "1"
        }

        //禁止低版本手机使用
        if (Build.VERSION.SDK_INT<=Build.VERSION_CODES.M){
            return "2"
        }

        return ""
    }


    fun initUrl() {
        var url_list = resources.getStringArray(R.array.url_list)
        list.clear()
        for (str in url_list) {
            list.add(str)
        }
        if (!AppConfig.isFormal) {
            list.clear()
            list.add("http://api1.yt.xingtaole.com")
            list.add("http://api2.yt.xingtaole.com")
        }
        listIndex = 0
        AppConfig.Host_Url = list[listIndex]
//       test01()
    }

    fun getCoptyText() {
        var content = "";
        var clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        try {
            var clipData = clipboard.primaryClip;
            var item = clipData?.getItemAt(0);
            content = item?.text.toString();
        } catch (e: Exception) {

        } finally {

            if (content.contains("u=") && content.contains("c=")) {
                pastCode = MyUtils2.getCode(content)
                pastChannel = MyUtils2.getChannel(content)
            }

            if (TextUtils.isEmpty(pastChannel)) {
                pastChannel = MyUtils2.getChannelName()
            }
            requestDataPing()
        }
    }

    private fun requestPermission() {
        mPermissionList.clear()
        for (i in PERMISSIONS_STORAGE.indices) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    PERMISSIONS_STORAGE[i]
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                mPermissionList.add(PERMISSIONS_STORAGE[i])
            }
        }
        if (mPermissionList.isEmpty() || mPermissionList.size == 0 || Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            isEndbleOpen = true
            getJpushId()
        } else {
            val permissions = mPermissionList.toTypedArray() //将List转为数组
            ActivityCompat.requestPermissions(this, permissions, REQUESTCODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUESTCODE) {
            for (i in grantResults.indices) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    val showRequestPermission: Boolean =
                        ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            permissions[i].toString()
                        )
                    if (showRequestPermission) {
                        isEndbleOpen = false
                        requestPermission()
                    } else {
                        isEndbleOpen = false
                        val builder = AlertDialog.Builder(this)
                        builder.setCancelable(false)
                        builder.setTitle("开启权限")
                        builder.setMessage("为了您更好的使用软件需要开启权限（开启权限后杀掉app进程重新打开）:" + "\n\n" + "电话权限,存储权限,相机权限")
                        builder.setPositiveButton("开启") { dialog, which ->
                            val i = Intent()
                            i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts("package", packageName, null)
                            i.data = uri
                            startActivity(i)
                            dialog.dismiss()
                        }
                        var dialog = builder.show()
                    }
                } else {
                    isEndbleOpen = true
                }
            }
            if (isEndbleOpen) {
                getJpushId()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }


    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)

        LogUtil.e("onExceptionType"+type)

        if (type == "ping") {
            reInitUrl()
        }

        if (type == "login") {
            listLoginIndex++
            if (listLoginIndex > 7) {
                stopProgressDialog()
                if (splashDialog == null){
                    splashDialog = SplashDialog(this)
                }
                splashDialog?.show()
                return
            }
            requestDataLogin2()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        verificationCountDownTimer?.cancel()
        verificationCountDownTimer = null
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "ping") {
            if (var2 == "tong") {
                requestDataLogin2()
            } else {
                reInitUrl()
            }
        }

        if (type == "login") {

            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map = JSONUtils.parseKeyAndValueToMap(str)
                var map_auth = JSONUtils.parseKeyAndValueToMap(map["auth"])
                var map_config = JSONUtils.parseKeyAndValueToMap(map["config"])
                var map_config_sys = JSONUtils.parseKeyAndValueToMap(map_config["sys"])
                var map_startup = JSONUtils.parseKeyAndValueToMap(map["startup"])
                var map_user_info = JSONUtils.parseKeyAndValueToMap(map["user_info"])

                id_ad = map_startup!!["id"]!!
                //公告
                PreferencesUtils.putString(x.app(), "notice", map_config_sys["notice"])
                //公告
                PreferencesUtils.putString(x.app(), "app_notice", map_config_sys["app_notice"])
                //客服信息
                PreferencesUtils.putString(x.app(), "service_info", map["service_info"])

                BaseApp.instance?.saveOneMapData("info_code", map_user_info["code"])
                BaseApp.instance?.saveOneMapData("info_vip_level", map_user_info["vip_level"])
                BaseApp.instance?.saveOneMapData("info_mobile", map_user_info["mobile"])

                //声网appid
                PreferencesUtils.putString(x.app(), "rtm_app_id", map_config_sys["rtm_app_id"])
                //城市地址
                PreferencesUtils.putString(x.app(), "new_area_json_url", map_config_sys["area_json_url"])

                BaseApp.instance?.saveOneMapData("tencent_license_url", map_config_sys["tencent_license_url"]!!)
                BaseApp.instance?.saveOneMapData("tencent_license_key", map_config_sys["tencent_license_key"]!!)
                BaseApp.instance?.saveOneMapData("tg_url", map_config_sys["tg_url"]!!)

//                var licenceUrl = "https://license.vod2.myqcloud.com/license/v2/1306008949_1/v_cube.license"
//                var licenseKey = "2b8e8b8805bb41d61775f16170bc4a66"

                (application as MyApp).initChatIm()

                if (!TextUtils.isEmpty(map_config_sys["tencent_license_url"])) {
                    (application as MyApp).initTxVideo(map_config_sys["tencent_license_url"]!!, map_config_sys["tencent_license_key"]!!)
                }

                if (TextUtils.isEmpty(map_user_info["code"])) {
                    showToastS("code is null！")
                    return
                }

                if (TextUtils.isEmpty(map_auth["token"])) {
                    showToastS("token is null！")
                    return
                }

                //is_fresh是否新用户 is_login_out是否退出登陆
                if (map_user_info!!["is_fresh"] == "1" || map_user_info!!["is_login_out"] == "1") {
                    loginType = "no_login"
                } else {
                    loginType = "login"
                }

                setLoginStatus(true)
                PreferencesUtils.putString(x.app(), "token", map_auth["token"])
                PreferencesUtils.putString(x.app(), "share_url", map_config_sys["share_url"])
                //全局域名
                PreferencesUtils.putString(x.app(), "resource_cdn", map_config_sys["resource_cdn"])
                AppConfig.TOKEN = map_auth["token"]
                //文件上传地址
                AppConfig.upload_url = map_config_sys["upload_url"]

                if (map_startup["type"] == "1") {
                    link = map_startup["link"]!!
//
//                    startProgressDialog()
                    ImageLoader.loadImageAesCallback(
                        this,
                        map_startup["cover"],
                        object : ImageLoader.BimapCallback {
                            override fun onLoadSuccess(bitmap: Bitmap?) {
                                stopProgressDialog()
                                mBinding.ivCover.setImageBitmap(bitmap)
                                mBinding.ivCover.visibility = View.VISIBLE
                                showAds()
                            }

                            override fun onLoadFailed() {
                                stopProgressDialog()
                                showAds()
                            }

                        })
                } else {
                    showAds()
                }

            } else {
                showToastS(map["message"])
            }
        }
    }

    fun showAds() {
        mBinding.tvTime.isEnabled = false
        mBinding.tvTime.visibility = View.VISIBLE
        var timeIndex = 10 * 1000
        if (AppConfig.debug) {
//            timeIndex = 1*1000
        }

        verificationCountDownTimer = object : VerificationCountDownTimer(timeIndex.toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                super.onTick(millisUntilFinished)
                runOnUiThread {
                    if ((millisUntilFinished / 1000) <= 5) {
                        mBinding.tvTime.text = "跳过"
                        mBinding.tvTime.isEnabled = true
                    } else {
                        mBinding.tvTime.text = ((millisUntilFinished / 1000) - 5).toString() + "S"
                    }

                }
            }

            override fun onFinish() {
                super.onFinish()
                runOnUiThread {
                    mBinding.tvTime.performClick()
                }
            }
        }
        verificationCountDownTimer?.start()
    }

    fun reInitUrl() {
        listIndex2++
        LogUtil.e("reInitUrl"+listIndex2)

        if (listIndex2 > 7) {
            stopProgressDialog()
            if (splashDialog == null){
                splashDialog = SplashDialog(this)
            }
            splashDialog?.show()
            return
        }
        listIndex++
        if (listIndex >= list.size) {
            listIndex = 0
        }
        AppConfig.Host_Url = list[listIndex]
        requestDataPing2()
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.tv_time -> {

                if (loginType == "no_login") {
                    var bundle = Bundle()
                    bundle.putString("pastCode", pastCode)
                    bundle.putString("pastChannel", pastChannel)
                    startActivity(LarAty::class.java, bundle)
                    finshPage("2")
                }

                if (loginType == "login") {
                    startActivity(MainAty::class.java)
                    finshPage("2")
                }
            }

            R.id.ivCover -> {
                if (TextUtils.isEmpty(link)) {
                    return
                }
                lar.b16(id_ad, this)
                if (link.startsWith("url")) {
                    var index = link.indexOf("//")
                    MyUtils3.openBrowser(this, link.substring(index + 2))
                }
            }
        }
    }
}