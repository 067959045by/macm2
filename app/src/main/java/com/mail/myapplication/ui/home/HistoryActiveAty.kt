package com.mail.myapplication.ui.home

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.TimeUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.zhy.autolayout.utils.AutoUtils

class HistoryActiveAty : BaseXAty() {

    lateinit var mBinding: AtyHistoryActiveBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2

    var home = Home()
    var page = 1
    var list = ArrayList<MutableMap<String, String>>()
    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData2()
    }

    fun requestData2() {
        home.a24(page,"","1",this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyHistoryActiveBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding) {
            var mLayoutManager2 = GridLayoutManager(this@HistoryActiveAty, 1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager = mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page=1
                    requestData2()
                }

                override fun loadMoreStart() {
                    page++
                    requestData2()
                }

            })
            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }

            })

        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_search -> {
                if (TextUtils.isEmpty(mBinding.editSearch.text.toString())) {
                    showToastS("请输入搜索内容")
                    return
                }
                var bundle = Bundle()
                bundle.putString("keywords", mBinding.editSearch.text.toString())
                startActivity(HistoryActiveSearchAty::class.java, bundle)
                mBinding.editSearch.setText("")

            }

        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type== "topic/list"){
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if(map["code"]=="200"){
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                if (page == 1){
                    list.clear()
                    list.addAll(mList)

                }else{
                    list.addAll(mList)
                }

                if (page==1&&mList.size==0){
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                }else{
                    if (mList!=null&&mList.size>0){
                        mAdapter2?.notifyDataSetChanged()
                    }
                }
            }else{
                if (page==1){
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type== "topic/list"){
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            if (page==1&&list.size==0){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
    }

    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemHistoryActiveBinding.inflate(LayoutInflater.from(this@HistoryActiveAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {
                with(holder){
                    with(mBinding){

                        itemView.setOnClickListener {
                            var bundle = Bundle()
                            bundle.putString("id",list[position]["id"])
                            bundle.putString("topic",list[position]["title"])
                            startActivity(HistoryActiveListAty::class.java,bundle)
                        }
                        var map = list[position]
                        var maxW = AutoUtils.getPercentWidthSizeBigger(300)
                        ImageLoader.loadImageAes(this@HistoryActiveAty,map["logo"],imgv,maxW,maxW)
                        tvTitle.text = map["title"]
                        tvContent.text = map["intro"]
                        tvZanNum.text = map["total_like"]
                        tvCommentNum.text = map["total_comment"]
                        tvShareNum.text = map["total_share"]
                        mBinding.tvTime.text ="活動截止："+map["end_time"]
                    }
                }
            }
        }

        inner class fGoldViewHolder(binding: ItemHistoryActiveBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemHistoryActiveBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}