package com.mail.myapplication.ui.lar

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil

class RegisterLikeAty : BaseXAty() {

    lateinit var mBinding: AtyRegisterLikeBinding
    lateinit var mAdapter: GoldRecyclerAdapter

    lateinit var list: ArrayList<String>
    var list_check = ArrayList<String>()
    var type = ""
    var login_job = ""
    var login_gender = ""

    override fun getLayoutId(): Int = 0

    override fun initView() {
        type = intent.getStringExtra("type")!!
        if (intent.hasExtra("login_job")){
            login_job = intent.getStringExtra("login_job")!!
        }
        if (intent.hasExtra("login_gender")){
            login_gender = intent.getStringExtra("login_gender")!!
        }
    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtyRegisterLikeBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var config_data = PreferencesUtils.getString(this, "config_data")

        LogUtil.e("config_data==" + config_data)
        var map = JSONUtils.parseKeyAndValueToMap(config_data)
        list = JSONUtils.parseKeyAndValueToListString(map["habit_data"])
        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvRight.text = "跳過"
            include.tvRight.visibility = View.VISIBLE

            if (type == "edit") {
                include.tvRight.visibility = View.GONE
            } else {
                include.tvRight.visibility = View.VISIBLE
            }

            var mLayoutManager2 = GridLayoutManager(this@RegisterLikeAty, 3)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager = mLayoutManager2
            mAdapter = GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter
        }

    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_ok -> {

                if (type == "edit") {
                    if (list_check.size == 0) {
                        showToastS("请选择你的爱好")
                        return
                    }
                    var stringBuffer = StringBuffer()
                    for (i in list_check.indices) {
                        stringBuffer.append(list_check[i])
                        if (i!=list_check.size-1){
                            stringBuffer.append(",")
                        }
                    }
                    var intent = Intent()
                    intent.putExtra("data", stringBuffer.toString())
                    setResult(RESULT_OK, intent)
                    finshPage("2")
                }else{
                    if (list_check.size == 0) {
                        showToastS("请选择你的爱好")
                        return
                    }
                    var stringBuffer = StringBuffer()
                    for (i in list_check.indices) {
                        stringBuffer.append(list_check[i])
                        if (i!=list_check.size-1){
                            stringBuffer.append(",")
                        }
                    }
                    var bundle = Bundle()
                    bundle.putString("type","login")
                    bundle.putString("login_job",login_job)
                    bundle.putString("login_gender",login_gender)
                    bundle.putString("login_like",stringBuffer.toString())
                    startActivity(RegisterBirthdayAty::class.java,bundle)
                }//                startActivity(RegisterBirthdayAty::class.java)
            }

            R.id.tv_right -> {
                var bundle = Bundle()
                bundle.putString("type","login")
                bundle.putString("login_job",login_job)
                bundle.putString("login_gender",login_gender)
                bundle.putString("login_like","")
                startActivity(RegisterBirthdayAty::class.java,bundle)
//                startActivity(RegisterCodeAty::class.java)
            }
        }
    }


    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemRegisterLikeBinding.inflate(LayoutInflater.from(this@RegisterLikeAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {

                    if (list_check.contains(list[position])) {
                        mBinding.tvName.setBackgroundResource(R.drawable.shape_33)

                    } else {
                        mBinding.tvName.setBackgroundResource(R.drawable.shape_9)
                    }

                    mBinding.tvName.text = list[position]

                    itemView.setOnClickListener {
                        if (list_check.size>=3&&!list_check.contains(list[position])){
                            showToastS("最多选择3个")
                            return@setOnClickListener
                        }
                        if (list_check.contains(list[position])) {
                            list_check.remove(list[position])

                        }else{
                            list_check.add(list[position])
                        }
                        mAdapter.notifyItemChanged(position)
                    }

                }


            }

        }

        inner class fGoldViewHolder(binding: ItemRegisterLikeBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemRegisterLikeBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}