package com.mail.myapplication.ui.video

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter


class PlayListVideoAdataper(fa: FragmentManager, lifecycle: Lifecycle, val list : ArrayList<String>) :

    FragmentStateAdapter(fa, lifecycle) {

    override fun getItemCount(): Int =list.size

    override fun createFragment(position: Int): Fragment =
        PlayList2Frg.create("", list[position])

}