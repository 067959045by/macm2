package com.mail.myapplication.ui.home

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.FrgHomeBinding
import com.mail.myapplication.databinding.ItemFrgHome01Binding
import com.mail.myapplication.databinding.ItemFrgHome02Binding
import com.mail.myapplication.databinding.ItemFrgHome03Binding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.MainAty
import com.mail.myapplication.ui.dg.RechargeDgFrg
import com.mail.myapplication.ui.dg.ReportDgFrg
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.mine.PostSendAty
import com.mail.myapplication.ui.utils.MyUtils3
import com.mail.myapplication.ui.video.PlayListAdataper
import com.mail.myapplication.ui.video.PlayListAty
import com.mail.myapplication.ui.video.PlayListFrg
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil

class HomeFrg:BaseXFrg() {

    lateinit var mBinding: FrgHomeBinding
    lateinit var mAdapter: GoldRecyclerAdapter
    lateinit var list_tv: Array<TextView>
    lateinit var list_v: Array<View>

    var info_code =""

    var list = ArrayList<MutableMap<String,String>>()

    var list_frg =  ArrayList<HomeListFrg>()

    var  lastClickTime = 0L

    override fun getLayoutId(): Int =0

    var home = Home()

    override fun getLayoutView(): View {
        mBinding = FrgHomeBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun initView() {
        with(mBinding){
            list_tv = arrayOf(tv01,tv02, tv03)
            list_v = arrayOf(v01,v02, v03)
        }
        info_code = MyUtils.getInfoDetails(requireActivity(),"info_code")
    }

    override fun onResume() {
        super.onResume()
        home.a9(this)
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "home/user/list"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                list = JSONUtils.parseKeyAndValueToMapList(str)
                if (list!=null&&list.size>0){
                    mAdapter.notifyDataSetChanged()
                }
            }
        }
    }

    override fun requestData() {}

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(mBinding){

            imgvSide.setOnClickListener {
                var mainAty = activity as MainAty
                mainAty.openSide()
            }

            var mLayoutManager = LinearLayoutManager(activity)
            mLayoutManager.orientation = RecyclerView.HORIZONTAL
            recyclerview.layoutManager =mLayoutManager
            mAdapter = GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter

            imgvSearch.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                startActivity(Search2Aty::class.java)
            }

            imgvPost.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                startActivity(PostSendAty::class.java)
            }

            linlay01.setOnClickListener {
//                if (!MyUtils3.isFastClick()){
//                    return@setOnClickListener
//                }
                viewPager.setCurrentItem(0,true)
            }
            linlay02.setOnClickListener {
//                if (!MyUtils3.isFastClick()){
//                    return@setOnClickListener
//                }
                viewPager.setCurrentItem(1,true)

            }
            linlay03.setOnClickListener {
//                if (!MyUtils3.isFastClick()){
//                    return@setOnClickListener
//                }
                viewPager.setCurrentItem(2,true)
            }


            relayToTop.setOnClickListener {
                val curClickTime = System.currentTimeMillis()
                if (curClickTime - lastClickTime < 500) {
                    refreshDataToTop()
                }else{
                    lastClickTime = curClickTime
                }
                return@setOnClickListener
            }

            list_frg.add(HomeListFrg.create("mine_recommend"))
            list_frg.add(HomeListFrg.create("mine_addention"))
            list_frg.add(HomeListFrg.create("mine_new"))

            viewPager.adapter = HomeListAdataper(childFragmentManager, this@HomeFrg.lifecycle, list_frg)

            viewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                   when(position){
                       0 -> {
                           setSelector(tv01)
                       }
                       1 -> {
                           setSelector(tv02)
                       }
                       2 -> {
                           setSelector(tv03)
                       }
                   }
                }
            })

            viewPager.setCurrentItem(0,false)
        }
    }

    fun refreshData() {
        var index = mBinding.viewPager.currentItem
        var frg = list_frg[index]
        frg.refreshData()
    }

    fun refreshDataToTop() {
        var index = mBinding.viewPager.currentItem
        var frg = list_frg[index]
        frg.refreshDataToTop()
    }

    private fun setSelector(tv: TextView) {
        for (i in list_tv.indices) {
            if (tv === list_tv[i]) {
                list_tv[i].setTextColor(Color.parseColor("#333438"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗


                list_tv[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentWidthSizeBigger(55).toFloat())
                list_v[i].visibility = View.VISIBLE
            } else {
                list_tv[i].setTextColor(Color.parseColor("#65666D"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));//加粗
                list_tv[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentWidthSizeBigger(45).toFloat())
                list_v[i].visibility = View.INVISIBLE
            }
        }
    }

    inner class HomeListAdataper(fa: FragmentManager, lifecycle: Lifecycle, val list: ArrayList<HomeListFrg>) :

        FragmentStateAdapter(fa, lifecycle) {

        override fun getItemCount(): Int =list.size

        override fun createFragment(position: Int): Fragment = list[position]

    }

    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(ItemFrgHome01Binding.inflate(LayoutInflater.from(context)))
        }

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder){
                    var maxW_head = AutoUtils.getPercentHeightSizeBigger(300)

                    ImageLoader.loadImageAes(activity,list[position]["avatar"],mBinding.ivHead,maxW_head,maxW_head)
                    itemView.setOnClickListener {
                        if (!MyUtils3.isFastClick()){
                            return@setOnClickListener
                        }
//                        var bundle = Bundle()
//                        bundle.putString("user_id",list[position]["code"])
//                        startActivity(PersonOtherDetailsAty::class.java,bundle)
                        var bundle = Bundle()
                        if (list[position]["code"]==info_code){
                            bundle.putString("typePage","my")
                            startActivity(PersonDetailsAty::class.java,bundle)
                        }else{
                            bundle.putString("user_id",list[position]["code"])
                            startActivity(PersonOtherDetailsAty::class.java,bundle)
                        }
                    }
                }
            }
        }

        inner class fGoldViewHolder(binding: ItemFrgHome01Binding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFrgHome01Binding = binding
            init {
                AutoUtils.autoSize(binding.root)
            }
        }


    }

}