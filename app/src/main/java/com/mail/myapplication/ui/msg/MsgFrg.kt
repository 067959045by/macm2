package com.mail.myapplication.ui.msg

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lzy.okgo.utils.HttpUtils.runOnUiThread
import com.mail.comm.app.AppConfig
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils2
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.utils.TimeUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.MyApp
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.MainAty
import com.mail.myapplication.ui.MainWalletAty
import com.mail.myapplication.ui.dg.RechargeDgFrg
import com.mail.myapplication.ui.mine.BlackListAty
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.msg.database.ChatUtils
import com.zhy.autolayout.utils.AutoUtils
import com.zhy.base.cache.disk.DiskLruCacheHelper
import io.agora.rtm.ErrorInfo
import io.agora.rtm.ResultCallback
import io.agora.rtm.RtmClient
import io.agora.rtm.RtmStatusCode
import org.xutils.common.util.LogUtil
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MsgFrg : BaseXFrg(), View.OnClickListener {

    lateinit var mBinding: FrgMsgBinding
    lateinit var mAdapter: GoldRecyclerAdapter
    var home = Home()
    var info_code = ""
    var resource_cdn =""
    var service_info =""

    var map_service_info:MutableMap<String,String>? = null
    var map_kefu_msg:MutableMap<String,String>? = null
    var map_click= HashMap<String,String>()

    var list = ArrayList<MutableMap<String,String>>()
    var rechargeDg2:RechargeDgFrg?=null

    override fun getLayoutView(): View {
        mBinding = FrgMsgBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun getLayoutId(): Int = 0

    override fun initView() {
        resource_cdn = PreferencesUtils.getString(activity,"resource_cdn")
        info_code = PreferencesUtils.getString(activity,"info_code")
        service_info = PreferencesUtils.getString(activity,"service_info")
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        home?.a44(this)
//      getChatPannel()
    }

    override fun onResume() {
        super.onResume()
        getChatPannel()
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "message/history/list"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)

            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList2 = JSONUtils.parseKeyAndValueToMapList(str)

                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)

                Thread{
                    var mList = ChatUtils.getAllPannel(info_code)

                    if (mList!=null&&mList.size>0){

                    }else{
                        for (map in mList2 ){
                            ChatUtils.saveChatpannel(map["payload"]!!)
                        }
                    }
                    getChatPannel()
                }.start()


            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }
        }

        if (type == "message/auth") {
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data = JSONUtils.parseKeyAndValueToMap(str)

                if (map_data["status"] != "0") {
//                    showToastS(map_data["message"])
                }
                when (map_data["status"]) {

                    "0" -> {
                        var map_dst_info = JSONUtils.parseKeyAndValueToMap(map_data["dst_info"])
                        var bundle = Bundle()
                        bundle.putString("code", map_click["code"])
                        bundle.putString("nick", map_dst_info!!["nick"]!!)
                        bundle.putString("avatar", map_dst_info!!["avatar"]!!)
                        startActivity(ChatAty::class.java, bundle)
                    }

                    "1"->{

                        if (rechargeDg2 == null) {
                            rechargeDg2 = RechargeDgFrg(activity as BaseAty)
                            rechargeDg2?.setRechargeListen(object : RechargeDgFrg.RechargeListen{
                                override fun onclik01() {
                                    rechargeDg2?.dismiss()
                                }

                                override fun onclik02() {
                                    rechargeDg2?.dismiss()
                                    var bundle = Bundle()
                                    bundle.putString("page_type","pay")
                                    startActivity(MainWalletAty::class.java,bundle)
                                }

                            })
                        }
                        rechargeDg2?.show()
                        rechargeDg2?.setData(map_data["message"]!!,"2")
                    }

                    "2"->{
                        startActivity(BlackListAty::class.java)
                        showToastS(map_data["message"])
                    }

                    "3"->{
                        showToastS(map_data["message"])

                    }

                    "4"->{
                        showToastS(map_data["message"])
                    }

                }

            } else {
                showToastS(map["message"])
            }
        }


    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (list.size==0){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        with(mBinding) {
            linlayZan.setOnClickListener(this@MsgFrg)
            linlayComment.setOnClickListener(this@MsgFrg)
            linlayFans.setOnClickListener(this@MsgFrg)
            relayNotify.setOnClickListener(this@MsgFrg)
            relayKefu.setOnClickListener(this@MsgFrg)

            var mLayoutManager = GridLayoutManager(activity, 1)
            mLayoutManager.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager = mLayoutManager
            mAdapter = GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter

            recyclerview?.isNestedScrollingEnabled = false

            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    getChatPannel()
//                    requestData()
                }

                override fun loadMoreStart() {
                }

            })

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }
            })

             map_service_info = JSONUtils.parseKeyAndValueToMap(service_info)
            tvKefu.text = map_service_info!!["nick"]

            var maxW = AutoUtils.getPercentWidthSizeBigger(300)
            ImageLoader.loadImageAes(context = null,map_service_info!!["avatar"],imgv,maxW,maxW)

        }
    }

    var handler :Handler=  @SuppressLint("HandlerLeak") object :Handler(){

        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            when(msg.what){
                1->{
                    mAdapter.notifyDataSetChanged()
                    mBinding.swipeRefreshLayout.finishLoadmore()
                    mBinding.swipeRefreshLayout.finishRefreshing()

                    if (map_kefu_msg!=null){
                        LogUtil.e("map_kefu_msg"+map_kefu_msg.toString())
                        var isReadNum = map_kefu_msg!!["unread_num"]
                        if (TextUtils.isEmpty(isReadNum)||isReadNum == "0"){
                            mBinding.tvNumKefu.visibility = View.GONE
                        }else{
                            mBinding.tvNumKefu.visibility = View.VISIBLE
                            mBinding.tvNumKefu.text = isReadNum
                        }
                        var time = (TimeUtils.getBeijinTime().toDouble() - map_kefu_msg!!["time"]!!.toDouble())/1000
                        mBinding.tvTimeKefu.text = TimeUtils.formatSecond3(time)

                        var  msg = AESCBCCrypt.aesDecrypt(map_kefu_msg!!["msg"])
                        var  map = JSONUtils.parseKeyAndValueToMap(msg)
                        when(map!!["type"]){

                            "1"->{
                                mBinding.tvMsgKefu.text = map!!["text"]
                            }

                            "2"->{
                                mBinding.tvMsgKefu.text = "[图片]"
                            }
                        }
                    }

                    if (list==null||list.size==0){
                        return
                    }
                    var allUnReadNum = 0
                    for (map in list){
                        var num = map["unread_num"]
                        if (!TextUtils.isEmpty(num)&&num!="0"){
                            var numInt = num!!.toInt()
                            allUnReadNum += numInt
                        }
                    }
                    var mainAty = activity as MainAty
                    mainAty.setUnReadNum(allUnReadNum.toString())
                }
                2->{
                    mBinding.swipeRefreshLayout.finishLoadmore()
                    mBinding.swipeRefreshLayout.finishRefreshing()
                }
            }
        }
    }

    fun  getChatPannel(){

        Thread{
            var mList = ChatUtils.getAllPannel(info_code)

            for (map in mList){
                if (map["user_code"]==map_service_info!!["code"]||map["to_user_code"]==map_service_info!!["code"]){
                    map_kefu_msg = map
                    mList.remove(map)
                    break
                }
            }
            if (mList!=null&&mList.size>0){

                Collections.sort(mList,object : Comparator< MutableMap<String,String>>{

                    override fun compare(p0: MutableMap<String, String>?, p1: MutableMap<String, String>?): Int {

                        try {
                            var time01 = p0!!["time"]!!.toLong()
                            var time02 = p1!!["time"]!!.toLong()
                            if (time01>time02){
                                return -1
                            }else{
                                return 1
                            }
                        }catch (e:Exception){
                            return 1
                        }

                    }

                })

                this.list.clear()
                this.list.addAll(mList)
            }
            handler.sendEmptyMessage(1)

        }.start()
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.linlay_zan -> {
                startActivity(ZanListAty::class.java)
            }

            R.id.linlay_zan -> {
                startActivity(ZanListAty::class.java)
            }

            R.id.linlay_comment -> {
                startActivity(CommentListAty::class.java)
            }

            R.id.linlay_fans -> {
                var bundle = Bundle()
                bundle.putString("type","fans")
                startActivity(FansAty::class.java,bundle)
            }

            R.id.relay_notify -> {
                startActivity(NotifyListAty::class.java)
            }

            R.id.relay_kefu ->{

                var num = kefuuUreadNum()

                if (num == "0"){

                    var bundle = Bundle()
                    bundle.putString("code",map_service_info!!["code"])
                    bundle.putString("nick",map_service_info!!["nick"])
                    bundle.putString("avatar",map_service_info!!["avatar"])
                    startActivity(CustomerServiceAty::class.java,bundle)

                }else{

                    var bundle = Bundle()
                    bundle.putString("code",map_service_info!!["code"])
                    bundle.putString("nick",map_service_info!!["nick"])
                    bundle.putString("avatar",map_service_info!!["avatar"])
                    startActivity(ChatAty::class.java,bundle)
                }

            }
        }
    }

    fun kefuuUreadNum(): String {
        var unread_num = "0"
        if (map_kefu_msg != null) {
            if (map_kefu_msg!!.containsKey("unread_num"))
                unread_num = map_kefu_msg!!["unread_num"]!!
        }
        return unread_num
    }


    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            return fGoldViewHolder(ItemFrgMsgBinding.inflate(LayoutInflater.from(activity)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        var  msg = AESCBCCrypt.aesDecrypt(list[position]["msg"])

                        LogUtil.e("AESCBCCrypt chaty=="+msg)
                        var  map = JSONUtils.parseKeyAndValueToMap(msg)
                        var  map_form_user = JSONUtils.parseKeyAndValueToMap(map["from_user"])
                        var  map_to_user = JSONUtils.parseKeyAndValueToMap(map["to_user"])

                        var nick =""
                        var avatar =""
                        var code = ""

                        if (info_code == map_form_user["user_code"]){
                            avatar = map_to_user["avatar"]!!
                            nick = map_to_user["nick"]!!
                            code = map_to_user["user_code"]!!
                        }else{
                            avatar = map_form_user["avatar"]!!
                            nick = map_form_user["nick"]!!
                            code = map_form_user["user_code"]!!
                        }


                        var maxW_head = AutoUtils.getPercentWidthSizeBigger(200)

                        ImageLoader.loadImageAes(activity,avatar,ivHead,maxW_head,maxW_head)

                        try {
                            var time = (TimeUtils.getBeijinTime().toDouble() - map["time"]!!.toDouble())/1000
                            tvTime.text = TimeUtils.formatSecond3(time)
                        }catch (e:Exception){
                        }

                        tvNick.text = nick
                        when(map["type"]){

                            "1"->{
                                tvMsg.text = map["text"]
                            }

                            "2"->{
                                tvMsg.text = "[图片]"
                            }
                        }

                        var isReadNum = list[position]["unread_num"]
                        if (TextUtils.isEmpty(isReadNum)||isReadNum == "0"){
                            tvNum.visibility = View.GONE
                        }else{
                            tvNum.visibility = View.VISIBLE
                            tvNum.text = isReadNum
                        }

                        framlayHead.setOnClickListener {
                            var codePage = ""
                            if (info_code == map_form_user["user_code"]){
                                codePage = map_to_user["user_code"]!!
                            }else{
                                codePage =map_form_user["user_code"]!!
                            }
                            var bundle = Bundle()
                            bundle.putString("user_id",codePage)
                            startActivity(PersonOtherDetailsAty::class.java,bundle)
                        }

                        itemView.setOnClickListener {
                            map_click.clear()
                            map_click["code"] = code
                            map_click["nick"] = nick
                            map_click["avatar"] = avatar
                            startProgressDialog()
                            home.a47(code, this@MsgFrg)


//                            var bundle = Bundle()
//                            bundle.putString("code",code)
//                            bundle.putString("nick",nick)
//                            bundle.putString("avatar",avatar)
//                            startActivity(ChatAty::class.java,bundle)

                        }
                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemFrgMsgBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFrgMsgBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}