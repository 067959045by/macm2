package com.mail.myapplication.ui.mine

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.zhy.autolayout.utils.AutoUtils

class PostSearchTopicAty : BaseXAty() {

    lateinit var mBinding: AtyPostSearchTopicBinding
    lateinit var mAdapter: GoldRecyclerAdapter
    lateinit var mAdapter2: GoldRecyclerAdapter2
    var home = Home()
    var page = 1
    var page_search = 1
    var list = ArrayList<MutableMap<String, String>>()
    var list_search = ArrayList<MutableMap<String, String>>()
    var lar = Lar()

    override fun getLayoutId(): Int = 0

    override fun initView() {
//      type = intent.getStringExtra("type").toString()
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData2()
    }

    fun requestDataSearch() {
        mBinding.loading2.setLoadingTip(XLoadTip.LoadStatus.finish)
        requestDataSearch2()
    }

    fun requestData2() {
//        lar.b13(page, this)
        home.a18("",this,page)

    }

    fun requestDataSearch2() {
//        home.a40(page_search, mBinding.editSearch.text.toString(),this)
        home.a182(mBinding.editSearch.text.toString(),this,page_search)

    }

    override fun getLayoutView(): View {
        mBinding = AtyPostSearchTopicBinding.inflate(layoutInflater);
        return mBinding.root
    }

    fun initLayout2() {

        with(mBinding) {

            var mLayoutManager = GridLayoutManager(this@PostSearchTopicAty, 1)
            mLayoutManager.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager = mLayoutManager
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout2.setEnableLoadmore(true)
            swipeRefreshLayout2.setEnableRefresh(true)
            swipeRefreshLayout2.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page_search = 1
                    requestDataSearch2()
                }

                override fun loadMoreStart() {
                    page_search++
                    requestDataSearch2()
                }

            })

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding) {

            var mLayoutManager = GridLayoutManager(this@PostSearchTopicAty, 1)
            mLayoutManager.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager = mLayoutManager
            mAdapter = GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page = 1
                    requestData2()
                }

                override fun loadMoreStart() {
                    page++
                    requestData2()
                }

            })

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    requestData()
                }

            })

            loading2.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    requestDataSearch()
                }

            })

            editSearch.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    if (TextUtils.isEmpty(editSearch.text.toString())){
                        relay01.visibility = View.VISIBLE
                        relay02.visibility = View.GONE
                    }
                }

            })
        }

        initLayout2()
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_search -> {

                if (TextUtils.isEmpty(mBinding.editSearch.text.toString())) {
                    showToastS("請輸入搜索內容")
                    return
                }

                requestDataSearch()
                mBinding.relay01.visibility = View.GONE
                mBinding.relay02.visibility = View.VISIBLE
//                finish()
            }
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "topic/search/list") {

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout2.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.swipeRefreshLayout2.finishLoadmore()
            mBinding.loading2.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                if (page_search == 1) {
                    list_search.clear()
                    list_search.addAll(mList)

                } else {
                    list_search.addAll(mList)
                }

                if (page_search == 1 && mList.size == 0) {

                    mBinding.loading2.setLoadingTip(XLoadTip.LoadStatus.empty)
                } else {
                    mAdapter2?.notifyDataSetChanged()
                }
            } else {
                if (page_search == 1) {
                    mBinding.loading2.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }

        if (type == "topic/list") {

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout2.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.swipeRefreshLayout2.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                if (page == 1) {
                    list.clear()
                    list.addAll(mList)

                } else {
                    list.addAll(mList)
                }

                if (page == 1 && mList.size == 0) {

                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                } else {
                    mAdapter?.notifyDataSetChanged()
                }
            } else {
                if (page == 1) {
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type == "topic/list") {
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.swipeRefreshLayout2.finishRefreshing()
            mBinding.swipeRefreshLayout2.finishLoadmore()
            if (page == 1 && list.size == 0) {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            } else {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
        if (type == "topic/search/list") {

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.swipeRefreshLayout2.finishRefreshing()
            mBinding.swipeRefreshLayout2.finishLoadmore()
            if (page_search == 1 && list_search.size == 0) {
                mBinding.loading2.setLoadingTip(XLoadTip.LoadStatus.error)
            } else {
                mBinding.loading2.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
    }

    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemPostSendPostBinding.inflate(LayoutInflater.from(this@PostSearchTopicAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {

                    with(mBinding) {

                        var map = list[position]

                        tvName.text = "#"+list[position]["title"]

                        relayTop.setOnClickListener {
//                            setEditextQ(list_post[position]["title"]!!,"#")
//                            mapTopic[map["title"]!!] = map["id"]!!

                            var list2 = ArrayList<String>()
                            list2.add(list!![position]["title"]!!)
                            list2.add(list!![position]["id"]!!)
                            var intent = Intent()
                            intent.putExtra("data",list2)
                            setResult(RESULT_OK,intent)
                            finish()
                        }



                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemPostSendPostBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemPostSendPostBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }

    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemPostSendPostBinding.inflate(LayoutInflater.from(this@PostSearchTopicAty)))
        }

        override fun getItemCount(): Int = list_search.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {

                    with(mBinding) {
                        var map = list_search[position]
                        tvName.text = "#"+list_search[position]["title"]
                        relayTop.setOnClickListener {
//                            setEditextQ(list_post[position]["title"]!!,"#")
//                            mapTopic[map["title"]!!] = map["id"]!!
                            var list2 = ArrayList<String>()
                            list2.add(list_search!![position]["title"]!!)
                            list2.add(list_search!![position]["id"]!!)
                            var intent = Intent()
                            intent.putExtra("data",list2)
                            setResult(RESULT_OK,intent)
                            finish()
                        }
                    }

                }


            }

        }

        inner class fGoldViewHolder(binding: ItemPostSendPostBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemPostSendPostBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}