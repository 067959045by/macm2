package com.mail.myapplication.ui.find

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import android.view.animation.LinearInterpolator
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.databinding.AtyMatchBinding
import com.mail.myapplication.ui.msg.ImageUtil

class MatchAty:BaseXAty() {

    lateinit var mBinding: AtyMatchBinding
    var objectAnimator: ObjectAnimator? = null

    override fun getLayoutId(): Int =0

    override fun initView() {}

    override fun getLayoutView(): View {
        mBinding = AtyMatchBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun requestData() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusColor("#000000")
        setAndroidNativeLightStatusBar(false)
        mBinding.ivShowPic.setClipToOutline(true)
        mBinding.ivShowPic.setOutlineProvider(ImageUtil.getOutline(true, 20, 1))

        objectAnimator = ObjectAnimator.ofFloat( mBinding.ivShowPic, "rotation", 0f, 360f)
        objectAnimator?.setDuration((20 * 1000).toLong())
        objectAnimator?.setRepeatMode(ValueAnimator.RESTART)
        objectAnimator?.setInterpolator(LinearInterpolator())
        objectAnimator?.setRepeatCount(-1)
        objectAnimator?.start()
    }

}