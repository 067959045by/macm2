package com.mail.myapplication.ui.home

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.mail.comm.base.XBaseAdaper
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.TimeUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.MainWalletAty
import com.mail.myapplication.ui.dg.DeletePostDialog
import com.mail.myapplication.ui.dg.RechargeDgFrg
import com.mail.myapplication.ui.dg.ReportDgFrg
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.mine.PostSearchPerAty
import com.mail.myapplication.ui.msg.BigImgListAty
import com.mail.myapplication.ui.utils.ClickListener
import com.mail.myapplication.ui.utils.MyUtils3
import com.mail.myapplication.ui.video.PlayListAty
import com.mail.myapplication.ui.video.PlayListVieoAty
import com.mail.myapplication.ui.wallet.RechargeAty
import com.mail.myapplication.ui.wallet.ShareAty
import com.zhy.autolayout.config.AutoLayoutConifg
import com.zhy.autolayout.utils.AutoUtils
import kotlinx.coroutines.CoroutineScope

class PostDetailsAty : BaseXAty() {

    lateinit var mBinding: AtyPostDetailsBinding
    lateinit var mAdapter: GoldRecyclerAdapter
    lateinit var mAdapter2: GoldRecyclerAdapter2

    var data = ""
    var page = 1
    var id = ""
    var avatar = ""
    var info_code = ""
    var parent_id = "0"
    var home = Home()
    var lar = Lar()
    var index_zan = -1
    var map: MutableMap<String, String>? = null
    var map_user: MutableMap<String, String>? = null
    var list = ArrayList<MutableMap<String, String>>()
    var mapAttenSearchCheck = HashMap<String, String>()
    var reportDgFrg:ReportDgFrg?=null
    var deletePostFrg: DeletePostDialog?=null
    var click_details_index = -1
    var rechargeDg:RechargeDgFrg?=null
    var rechargeDg2:RechargeDgFrg?=null
    override fun getLayoutId(): Int = 0

    override fun initView() {
        id = intent.getStringExtra("id").toString()
        if (intent.hasExtra("avatar")){
            avatar = intent.getStringExtra("avatar").toString()

            var maxW = AutoUtils.getPercentWidthSizeBigger(200)

            ImageLoader.loadImageAes(this@PostDetailsAty, avatar, mBinding.ivHead,maxW,maxW)
        }

        if (intent.hasExtra("name")) {
            var nickname = intent.getStringExtra("name").toString()
            mBinding.tvName.text = nickname
        }

        info_code = MyUtils.getInfoDetails(this,"info_code")
//        setEnableOverAnim(true)
//        initOverAnim(arrayOf(mBinding.framlayHead,mBinding.tvName), arrayOf("details_head","details_name"))
    }



   override fun requestData() {
        mBinding.loading.setBackgrouneColorsX(resources.getString(R.string.main_02))
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData3()
    }

    fun requestData2() {
        home.a13(page, id, this)
    }

    //鉴权
    fun requestData3() {
        home.a10(id,this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyPostDetailsBinding.inflate(layoutInflater);
        return mBinding.root
    }

    fun initRecyclerviewPost() {
        with(mBinding) {
            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page = 1
                    requestData2()
                }

                override fun loadMoreStart() {
                    page++
                    requestData2()
                }

            })

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    requestData()
                }
            })
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusColor(resources.getString(R.string.main_02))
        initRecyclerviewPost()

        with(mBinding) {
            initTopview2(include.relayTopBg, resources.getString(R.string.main_02))
            include.tvTitle.text = "帖子詳情"

            var mLayoutManager = GridLayoutManager(this@PostDetailsAty, 1)
            mLayoutManager.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager = mLayoutManager
            mAdapter2 = GoldRecyclerAdapter2(this@PostDetailsAty)
            recyclerview2.adapter = mAdapter2
            recyclerview2.isNestedScrollingEnabled = false

            editChat.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    MyUtils3.showInput(editChat, this@PostDetailsAty)
                } else {
                    MyUtils3.hideInput(this@PostDetailsAty)
                }
            }

            editChat.addTextChangedListener(object : TextWatcher {

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    if (TextUtils.isEmpty(editChat.text.toString())) {
                        parent_id="0"
                        editChat.clearFocus();
                    }
                }
            })

            editChat.clearFocus();
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type!!.startsWith("post/buy")) {
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                rechargeDg2?.dismiss()
                rechargeDg?.dismiss()
            }else{
                showToastS(map["message"])
            }
        }

        if (type!!.startsWith("post/details") ){
            var maps = JSONUtils.parseKeyAndValueToMap(var2)
//            showToastS(maps["message"])

            if (maps["code"]=="1004"){
                showToastS(maps["message"])
                finish()
                return
            }
            if (maps["code"] == "200") {
                requestData2()
//                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                data = AESCBCCrypt.aesDecrypt(maps["data"])
                map =JSONUtils.parseKeyAndValueToMap(data)
                var map_data_auth_error = JSONUtils.parseKeyAndValueToMap(map!!["auth_error"])
                initData()
                if (map_data_auth_error != null){
                    when(map_data_auth_error["key"]){
                        //错误码 1001=次数已用完，开通VIP享不限次观,1002=金币帖子查看完整版需支付金币,1003=粉丝帖子仅限粉丝观看
                        "1001"->{
                            if (rechargeDg2 == null) {
                                rechargeDg2 = RechargeDgFrg(this)
                                rechargeDg2?.setRechargeListen(object : RechargeDgFrg.RechargeListen{
                                    override fun onclik01() {
                                        rechargeDg2?.dismiss()
                                        this@PostDetailsAty.finish()
                                    }

                                    override fun onclik02() {
                                        rechargeDg2?.dismiss()
                                        var bundle = Bundle()
                                        bundle.putString("page_type","pay")
                                        startActivity(MainWalletAty::class.java,bundle)
                                    }

                                })
                            }
                            rechargeDg2?.show()
                            rechargeDg2?.setData(map_data_auth_error!!["info"]!!,"2")
                            rechargeDg2?.setNoEnableCancel()
                        }
                        //1002=金币帖子查看完整版需支付金币
                        "1002" -> {
                            if (rechargeDg == null) {
                                rechargeDg = RechargeDgFrg(this)
                                rechargeDg?.setRechargeListen(object : RechargeDgFrg.RechargeListen{
                                    override fun onclik01() {
                                        rechargeDg?.dismiss()
                                        startActivity(RechargeAty::class.java)
                                    }

                                    override fun onclik02() {
                                        rechargeDg?.dismiss()
                                        startProgressDialog()
                                        lar.b12(this@PostDetailsAty.map!!["id"]!!,this@PostDetailsAty)
                                    }

                                })
                            }
                            rechargeDg?.show()
                            rechargeDg?.setData(map_data_auth_error!!["info"]!!,"1")
//                            rechargeDg?.setNoEnableCancel()
                        }
                        else->{

                        }
                    }
                }

            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                showToastS(maps["message"])
            }
        }


        if (type == "post/block"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                showToastS("已屏蔽")
                finish()
            }else{
                showToastS(map["message"])
            }
        }

        if (type == "post/delete"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                finish()
//                mAdapter2.notifyItemRemoved(index_delete)
//                mAdapter2.notifyItemRangeChanged(index_delete,mAdapter2.itemCount) //刷新被删除数据，以及其后面的数据
                showToastS("删除成功")
                var intent = Intent()
                intent.putExtra("delete_index",click_details_index)
                setResult(RESULT_OK,intent)
                finish()
            }else{
                showToastS(map["message"])
            }

        }

        if (type == "post/report"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                showToastS("举报成功")
            }else{
                showToastS(map["message"])
            }
        }

        if (type!!.startsWith("attention")){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data =JSONUtils.parseKeyAndValueToMap(str)
                if (map_data["status"]=="1"){
//                    showToastS(resources.getString(String.p))
                    mBinding.imgvAtten.setImageResource(R.mipmap.ic_66)
                }else{
                    mBinding.imgvAtten.setImageResource(R.mipmap.ic_10)
                    showToastS("取消关注")
                }

            }else{
                showToastS(map["message"])
            }
        }

        if (type=="post/zan"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {

                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data =JSONUtils.parseKeyAndValueToMap(str)

                var total_zan_num =mBinding.tvZanNum.text.toString().toInt()
                if (map_data["status"]=="1"){
                    mBinding.imgvZan.setImageResource(R.mipmap.ic_74)
                    total_zan_num += 1
                }else{
                    mBinding.imgvZan.setImageResource(R.mipmap.ic_12)
                    total_zan_num -= 1
                    if (total_zan_num<0){
                        total_zan_num = 0
                    }
                }
                mBinding.tvZanNum.text = total_zan_num.toString()

            }else{
                showToastS(map["message"])
            }
        }

        if (type == "comment/zan"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                if (index_zan == -1)return
                var index =index_zan
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data =JSONUtils.parseKeyAndValueToMap(str)
                var mapList = list[index]
                mapList["is_like"] = map_data["id"]!!
                var total_zan_num = mapList["total_support"]!!.toInt()
                if (map_data["id"]=="1"){
                    total_zan_num += 1
                }else{
                    total_zan_num -= 1
                    if (total_zan_num<0){
                        total_zan_num = 0
                    }
                }
                mapList["total_support"]= (total_zan_num).toString()
                list[index]=mapList
                mAdapter2.notifyItemChanged(index)
                index_zan = -1

            }else{
                showToastS(map["message"])
            }
        }

        if (type == "comment/write"){
            stopProgressDialog()
            mBinding.editChat.hint = "有爱评论，说点好听的～"
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                page = 1
                requestData2()
                mBinding.editChat.setText("")
            }else{
                showToastS(map["message"])

            }
        }

        if (type == "comment/list") {
            with(mBinding) {
                swipeRefreshLayout.finishRefreshing()
                swipeRefreshLayout.finishLoadmore()
                loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                var map = JSONUtils.parseKeyAndValueToMap(var2)
                if (map["code"] == "200") {
                    var str = AESCBCCrypt.aesDecrypt(map["data"])
                    tvTotal.text = map["total"] + "条评论"
                    var mList = JSONUtils.parseKeyAndValueToMapList(str)
                    if (page == 1) {
                        list.clear()
                        list.addAll(mList)
                    } else {
                        list.addAll(mList)
                    }

                    if (page == 1 && mList.size == 0) {
                    } else {
                        mAdapter2?.notifyDataSetChanged()
                    }
                } else {
                    if (page == 1) {
                        loading.setLoadingTip(XLoadTip.LoadStatus.error)
                    }
                }
            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        startPostponedEnterTransition()
        if (type == "comment/list") {
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            if (page == 1 && list.size == 0) {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            } else {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
    }


    fun initData() {
        with(mBinding) {
             map_user = JSONUtils.parseKeyAndValueToMap(map!!["user"])
            var list_img_video = JSONUtils.parseKeyAndValueToMapList(map!!["img_data"])
            var info_code = MyUtils.getInfoDetails(this@PostDetailsAty,"info_code")

            var maxW = AutoUtils.getPercentWidthSizeBigger(200)
            ImageLoader.loadImageAes(this@PostDetailsAty, map_user!!["avatar"], ivHead,maxW,maxW)
            tvName.text = map_user!!["nick"]
            if (map_user!!["is_follow"] == "1") {
                imgvAtten.setImageResource(R.mipmap.ic_66)
            } else {
                imgvAtten.setImageResource(R.mipmap.ic_10)
            }

            if (map_user!!["code"] == info_code) {
                imgvAtten.visibility = View.GONE
            }else{
                imgvAtten.visibility = View.VISIBLE
            }

            if (map_user!!["is_follow"] == "1") {
                imgvAtten.setImageResource(R.mipmap.ic_66)
            } else {
                imgvAtten.setImageResource(R.mipmap.ic_10)
            }

//            when (map_user!!["vip_level"]) {
//                "0" -> {
//                    mBinding.imgvVipLevel.visibility = View.GONE
//                }
//
//                "1" -> {
//                    mBinding.imgvVipLevel.visibility = View.VISIBLE
//                    mBinding.imgvVipLevel.setImageResource(R.mipmap.ic_71)
//
//                }
//                else -> {
//                    mBinding.imgvVipLevel.visibility = View.VISIBLE
//                    mBinding.imgvVipLevel.setImageResource(R.mipmap.ic_69)
//                }
//
//            }

            MyUtils3.setVipLevel(map_user!!["vip_level"],mBinding.imgvVipLevel,0)

            when (map_user!!["gender"]) {
                "1" -> {
                    mBinding.imgvGender.visibility = View.VISIBLE
                    mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
                }
                "0" -> {
                    mBinding.imgvGender.visibility = View.VISIBLE
                    mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
                }
                "10" -> {
                    mBinding.imgvGender.visibility = View.VISIBLE
                    mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
                }
                else -> {
                    mBinding.imgvGender.visibility = View.GONE
                }
            }

            if (map_user!!["is_creator"] == "1") {
                mBinding.imgvCreator.visibility = View.VISIBLE
            } else {
                mBinding.imgvCreator.visibility = View.GONE
            }


            if (map!!["is_like"] == "1") {
                imgvZan.setImageResource(R.mipmap.ic_74)
            } else {
                imgvZan.setImageResource(R.mipmap.ic_12)
            }

            relayReport.setOnClickListener {
                if (map_user!!["code"]==info_code){

                    if (deletePostFrg == null){
                        deletePostFrg = DeletePostDialog(this@PostDetailsAty)

                    }
                    deletePostFrg?.setRechargeListen(object : DeletePostDialog.RechargeListen{
                        override fun onclik01() {
                            deletePostFrg?.dismiss()
                            startProgressDialog()
                            home.a31(map!!["id"]!!,this@PostDetailsAty)
                        }

                    })
                    deletePostFrg?.show()
                    return@setOnClickListener
                }
                if (reportDgFrg==null){
                    reportDgFrg = ReportDgFrg(this@PostDetailsAty)
                }
                reportDgFrg?.setRechargeListen(object : ReportDgFrg.RechargeListen{
                    override fun onclik02(type: String, id: String) {
                        if (type =="1"){
                            startProgressDialog()
                            home.a27(map!!["id"]!!,this@PostDetailsAty)
                        }else{
                            startProgressDialog()
                            home.a28("1",map_user!!["code"]!!,this@PostDetailsAty)
                        }
                    }

                })
                reportDgFrg?.show()
                reportDgFrg?.setData("")
            }


            if (map!!["content"]!!.contains("[")) {
                var listContent = JSONUtils.parseKeyAndValueToMapList(map!!["content"])
                var str_list = ArrayList<String>();
                var color_list = ArrayList<Int>()
                var text_size_list = ArrayList<Float>()
                for (i in listContent.indices) {

                    var sp = ""
                    if (i!=0){
                        sp = "  "
                    }

                    text_size_list.add(AutoUtils.getPercentHeightSize(40).toFloat())

                    when (listContent[i]["type"]) {

                        "" -> {
                            color_list.add(Color.parseColor(resources.getString(R.string.main_04)))
                            str_list.add(sp+""+listContent[i]["content"]!!)
                        }

                        "code" -> {
                            color_list.add(Color.parseColor(resources.getString(R.string.main_05)))
                            str_list.add(sp+"@" + listContent[i]["content"]!!)
                        }

                        "topic" -> {
                            color_list.add(Color.parseColor(resources.getString(R.string.main_03)))
                            str_list.add(sp+"#" + listContent[i]["content"]!!)
                        }

                        "link" -> {
                            color_list.add(Color.YELLOW)
                            str_list.add(sp+listContent[i]["content"]!!)
                        }
                    }
                }

                MyUtils3.setText(this@PostDetailsAty, tvContent, str_list, color_list, text_size_list) { position->

                    when(listContent[position]["type"]){

                        "" ->{
//                            itemView.performClick()
                        }
                        "code"->{
                            var bundle = Bundle()
                            bundle.putString("user_id",listContent[position]["related"])
                            startActivity(PersonOtherDetailsAty::class.java,bundle)
                        }
                        "topic"->{
                            var bundle = Bundle()
                            bundle.putString("id",listContent[position]["related"])
                            bundle.putString("topic",listContent[position]["content"])
                            startActivity(HistoryActiveListAty::class.java,bundle)
                        }
                    }

                }

            } else {
                tvContent?.text = map!!["content"]
            }

            if (TextUtils.isEmpty(map!!["content"])) {
                tvContent.visibility = View.GONE
            } else {
                tvContent.visibility = View.VISIBLE
            }
            tvZanNum.text = map!!["total_like"]
            tvCommentNum.text = map!!["total_comment"]
            tvShareNum.text = map!!["total_share"]
            var time = (TimeUtils.getBeijinTime().toDouble() - TimeUtils.getTime(map!!["created_at"])
                    .toDouble()) / 1000
            tvTime.text = TimeUtils.formatSecond3(time)


            when (map!!["type"]) {
                "3", "4" -> {
                    recyclerview.visibility = View.VISIBLE
                    var mLayoutManager2 = GridLayoutManager(this@PostDetailsAty, 1)
                    mLayoutManager2.orientation = RecyclerView.VERTICAL
                    mBinding.recyclerview.layoutManager = mLayoutManager2
                    mBinding.recyclerview?.isNestedScrollingEnabled = false
                    mBinding.recyclerview.setItemViewCacheSize(200)
//                                        mBinding.recyclerview.setHasFixedSize(true)
                    var mAdapter = GoldRecyclerAdapter(list_img_video)
                    mBinding.recyclerview.adapter = mAdapter
                }
                "2" -> {
                    recyclerview.visibility = View.VISIBLE
                    when (list_img_video.size) {
                        1 -> {
                            var mAdapter: GoldRecyclerAdapter? = null
                            var mLayoutManager2 = GridLayoutManager(this@PostDetailsAty, 1)
                            mLayoutManager2.orientation = RecyclerView.VERTICAL
                            mBinding.recyclerview.layoutManager = mLayoutManager2
                            mBinding.recyclerview?.isNestedScrollingEnabled = false
                            mBinding.recyclerview.setItemViewCacheSize(200)
//                                        mBinding.recyclerview.setHasFixedSize(true)
                            mAdapter = GoldRecyclerAdapter(list_img_video)
                            mBinding.recyclerview.adapter = mAdapter
                        }
                        2, 4 -> {
                            var mAdapter: GoldRecyclerAdapter? = null
                            var mLayoutManager2 = GridLayoutManager(this@PostDetailsAty, 2)
                            mLayoutManager2.orientation = RecyclerView.VERTICAL
                            mBinding.recyclerview.layoutManager = mLayoutManager2
                            mBinding.recyclerview?.isNestedScrollingEnabled = false
                            mBinding.recyclerview.setItemViewCacheSize(200)
//                                        mBinding.recyclerview.setHasFixedSize(true)
                            mAdapter = GoldRecyclerAdapter(list_img_video)
                            mBinding.recyclerview.adapter = mAdapter
                        }
                        3, 5, 6, 7, 8, 9 -> {
                            var mAdapter: GoldRecyclerAdapter? = null
                            var mLayoutManager2 = GridLayoutManager(this@PostDetailsAty, 3)
                            mLayoutManager2.orientation = RecyclerView.VERTICAL
                            mBinding.recyclerview.layoutManager = mLayoutManager2
                            mBinding.recyclerview?.isNestedScrollingEnabled = false
                            mBinding.recyclerview.setItemViewCacheSize(200)
//                                      mBinding.recyclerview.setHasFixedSize(true)
                            mAdapter = GoldRecyclerAdapter(list_img_video)
                            mBinding.recyclerview.adapter = mAdapter
                        }
                    }
                }
                else -> {
                    recyclerview.visibility = View.GONE
                }
            }

            if (map!!["is_ad"] == "1"){
                var map_ad = JSONUtils.parseKeyAndValueToMap(map!!["ad"])
                linlayAd.visibility = View.VISIBLE

                var maxW_ad =AutoLayoutConifg.getInstance().screenWidth

                ImageLoader.loadImageAes(this@PostDetailsAty,map_ad["cover"],imgvAd,maxW_ad,0)

                var maxW = AutoUtils.getPercentWidthSizeBigger(200)

                ImageLoader.loadImageAes(this@PostDetailsAty,map_ad["product_logo"],imgvAdLog,maxW,maxW)
                tvAdName.text = map_ad["title"]
                tvAdNum.text = map_ad["view_num"]+"次下载"

                if (map_ad["type"] == "1"){
                    imgvAdVideo.visibility = View.GONE
                }else{
                    imgvAdVideo.visibility = View.VISIBLE
                }

                linlayAdLink.setOnClickListener {
                    MyUtils3.openTowPage(this@PostDetailsAty,map_ad["link"])
                }

                imgvAd.setOnClickListener {

                    if (map_ad["type"]=="1"){
                        var list = ArrayList<String>()
                        list.add(map_ad["cover"]!!)
                        var bundle = Bundle()
                        bundle.putStringArrayList("data",list)
                        startActivity(BigImgListAty::class.java,bundle)
                    }else{

                        var list = ArrayList<String>()
                        list.add(map_ad["play"]!!)
                        var bundle = Bundle()
                        bundle.putStringArrayList("data",list)
                        startActivity(PlayListVieoAty::class.java,bundle)
                    }

                }

            }else{
                linlayAd.visibility = View.GONE
            }

        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.framlay_head ->{

                var info_code = MyUtils.getInfoDetails(this,"info_code")
                var bundle = Bundle()
                if (map_user!!["code"]==info_code){
                    bundle.putString("typePage","my")
                    startActivity(PersonDetailsAty::class.java,bundle)
                }else{
                    bundle.putString("user_id",map_user!!["code"])
                    startActivity(PersonOtherDetailsAty::class.java,bundle)
                }
            }

            R.id.imgv_q ->{
                startActivityForResult(PostSearchPerAty::class.java,123)
            }

            R.id.relay_back -> {
                finish()
            }

            R.id.linlay_zan ->{
                startProgressDialog()
                home.a15(id,this)
            }

            R.id.imgv_atten ->{
                startProgressDialog()
                lar.b9(map_user!!["code"]!!,this)
            }

            R.id.linlay_share ->{
                var bundle = Bundle()
                bundle.putString("id",id)
                startActivity(ShareAty::class.java,bundle)
            }

            R.id.tv_send -> {

                if (TextUtils.isEmpty(mBinding.editChat.text.toString())) {
                    showToastS("评论不能为空！")
                    return
                }
                startProgressDialog()
                var list_post = ArrayList<MutableMap<String, String>>()

                var ss = mBinding.editChat.spDatas
                var strE = mBinding.editChat.text.toString()
                if (ss.size > 0) {
                    for (i in ss.indices) {
                        var cruS = ss[i].start
                        var cruE = ss[i].end
                        if (i == 0 && cruS != 0) {
                            var s01 = strE.substring(0, cruS)
                            var map = HashMap<String, String>()
                            map["content"] = s01
                            map["code"] = ""
                            list_post.add(map)
                        }
                        if (i != ss.size - 1) {
                            var nextS = ss[i + 1].start
                            var s02 = ss[i].showContent.toString()
                                .substring(3, ss[i].showContent.toString().length - 2)
                            var map = HashMap<String, String>()
                            map["content"] = s02
                            map["code"] = mapAttenSearchCheck[s02]!!
                            list_post.add(map)

                            if (cruE != nextS) {
                                var s03 = strE.substring(cruE, nextS)
                                var map = HashMap<String, String>()
                                map["content"] = s03
                                map["code"] = ""
                                list_post.add(map)
                            }

                        } else {
                            var s02 = ss[i].showContent.toString()
                                .substring(3, ss[i].showContent.toString().length - 2)

                            var map = HashMap<String, String>()
                            map["content"] = s02
                            map["code"] = mapAttenSearchCheck[s02]!!
                            list_post.add(map)

                            var cruE = ss[i].end

                            if (cruE != strE.length) {
                                var s04 = strE.substring(cruE, strE.length)
                                var map = HashMap<String, String>()
                                map["content"] = s04
                                map["code"] = ""
                                list_post.add(map)
                            }
                        }
                    }
                } else {
                    var map = HashMap<String, String>()
                    map["content"] = mBinding.editChat.text.toString()
                    map["code"] = ""
                    list_post.add(map)
                }
                val jsonString = Gson().toJson(list_post)
                home.a14( id, jsonString, parent_id, this)
            }

        }
    }


    fun setEditextQ(key:String){
        if (!mBinding.editChat.text.toString().contains(key)) {
            mBinding.editChat.insertSpecialStr("  @" + key + "  ", false, 0, ForegroundColorSpan(Color.parseColor(resources.getString(R.string.main_05))));
            mBinding.editChat.requestFocus()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != RESULT_OK) {
            return
        }

        when (requestCode) {
            123->{
                var list = data?.getStringArrayListExtra("data")
                setEditextQ(list!![0])
                mapAttenSearchCheck[list!![0]]= list!![1]
            }

        }
    }

    inner class GoldRecyclerAdapter(list: ArrayList<MutableMap<String, String>>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        var list = ArrayList<MutableMap<String, String>>()

        init {
            this.list = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(ItemFrgHome03Binding.inflate(LayoutInflater.from(this@PostDetailsAty)))
        }

        inner class fGoldViewHolder(binding: ItemFrgHome03Binding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFrgHome03Binding = binding

            init {
                AutoUtils.autoSize(binding.root)
            }
        }

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    when (list.size) {
                        2, 4 -> {
                            var mlayoutParams = mBinding.imgv.layoutParams
                            mlayoutParams.height = AutoUtils.getPercentWidthSize(490)
                            mlayoutParams.width = AutoUtils.getPercentWidthSize(490)
                            mBinding.imgv.layoutParams = mlayoutParams
                            mBinding.imgv.scaleType = ImageView.ScaleType.CENTER_CROP
                        }
                        3, 5, 6, 7, 8, 9 -> {
                            var mlayoutParams = mBinding.imgv.layoutParams
                            mlayoutParams.height = AutoUtils.getPercentWidthSize(320)
                            mlayoutParams.width = AutoUtils.getPercentWidthSize(320)
                            mBinding.imgv.layoutParams = mlayoutParams
                            mBinding.imgv.scaleType = ImageView.ScaleType.CENTER_CROP

                        }
                    }

                    if (TextUtils.isEmpty(list[position]["m3u"])) {
                        mBinding.imgvVideo.visibility = View.GONE
                    } else {
                        mBinding.imgvVideo.visibility = View.VISIBLE
                    }

                    itemView.setOnClickListener {
                        var bundle = Bundle()
                        bundle.putString("data", data)
                        bundle.putString("index", position.toString())
                        startActivity(PlayListAty::class.java, bundle)

                    }

                    var maxW = AutoLayoutConifg.getInstance().screenWidth
                    var maxH = AutoLayoutConifg.getInstance().screenHeight/2
                    ImageLoader.loadImageAes(
                        this@PostDetailsAty,
                        list[position]["img"],
                        mBinding.imgv,maxW,maxH
                    )
                }

            }

        }

    }

    inner class GoldRecyclerAdapter2(context: Context) :

        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        private val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            val view: View = inflater.inflate(R.layout.item_comment, parent, false)
            return fGoldViewHolder(view)
        }

//        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//
//
//        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (holder is fGoldViewHolder) {

                with(holder) {

                    var maxW = AutoUtils.getPercentWidthSizeBigger(200)
                    ImageLoader.loadImageAes(this@PostDetailsAty, list[position]["avatar"], imgv_head,maxW,maxW)
                    tv_name?.text = list[position]["nick"]
                    var index =position

                    if (list[index]["is_like"]=="1"){
                        imgv_xin?.setImageResource(R.mipmap.ic_74)
                    }else{
                        imgv_xin?.setImageResource(R.mipmap.ic_12)
                    }

                    if (list[position]["content"]!!.contains("[")){
                        var listContent = JSONUtils.parseKeyAndValueToMapList(list[position]["content"])
                        var str_list =  ArrayList<String>();
                        var color_list =  ArrayList<Int>()
                        var text_size_list =  ArrayList<Float>()
                        for ( i in listContent.indices){
                            text_size_list.add(AutoUtils.getPercentHeightSize(40).toFloat())

                            var sp = ""
                            if (i!=0){
                                sp = "  "
                            }

                            if (TextUtils.isEmpty(listContent[i]["code"])){
                                color_list.add(Color.BLACK)
                                str_list.add(sp+"" + listContent[i]["content"]!! + " ")
                            }else{
                                color_list.add(Color.parseColor(resources.getString(R.string.main_05)))
                                str_list.add(sp+"@" + listContent[i]["content"]!!)
                            }
                        }

                        MyUtils3.setText(this@PostDetailsAty, tv_content, str_list, color_list, text_size_list, object : ClickListener {
                            override fun click(position: Int) {
                                if (TextUtils.isEmpty(listContent[position]["code"])) {
                                    this@PostDetailsAty.mBinding.editChat.hint = "回复"+list[index]["nick"]
                                    parent_id = list[index]["id"]!!
                                    this@PostDetailsAty.mBinding.editChat.requestFocus()
                                    return
                                }

                                var bundle = Bundle()
                                if (listContent[position]["code"]==info_code){
                                    bundle.putString("typePage","my")
                                    startActivity(PersonDetailsAty::class.java,bundle)
                                }else{
                                    bundle.putString("user_id",listContent[position]["code"])
                                    startActivity(PersonOtherDetailsAty::class.java,bundle)
                                }
                            }
                        })

                    }else{
                        tv_content?.text = list[position]["content"]
                    }


                    var time = (TimeUtils.getBeijinTime().toDouble() - TimeUtils.getTime(list[position]["created_at"]).toDouble())/1000
                    tv_time?.text = TimeUtils.formatSecond3(time)+"        回复"
                    tv_num?.text = list[position]["total_support"]


                    imgv_head?.setOnClickListener {

                        var info_code = MyUtils.getInfoDetails(this@PostDetailsAty,"info_code")
                        var bundle = Bundle()
                        if (list[position]["user_id"]==info_code){
                            bundle.putString("typePage","my")
                            startActivity(PersonDetailsAty::class.java,bundle)
                        }else{
                            bundle.putString("user_id",list[position]["user_id"])
                            startActivity(PersonOtherDetailsAty::class.java,bundle)
                        }
                    }

                    linlay_zan?.setOnClickListener {
                        index_zan = position
                        startProgressDialog()
                        home?.a16(list[position]["id"]!!, this@PostDetailsAty)
                    }

                    tv_time?.setOnClickListener {
                        this@PostDetailsAty.mBinding.editChat.hint = "回复"+list[position]["nick"]
                        parent_id = list[position]["id"]!!
                        this@PostDetailsAty.mBinding.editChat.requestFocus()
                    }

                    relay_replay?.setOnClickListener {
                        var bundle = Bundle()
                        bundle.putString("id",this@PostDetailsAty.id)
                        bundle.putString("parent_id",list[position]["id"])
                        startActivity(ReplayListAty::class.java,bundle)
                    }

                    itemView.setOnClickListener {
                        parent_id = "0"
                        this@PostDetailsAty.mBinding.editChat.hint = "有爱评论，说点好听的～"
                        this@PostDetailsAty.mBinding.editChat.setText("")
                        this@PostDetailsAty.mBinding.editChat.clearFocus()

                    }

                    var list2 = JSONUtils.parseKeyAndValueToMapList(list[position]["comment_reply_user_data"])

                    if (list2 !=null&&list2.size!=0){

                        var mLayoutManager = LinearLayoutManager(this@PostDetailsAty)
                        recyclerView?.layoutManager = mLayoutManager
                        recyclerView?.isNestedScrollingEnabled = false

                        var adapter = GoldRecyclerAdapter3(this@PostDetailsAty, list2,list[position]["user_id"]!!)
                        recyclerView?.adapter = adapter
                        recyclerView?.visibility = View.VISIBLE
                        relay_replay?.visibility = View.VISIBLE

                    }else{
                        recyclerView?.visibility = View.GONE
                        relay_replay?.visibility = View.GONE
                    }

                }
            }

        }

        override fun getItemCount(): Int = list.size

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv_head: ImageView? = null
            var imgv_xin: ImageView? = null
            var tv_name: TextView? = null
            var tv_content: TextView? = null
            var tv_time: TextView? = null
            var tv_num: TextView? = null
            var linlay_zan: LinearLayout? = null
            var recyclerView: RecyclerView? = null
            var relay_replay: RelativeLayout? = null

            init {
                AutoUtils.autoSize(itemView)
                imgv_head = itemView.findViewById(R.id.imgv_head)
                imgv_xin = itemView.findViewById(R.id.imgv_xin)
                tv_name = itemView.findViewById(R.id.tv_name)
                tv_content = itemView.findViewById(R.id.tv_content)
                tv_time = itemView.findViewById(R.id.tv_time)
                tv_num = itemView.findViewById(R.id.tv_num)
                linlay_zan = itemView.findViewById(R.id.linlay_zan)
                recyclerView = itemView.findViewById(R.id.recyclerView)
                relay_replay = itemView.findViewById(R.id.relay_replay)
            }
        }

    }


    inner class GoldRecyclerAdapter3(context: Context, list: ArrayList<MutableMap<String, String>>, id:String) :
        XBaseAdaper() {

        val inflater: LayoutInflater = LayoutInflater.from(context)
        var list = ArrayList<MutableMap<String, String>>()
        var index_zan = -1
        var id = ""

        init {
            index_zan = -1
            this@GoldRecyclerAdapter3.list = list
            this@GoldRecyclerAdapter3.id = id
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(inflater.inflate(R.layout.item_comment_replay, parent, false))
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    var maxW = AutoUtils.getPercentWidthSizeBigger(200)

                    ImageLoader.loadImageAes(this@PostDetailsAty, list[position]["avatar"], imgv_head,maxW,maxW)

                    if (list[position]["parent_user_id"]==id){
                        tv_name?.text = list[position]["nick"]

                    }else{
                        tv_name?.text = list[position]["nick"]+" 回复 "+list[position]["parent_user_nick"]

                    }

                    var index =position

                    if (list[index]["is_like"]=="1"){
                        imgv_xin?.setImageResource(R.mipmap.ic_74)
                    }else{
                        imgv_xin?.setImageResource(R.mipmap.ic_12)
                    }

                    if (list[position]["content"]!!.contains("[")){
                        var listContent = JSONUtils.parseKeyAndValueToMapList(list[position]["content"])
                        var str_list =  ArrayList<String>();
                        var color_list =  ArrayList<Int>()
                        var text_size_list =  ArrayList<Float>()
                        for ( i in listContent.indices){
                            text_size_list.add(AutoUtils.getPercentHeightSize(40).toFloat())
                            var sp = ""
                            if (i!=0){
                                sp = "  "
                            }
                            if (TextUtils.isEmpty(listContent[i]["code"])){
                                color_list.add(Color.BLACK)
                                str_list.add(sp + listContent[i]["content"]!! + " ")
                            }else{
                                color_list.add(Color.parseColor(resources.getString(R.string.main_05)))
                                str_list.add(sp+"@" + listContent[i]["content"]!!)
                            }
                        }

                        MyUtils3.setText(this@PostDetailsAty, tv_content, str_list, color_list, text_size_list, object : ClickListener {
                            override fun click(position: Int) {
//                                if (TextUtils.isEmpty(listContent[position]["code"])) {
//                                    edit_chat.hint = "回复" + list[index]["nick"]
//                                    parent_id = list[index]["id"]!!
//                                    edit_chat.requestFocus()
//                                    return
//                                }
//                                var bundle = Bundle()
//                                bundle.putString("userId", listContent[position]["code"])
//                                startActivity(UserCenterActivity::class.java, bundle)
                            }
                        })

                    }else{
                        tv_content?.text = list[position]["content"]
                    }

//                    tv_time?.text = list[position]["created_at"]+"    回复"

                    var time = (TimeUtils.getBeijinTime().toDouble() - TimeUtils.getTime(list[position]["created_at"]).toDouble())/1000
                    tv_time?.text = TimeUtils.formatSecond3(time)+"        回复"

                    tv_num?.text = list[position]["total_support"]

                    if (list[position]["is_author"]=="1"){
                        tv_name2?.visibility = View.VISIBLE
                    }else{
                        tv_name2?.visibility = View.GONE
                    }
                    linlay_zan?.setOnClickListener {
                        this@GoldRecyclerAdapter3.index_zan = position
                        startProgressDialog()
                        home?.a16(list[position]["id"]!!, this@GoldRecyclerAdapter3)
                    }

                    tv_time?.setOnClickListener {
                        this@PostDetailsAty.mBinding.editChat.hint = "回复"+list[position]["nick"]
                        parent_id = list[position]["id"]!!
                        this@PostDetailsAty.mBinding.editChat.requestFocus()
                    }

                    imgv_head?.setOnClickListener {
//                        var bundle = Bundle()
//                        bundle.putString("userId", list[position]["user_id"])
//                        startActivity(UserCenterActivity::class.java, bundle)

                            var info_code = MyUtils.getInfoDetails(this@PostDetailsAty,"info_code")
                            var bundle = Bundle()
                            if (list[position]["user_id"]==info_code){
                                bundle.putString("typePage","my")
                                startActivity(PersonDetailsAty::class.java,bundle)
                            }else{
                                bundle.putString("user_id",list[position]["user_id"])
                                startActivity(PersonOtherDetailsAty::class.java,bundle)
                            }
                    }

                    itemView.setOnClickListener {
                        parent_id = "0"
                        this@PostDetailsAty.mBinding.editChat.hint = "有爱评论，说点好听的～"
                        this@PostDetailsAty.mBinding.editChat.setText("")
                        this@PostDetailsAty.mBinding.editChat.clearFocus()
                    }
                }

            }
        }

        override fun onComplete(var2: String?, type: String?) {
            if (type == "comment/zan"){
                stopProgressDialog()
                var map = JSONUtils.parseKeyAndValueToMap(var2)
                if (map["code"] == "200") {
                    if (index_zan == -1)return
                    var index =index_zan
                    var str = AESCBCCrypt.aesDecrypt(map["data"])
                    var map_data =JSONUtils.parseKeyAndValueToMap(str)
                    var mapList = list[index]
                    mapList["is_like"] = map_data["id"]!!
                    var total_zan_num = mapList["total_support"]!!.toInt()
                    if (map_data["id"]=="1"){
                        total_zan_num += 1
                    }else{
                        total_zan_num -= 1
                        if (total_zan_num<0){
                            total_zan_num = 0
                        }
                    }
                    mapList["total_support"]= (total_zan_num).toString()
                    list[index]=mapList
                    notifyItemChanged(index)
                    index_zan = -1

                }else{
                    showToastS(map["message"])
                }
            }


        }

        override fun onExceptionType(type: String?) {
            stopProgressDialog()
            showToastS("网络异常")

        }
        override fun getItemCount(): Int = list.size

        override fun getCoroutineScope(): CoroutineScope? {
            return lifecycleScope
        }

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv_head: ImageView? = null
            var imgv_xin: ImageView? = null
            var tv_name: TextView? = null
            var tv_name2: TextView? = null
            var tv_content: TextView? = null
            var tv_time: TextView? = null
            var tv_num: TextView? = null
            var linlay_zan: LinearLayout? = null

            init {
                AutoUtils.autoSize(itemView)
                imgv_head = itemView.findViewById(R.id.imgv_head)
                imgv_xin = itemView.findViewById(R.id.imgv_xin)
                tv_name = itemView.findViewById(R.id.tv_name)
                tv_name2 = itemView.findViewById(R.id.tv_name2)
                tv_content = itemView.findViewById(R.id.tv_content)
                tv_time = itemView.findViewById(R.id.tv_time)
                tv_num = itemView.findViewById(R.id.tv_num)
                linlay_zan = itemView.findViewById(R.id.linlay_zan)
            }
        }

    }


}