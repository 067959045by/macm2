package com.mail.myapplication.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.mail.comm.image.ImageLoader
import com.mail.comm.utils.PreferencesUtils
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyMainWalletBinding
import com.mail.myapplication.ui.wallet.WalletFrg

class MainWalletAty : BaseXAty() {

    var position: Int = 0

    lateinit var mBinding: AtyMainWalletBinding

    var list_tv: Array<TextView>? = null

    var list_imgv: Array<ImageView>? = null

    var page_type = ""

    override fun getLayoutId(): Int = 0

    override fun getLayoutView(): View {
        mBinding = AtyMainWalletBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onRestart() {
        super.onRestart()
        var f1 = supportFragmentManager?.findFragmentByTag(WalletFrg::class.java.toString())
        if (f1 is WalletFrg){
          f1.onRestart()
        }
    }


    fun getPageType() = page_type

    override fun getFragmentContainerId(): Int = R.id.fralay_content

    override fun initView() {
        if (intent.hasExtra("page_type")){
            page_type = intent.getStringExtra("page_type").toString()
        }
        if (page_type == "pay"){
            mBinding.relayBottom.visibility = View.GONE
        }else{
            setBackTwo(true)
        }
        list_tv = arrayOf(mBinding.tv01, mBinding.tv02, mBinding.tv03, mBinding.tv04)
        list_imgv = arrayOf(mBinding.imgv01, mBinding.imgv02, mBinding.imgv03, mBinding.imgv04)
    }

    override fun requestData() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTranslanteBar()
        addFragment(WalletFrg::class.java, null)
        list_tv?.let { t1 -> list_imgv?.let { t2 -> setSelector(mBinding.tv04, t1, t2) } }
    }


    
    fun mainClick(v: View) {

        when (v.id) {

            R.id.linlay_01 -> {
                var intent = Intent()
                intent.putExtra("type", "home_01")
                setResult(RESULT_OK,intent)
                finshPage("3")
            }

            R.id.linlay_02 -> {
                var intent = Intent()
                intent.putExtra("type", "home_02")
                setResult(RESULT_OK,intent)
                finshPage("3")
            }

            R.id.linlay_03 -> {
                var intent = Intent()
                intent.putExtra("type", "home_03")
                setResult(RESULT_OK,intent)
                finshPage("3")
            }

            R.id.linlay_04 -> {
            }

            R.id.relay_set -> {
                var intent = Intent()
                intent.putExtra("type", "home_set")
                setResult(RESULT_OK,intent)
                finshPage("3")
            }

            R.id.relay_apply -> {
                var intent = Intent()
                intent.putExtra("type", "home_applay")
                setResult(RESULT_OK,intent)
                finshPage("3")
            }
            R.id.linlay_atten -> {
                var intent = Intent()
                intent.putExtra("type", "home_atten")
                setResult(RESULT_OK,intent)
                finshPage("3")
            }
            R.id.linlay_fans -> {
                var intent = Intent()
                intent.putExtra("type", "home_fans")
                setResult(RESULT_OK,intent)
                finshPage("3")
            }
            R.id.relay_info -> {
                var intent = Intent()
                intent.putExtra("type", "home_info")
                setResult(RESULT_OK,intent)
                finshPage("3")
            }
        }
    }

    private fun setSelector(tv: TextView, list_tv: Array<TextView>, list_imgv: Array<ImageView>) {
        for (i in list_tv.indices) {
            if (tv === list_tv[i]) {
                list_tv[i].setTextColor(resources.getColor(R.color.main_01))

                when (i) {
                    0 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab1_checked)
                    1 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab2_checked)
                    2 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab3_checked)
                    3 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab4_checked)
                }
            } else {
                list_tv[i].setTextColor(resources.getColor(R.color.mainnocheck))
                when (i) {
                    0 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab1_normal)
                    1 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab2_normal)
                    2 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab3_normal)
                    3 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab4_normal)
                }
            }
        }
    }


}


