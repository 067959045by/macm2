package com.mail.myapplication.ui.lar

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.lifecycleScope
import com.mail.comm.app.AppManager
import com.mail.comm.app.BaseApp
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.view.AppCountdown
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyBindPhoneBinding
import com.mail.myapplication.databinding.AtyRegisterCodeBinding
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.mine.EditInfoAty
import com.mail.myapplication.ui.utils.MyUtils3
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.xutils.common.util.LogUtil

class BindPhoneAty : BaseXAty() {

    lateinit var mBinding: AtyBindPhoneBinding
    var apCountdown: AppCountdown?=null
    var lar = Lar()
    var area_code = "+86"
    var phone_my = ""
    var country_name =""

    override fun getLayoutId(): Int = 0

    override fun initView() {}

    override fun requestData() {}

    override fun getLayoutView(): View {
        mBinding = AtyBindPhoneBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "綁定手機"

            phone_my = MyUtils.getInfoDetails(this@BindPhoneAty,"info_mobile")
            if (!TextUtils.isEmpty(phone_my)){
                editPhone.setText(phone_my)
                mBinding.editPhone.setSelection(phone_my.length);//将光标移至文字末尾
                imgvCancel.visibility = View.VISIBLE
            }

            apCountdown = AppCountdown.getInstance()
            apCountdown?.setMyText("獲取驗證碼")
            apCountdown?.play(tvCode)

            editPhone.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    if (!TextUtils.isEmpty(editPhone.text.toString())){
                        imgvCancel.visibility = View.VISIBLE
                    }else{
                        imgvCancel.visibility = View.GONE
                    }
                }

            })
        }

    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.imgv_cancel -> {
                mBinding.editPhone.setText("")
            }

            R.id.relay_area -> {
                startActivityForResult(AreaListAty::class.java, 123)
            }

            R.id.tv_code ->{

                if (TextUtils.isEmpty(mBinding.editPhone.text.toString())){
                    showToastS("手機號不能為空")
                    return
                }

                if (phone_my == mBinding.editPhone.text.toString()){
                    showToastS("不能綁定同一個手機號")
                    return
                }

                startProgressDialog()
                lar.b2(mBinding.editPhone.text.toString(),area_code,this)
            }

            R.id.tv_ok ->{
                if (TextUtils.isEmpty(mBinding.editPhone.text.toString())){
                    showToastS("手機號不能為空")
                    return
                }

                if (phone_my == mBinding.editPhone.text.toString()){
                    showToastS("不能綁定同一個手機號")
                    return
                }

                if (TextUtils.isEmpty(mBinding.editCode.text.toString())){
                    showToastS("驗證碼不能為空")
                    return
                }

                startProgressDialog()
                lar.b3(mBinding.editPhone.text.toString(),mBinding.editCode.text.toString(),area_code,this)

            }
        }
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type=="sendmsg"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                apCountdown?.start()
            }else{
                showToastS(map["message"])
            }
        }

        if (type=="bindphone"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                apCountdown?.start()
                showToastS("綁定成功")
                BaseApp.instance?.saveOneMapData("info_mobile",mBinding.editPhone.text.toString())
//                MyUtils.saveInfoDetails(this,"info_mobile",mBinding.editPhone.text.toString())
                AppManager.getInstance().killActivity(EditInfoAty::class.java)
                finish()
            }else{
                showToastS(map["message"])
            }

        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        showToastS("網路異常")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK){
            when(requestCode){

                123 ->{
                     area_code =data?.getStringExtra("area_code")!!
                     country_name =data?.getStringExtra("country_name")!!
                     mBinding.tvConutry.text = country_name
                     mBinding.tvConutryCode.text = area_code

                }
            }

        }
    }



}