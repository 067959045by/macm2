package com.mail.myapplication.ui.dg

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.base.BaseAty
import com.mail.comm.base.BaseDgFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.ui.mine.Set2Aty

class ReportDgFrg (context: BaseAty) : Dialog(context)  {

    lateinit var mBinding: DgReportBinding
    private var baseAty = context
    var type ="1"
    var id =""
    var listener : RechargeListen? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DgReportBinding.inflate(layoutInflater);
        setContentView(mBinding.root)
        window?.setWindowAnimations(R.style.dialogFullscreen3)
        val dialogWindow = window
        dialogWindow!!.setBackgroundDrawable(null)
        dialogWindow.setGravity(Gravity.BOTTOM)
        setCanceledOnTouchOutside(true)
        val m = baseAty.windowManager
        val d = m.defaultDisplay// 获取屏幕宽、高
        val p = dialogWindow.attributes
        window?.getDecorView()?.setPadding(0, 0, 0, 0)
//        p.height =  (d.height * 1) // 高度设置为屏幕的0.6
        p.width = (d.width * 1) // 宽度设置为屏幕的0.85
        dialogWindow.attributes = p

        with(mBinding) {

            tvOk.setOnClickListener {
                dismiss()
                listener?.onclik02(type,id)
            }


            tvCancel.setOnClickListener {
                dismiss()
            }

            tv01.setOnClickListener {
                type="1"
                tv01.setBackgroundResource(R.drawable.shape_23)
                tv02.setBackgroundResource(R.drawable.shape_22)
            }

            tv02.setOnClickListener {
                type ="2"
                tv01.setBackgroundResource(R.drawable.shape_21)
                tv02.setBackgroundResource(R.drawable.shape_24)
            }
        }
    }

    fun setData(id:String ){
        this.id = id
        mBinding.tv01.performClick()
    }

    interface  RechargeListen{
        fun onclik02(type:String,id:String)
    }

    fun setRechargeListen(listener:RechargeListen){
        this.listener =listener
    }





}