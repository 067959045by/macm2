package com.mail.myapplication.ui.lar

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.compose.Composable
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.MainAty
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil
import java.lang.reflect.Modifier

class RegisterJobAty : BaseXAty() {

    lateinit var mBinding: AtyRegisterJobBinding
    lateinit var mAdapter: GoldRecyclerAdapter
    lateinit var list: ArrayList<String>
    var lar = Lar()

    var type = ""
    var checkIndex = -1

    override fun getLayoutId(): Int = 0

    override fun initView() {
        type = intent.getStringExtra("type")!!
    }

    override fun requestData() {

    }

    override fun getLayoutView(): View {
        mBinding = AtyRegisterJobBinding.inflate(layoutInflater);
        return mBinding.root
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var config_data = PreferencesUtils.getString(this, "config_data")

        var map = JSONUtils.parseKeyAndValueToMap(config_data)
        list = JSONUtils.parseKeyAndValueToListString(map["job_data"])

        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvRight.text = "跳過"
            include.tvRight.visibility = View.VISIBLE

            if (type == "edit") {
                include.tvRight.visibility = View.GONE
            } else {
                include.tvRight.visibility = View.VISIBLE

            }

            var mLayoutManager2 = GridLayoutManager(this@RegisterJobAty, 3)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager = mLayoutManager2
            mAdapter = GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type == "update") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
            } else {
                showToastS(map["message"])
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type == "update") {
            showToastS("网络异常")
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (type == "edit"){
            super.onBackPressed()
        }else{
            finish()
            startActivity(MainAty::class.java)
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                if (type == "edit"){
                    finish()
                }else{
                    finish()
                    startActivity(MainAty::class.java)
                }
            }

            R.id.tv_ok -> {
                if (type == "edit") {
                    if (checkIndex ==-1) {
                        showToastS("请选择你的职业")
                        return
                    }
                    var intent = Intent()
                    intent.putExtra("data", list[checkIndex])
                    setResult(RESULT_OK, intent)
                    finshPage("2")
                }else{
                    if (checkIndex ==-1) {
                        showToastS("请选择你的职业")
                        return
                    }
                    PreferencesUtils.putString(this,"login_job",list[checkIndex])
                    var bundle = Bundle()
                    bundle.putString("type","login")
                    bundle.putString("login_job",list[checkIndex])
                    startActivity(RegisterSexAty::class.java,bundle)
                }
            }

            R.id.tv_right -> {
                var bundle = Bundle()
                bundle.putString("type","login")
                startActivity(RegisterSexAty::class.java,bundle)
            }
        }
    }


    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemRegisterJobBinding.inflate(LayoutInflater.from(this@RegisterJobAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    mBinding.tvName.text = list[position]

                    if (position == checkIndex) {

                        mBinding.tvName.setBackgroundResource(R.drawable.shape_33)
                    } else {
                        mBinding.tvName.setBackgroundResource(R.drawable.shape_9)

                    }
                    itemView.setOnClickListener {
                        checkIndex = position
                        mAdapter.notifyDataSetChanged()
                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemRegisterJobBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemRegisterJobBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}