package com.mail.myapplication.ui.wallet

import android.graphics.Bitmap
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.bingoogolapple.qrcode.zxing.QRCodeEncoder
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil
import java.util.HashMap

class ShareAty : BaseXAty() {

    lateinit var mBinding: AtyShareBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2
    var home = Home()
    var list = ArrayList<MutableMap<String, String>>()
    var lar = Lar()

    var form_id = ""
    override fun getLayoutId(): Int = 0
    var share_url = ""

    override fun initView() {
        if (intent.hasExtra("id")){
            form_id = intent.getStringExtra("id").toString()
        }
    }

    override fun requestData() {
        if (!TextUtils.isEmpty(form_id)){
            home.a49(form_id,this)
        }
        home.a26(this)
        lar.b6(this)
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        mBinding.swipeRefreshLayout.finishRefreshing()
        mBinding.swipeRefreshLayout.finishLoadmore()
        if (type == "user/info") {

            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str_info = AESCBCCrypt.aesDecrypt(map["data"])
                var map_info = JSONUtils.parseKeyAndValueToMap(str_info)
                mBinding.tvNum.text = map_info["invite"]

            }
        }

        if (type == "invite/rule"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                list = JSONUtils.parseKeyAndValueToMapList(str)
                mAdapter2?.notifyDataSetChanged()
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        mBinding.swipeRefreshLayout.finishRefreshing()
        mBinding.swipeRefreshLayout.finishLoadmore()
    }

    override fun getLayoutView(): View {
        mBinding = AtyShareBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTranslanteBar()
        setAndroidNativeLightStatusBar(true)

        with(mBinding) {
            var mLayoutManager2 = GridLayoutManager(this@ShareAty, 1)
            mLayoutManager2.orientation = RecyclerView.HORIZONTAL
            recyclerview.layoutManager = mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview.adapter = mAdapter2
            share_url = MyUtils.getInfoDetails(this@ShareAty, "share_url")
            var info_code = MyUtils.getInfoDetails(this@ShareAty, "info_code")
            val bitmap: Bitmap = QRCodeEncoder.syncEncodeQRCode(share_url, AutoUtils.getPercentWidthSize(400))
            imgvCode.setImageBitmap(bitmap)
            tvCode.text = "邀请码："+info_code
            tvLink.text = share_url

            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setIsPinContentView(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    requestData()
                }

                override fun loadMoreStart() {
                }

            })

//            tvNum.text =  MyUtils.getInfoDetails(this@ShareAty, "info_invite")
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
            R.id.tv_save -> {
                MyUtils3.saveBmpGallery(this,mBinding.relaySave,"lvban_poster2.jpg")

            }
            R.id.tv_copty -> {
                MyUtils3.copyString(share_url, this)
                showToastS("复制成功")
            }

        }
    }

    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemShare2Binding.inflate(LayoutInflater.from(this@ShareAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        tv01.text = list[position]["days"]+"天VIP"
                        tvPer.text = "邀请"+list[position]["person"]+"人"
                        tvDay.text = "免费"+list[position]["free_times"]+"次"

                        if (list[position]["free_times"]=="0"){
                            linlayPer.visibility = View.GONE
                        }

                        if (list[position]["days"]=="0"){
                            linlayDay.visibility = View.GONE
                        }
                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemShare2Binding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemShare2Binding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}