package com.mail.myapplication.ui.video

import android.os.Bundle
import android.view.View
import cn.jzvd.Jzvd
import cn.jzvd.JzvdStd
import com.mail.comm.image.ImageLoader
import com.mail.comm.video.jz.JZMediaExo
import com.mail.comm.video.jz.JZMediaIjk
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.FrgPlayList2Binding
import com.mail.myapplication.databinding.FrgPlayListBinding
import org.xutils.common.util.LogUtil

class PlayListFrg : BaseXFrg() {

    lateinit var mBinding: FrgPlayListBinding

    override fun getLayoutView(): View {
        mBinding = FrgPlayListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    companion object {

        fun create(type: String): PlayListFrg {
            val fragment = PlayListFrg()
            val bundle = Bundle()
            bundle.putString("type", type)
            fragment.arguments = bundle
            return fragment
        }
    }

    var type = ""
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var type = arguments?.getString("type")
        this@PlayListFrg.type = type!!
        with(mBinding) {
            tv01.text = type
            var urlPlay ="https://media.w3.org/2010/05/sintel/trailer.mp4"
//            var url = "http://cncdn.jumu8.com/fb738180c423f75771ed8f51a46163e8/6103f359/ig/admin_video/070821/JwMj/ubUS/0fe56b43876f87e34579bf9d49ebb511short.m3u8"
            Jzvd.setVideoImageDisplayType(Jzvd.VIDEO_IMAGE_DISPLAY_TYPE_ORIGINAL)
            jzVideo.setUp(urlPlay, "", JzvdStd.SCREEN_NORMAL, JZMediaIjk::class.java)
            jzVideo.startButton.performClick()

            var urlImg = "https://img1.baidu.com/it/u=231471902,3222054569&fm=26&fmt=auto&gp=0.jpg"
            if (type == "1") {

               jzVideo.visibility = View.GONE
               imgv.visibility = View.VISIBLE
//                imgv.setImageResource(R.mipmap.ic_15)
                ImageLoader.loadImage(this@PlayListFrg.activity,urlImg,imgv)

            }else{
                jzVideo.visibility = View.VISIBLE
                imgv.visibility = View.GONE
            }

        }
        LogUtil.e("PlayListFrg onActivityCreated type=" + type.toString())



    }

    override fun onResume() {
        super.onResume()
//        mBinding.jzVideo.startPreloading()
        mBinding.jzVideo.startButton.performClick()
        LogUtil.e("PlayListFrg onResume type=" + type.toString())
    }

    override fun onPause() {
        super.onPause()
        LogUtil.e("PlayListFrg onPause type=" + type.toString())
        Jzvd.releaseAllVideos()
    }

    override fun onDestroy() {
        super.onDestroy()
        LogUtil.e("PlayListFrg onDestroy type=" + type.toString())
    }

    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
    }


}