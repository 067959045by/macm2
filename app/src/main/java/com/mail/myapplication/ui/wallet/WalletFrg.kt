package com.mail.myapplication.ui.wallet

import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.utils.TimeUtils
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.MainWalletAty
import com.mail.myapplication.ui.dg.PayDialog
import com.mail.myapplication.ui.dg.PaySure2DgFrg
import com.mail.myapplication.ui.dg.PaySureDgFrg
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.msg.CustomerServiceAty
import com.mail.myapplication.ui.utils.MyUtils3
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.listener.OnPageChangeListener
import com.zhy.autolayout.utils.AutoUtils

class WalletFrg : BaseXFrg(), PayDialog.PayListen {

    lateinit var mBinding: FrgWalletBinding
    lateinit var mAdapter: GoldRecyclerAdapter
    var dgPayDgFrg: PayDialog? = null
    var dgPaySureDgFrg: PaySureDgFrg? = null
    var dgPaySure2DgFrg: PaySure2DgFrg? = null
    var lar = Lar()
    var list_vip = ArrayList<MutableMap<String, String>>()
    var list_vip_des = ArrayList<MutableMap<String, String>>()
    var str_wallet = ""
    var order_sn = ""
    var pay_time = 0L
    var map_service_info:MutableMap<String,String>? = null

    override fun getLayoutView(): View {
        mBinding = FrgWalletBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun getLayoutId(): Int = 0

    override fun initView() {
        initMyInfo()
        var service_info = PreferencesUtils.getString(activity,"service_info")
        map_service_info = JSONUtils.parseKeyAndValueToMap(service_info)
    }

    override fun requestData() {
        stopProgressDialog()
        lar.b10(this)
    }

    override fun toPay(payment_id: String) {
        startProgressDialog()
        var index = mBinding.banner1.currentItem - 1
        lar.b11("", payment_id, list_vip[index]["id"]!!, this)
    }


    fun onRestart() {
        if (dgPayDgFrg?.isShowing == true) {
            dgPayDgFrg?.dismiss()
            if (dgPaySureDgFrg == null) {
                dgPaySureDgFrg = PaySureDgFrg(activity as BaseAty)
                dgPaySureDgFrg?.setPaySureListen(object : PaySureDgFrg.PaySureListen {
                    override fun surePay() {
                        if (dgPaySure2DgFrg == null) {
                            dgPaySure2DgFrg = PaySure2DgFrg(activity as BaseAty)
                            dgPaySure2DgFrg?.setRechargeListen(object : PaySure2DgFrg.RechargeListen{
                                override fun onclick01() {
                                    mBinding.swipeRefreshLayout.startRefresh()
                                }

                            })
                        }
                        dgPaySure2DgFrg?.show()
                        dgPaySure2DgFrg?.startLoor(order_sn,"1")
                    }
                })
            }
            dgPaySureDgFrg?.show()
        }
    }

    override fun onResume() {
        super.onResume()
        lar.b10(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(mBinding) {

            var mainAty = activity as MainWalletAty

            if (mainAty.getPageType() == "pay") {
                relayBack.visibility = View.VISIBLE
            } else {
                relayBack.visibility = View.GONE
            }

            relayBack.setOnClickListener {
                mainAty.finish()
            }

            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setIsPinContentView(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    lar.b10(this@WalletFrg)
                }

                override fun loadMoreStart() {
                }

            })


//            tvName


            var mLayoutManager = GridLayoutManager(activity, 1)
            mLayoutManager.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager = mLayoutManager
            mAdapter = GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter

            banner1.indicator = CircleIndicator(activity)
            banner1.setBannerGalleryEffect(
                AutoUtils.getPercentWidthSizeBigger(100),
                AutoUtils.getPercentWidthSizeBigger(35)
            )
            banner1.addBannerLifecycleObserver(this@WalletFrg)
            banner1.isAutoLoop(false)
            banner1.addOnPageChangeListener(object : OnPageChangeListener {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {

                }

                override fun onPageSelected(position: Int) {

                    list_vip_des = JSONUtils.parseKeyAndValueToMapList(list_vip[position]["vip_rights"])
                    mAdapter.notifyDataSetChanged()
                }

                override fun onPageScrollStateChanged(state: Int) {
                }

            })

            imgvShare.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                startActivity(ShareAty::class.java)
            }

            linlayLog.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                var bundle = Bundle()
                bundle.putInt("type_index", 2)
                startActivity(WithdrawLogAty::class.java, bundle)
            }

            tvPay.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                order_sn = ""
                if (dgPayDgFrg == null) {
                    dgPayDgFrg = PayDialog(activity as BaseAty)
                    dgPayDgFrg?.setPayListen(this@WalletFrg)
                }
                var index = mBinding.banner1.currentItem - 1

                if (linlayVip==null||list_vip.size==0){
                    return@setOnClickListener
                }
                var list = JSONUtils.parseKeyAndValueToMapList(list_vip[index]["payment"])
                dgPayDgFrg?.show()
                dgPayDgFrg?.setData(list)
//                startActivity(RechargeAty::class.java)
            }

            linlayCode.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                startActivity(DuiCodeAty::class.java)
            }

            linlayWithdraw.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                startActivity(WithdrawAty::class.java)
            }

            linlayRecharge.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                if (TextUtils.isEmpty(str_wallet)) {
                    requestData()
                    return@setOnClickListener
                }
                var bundle = Bundle()
                bundle.putString("data", str_wallet)
                startActivity(RechargeAty::class.java, bundle)
            }



            relay01.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                var bundle = Bundle()
                bundle.putString("typePage", "my")
                startActivity(PersonDetailsAty::class.java, bundle)
            }

            linlayPer.setOnClickListener {
                if (!MyUtils3.isFastClick()){
                    return@setOnClickListener
                }
                var bundle = Bundle()
                bundle.putString("typePage", "my")
                startActivity(PersonDetailsAty::class.java, bundle)
            }

            var str_list = ArrayList<String>();
            var color_list = ArrayList<Int>()
            var text_size_list = ArrayList<Float>()
            str_list.add((activity as MainWalletAty).resources.getString(R.string.s_13))
            str_list.add("在線客服")
            color_list.add((activity as MainWalletAty).resources.getColor(R.color.main_04))
            color_list.add((activity as MainWalletAty).resources.getColor(R.color.main_03))
            text_size_list.add(AutoUtils.getPercentHeightSize(38).toFloat())
            text_size_list.add(AutoUtils.getPercentHeightSize(42).toFloat())

            MyUtils3.setText((activity as MainWalletAty), tvKefu, str_list, color_list, text_size_list) { position->

                if (position==1){
                    var bundle = Bundle()
                    bundle.putString("code",map_service_info!!["code"])
                    bundle.putString("nick",map_service_info!!["nick"])
                    bundle.putString("avatar",map_service_info!!["avatar"])
                    startActivity(CustomerServiceAty::class.java,bundle)
                }

            }
        }
    }

    fun initMyInfo() {
        with(mBinding) {

            var info_nick = PreferencesUtils.getString(activity, "info_nick")
            var info_flow = PreferencesUtils.getString(activity, "info_flow")
            var info_fans = PreferencesUtils.getString(activity, "info_fans")
            var info_avatar = PreferencesUtils.getString(activity, "info_avatar")
            var info_intro = PreferencesUtils.getString(activity, "info_intro")
            var info_city = PreferencesUtils.getString(activity, "info_city")
            var info_years = PreferencesUtils.getString(activity, "info_years")
            var info_job = PreferencesUtils.getString(activity, "info_job")
            var info_gender = PreferencesUtils.getString(activity, "info_gender")
            var info_code = PreferencesUtils.getString(activity, "info_code")

            mBinding.tvId.text = "ID: ${info_code}"

            var maxW = AutoUtils.getPercentWidthSizeBigger(300)

            ImageLoader.loadImageAes(activity, info_avatar, ivHead,maxW,maxW)
            mBinding.tvName.text = info_nick

//            var colors = arrayOf(R.color.main_05,R.color.main_06,R.color.main_07)
            val colors = intArrayOf(Color.parseColor("#FFE9C5"),Color.parseColor("#FFFFFF"),Color.parseColor("#FFE9C5"),Color.parseColor("#ffffff"),Color.parseColor("#FFE9C5"))
            var positions = floatArrayOf(0f,0.2f,0.6f,0.8f,1f)
            var linearGradient = LinearGradient(0f,0f,(tvName.paint.textSize*tvName.text.length).toFloat(), 0f,colors,positions,Shader.TileMode.CLAMP)
            tvName.paint.shader = linearGradient
            tvName.invalidate()

            when (info_gender) {
                "1" -> {
                    imgvGender.visibility = View.VISIBLE
                    imgvGender.setImageResource(R.mipmap.ic_60)
                }
                "0" -> {
                    imgvGender.visibility = View.VISIBLE
                    imgvGender.setImageResource(R.mipmap.ic_61)
                }
                "10" -> {
                    imgvGender.visibility = View.VISIBLE
                    imgvGender.setImageResource(R.mipmap.ic_18)
                }
                else -> {
                    imgvGender.visibility = View.GONE
                }

            }
        }
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        mBinding.swipeRefreshLayout.finishLoadmore()
        mBinding.swipeRefreshLayout.finishRefreshing()
        stopProgressDialog()


        if (type == "pay") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map1 = JSONUtils.parseKeyAndValueToMap(str)
                order_sn = map1["order_sn"]!!
                pay_time = System.currentTimeMillis()
                MyUtils3.openBrowser(activity as BaseAty, map1["link"])
            } else {
                showToastS(map["message"])
            }
        }

        if (type == "wallet/info") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                str_wallet = AESCBCCrypt.aesDecrypt(map["data"])
                var map_info = JSONUtils.parseKeyAndValueToMap(str_wallet)
                list_vip = JSONUtils.parseKeyAndValueToMapList(map_info["vip_list"])
                var coin_list = JSONUtils.parseKeyAndValueToMapList(map_info["coin_list"])

                var maxW = AutoUtils.getPercentWidthSizeBigger(300)

                ImageLoader.loadImageAes(activity, map_info["avatar"], mBinding.ivHead,maxW,maxW)
                mBinding.tvName.text = map_info["nick"]
                when (map_info["gender"]) {
                    "1" -> {
                        mBinding.imgvGender.visibility = View.VISIBLE
                        mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
                    }
                    "0" -> {
                        mBinding.imgvGender.visibility = View.VISIBLE
                        mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
                    }
                    "10" -> {
                        mBinding.imgvGender.visibility = View.VISIBLE
                        mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
                    }
                    else -> {
                        mBinding.imgvGender.visibility = View.GONE
                    }
                }
                mBinding.tvMoney01.text = map_info["coins"]
                mBinding.tvMoney02.text = map_info["diamonds"]
                mBinding.tvMoney03.text = map_info["withdraw_diamonds"]

                mBinding.tvFreeNum.text = map_info["play_num_notice"]

                var adapterImg = ImageAdapter(list_vip)
                mBinding.banner1.setAdapter(adapterImg)
                list_vip_des = JSONUtils.parseKeyAndValueToMapList(list_vip[mBinding.banner1.currentItem - 1]["vip_rights"])
                mAdapter.notifyDataSetChanged()

                MyUtils3.setVipLevel2(map_info["vip_level"], mBinding.imgvVipLevel,mBinding.linlayVip,0)

                if (mBinding.linlayVip.visibility == View.GONE){
                    mBinding.linlayNoVip.visibility = View.VISIBLE
                }else{
                    mBinding.linlayNoVip.visibility = View.GONE

                    var time =  (TimeUtils.getTime(map_info["vip_expired"]).toDouble()
                            -TimeUtils.getBeijinTime().toDouble())/1000
                    mBinding.tvVipTime.text = TimeUtils.formatSecond3(time).replace("前","后")+"到期"
                }

//                mBinding.tvVipTime.text = map_info["vip_expired"] + "到期"
//              var time = (TimeUtils.getBeijinTime().toDouble() - TimeUtils.getTime(map["created_at"]).toDouble())/1000



                if (map_info["is_creator"] == "1") {
                    mBinding.imgvCreator.visibility = View.VISIBLE
                } else {
                    mBinding.imgvCreator.visibility = View.GONE
                }

            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        mBinding.swipeRefreshLayout.finishLoadmore()
        mBinding.swipeRefreshLayout.finishRefreshing()
        stopProgressDialog()
    }

    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(ItemFrgWalletBinding.inflate(LayoutInflater.from(context)))
        }

        override fun getItemCount(): Int = list_vip_des.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    mBinding.tv01.text = (position + 1).toString()
                    mBinding.tv02.text = list_vip_des[position]["remark"]
                }
            }
        }

        inner class fGoldViewHolder(binding: ItemFrgWalletBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFrgWalletBinding = binding

            init {
                AutoUtils.autoSize(binding.root)
            }
        }


    }


}