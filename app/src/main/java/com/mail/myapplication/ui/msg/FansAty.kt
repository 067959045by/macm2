package com.mail.myapplication.ui.msg

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils

class FansAty : BaseXAty() {

    lateinit var mBinding: AtyFansBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2
    var home = Home()
    var page = 1
    var list = ArrayList<MutableMap<String, String>>()
    var atten_index = -1
    var lar = Lar()
    var type =""
    override fun getLayoutId(): Int = 0

    override fun initView() {
        type = intent.getStringExtra("type").toString()
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        home.a4(page,type,this)
    }

    fun requestData2() {
        home.a4(page,type,this)
    }
    override fun getLayoutView(): View {
        mBinding = AtyFansBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding){
            initTopview2(include.relayTopBg)
            if (type == "fans"){
                include.tvTitle.text = "粉絲"
            }else{
                include.tvTitle.text = "關注"
            }

            var mLayoutManager2 = GridLayoutManager(this@FansAty,1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager =mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page =1
                    requestData2()
                }

                override fun loadMoreStart() {
                    page++
                    requestData2()
                }

            })

            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }

            })


        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type!!.startsWith("attention")){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                if (atten_index == -1)return
                var index =atten_index
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data =JSONUtils.parseKeyAndValueToMap(str)
                var mapList = list[index]
                mapList["is_follow"] =map_data["status"]!!
                list[index]=mapList
                mAdapter2.notifyItemChanged(index)
                if (map_data["status"]=="1"){
                    showToastS("已关注")
                }else{
                    showToastS("取消关注")
                }
                atten_index = -1

            }else{
                showToastS(map["message"])
            }
        }

        if (type== "fans/lists"){

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if(map["code"]=="200"){
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                if (page == 1){
                    list.clear()
                    list.addAll(mList)

                }else{
                    list.addAll(mList)
                }

                if (page==1&&mList.size==0){
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                }else{
                    if (mList!=null&&mList.size>0){
                        mAdapter2?.notifyDataSetChanged()
                    }
                }
            }else{
                if (page==1){
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type== "fans/lists"){
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            if (page==1&&list.size==0){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
    }

    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemFansBinding.inflate(LayoutInflater.from(this@FansAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        var map = list[position]
                        var map_user = map

                        var maxW_head = AutoUtils.getPercentWidthSizeBigger(200)
                        ImageLoader.loadImageAes(this@FansAty, map_user["avatar"], ivHead,maxW_head,maxW_head)
                        tvName.text = map_user["nick"]
                        if (map_user["is_follow"] == "1") {
                            imgvAtten.setImageResource(R.mipmap.ic_66)
                        } else {
                            imgvAtten.setImageResource(R.mipmap.ic_10)
                        }

                        tvContent.text = list[position]["intro"]

                        ivHead.setOnClickListener {
                            var bundle = Bundle()
                            bundle.putString("user_id",map_user["code"])
                            startActivity(PersonOtherDetailsAty::class.java,bundle)
                        }

                        MyUtils3.setVipLevel(map_user["vip_level"], mBinding.imgvVipLevel,0)

                        when (map_user["gender"]) {
                            "1" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
                            }
                            "0" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
                            }
                            "10" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
                            }
                            else -> {
                                mBinding.imgvGender.visibility = View.GONE
                            }
                        }

                        if (map_user["is_creator"]=="1"){
                            mBinding.imgvCreator.visibility = View.VISIBLE
                        }else{
                            mBinding.imgvCreator.visibility = View.GONE
                        }


                        imgvAtten.setOnClickListener {
                            atten_index = position
                            startProgressDialog()
                            lar.b9(map_user["code"]!!,this@FansAty)
                        }

                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemFansBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFansBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }




}