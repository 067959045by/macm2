package com.mail.myapplication.ui.home

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.MyUtils2
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil
import java.lang.StringBuilder

class Search2Aty : BaseXAty() {

    lateinit var mBinding: AtySearch2Binding
    lateinit var mAdapter: GoldRecyclerAdapter
    lateinit var mAdapter2: GoldRecyclerAdapter2
    lateinit var adapterLog: GoldRecyclerAdapter1
    lateinit var adapterMohu: GoldRecyclerAdapter3

    var home = Home()
    var type_page = 0
    var list_user = ArrayList<MutableMap<String,String>>()
    var list_title = ArrayList<String>()
    var list_mohu = ArrayList<String>()
    var list_log = ArrayList<String>()
    var list_log_all = ArrayList<String>()

    var info_code =""

    override fun getLayoutId(): Int = 0

    override fun initView() {
        info_code = MyUtils.getInfoDetails(this,"info_code")

    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        home.a20(this)
        home.a21(this)
    }

    fun requestKeywords(){
        startProgressDialog()
        home.a21(this)
    }
    fun requestMohu(){
        home.a41(mBinding.editSearch.text.toString(),this)
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        stopProgressDialog()

        if (type =="search/elastic"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var list = JSONUtils.parseKeyAndValueToListString(str)
                if (list!=null&&list.size>0){
                    this.list_mohu.clear()
                    this.list_mohu.addAll(list)
                    adapterMohu.notifyDataSetChanged()
                    if (!TextUtils.isEmpty(mBinding.editSearch.text.toString())){
                        mBinding.linlayMohu.visibility = View.VISIBLE
                        mBinding.linlayLog.visibility = View.GONE
                        mBinding.linlayUser.visibility = View.GONE


                    }
                }
            }

        }

        if (type == "user/list"){
            type_page++
            if (type_page>=2){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                list_user = JSONUtils.parseKeyAndValueToMapList(str)
                mAdapter.notifyDataSetChanged()
            }

        }

        if (type == "keywords/list"){
            if (type_page>=2){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToListString(str)
                if (mList.size>0){
                    list_title.clear()
                    list_title.addAll(mList)
                    mAdapter2.notifyDataSetChanged()
                }

            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)

        if (type == "user/list"){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }

        if (type == "keywords/list"){
            if (type_page<2){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }
        }
    }

    override fun getLayoutView(): View {
        mBinding = AtySearch2Binding.inflate(layoutInflater);
        return mBinding.root
    }

    fun initLog(){

        var mLayoutManager = LinearLayoutManager(this@Search2Aty)
        mLayoutManager.orientation = RecyclerView.VERTICAL
        mBinding.recyclerviewLog.layoutManager =mLayoutManager
        adapterLog = GoldRecyclerAdapter1(this)
        mBinding.recyclerviewLog?.isNestedScrollingEnabled = false
        mBinding.recyclerviewLog.adapter = adapterLog

        var search_log =PreferencesUtils.getString(this,"search_log","")

        if (!TextUtils.isEmpty(search_log)){

            if (search_log.contains(",")){
                LogUtil.e("search_search_loglog====="+search_log)
                list_log_all = (search_log.split(",").toList()) as ArrayList<String>
            }else{
                list_log_all.clear()
                list_log_all.add(search_log)
            }

            list_log.clear()
            for (i in list_log_all.indices) {
                if (i < 2) {
                    list_log.add(list_log_all[i])
                }
            }

            LogUtil.e("search_log2====="+list_log.toString())
            adapterLog?.notifyDataSetChanged()
        }
    }

    fun initMohu(){

        var mLayoutManager = LinearLayoutManager(this@Search2Aty)
        mLayoutManager.orientation = RecyclerView.VERTICAL
        mBinding.recyclerviewMohu.layoutManager =mLayoutManager
        adapterMohu = GoldRecyclerAdapter3(this)
        mBinding.recyclerviewMohu?.isNestedScrollingEnabled = false
        mBinding.recyclerviewMohu.adapter = adapterMohu

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initLog()
        initMohu()
        with(mBinding){

            var mLayoutManager = LinearLayoutManager(this@Search2Aty)
            mLayoutManager.orientation = RecyclerView.HORIZONTAL
            recyclerview.layoutManager =mLayoutManager
            mAdapter = GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter
            recyclerview?.isNestedScrollingEnabled = false


            var mLayoutManager2 = LinearLayoutManager(this@Search2Aty)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager =mLayoutManager2
            recyclerview2?.isNestedScrollingEnabled = false
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            val ft = supportFragmentManager.beginTransaction()
            var frg = HomeListFrg.create("search_recommend")
            ft.add(R.id.fralay_content,frg,"HomeListFrg.search_recommend")
            ft.commitAllowingStateLoss()

            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }
            })

            editSearch.clearFocus()
            editSearch.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun afterTextChanged(p0: Editable?) {
                    if (TextUtils.isEmpty(editSearch.text.toString())){

                        linlayLog.visibility = View.GONE
                        linlayUser.visibility = View.VISIBLE

                        list_log.clear()
                        for (i in list_log_all.indices) {
                            if (i < 2) {
                                list_log.add(list_log_all[i])
                            }
                        }
                        adapterLog.notifyDataSetChanged()
                        editSearch.clearFocus()
                    }else{
                        requestMohu()

//                        if (list_log.size >0){
//                            linlayLog.visibility = View.VISIBLE
//                            linlayUser.visibility = View.GONE
//                            tvAllDelLog.text = "全部搜索記錄"
//                        }

                    }
                }
            })

            editSearch.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    MyUtils3.showInput(editSearch,this@Search2Aty)

                    if (list_log.size >0){
                        linlayLog.visibility = View.VISIBLE
                        linlayUser.visibility = View.GONE
                        linlayMohu.visibility = View.GONE
                        tvAllDelLog.text = "全部搜索記錄"

                    }

                } else {
                    MyUtils3.hideInput(this@Search2Aty)
                    linlayLog.visibility = View.GONE
                    linlayMohu.visibility = View.GONE
                    linlayUser.visibility = View.VISIBLE
                }
            }

//            var list =  ArrayList<String>()
//            list.add("search_recommend")
//            viewPager.adapter = HomeListAdataper(supportFragmentManager, this@Search2Aty.lifecycle, list)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (list_log_all.size > 0) {
            var stringBuilder = StringBuilder()
            for (i in list_log_all.indices) {
                if (i!=0){
                    stringBuilder.append(",")
                }
                stringBuilder.append(list_log_all[i])
            }
            PreferencesUtils.putString(this,"search_log", stringBuilder.toString())
        }else{
            PreferencesUtils.putString(this,"search_log", "")
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {

                if (mBinding.linlayLog.visibility ==View.VISIBLE||mBinding.linlayMohu.visibility ==View.VISIBLE){
                    mBinding.editSearch.setText("")
                    mBinding.linlayLog.visibility = View.GONE
                    mBinding.linlayMohu.visibility = View.GONE
                    mBinding.linlayUser.visibility = View.VISIBLE
                }else{
                   finish()
                }
            }
            R.id.tv_search -> {
                if (TextUtils.isEmpty(mBinding.editSearch.text.toString())) {
                    showToastS("请输入搜索内容")
                    return
                }
                if (!list_log_all.contains(mBinding.editSearch.text.toString())) {
                    list_log_all.add(mBinding.editSearch.text.toString())
                    adapterLog?.notifyDataSetChanged()
                }

                var bundle = Bundle()
                bundle.putString("keywords", mBinding.editSearch.text.toString())
                startActivity(SearchResultAty::class.java, bundle)
                this@Search2Aty.mBinding.editSearch.setText("")

            }

            R.id.tv_refresh ->{
                requestKeywords()
            }

            R.id.tv_all_del_log ->{

                if (mBinding.tvAllDelLog.text.contains("清除")){
                    list_log_all.clear()
                    list_log.clear()
                    adapterLog?.notifyDataSetChanged()

                    mBinding.linlayLog.visibility = View.GONE
                    mBinding.linlayUser.visibility = View.VISIBLE

                }else{

                    list_log.clear()
                    list_log.addAll(list_log_all)
                    adapterLog?.notifyDataSetChanged()
                    mBinding.tvAllDelLog.text = "清除搜索紀錄"
                }

            }
        }
    }

    override fun onBackPressed() {
        if (mBinding.linlayLog.visibility ==View.VISIBLE||mBinding.linlayMohu.visibility ==View.VISIBLE){
            mBinding.editSearch.setText("")
            mBinding.linlayLog.visibility = View.GONE
            mBinding.linlayMohu.visibility = View.GONE
            mBinding.linlayUser.visibility = View.VISIBLE
        }else{
            super.onBackPressed()
        }
    }

    inner class HomeListAdataper(fa: FragmentManager, lifecycle: Lifecycle, val docs : MutableList<String>) :

        FragmentStateAdapter(fa, lifecycle) {

        override fun getItemCount(): Int =docs.size

        override fun createFragment(position: Int): Fragment = HomeListFrg.create((docs[position]))

    }

    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            return fGoldViewHolder(ItemSearch2Binding.inflate(LayoutInflater.from(this@Search2Aty)))
        }

        override fun getItemCount(): Int = list_user.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        if (position==0){
                            v01.visibility = View.VISIBLE
                        }else{
                            v01.visibility = View.GONE
                        }

                        var maxW_head = AutoUtils.getPercentHeightSizeBigger(300)

                        ImageLoader.loadImageAes(this@Search2Aty,list_user[position]["avatar"],ivHead,maxW_head,maxW_head)
                        tvName.text = list_user[position]["nick"]
                        tvCode.text = list_user[position]["code"]

                        ivHead.setOnClickListener {

                            var bundle = Bundle()
                            if (list_user[position]["code"]==info_code){
                                bundle.putString("typePage","my")
                                startActivity(PersonDetailsAty::class.java,bundle)
                            }else{
                                bundle.putString("user_id",list_user[position]["code"])
                                startActivity(PersonOtherDetailsAty::class.java,bundle)
                            }

//                            var bundle = Bundle()
//                            bundle.putString("user_id",list_user[position]["code"])
//                            startActivity(PersonOtherDetailsAty::class.java,bundle)
                        }

                        when (list_user[position]["gender"]) {
                            "1" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
                            }
                            "0" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
                            }
                            "10" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
                            }
                            else -> {
                                mBinding.imgvGender.visibility = View.INVISIBLE
                            }
                        }
                    }

                }

            }

        }

        inner class fGoldViewHolder(binding: ItemSearch2Binding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemSearch2Binding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }

    inner class GoldRecyclerAdapter1(context: Context) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        private val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(inflater.inflate(R.layout.item_search_log, parent, false))
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    tv_name?.text = list_log[position]

                    imgv_del?.setOnClickListener {
                        list_log.removeAt(position)
                        adapterLog?.notifyDataSetChanged()
                    }

                    itemView.setOnClickListener {
                        var bundle = Bundle()
                        bundle.putString("keywords", list_log[position])
                        startActivity(SearchResultAty::class.java, bundle)
                        this@Search2Aty.mBinding.editSearch.setText("")
                    }
//                    itemView.setOnClickListener {
////                        index = 1
////                        edit_search.setText(list_log[position])
////                        scro_01.visibility = View.GONE
////                        linlay_001.visibility = View.VISIBLE
////                        setSelector(tv_01, list_tv!!, list_v!!)
////                        addFragment(SearchVideoFrg::class.java, edit_search.text.toString())
////                        edit_search.clearFocus()
//                    }
                }

            }
        }

        override fun getItemCount(): Int = list_log.size

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var tv_name: TextView? = null
            var imgv_del: ImageView? = null

            init {
                AutoUtils.autoSize(itemView)
                tv_name = itemView.findViewById(R.id.tv_name)
                imgv_del = itemView.findViewById(R.id.imgv_del)
            }
        }

    }

    inner class GoldRecyclerAdapter2() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(ItemSearch202Binding.inflate(LayoutInflater.from(this@Search2Aty)))
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun getItemCount(): Int = list_title.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder){

                    mBinding.tvName.text = list_title[position]

                    itemView.setOnClickListener {

                        if (!list_log_all.contains( list_title[position])) {
                            list_log_all.add( list_title[position])
                            adapterLog?.notifyDataSetChanged()
                        }

                        var bundle = Bundle()
                        bundle.putString("keywords", list_title[position])
                        startActivity(SearchResultAty::class.java, bundle)
                    }
                }
            }

        }

        inner class fGoldViewHolder(binding: ItemSearch202Binding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemSearch202Binding = binding
            init {
                AutoUtils.autoSize(binding.root)
            }
        }

    }


    inner class GoldRecyclerAdapter3(context: Context) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        private val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(inflater.inflate(R.layout.item_search_mohu, parent, false))
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    tv_name?.text = list_mohu[position]

                    itemView.setOnClickListener {

                        if (!list_log_all.contains(list_mohu[position])) {
                            list_log_all.add(list_mohu[position])
                            adapterLog?.notifyDataSetChanged()
                        }

                        var bundle = Bundle()
                        bundle.putString("keywords", list_mohu[position])
                        startActivity(SearchResultAty::class.java, bundle)
                        this@Search2Aty.mBinding.editSearch.setText("")
                    }
//
                }

            }
        }

        override fun getItemCount(): Int =list_mohu.size

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var tv_name: TextView? = null

            init {
                AutoUtils.autoSize(itemView)
                tv_name = itemView.findViewById(R.id.tv_name)
            }
        }

    }


}