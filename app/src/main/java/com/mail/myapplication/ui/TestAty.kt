package com.mail.myapplication.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.datastore.core.DataStore
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.app.BaseApp
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.zhy.autolayout.utils.AutoUtils
import kotlinx.coroutines.*
import java.util.prefs.Preferences
import kotlin.system.measureTimeMillis

class TestAty : BaseXAty() {


    //    // 创建DataStore
//    private val BaseApp.dataStore: DataStore<Preferences> by createDataStore(
//        name = "settings"
//    )
    suspend fun concurrentSum(): Int = coroutineScope {
//        val one = async { doSomethingUsefulOne() }
//        val two = async { doSomethingUsefulTwo() }
//        one.await() + two.await()
        1
    }

    lateinit var mBinding: AtyFansBinding

    lateinit var mAdapter2: GoldRecyclerAdapter2

    override fun getLayoutId(): Int = 0

    override fun initView() {

        val deferred = (1..1_000_000).map { n ->
            GlobalScope.async {
                n
            }
        }

        runBlocking {
            val sum = deferred.sumOf { it.await().toLong() }
            println("Sum: $sum")
        }

        runBlocking {
            val startTime = System.currentTimeMillis()
            val job = launch(Dispatchers.Default) {
                var nextPrintTime = startTime
                var i = 0
                while (isActive) { // 一个执行计算的循环，只是为了占用 CPU
                    // 每秒打印消息两次
                    if (System.currentTimeMillis() >= nextPrintTime) {
                        println("job: I'm sleeping ${i++} ...")
                        nextPrintTime += 500L
                    }
                }
            }

            delay(1300L) // 等待一段时间
            println("main: I'm tired of waiting!")
            job.cancelAndJoin() // 取消一个作业并且等待它结束
            println("main: Now I can quit.")

        }

        runBlocking {
            withTimeout(1300L) {
                repeat(1000) { i ->
                    println("I'm sleeping $i ...")
                    delay(500L)
                }
            }
//          val time = measureTimeMillis {
//                val one = doSomethingUsefulOne()
//                val two = doSomethingUsefulTwo()
//                println("The answer is ${one + two}")
//            }
//            println("Completed in $time ms")
        }

    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtyFansBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "粉絲"

            var mLayoutManager2 = GridLayoutManager(this@TestAty, 1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager = mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    swipeRefreshLayout.finishLoadmore()
                    swipeRefreshLayout.finishRefreshing()
                }

                override fun loadMoreStart() {
                    swipeRefreshLayout.finishLoadmore()
                    swipeRefreshLayout.finishRefreshing()
                }

            })


        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
        }
    }


    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemFansBinding.inflate(LayoutInflater.from(this@TestAty)))
        }

        override fun getItemCount(): Int = 3

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemFansBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFansBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}