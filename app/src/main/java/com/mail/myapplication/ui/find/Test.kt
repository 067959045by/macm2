package com.mail.myapplication.ui.find

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class Test {


    fun main(){
        println("Start")

// 启动一个协程
        GlobalScope.launch {
            delay(1000)
            println("Hello")
        }

        Thread.sleep(2000) // 等待 2 秒钟
        println("Stop")
    }
}