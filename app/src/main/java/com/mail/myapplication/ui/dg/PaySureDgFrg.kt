package com.mail.myapplication.ui.dg

import android.app.Dialog
import android.os.Bundle
import android.view.*
import com.lzy.okgo.utils.HttpUtils.runOnUiThread
import com.mail.comm.base.BaseAty
import com.mail.comm.base.BaseDgFrg
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.WheelView.OnWheelViewListener
import com.mail.myapplication.R
import com.mail.myapplication.databinding.DgDateSelectBinding
import com.mail.myapplication.databinding.DgPayBinding
import com.mail.myapplication.databinding.DgPaySureBinding
import com.mail.myapplication.databinding.DgRechargeBinding
import com.mail.myapplication.ui.lar.RegisterBirthdayAty
import org.xutils.common.util.LogUtil


class PaySureDgFrg (context: BaseAty) : Dialog(context) {
    private var baseAty = context
    lateinit var mBinding: DgPaySureBinding

    var listener:PaySureListen? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DgPaySureBinding.inflate(layoutInflater);
        setContentView(mBinding.root)
        getWindow()?.setWindowAnimations(R.style.dialogFullscreen2)
        val dialogWindow = window
        dialogWindow!!.setBackgroundDrawable(null)
        dialogWindow.setGravity(Gravity.CENTER)
        setCanceledOnTouchOutside(false)
        val m = baseAty.windowManager
        val d = m.defaultDisplay// 获取屏幕宽、高
        val p = dialogWindow.attributes
        window?.getDecorView()?.setPadding(0, 0, 0, 0)

//        p.height =  (d.height * 1) // 高度设置为屏幕的0.6
        p.width = (d.width * 1) // 宽度设置为屏幕的0.85
        dialogWindow.attributes = p
        with(mBinding){
            tvOk.setOnClickListener {
                if (listener!=null){
                    listener?.surePay()
                }
                dismiss()
            }
            tvCancel.setOnClickListener {
                dismiss()
            }
        }
    }

    interface  PaySureListen{
        fun surePay()
    }

    fun setPaySureListen(listener:PaySureListen){
        this.listener =listener
    }

}