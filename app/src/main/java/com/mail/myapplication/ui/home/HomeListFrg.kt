package com.mail.myapplication.ui.home

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.compose.key
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.gson.Gson
import com.mail.comm.app.AppManager
import com.mail.comm.app.BaseApp
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.utils.TimeUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.FrgHomeListBinding
import com.mail.myapplication.databinding.ItemFrgHome02Binding
import com.mail.myapplication.databinding.ItemFrgHome03Binding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.MainWalletAty
import com.mail.myapplication.ui.dg.*
import com.mail.myapplication.ui.lar.BindPhoneAty
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.msg.BigImgListAty
import com.mail.myapplication.ui.utils.ClickListener
import com.mail.myapplication.ui.utils.MyUtils3
import com.mail.myapplication.ui.utils.NoAlphaItemAnimator
import com.mail.myapplication.ui.video.PlayListAty
import com.mail.myapplication.ui.video.PlayListVieoAty
import com.mail.myapplication.ui.wallet.RechargeAty
import com.mail.myapplication.ui.wallet.ShareAty
import com.zhy.autolayout.AutoLayoutActivity
import com.zhy.autolayout.config.AutoLayoutConifg
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil

class HomeListFrg : BaseXFrg() {

    lateinit var mBinding: FrgHomeListBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2
    var list = ArrayList<MutableMap<String, String>>()
    var home = Home()
    var lar = Lar()
    var page = 1
    var type = ""

    var click_type =""
    var click_index =0
    var click_details_index =0
    var click_id =""
    var atten_index = -1
    var atten_code= ""
    var zan_index = -1
    var user_id =""
    var keywords =""
    var topic_id =""
    var service_info =""
    var index_delete = -1
    var reportDgFrg:ReportDgFrg?=null
    var deletePostFrg: DeletePostDialog?=null
    var deletePostSureFrg: DeletePostSureDialog?=null

    var rechargeDg:RechargeDgFrg?=null
    var rechargeDg2:RechargeDgFrg?=null
    var index_check_phone = 0
    var mBindPhoneDialog: BindPhoneDialog? = null

    override fun getLayoutId(): Int = 0

    override fun initView() {
        service_info = PreferencesUtils.getString(activity,"service_info")
        type = arguments?.getString("type").toString()
        user_id =arguments?.getString("user_id","").toString()
        keywords =arguments?.getString("keywords","").toString()
        topic_id =arguments?.getString("topic_id","").toString()
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData2()
    }

    fun refreshData(){
        mBinding.recyclerview2.scrollToPosition(0)

        mBinding.swipeRefreshLayout.startRefresh()
//        requestData()
    }

    fun refreshDataToTop(){
//        mBinding.swipeRefreshLayout.startRefresh()
//        requestData()
        mBinding.recyclerview2.scrollToPosition(0)

    }

    fun refreshFrgAllAttentionData(status:String,atten_code:String=""){
        for (index in list.indices){
            var mapList = list[index]
            var mapListUser = JSONUtils.parseKeyAndValueToMap(mapList["user"])
            if (atten_code==mapListUser["code"]){
                mapListUser["is_follow"] =status
                val json_user = Gson().toJson(mapListUser)
                mapList["user"] = json_user
                list[index]=mapList
                mAdapter2.notifyItemChanged(index)
            }
        }
//        mAdapter2.notifyDataSetChanged()
    }

    fun requestData2() {
        when (type) {
            "mine_new" -> {
                home.a7(page, "", "1", "", "", "", "",this)
            }
            "mine_recommend" -> {
                home.a7(page, "", "", "", "", "1","", this)
            }

            //用戶喜歡的貼紙
            "mine_like" -> {
                home.a8(page, user_id, this)
            }

            //用戶關注的貼紙
            "mine_addention" -> {
                home.a7(page, "", "", "", "", "", "1",this)
            }
            "mine_my" -> {
                home.a12(user_id,page,  this)
            }
            "mine_buy" -> {
                home.a122(user_id,page,  this)
            }

            "search_recommend" -> {
                home.a22( this)
            }

            "search_keywords" -> {
                home.a7(page, "", "", "", keywords, "", "",this)
            }

            "topic_hot" -> {
                home.a7(page, topic_id, "", "1", keywords, "", "",this)
            }

            "topic_new" -> {
                home.a7(page, topic_id, "1", "", keywords, "", "",this)
            }
        }
    }

    companion object {

        fun create(type: String): HomeListFrg {
            val fragment = HomeListFrg()
            val bundle = Bundle()
            bundle.putString("type", type)
            fragment.arguments = bundle
            return fragment
        }

        fun create2(type: String,user_id:String): HomeListFrg {
            val fragment = HomeListFrg()
            val bundle = Bundle()
            bundle.putString("type", type)
            bundle.putString("user_id", user_id)
            fragment.arguments = bundle
            return fragment
        }


        fun create3(type: String,keywords:String): HomeListFrg {
            val fragment = HomeListFrg()
            val bundle = Bundle()
            bundle.putString("type", type)
            bundle.putString("keywords", keywords)
            fragment.arguments = bundle
            return fragment
        }

        fun create4(type: String,topic_id:String): HomeListFrg {
            val fragment = HomeListFrg()
            val bundle = Bundle()
            bundle.putString("type", type)
            bundle.putString("topic_id", topic_id)
            fragment.arguments = bundle
            return fragment
        }

        fun create5(type: String,topic_id:String,keywords:String): HomeListFrg {
            val fragment = HomeListFrg()
            val bundle = Bundle()
            bundle.putString("type", type)
            bundle.putString("topic_id", topic_id)
            bundle.putString("keywords", keywords)
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode!= AutoLayoutActivity.RESULT_OK){
            return
        }
        when(requestCode){
            100->{
                showToastS("100")
            }
        }
    }

    var viewPP:View?=null

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "post/delete"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                if (index_delete==-1||index_delete>=list.size){
                    return
                }
                list.removeAt(index_delete)
                mAdapter2.notifyItemRemoved(index_delete)
                mAdapter2.notifyItemRangeChanged(index_delete,mAdapter2.itemCount) //刷新被删除数据，以及其后面的数据

                showToastS("删除成功")
            }else{
                showToastS(map["message"])
            }

        }

        if (type == "post/report"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                showToastS("举报成功")
            }else{
                showToastS(map["message"])
            }
        }

        if (type == "post/block"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                showToastS("已屏蔽")
            }else{
                showToastS(map["message"])
            }
        }

        if (type!!.startsWith("post/buy")) {
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var bundle = Bundle()
                bundle.putString("data",str)
                if (click_type=="page_details"){
                    bundle.putString("id",list[click_details_index]["id"])
                    startActivity(PostDetailsAty::class.java,bundle)
                }else{
                    bundle.putString("index", click_index.toString())
                    startActivityForResult(PlayListAty::class.java,bundle,100)
                }

            }else{
                showToastS(map["message"])
            }
        }

        if (type!!.startsWith("post/details") ){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"]=="1004"){
                showToastS(map["message"])
                list.removeAt(click_details_index)
                mAdapter2.notifyItemRemoved(click_details_index)
                mAdapter2.notifyItemRangeChanged(click_details_index,mAdapter2.itemCount) //刷新被删除数据，以及其后面的数据
                return
            }

            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data =JSONUtils.parseKeyAndValueToMap(str)
                var map_data_auth_error = JSONUtils.parseKeyAndValueToMap(map_data["auth_error"])
                var map_data_user = JSONUtils.parseKeyAndValueToMap(map_data["user"])
                if (map_data_auth_error == null){
                    var bundle = Bundle()
                    bundle.putString("data",str)
                    if (click_type=="page_details"){
                        bundle.putString("id",list[click_details_index]["id"])
                        bundle.putString("avatar",map_data_user["avatar"])
                        bundle.putString("name",map_data_user["nick"])
                        startActivity(PostDetailsAty::class.java,bundle)
//                        startOverAnimActivity(PostDetailsAty::class.java,bundle, Pair(viewPP?.findViewById(R.id.ivHead), "details_head"),
//                            Pair(viewPP?.findViewById(R.id.tv_name), "details_name"))
                    }else{
                        bundle.putString("index", click_index.toString())
                        startActivity(PlayListAty::class.java,bundle)
                    }

                }else{

                    when(map_data_auth_error["key"]){
                        //错误码 1001=次数已用完，开通VIP享不限次观,1002=金币帖子查看完整版需支付金币,1003=粉丝帖子仅限粉丝观看
                        "1001"->{
                            if (rechargeDg2 == null) {
                                rechargeDg2 = RechargeDgFrg(activity as BaseAty)
                                rechargeDg2?.setRechargeListen(object : RechargeDgFrg.RechargeListen{
                                    override fun onclik01() {
                                        rechargeDg2?.dismiss()
                                    }

                                    override fun onclik02() {
                                        rechargeDg2?.dismiss()
                                        var bundle = Bundle()
                                        bundle.putString("page_type","pay")
                                        startActivity(MainWalletAty::class.java,bundle)
                                    }

                                })
                            }
                            rechargeDg2?.show()
                            rechargeDg2?.setData(map_data_auth_error!!["info"]!!,"2")
                        }
                        //1002=金币帖子查看完整版需支付金币
                        "1002" -> {
                            if (rechargeDg == null) {
                                rechargeDg = RechargeDgFrg(activity as BaseAty)
                                rechargeDg?.setRechargeListen(object : RechargeDgFrg.RechargeListen{
                                    override fun onclik01() {
                                        rechargeDg?.dismiss()
                                        startActivity(RechargeAty::class.java)
                                    }

                                    override fun onclik02() {
                                        rechargeDg?.dismiss()
                                        startProgressDialog()
                                        lar.b12(this@HomeListFrg.click_id,this@HomeListFrg)
                                    }

                                })
                            }
                            rechargeDg?.show()
                            rechargeDg?.setData(map_data_auth_error!!["info"]!!,"1")
                        }
                        else->{

                        }
                    }
                }

            }else{
                showToastS(map["message"])
            }
        }

        if (type!!.startsWith("follow")||type!!.startsWith("cancel/follow")){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                if (atten_index == -1)return
                var index =atten_index
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data =JSONUtils.parseKeyAndValueToMap(str)
                var mapList = list[index]
                var mapListUser = JSONUtils.parseKeyAndValueToMap(mapList["user"])
                mapListUser["is_follow"] =map_data["status"]
                val json_user = Gson().toJson(mapListUser)
                mapList["user"] = json_user
                list[index]=mapList
                mAdapter2.notifyItemChanged(index,"attention")
                if (map_data["status"]=="1"){
                    showToastS("已关注")
                }else{
                    showToastS("取消关注")
                }
//                refreshFrgAllAttentionData(map_data["status"]!!,atten_code)
                atten_index = -1

            }else{
                showToastS(map["message"])
            }
        }

        if (type=="post/zan"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                if (zan_index == -1)return
                var index =zan_index
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data =JSONUtils.parseKeyAndValueToMap(str)
                var mapList = list[index]
                mapList["is_like"] = map_data["status"]!!
                 var total_zan_num = mapList["total_like"]!!.toInt()
                if (map_data["status"]=="1"){
                    total_zan_num += 1
                }else{
                    total_zan_num -= 1
                    if (total_zan_num<0){
                        total_zan_num = 0
                    }
                }
                mapList["total_like"]= (total_zan_num).toString()
                list[index]=mapList
                mAdapter2.notifyItemChanged(index,"zan")
                zan_index = -1

            }else{
                showToastS(map["message"])
            }
        }

        if (type == "home/list") {

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                var index1 = list.size
                if (page == 1) {
                    list.clear()
                    list.addAll(mList)

                } else {
                    list.addAll(mList)
                }

                if (page == 1 && mList.size == 0) {
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                } else {
                    if(mList!=null&&mList.size>0){
//                        mAdapter2.notifyItemRangeChanged(0,list.size-1)
//                        mAdapter2.notifyDataSetChanged()
                        if (page==1){
                            mAdapter2.notifyDataSetChanged()

                            mBinding.recyclerview2.scrollToPosition(0)
//                            mAdapter2.notifyItemRangeRemoved(0, index1)
//                            mAdapter2.notifyItemRangeChanged(0,list.size) //刷新被删除数据，以及其后面的数据

                        }else{
                            mAdapter2.notifyItemRangeInserted(index1,list.size-1)
                        }
//                        mAdapter2.notifyItemRangeChanged(0,list.size-1); //进行列表全部刷新

                    }
                }
            } else {
                if (page == 1) {
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type == "home/list") {
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            if (page == 1 && list.size == 0) {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            } else {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
    }

    fun showBindPhoneDialog():Boolean{
//        LogUtil.e("ppppp")
        var info_mobile =BaseApp.instance?.getOneMapData("info_mobile")
        var info_vip_level = BaseApp.instance?.getOneMapData("info_vip_level")
//        showToastS(info_mobile+",,"+info_vip_level)
//        LogUtil.e("ppppp3"+info_mobile+",,,"+info_vip_level)
        if (!TextUtils.isEmpty(info_mobile)){
            return true
        }
        if (info_vip_level=="0"||info_vip_level=="20"){
            return true
        }
        if (index_check_phone<5){
            index_check_phone++
            return true
        }

        index_check_phone = 0
        if (mBindPhoneDialog == null) {
            mBindPhoneDialog = BindPhoneDialog(activity as BaseAty)
            mBindPhoneDialog?.setRechargeListen(object : BindPhoneDialog.RechargeListen {
                override fun onclik01() {
                    mBindPhoneDialog?.dismiss()
                }

                override fun onclik02() {
                    mBindPhoneDialog?.dismiss()
                    startActivity(BindPhoneAty::class.java)
                }

            })
        }
        mBindPhoneDialog?.show()
        return  false
    }

    override fun getLayoutView(): View {
        mBinding = FrgHomeListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(mBinding) {

            var mLayoutManager2 = GridLayoutManager(activity, 1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager = mLayoutManager2
//            recyclerview2.itemAnimator = NoAlphaItemAnimator()
//            recyclerview2.itemAnimator?.changeDuration = 0; //防止recyclerView刷新闪屏
//            (recyclerview2.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.setItemViewCacheSize(200)
            recyclerview2.setHasFixedSize(true)
            recyclerview2.adapter = mAdapter2

//            recyclerview2.itemAnimator?.changeDuration = 0; //防止recyclerView刷新闪屏
//            (recyclerview2.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
//            recyclerview2.itemAnimator = NoAlphaItemAnimator()
            if (type == "search_recommend"){
                swipeRefreshLayout.setEnableLoadmore(false)
                swipeRefreshLayout.setEnableRefresh(false)
                recyclerview2?.isNestedScrollingEnabled = false
            }else{
                swipeRefreshLayout.setEnableLoadmore(true)
                swipeRefreshLayout.setEnableRefresh(true)
                recyclerview2?.isNestedScrollingEnabled = true
                recyclerview2?.isNestedScrollingEnabled = true
            }

            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page = 1
                    requestData2()
                }

                override fun loadMoreStart() {
                    page++
                    requestData2()
                }

            })


            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    requestData()
                }
            })

        }

    }


    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            return fGoldViewHolder(ItemFrgHome02Binding.inflate(LayoutInflater.from(activity)))
        }

        override fun getItemCount(): Int = list.size

//        override fun getItemId(position: Int): Long {
//            return  position.toLong()
//        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {

            if (payloads.isEmpty()){
                super.onBindViewHolder(holder, position, payloads)
            }else{

                if (holder is fGoldViewHolder) {

                    with(holder) {
                        with(mBinding) {
                            var map = list[position]
                            var map_user = JSONUtils.parseKeyAndValueToMap(map["user"])

                            if (map["is_like"] == "1") {
                                imgvZan.setImageResource(R.mipmap.ic_74)
                            } else {
                                imgvZan.setImageResource(R.mipmap.ic_12)
                            }

                            tvZanNum.text = map["total_like"]

                            if (map_user["is_follow"] == "1") {
                                imgvAtten.setImageResource(R.mipmap.ic_66)
                            } else {
                                imgvAtten.setImageResource(R.mipmap.ic_10)
                            }
                        }
                    }
                }
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, @SuppressLint("RecyclerView") position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        ViewCompat.setTransitionName(itemView, "1")

                        var map = list[position]
                        var map_user = JSONUtils.parseKeyAndValueToMap(map["user"])
                        var list_img_video = JSONUtils.parseKeyAndValueToMapList(map["img_data"])
                        var info_code = MyUtils.getInfoDetails(activity!!,"info_code")

//                        LogUtil.e("list_img_video=" + list_img_video.toString())
//                        LogUtil.e("list_content=" + map["content"])

                        var maxW = AutoUtils.getPercentWidthSizeBigger(200)

                        ImageLoader.loadImageAes(activity, map_user["avatar"], ivHead,maxW,maxW)
                        tvName.text = map_user["nick"]
                        if (map_user["is_follow"] == "1") {
                            imgvAtten.setImageResource(R.mipmap.ic_66)
                        } else {
                            imgvAtten.setImageResource(R.mipmap.ic_10)
                        }

                        if (this@HomeListFrg.type=="mine_my"){
                            imgvAtten.visibility = View.GONE
                        }

                        if (map["is_like"] == "1") {
                            imgvZan.setImageResource(R.mipmap.ic_74)
                        } else {
                            imgvZan.setImageResource(R.mipmap.ic_12)
                        }

                        linlayZan.setOnClickListener {
                            if (!MyUtils3.isFastClick()){
                                return@setOnClickListener
                            }
                            zan_index = position
                            startProgressDialog()
                            home.a15(map["id"]!!,this@HomeListFrg)
                        }

                        linlayShare.setOnClickListener {
                            if (!MyUtils3.isFastClick()){
                                return@setOnClickListener
                            }
                            var bundle = Bundle()
                            bundle.putString("id",map["id"])
                            startActivity(ShareAty::class.java,bundle)
                        }

                        ivHead.setOnClickListener {
                            if (!MyUtils3.isFastClick()){
                                return@setOnClickListener
                            }
                            var bundle = Bundle()
                            if (map["user_code"]==info_code){
                                bundle.putString("typePage","my")
                                startActivity(PersonDetailsAty::class.java,bundle)
                            }else{
                                bundle.putString("user_id",map["user_code"])
                                startActivity(PersonOtherDetailsAty::class.java,bundle)
                            }
                        }

                        if (this@HomeListFrg.type == "mine_my"&&map_user["code"] == info_code){
                            linlayStatus.visibility = View.VISIBLE

                            when(map["status"]){

                                "0"->{
                                    tvStatus.text = "不通过"
                                    tvStatus.setTextColor(Color.parseColor("#aaff0000"))
                                    if (!TextUtils.isEmpty(map["refuse_content"])){
                                         tvRefuseContent.text = "(${map["refuse_content"]})"
                                         tvRefuseContent.visibility = View.VISIBLE
                                    }

                                }

                                "1"->{
                                    tvStatus.text = "已发布"
                                    tvStatus.setTextColor(activity!!.resources.getColor(R.color.main_01))
                                }

                                "2"->{
                                    tvStatus.text = "审核中"
                                    tvStatus.setTextColor(Color.parseColor("#FFAD03"))
                                }
                            }
                        }

                        MyUtils3.setVipLevel(map_user["vip_level"], mBinding.imgvVipLevel,position)

                        when (map_user["gender"]) {
                            "1" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
                            }
                            "0" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
                            }
                            "10" -> {
                                mBinding.imgvGender.visibility = View.VISIBLE
                                mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
                            }
                            else -> {
                                mBinding.imgvGender.visibility = View.GONE
                            }
                        }

                        if (map_user["is_creator"]=="1"){
                            mBinding.imgvCreator.visibility = View.VISIBLE
                        }else{
                            mBinding.imgvCreator.visibility = View.GONE
                        }

                        imgvAtten.setOnClickListener {
                            var map_user = JSONUtils.parseKeyAndValueToMap(list[position]["user"])
                            atten_index = position
                            atten_code = map_user["code"]!!
                            startProgressDialog()
                            if (map_user["is_follow"] == "1") {
                                lar.b20(map_user["code"]!!, this@HomeListFrg)
                            }else{
                                lar.b19(map_user["code"]!!, this@HomeListFrg)
                            }
                        }

                        if (map["content"]!!.contains("[")) {
                            var listContent = JSONUtils.parseKeyAndValueToMapList(map["content"])
                            var str_list = ArrayList<String>();
                            var color_list = ArrayList<Int>()
                            var text_size_list = ArrayList<Float>()
                            for (i in listContent.indices) {

                                var sp = ""
                                if (i!=0){
                                    sp = "  "
                                }

                                text_size_list.add(AutoUtils.getPercentHeightSize(40).toFloat())

                                when (listContent[i]["type"]) {
                                    "" -> {
                                        color_list.add(Color.parseColor(resources.getString(R.string.main_04)))
                                        str_list.add(sp+listContent[i]["content"]!!)
                                    }
                                    "code" -> {
                                        color_list.add(Color.parseColor(resources.getString(R.string.main_05)))
                                        str_list.add(sp+"@" + listContent[i]["content"]!!)
                                    }
                                    "topic" -> {
                                        color_list.add(Color.parseColor(resources.getString(R.string.main_03)))
                                        str_list.add(sp+"#" + listContent[i]["content"]!!)
                                    }
                                    "link" -> {
                                        color_list.add(Color.YELLOW)
                                        str_list.add(sp+""+listContent[i]["content"]!!)
                                    }
                                }
                            }

                            MyUtils3.setText(activity, tvContent, str_list, color_list, text_size_list, object : ClickListener {
                                    override fun click(position: Int) {

                                        when(listContent[position]["type"]){
                                            "" ->{
                                                itemView.performClick()
                                            }
                                            "code"->{
                                                if (!MyUtils3.isFastClick()){
                                                    return
                                                }
                                                var bundle = Bundle()
                                                bundle.putString("user_id",listContent[position]["related"])
                                                startActivity(PersonOtherDetailsAty::class.java,bundle)
                                            }
                                            "topic"->{
                                                if (!MyUtils3.isFastClick()){
                                                    return
                                                }
                                                var bundle = Bundle()
                                                bundle.putString("id",listContent[position]["related"])
                                                bundle.putString("topic",listContent[position]["content"])
                                                startActivity(HistoryActiveListAty::class.java,bundle)
                                            }
                                        }

                                    }
                                })

                        } else {
                            tvContent?.text = map["content"]
                        }

                        LogUtil.e("tvContentlength="+tvContent.text.toString().length)
                        if (tvContent.text.toString().length>=60){
                            tvContentMore.visibility = View.VISIBLE
                            tvContentMore01.visibility = View.VISIBLE
                        }

                        if (TextUtils.isEmpty(map["content"])){
                            tvContent.visibility = View.GONE
                        }else{
                            tvContent.visibility = View.VISIBLE
                        }
                        tvZanNum.text = map["total_like"]
                        tvCommentNum.text = map["total_comment"]
                        tvShareNum.text = map["total_share"]

                        if(map.containsKey("created_at")){
                            var time = (TimeUtils.getBeijinTime().toDouble() - TimeUtils.getTime(map["created_at"]).toDouble())/1000
                            tvTime.text = TimeUtils.formatSecond3(time)
                        }

                        relayReport.setOnClickListener {
                            if (!MyUtils3.isFastClick()){
                                return@setOnClickListener
                            }
                            if (map_user["code"]==info_code){

                                if (deletePostFrg == null){
                                    deletePostFrg = DeletePostDialog(activity as BaseAty)
                                }

                                deletePostFrg?.setRechargeListen(object : DeletePostDialog.RechargeListen {
                                    override fun onclik01() {
                                        deletePostFrg?.dismiss()

                                        if (deletePostSureFrg == null) {
                                            deletePostSureFrg = DeletePostSureDialog(activity as BaseAty)
                                        }

                                        deletePostSureFrg?.setRechargeListen(object :
                                            DeletePostSureDialog.RechargeListen {
                                            override fun onclik01() {
                                                deletePostSureFrg?.dismiss()
                                            }

                                            override fun onclik02() {
                                                deletePostSureFrg?.dismiss()
                                                index_delete = position
                                                startProgressDialog()
                                                home.a31(map["id"]!!, this@HomeListFrg)
                                            }

                                        })
                                        deletePostSureFrg?.show()
                                    }

                                })
                                deletePostFrg?.show()
                                return@setOnClickListener
                            }
                            if (reportDgFrg==null){
                                reportDgFrg = ReportDgFrg(activity as BaseAty)
                            }
                            reportDgFrg?.setRechargeListen(object : ReportDgFrg.RechargeListen{
                                override fun onclik02(type: String, id: String) {
                                    if (type =="1"){
                                        startProgressDialog()
                                        home.a27(map["id"]!!,this@HomeListFrg)
                                    }else{
                                        startProgressDialog()
                                        home.a28("1",map_user["code"]!!,this@HomeListFrg)
                                    }
                                }

                            })
                            reportDgFrg?.show()
                            reportDgFrg?.setData("")
                        }
//                        bundle.putString("id",list[click_details_index]["id"])
//                        startActivity(PostDetailsAty::class.java,bundle)

                        if (map["is_ad"] == "1"){
                            var map_ad = JSONUtils.parseKeyAndValueToMap(map["ad"])
                            linlayAd.visibility = View.VISIBLE

                            var maxW_ad =AutoLayoutConifg.getInstance().screenWidth
                            ImageLoader.loadImageAes(activity,map_ad["cover"],imgvAd,maxW_ad,0)

                            var maxW = AutoUtils.getPercentWidthSizeBigger(200)
                            ImageLoader.loadImageAes(activity,map_ad["product_logo"],imgvAdLog,maxW,maxW)
                            tvAdName.text = map_ad["title"]
                            tvAdNum.text = map_ad["view_num"]+"次下载"

                            if (map_ad["type"] == "1"){
                                imgvAdVideo.visibility = View.GONE
                            }else{
                                imgvAdVideo.visibility = View.VISIBLE
                            }

                            linlayAdLink.setOnClickListener {
                                if (!MyUtils3.isFastClick()){
                                    return@setOnClickListener
                                }
                              MyUtils3.openTowPage(activity as BaseAty,map_ad["link"])
                            }

                            imgvAd.setOnClickListener {
                                if (!MyUtils3.isFastClick()){
                                    return@setOnClickListener
                                }
                                if (map_ad["type"]=="1"){
                                    var list = ArrayList<String>()
                                    list.add(map_ad["cover"]!!)
                                    var bundle = Bundle()
                                    bundle.putStringArrayList("data",list)
                                    startActivity(BigImgListAty::class.java,bundle)
                                }else{

                                    var list = ArrayList<String>()
                                    list.add(map_ad["play"]!!)
                                    var bundle = Bundle()
                                    bundle.putStringArrayList("data",list)
                                    startActivity(PlayListVieoAty::class.java,bundle)
                                }

                            }

                        }else{
                            linlayAd.visibility = View.GONE
                        }

                        itemView.setOnClickListener {

                            if (!MyUtils3.isFastClick()){
                                return@setOnClickListener
                            }

                            if (!showBindPhoneDialog()){
                                return@setOnClickListener
                            }

                            viewPP = itemView
                            click_type ="page_details"
                            click_index = 0
                            click_id = map["id"]!!
                            startProgressDialog()
                            click_details_index = position
                            home.a10(map["id"]!!,this@HomeListFrg)
//                          startActivity(PostDetailsAty::class.java)
                        }

                        recyclerview.visibility = View.VISIBLE

                        when (map["type"]) {
                            "3","4"->{
                                var mLayoutManager2 = GridLayoutManager(activity, 1)
                                mLayoutManager2.orientation = RecyclerView.VERTICAL
                                mBinding.recyclerview.layoutManager = mLayoutManager2
//                                recyclerview.itemAnimator = NoAlphaItemAnimator()
//                                recyclerview.itemAnimator?.changeDuration = 0; //防止recyclerView刷新闪屏
//                                recyclerview?.isNestedScrollingEnabled = false
                                var mAdapter = GoldRecyclerAdapter3(list_img_video,map["id"]!!, map["type"]!!,position,itemView)
//                                mAdapter.setHasStableIds(true)
                                mBinding.recyclerview.adapter = mAdapter
                            }
                            "2" -> {

                                when (list_img_video.size) {
                                    1 -> {
                                        var mAdapter: GoldRecyclerAdapter3? = null
                                        var mLayoutManager2 = GridLayoutManager(activity, 1)
                                        mLayoutManager2.orientation = RecyclerView.VERTICAL
                                        mBinding.recyclerview.layoutManager = mLayoutManager2
//                                        recyclerview.itemAnimator = NoAlphaItemAnimator()
//                                        recyclerview.itemAnimator?.changeDuration = 0; //防止recyclerView刷新闪屏
                                        recyclerview?.isNestedScrollingEnabled = false
                                        mAdapter = GoldRecyclerAdapter3(list_img_video,map["id"]!!,map["type"]!!,position,itemView)
//                                        mAdapter.setHasStableIds(true)

                                        mBinding.recyclerview.adapter = mAdapter
                                    }
                                    2,4 -> {
                                        var mAdapter: GoldRecyclerAdapter3? = null
                                        var mLayoutManager2 = GridLayoutManager(activity, 2)
                                        mLayoutManager2.orientation = RecyclerView.VERTICAL
                                        mBinding.recyclerview.layoutManager = mLayoutManager2
//                                        recyclerview.itemAnimator = NoAlphaItemAnimator()
//                                        recyclerview.itemAnimator?.changeDuration = 0; //防止recyclerView刷新闪屏
                                        recyclerview?.isNestedScrollingEnabled = false
                                        mAdapter = GoldRecyclerAdapter3(list_img_video,map["id"]!!,map["type"]!!,position,itemView)
//                                        mAdapter.setHasStableIds(true)

                                        mBinding.recyclerview.adapter = mAdapter
                                    }
                                    3,5,6,7,8,9->{
                                        var mAdapter: GoldRecyclerAdapter3? = null
                                        var mLayoutManager2 = GridLayoutManager(activity, 3)
                                        mLayoutManager2.orientation = RecyclerView.VERTICAL
                                        mBinding.recyclerview.layoutManager = mLayoutManager2
//                                        recyclerview.itemAnimator = NoAlphaItemAnimator()
//                                        recyclerview.itemAnimator?.changeDuration = 0; //防止recyclerView刷新闪屏
                                        recyclerview?.isNestedScrollingEnabled = false
                                        mAdapter = GoldRecyclerAdapter3(list_img_video,map["id"]!!,map["type"]!!,position,itemView)
//                                        mAdapter.setHasStableIds(true)
                                        mBinding.recyclerview.adapter = mAdapter
                                    }
                                }
                            }
                            else -> {
                                recyclerview.visibility = View.GONE
                            }
                        }

                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemFrgHome02Binding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFrgHome02Binding = binding

            init {
                AutoUtils.autoSize(binding.root)
            }
        }

    }

    inner class GoldRecyclerAdapter3(list: ArrayList<MutableMap<String, String>>,id:String,type_show: String,index_p:Int,viewP:View) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        var viewP:View?=null
        var list = ArrayList<MutableMap<String, String>>()
        var id =""
        var type_show =""
        var index_p = -1

        init {
            this.list = list
            this.id = id
            this.type_show = type_show
            this.index_p = index_p
            this.viewP = viewP
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(ItemFrgHome03Binding.inflate(LayoutInflater.from(context)))
        }

        inner class fGoldViewHolder(binding: ItemFrgHome03Binding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFrgHome03Binding = binding

            init {
                AutoUtils.autoSize(binding.root)
            }
        }

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder){
                    when(list.size){
                        2,4->{
                            var mlayoutParams = mBinding.imgv.layoutParams
                            mlayoutParams.height = AutoUtils.getPercentWidthSize(490)
                            mlayoutParams.width = AutoUtils.getPercentWidthSize(490)
                            mBinding.imgv.layoutParams = mlayoutParams
                            mBinding.imgv.scaleType = ImageView.ScaleType.CENTER_CROP
                        }
                        3,5,6,7,8,9->{
                            var mlayoutParams = mBinding.imgv.layoutParams
                            mlayoutParams.height = AutoUtils.getPercentWidthSize(320)
                            mlayoutParams.width = AutoUtils.getPercentWidthSize(320)
                            mBinding.imgv.layoutParams = mlayoutParams
                            mBinding.imgv.scaleType = ImageView.ScaleType.CENTER_CROP

                        }
                    }

                    if (type_show=="3"||type_show=="4"){
                        mBinding.imgvVideo.visibility = View.VISIBLE
                    }else{
                        mBinding.imgvVideo.visibility = View.GONE
                    }

                    mBinding.imgv.setOnClickListener {
                        if (!MyUtils3.isFastClick()){
                            return@setOnClickListener
                        }

                        if (!showBindPhoneDialog()){
                            return@setOnClickListener
                        }
                        startProgressDialog()
                        click_type ="page_list"
                        click_index = position
                        click_id =id
                        home.a10(id,this@HomeListFrg)
                    }

                    itemView.setOnClickListener {

                        if (!MyUtils3.isFastClick()){
                            return@setOnClickListener
                        }

                        if (!showBindPhoneDialog()){
                            return@setOnClickListener
                        }

                        viewPP = viewP

                        click_type ="page_details"
                        click_index = 0
                        click_id =id
                        startProgressDialog()
                        click_details_index = index_p
                        home.a10(id,this@HomeListFrg)
                    }
//                    var url = "https://nimg.ws.126.net/?url=http%3A%2F%2Fdingyue.ws.126.net%2F2021%2F0114%2Fea09bd73p00qmxkvg008wc000fk00f4m.png&thumbnail=650x2147483647&quality=80&type=jpg"
//                    mBinding.imgv.setImageResource(R.drawable.ib_01)

                    var maxW = AutoLayoutConifg.getInstance().screenWidth
                    var maxH = AutoLayoutConifg.getInstance().screenHeight/2
                    ImageLoader.loadImageAes(activity, list[position]["cover_middle"], mBinding.imgv,maxW,maxH)

//                    ImageLoader.loadImage(activity, url,  mBinding.imgv)
                }

//                with(holder) {
//                    when (list.size) {
//                        1 -> {
////                            var mlayoutParams = mBinding.framlayImgv.layoutParams
////                            mlayoutParams.height = AutoUtils.getPercentWidthSize(1040)
////                            mBinding.framlayImgv.layoutParams = mlayoutParams
//                            //scaleType
//                        }
//                        2 -> {
//                            var mlayoutParams = mBinding.framlayImgv.layoutParams
//                            mlayoutParams.height = AutoUtils.getPercentWidthSize(510)
//                            mlayoutParams.width = AutoUtils.getPercentWidthSize(510)
//                            mBinding.framlayImgv.layoutParams = mlayoutParams
////                            mBinding.imgv.sets
//                            4 -> {
////                            var mlayoutParams = mBinding.framlayImgv.layoutParams
////                            mlayoutParams.height = AutoUtils.getPercentWidthSize(510)
////                            mBinding.framlayImgv.layoutParams = mlayoutParams
//                            }
//                            else -> {
////                            var mlayoutParams = mBinding.framlayImgv.layoutParams
////                            mlayoutParams.height = AutoUtils.getPercentWidthSize(320)
////                            mBinding.framlayImgv.layoutParams = mlayoutParams
//                            }
//                        }
//
////                        with(mBinding) {
////                            ImageLoader.loadImageAes(activity, list[position]["img"], imgv)
////                        }
//
////                                itemView.setOnClickListener {
////
////                            if (position == 0) {
////                                var dg = RechargeDgFrg()
//////                            var dg = RechargeDgFrg()
//////                            var ft = childFragmentManager.beginTransaction()
//////                            ft.setCustomAnimations(R.anim.jump_in,R.anim.jump_out,R.anim.jump_in,R.anim.jump_out);
//////                            ft.add(dg,"RechargeDgFrg");
//////                            ft.show(dg).commit();
////                                dg.show(childFragmentManager, "RechargeDgFrg")
////                                return@setOnClickListener
////                            }
////
////                            startActivity(PlayListAty::class.java)
//
//                        }
//                    }

            }

        }


    }


}