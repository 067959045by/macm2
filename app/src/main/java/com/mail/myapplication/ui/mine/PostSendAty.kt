package com.mail.myapplication.ui.mine

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.lzy.okgo.utils.HttpUtils.runOnUiThread
import com.mail.comm.function.chooseimg.ChooseImgAty
import com.mail.comm.function.choosevideo.ChooseVideoAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.FileSizeUtil
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.MyUtils2
import com.mail.comm.view.load.XLoadTip
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.dg.UploadDialog
import com.mail.myapplication.ui.utils.MyUtils3
import com.sunhapper.spedittool.view.SpEditText
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil


class PostSendAty : BaseXAty() {

    lateinit var mBinding: AtyPostSendBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2
    lateinit var mAdapterPer: GoldRecyclerAdapterPer
    lateinit var mAdapterPost: GoldRecyclerAdapterPost
    var lar = Lar()
    var home = Home()
    var content = ""
    var coins = ""
    var type_post = ""
    var mulit_pic = ""
    var mulit_video = ""
    var topic = ""
    var is_creator = ""
    var list = ArrayList<MutableMap<String, String>>()
    var list_img = ArrayList<String>()
    var list_img_post = ArrayList<String>()
    var list_video = ArrayList<String>()
    var list_video_post = ArrayList<String>()

    var page_per= 1
    var list_per = ArrayList<MutableMap<String, String>>()
    var list_post = ArrayList<MutableMap<String, String>>()
    var mapAttenSearchCheck = HashMap<String, String>()
    var mapTopic= HashMap<String, String>()
    var type_index =0
    var uploadDialog: UploadDialog?=null
    var myTextWatcher:MyTextWatcher?=null
     var canEdit = true
    var topic_num = 0

    override fun getLayoutId(): Int = 0

    override fun initView() {
        if (intent.hasExtra("topic")){
            topic = intent.getStringExtra("topic").toString()
            var topic_id = intent.getStringExtra("topic_id").toString()
            mapTopic[topic] = topic_id
            if (!TextUtils.isEmpty(topic)){
                mBinding.editPost.setText("#")
//                showToastS(topic)
                setEditextQ(topic,"#")
            }
        }

        var map = HashMap<String, String>()
        map["type"] = "add_img"
        var map2 = HashMap<String, String>()
        map2["type"] = "add_video"
        list.add(map)
        list.add(map2)
        list_per.add(HashMap())
        list_post.add(HashMap())

        is_creator = MyUtils.getInfoDetails(this,"info_is_creator")
        myTextWatcher = MyTextWatcher()
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        lar.b13(page_per,this)
        home.a18("",this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyPostSendBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding) {

            var mLayoutManager2 = GridLayoutManager(this@PostSendAty, 3)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager = mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            var mLayoutManagerPer = GridLayoutManager(this@PostSendAty, 1)
            mLayoutManagerPer.orientation = RecyclerView.VERTICAL
            recyclerviewPer.layoutManager = mLayoutManagerPer
            mAdapterPer = GoldRecyclerAdapterPer()
            recyclerviewPer.adapter = mAdapterPer

            var mLayoutManagerPost = GridLayoutManager(this@PostSendAty, 1)
            mLayoutManagerPost.orientation = RecyclerView.VERTICAL
            recyclerviewPost.layoutManager = mLayoutManagerPost
            mAdapterPost = GoldRecyclerAdapterPost()
            recyclerviewPost.adapter = mAdapterPost

            if (is_creator == "1"){
                linlayCoins.visibility = View.VISIBLE
            }

            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }
            })

            editPost.setReactKeys("#");
            editPost.setKeyReactListener { key -> //key被响应的字符,长度为1
                LogUtil.e("setKeyReactListener=:" + key)
            }
            editPost.addTextChangedListener(myTextWatcher)

        }
    }

  inner  class MyTextWatcher : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
      override fun afterTextChanged(editable: Editable) {

          if (canEdit) {
              canEdit = true
              mBinding.editPost.removeTextChangedListener(myTextWatcher)
              var str = mBinding.editPost.text.toString()
              if (TextUtils.isEmpty(str)) {
                  mBinding.recyclerviewPer.visibility = View.GONE
                  mBinding.recyclerview2.visibility = View.VISIBLE
                  mBinding.recyclerviewPost.visibility = View.GONE
                  mBinding.editPost.addTextChangedListener(myTextWatcher)
                  return
              }
              var strEnd = str.substring(str.length - 1)

              if (strEnd == "@") {
                  mBinding.recyclerviewPer.visibility = View.VISIBLE
                  mBinding.recyclerview2.visibility = View.GONE
                  mBinding.recyclerviewPost.visibility = View.GONE
              } else if (strEnd == "#") {
                  mBinding.recyclerviewPer.visibility = View.GONE
                  mBinding.recyclerview2.visibility = View.GONE
                  mBinding.recyclerviewPost.visibility = View.VISIBLE
              } else {
                  setEditextQ2()
                  mBinding.recyclerviewPer.visibility = View.GONE
                  mBinding.recyclerview2.visibility = View.VISIBLE
                  mBinding.recyclerviewPost.visibility = View.GONE
              }
              mBinding.editPost.addTextChangedListener(myTextWatcher)

          }



      }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.tv_left -> {
                finish()
            }
            R.id.tv_right -> {
                content = mBinding.editPost.text.toString()
                if (!TextUtils.isEmpty(content)){
                    content = content.trim()
                }else{
                    content = ""
                }

                coins = mBinding.editCoins.text.toString()
                if (TextUtils.isEmpty(content) && list.size <= 2) {
                    showToastS(resources.getString(R.string.s_01))
                    return
                }
//                var list_post = ArrayList<MutableMap<String, String>>()
                list_img.clear()
                list_img_post.clear()
                list_video.clear()
                list_video_post.clear()

                if (list.size > 2) {
                    for (i in list.indices) {
                        if (list[i]["type"] == "img") {
                            list_img.add(list[i]["path"]!!)
                        }
                        if (list[i]["type"] == "video") {
                            list_video.add(list[i]["path"]!!)
                        }
                    }
                }

                //图文
                if (list_img.size > 0) {
                    type_post = "2"
                }
//                //单独图片
//                if (TextUtils.isEmpty(content) && list_img.size > 0) {
//                    type_post = "2"
//                }
                //视文
                if (list_video.size > 0) {
                    type_post = "3"
                }
//                //单独视频
//                if (TextUtils.isEmpty(content) && list_video.size > 0) {
//                    type_post = "3"
//                }
                //单独文字
                if (!TextUtils.isEmpty(content) && list_img.size == 0 && list_video.size == 0) {
                    type_post = "1"
                }

                // 1单独文字 2单独图片 4单独视频

//                if (!TextUtils.isEmpty(content)) {
//                    var map = HashMap<String, String>()
//                    map["type"] = ""
//                    map["content"] = content
//                    map["related"] = ""
//                    list_post.add(map)
//                }

                val jsonString =getPostJson()

                if (topic_num>3){
                    showToastS("最多添加3个话题")
                    return
                }

                startProgressDialog()

                if (type_post == "1"){
                    lar.b8(jsonString, type_post, mulit_pic, mulit_video, coins,this)
                }else{
                    if (type_post =="2"){
                        startProgressDialog("图片上传中..${1}/${list_img.size}")
                        lar.b72(list_img[0], "img_0_${list_img.size}", this)
                    }else{
                        startProgressDialog("视频上传中..")
                        lar.b72(list_video[0], "video_0_${list_video.size}", this)
                    }
                }
            }
        }
    }


    fun getPostJson():String {
        topic_num = 0
        var list_post = ArrayList<MutableMap<String, String>>()

        var strImg = StringBuffer()
        var strVideo = StringBuffer()
        for (i in list_img_post.indices) {
            if (i != 0) {
                strImg.append(",")
            }
            strImg.append(list_img_post[i])
        }
        mulit_pic = strImg.toString()
        for (i in list_video_post.indices) {
            if (i != 0) {
                strVideo.append(",")
            }
            strVideo.append(list_video_post[i])
        }

        mulit_video = strVideo.toString()

        if (TextUtils.isEmpty(content)) {
            return ""
        }

        var ss = mBinding.editPost.spDatas
        var strE = mBinding.editPost.text.toString()
        if (ss.size > 0) {
            for (i in ss.indices) {
                var cruS = ss[i].start
                var cruE = ss[i].end
                if (i == 0 && cruS != 0) {
                    var s01 = strE.substring(0, cruS)
                    s01 = s01.trim()
                    if (!TextUtils.isEmpty(s01)){
                        var map = HashMap<String, String>()
                        map["content"] = s01
                        map["related"] = ""
                        map["type"] = ""
                        list_post.add(map)
                    }
                }
                if (i != ss.size - 1) {
                    var nextS = ss[i + 1].start
                    var s02 = ss[i].showContent.toString().substring(1, ss[i].showContent.toString().length).trim()
                    var s02_type = ss[i].showContent.toString().substring(0, 1)

                    var map = HashMap<String, String>()

                    LogUtil.e("str==="+s02+","+s02_type)
                    map["content"] = s02
                    if (s02_type.startsWith("@")){
                        map["related"] = mapAttenSearchCheck[s02]!!
                        map["type"]="code"
                    }else{
                        topic_num++
                        if (mapTopic.containsKey(s02)){
                            map["related"] = mapTopic[s02]!!
                        }else{
                            map["related"] = ""
                        }

                        map["type"]="topic"
                    }
                    list_post.add(map)

                    if (cruE != nextS) {
                        var s03 = strE.substring(cruE, nextS)
                        var map = HashMap<String, String>()
                        map["content"] = s03
                        map["related"] = ""
                        map["type"] = ""
                        list_post.add(map)
                    }

                } else {
                    var s02 = ss[i].showContent.toString().substring(1, ss[i].showContent.toString().length).trim()
                    var s02_type = ss[i].showContent.toString().substring(0, 1)

                    LogUtil.e("str==="+s02+","+s02_type)

                    var map = HashMap<String, String>()
                    map["content"] = s02
                    if (s02_type.startsWith("@")){
                        map["related"] = mapAttenSearchCheck[s02]!!
                        map["type"] = "code"
                    }else{
                        topic_num++
                        if (mapTopic.containsKey(s02)){
                            map["related"] = mapTopic[s02]!!
                        }else{
                            map["related"] = ""

                        }
                        map["type"] = "topic"
                    }

                    list_post.add(map)
                    var cruE = ss[i].end

                    if (cruE != strE.length) {
                        var s04 = strE.substring(cruE, strE.length)
                        var map = HashMap<String, String>()
                        map["content"] = s04
                        map["related"] = ""
                        map["type"] = ""
                        list_post.add(map)
                    }
                }
            }
        } else {
            var map = HashMap<String, String>()
            map["content"] = content
            map["type"] = ""
            map["related"] = ""
            list_post.add(map)
        }
//
//        if (!TextUtils.isEmpty(content)) {
//            var map = HashMap<String, String>()
//            map["type"] = ""
//            map["content"] = content
//            map["related"] = ""
//            list_post.add(map)
//        }else{
//            return ""
//        }

//        [{"related":"","type":"","content":"u660eu5e74"},{"related":"110","type":"topic","content":"\u808c\u8089\"}]
        val jsonString = Gson().toJson(list_post)
        LogUtil.e("jsonString==="+jsonString)
        return jsonString

    }

    override fun onLoading(type: String, total: Long, current: Long, isDownloading: Boolean) {
        super.onLoading(type, total, current, isDownloading)

        if (type!!.startsWith("video")){
            runOnUiThread {
                if (isFinishing){
                    return@runOnUiThread
                }
                if (Math.abs(total-current)<50000){
                    startProgressDialog()
                    uploadDialog?.dismiss()
                    uploadDialog = null
                    return@runOnUiThread
                }

                stopProgressDialog()
                var c = FileSizeUtil.FormetFileSize(current)
                var t = FileSizeUtil.FormetFileSize(total)
                if (uploadDialog == null){
                    uploadDialog = UploadDialog(this)
                    uploadDialog?.show()
                }
//                if (!uploadDialog!!.isShowing){
//                    uploadDialog?.show()
//                }
                uploadDialog?.setData("${c}/${t}","视频上传中..")
            }
        }

        if (type!!.startsWith("img")) {
            runOnUiThread {
//
//                var str = type.split("_")
//                var index = str[1].toInt()
//                var indexMax = str[2].toInt()
//
//                if (Math.abs(total-current)<10000&& index+1 == indexMax){
//                    startProgressDialog()
//                    uploadDialog?.dismiss()
//                    uploadDialog = null
//                    return@runOnUiThread
//                }
//
//                stopProgressDialog()
//                var c = FileSizeUtil.FormetFileSize(current)
//                var t = FileSizeUtil.FormetFileSize(total)
//                if (uploadDialog == null){
//                    uploadDialog = UploadDialog(this)
//                }
////                if (!uploadDialog!!.isShowing){
////                    uploadDialog?.show()
////                }
//                uploadDialog?.setData("${c}/${t}","图片上传中..${index+1}/${list_img.size}")
            }
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type=="topic/list"){
            type_index++
            if (type_index>=2){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if(map["code"]=="200"){
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                list_post.clear()
                list_post.addAll(mList)
                list_post.add(HashMap())
                mAdapterPost?.notifyDataSetChanged()
            }else{

            }
        }

        if (type== "friend/list"){
            type_index++
            if (type_index>=2){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if(map["code"]=="200"){
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
//                if (page_per== 1){
//                    list_per.clear()
//                    list_per.addAll(mList)
//
//                }else{
//                    list_per.addAll(mList)
//                }
                if (mList!=null&&mList.size>0){
                    list_per.clear()
                    list_per.addAll(mList)
                    list_per.add(HashMap())
                    mAdapterPer?.notifyDataSetChanged()
                }

//                list_per.addAll(mList)
//                if (page_per==1&&mList.size==0){
//                }else{
//                    mAdapterPer?.notifyDataSetChanged()
//                }
            }else{

            }
        }

        if (type!!.startsWith("img")) {
            var str = type.split("_")
            var index = str[1].toInt()
            var indexMax = str[2].toInt()

            LogUtil.e("img====="+type)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "0") {
                var dataMap = JSONUtils.parseDataToMap(var2)
                var file_path = dataMap["file_path"]!!
                list_img_post.add(file_path)
                if (index == indexMax-1) {
                    stopProgressDialog()
//                    showToastS("上传成功")
                    startProgressDialog()
                    var json = getPostJson()
                    lar.b8(json, type_post, mulit_pic, mulit_video, coins,this)

                } else {
                    startProgressDialog("图片上传中..${index+2}/${list_img.size}")
                    lar.b72(list_img[index + 1], "imgv_${index + 1}_${list_img.size}", this)
                }
            } else {
                showToastS(map["message"])
            }
        }


        if (type!!.startsWith("video")) {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "0") {
                var dataMap = JSONUtils.parseDataToMap(var2)
                var file_path = dataMap["file_path"]!!
                list_video_post.add(file_path)
                stopProgressDialog()
//                showToastS("上传成功")
                var json = getPostJson()
                lar.b8(json, type_post, mulit_pic, mulit_video, coins, this)
            } else {
                showToastS(map["message"])
            }
        }

        if (type == "send/post") {
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
//          LogUtil.e("onComplete2=========" + type+",,,,"+var2)
            if (map["code"] == "200") {
                showToastS("已提交")
                finish()
//                var str = AESCBCCrypt.aesDecrypt(map["data"])
//                LogUtil.e("onComplete3=======" + type+",,,,"+ str)
            } else {
                showToastS(map["message"])
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type == "send/post") {
            showToastS("网络异常")
        }
        if (type!!.startsWith("img")) {
            uploadDialog?.dismiss()
            uploadDialog = null
            showToastS("图片上传失败，请重试")
        }
        if (type!!.startsWith("video")) {
            uploadDialog?.dismiss()
            uploadDialog = null
            showToastS("视频上传失败，请重试")
        }

        if (type=="friend/list"){
//            mBinding.swipeRefreshLayoutPer.finishRefreshing()
//            mBinding.swipeRefreshLayoutPost.finishRefreshing()
//            mBinding.swipeRefreshLayoutPer.finishLoadmore()
//            mBinding.swipeRefreshLayoutPost.finishLoadmore()
            if (page_per==1){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
    }

    fun setEditextQ(key:String,type:String){

        if (!mBinding.editPost.text.toString().contains(key)) {

            var str =mBinding.editPost.text.toString()
            mBinding.editPost.setSelection(str.length);//将光标移至文字末尾

            var strEnd = str.substring(0,str.length - 1)
            var strEnd_type = str.substring(str.length-1,str.length)

            var index= mBinding.editPost.selectionStart;   //获取Edittext光标所在位置
            if (strEnd_type =="@"||strEnd_type=="#"){
                mBinding.editPost.text!!.delete(index-1,index);
            }
//            mBinding.editPost.insertNormalStr()

            mBinding.editPost.setSelection(strEnd.length);//将光标移至文字末尾

            if (type=="@"){
                mBinding.editPost.insertSpecialStr("@" + key + "", false, 0, ForegroundColorSpan(Color.parseColor(resources.getString(R.string.main_05))))
                mBinding.editPost.requestFocus()
            }else{
                mBinding.editPost.insertSpecialStr("#" + key + "", false, 0, ForegroundColorSpan(Color.parseColor(resources.getString(R.string.main_03))))
                mBinding.editPost.requestFocus()
            }
//          showToastS(registrationId)
        }
    }


    fun setEditextQ2() {

        var str = mBinding.editPost.text.toString()
        if (TextUtils.isEmpty(str)){
            return
        }
//        mBinding.editPost.setSelection(str.length);//将光标移至文字末尾

        var strEnd_type = str.substring(str.length - 1, str.length)

        if (strEnd_type != " ") {
            return
        }

        if (strEnd_type.length>1){
            var strEnd_type2 = str.substring(str.length - 2, str.length-1)
            if (strEnd_type == " "&&strEnd_type2 == " ") {
                return
            }
        }

        var index_l = str.lastIndexOf("#")

        if (index_l < 0) {
            return
        }

        var key = str.substring(index_l+1)

        var key2 =key.replace("\\s".toRegex(), "")

        if (TextUtils.isEmpty(key2)){
            return
        }

        var key3 = key.trim()

        if (key3.indexOf(" ") != -1) {
            return
        }

        LogUtil.e("key====="+key)

//      if (key.indexOf(" ")!=-1){
//            return
//      }

        for ( i in 0..key.length){
            var index = mBinding.editPost.selectionStart;   //获取Edittext光标所在位置
            mBinding.editPost.text!!.delete(index - 1, index)
        }

         str = mBinding.editPost.text.toString()
         mBinding.editPost.setSelection(str.length);//将光标移至文字末尾

         mBinding.editPost.insertSpecialStr(
            "#" + key + "",
            false,
            0,
            ForegroundColorSpan(Color.parseColor(resources.getString(R.string.main_03)))
        )
        mBinding.editPost.requestFocus()
    }

    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            return fGoldViewHolder(ItemPostSend01Binding.inflate(LayoutInflater.from(this@PostSendAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        var map = list[position]
                        when (map["type"]) {
                            "add_img" -> {
                                relayAddImg.visibility = View.VISIBLE
                                relayAddVideo.visibility = View.GONE
                                relay.visibility = View.GONE
                            }

                            "add_video" -> {
                                relayAddImg.visibility = View.GONE
                                relayAddVideo.visibility = View.VISIBLE
                                relay.visibility = View.GONE
                                ImageLoader.loadImage(
                                    this@PostSendAty,
                                    list[position]["path"],
                                    imgv
                                )
                            }
                            "img" -> {
                                relayAddImg.visibility = View.GONE
                                relayAddVideo.visibility = View.GONE
                                relay.visibility = View.VISIBLE
                                imgvVideo.visibility = View.GONE
                                ImageLoader.loadImage(
                                    this@PostSendAty,
                                    list[position]["path"],
                                    imgv,false
                                )
                            }
                            "video" -> {
                                relayAddImg.visibility = View.GONE
                                relayAddVideo.visibility = View.GONE
                                relay.visibility = View.VISIBLE
                                imgvVideo.visibility = View.VISIBLE
                                ImageLoader.loadImage(this@PostSendAty, list[position]["path"], imgv,false)
                            }
                            else -> {


                            }
                        }


                        relayAddImg.setOnClickListener {
                            var type =""
                            if (list.size>2){
                               type =list[0]["type"]!!
                            }

                            if (type == "video"){
                                showToastS("视频不能跟图片不能一起发布！")
                                return@setOnClickListener
                            }

                            if (list.size>=11){
                                showToastS("最多上传9张图")
                                return@setOnClickListener
                            }

                            var bundle = Bundle()
                            bundle.putInt("max_num", 9-(list.size-2))
                            bundle.putString("ratio", "0,0")
                            startActivityForResult(ChooseImgAty::class.java, bundle, 300)
                        }

                        relayAddVideo.setOnClickListener {
                            var type =""
                            if (list.size>2){
                                type =list[0]["type"]!!
                            }
                            if (type == "img"){
                                showToastS("视频不能跟图片不能一起发布！")
                                return@setOnClickListener
                            }
                            if (type == "video"){
                                showToastS("最多发布一个视频！")
                                return@setOnClickListener
                            }
                            var bundle = Bundle()
                            startActivityForResult(ChooseVideoAty::class.java, bundle, 400)
                        }

                        imgvCancel.setOnClickListener {
                            list.removeAt(position)
                            notifyDataSetChanged()
                        }

//                        ImageLoader.loadImageAes(this@PostSendAty,list[position]["avatar"],ivHead)
                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemPostSend01Binding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemPostSend01Binding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }

    inner class GoldRecyclerAdapterPer : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemPostSendPerBinding.inflate(LayoutInflater.from(this@PostSendAty)))
        }

        override fun getItemCount(): Int = list_per.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        if (position!=list_per.size-1){
                            var map = list_per[position]
                            var map_user = map

                            var maxW = AutoUtils.getPercentWidthSizeBigger(200)
                            ImageLoader.loadImageAes(this@PostSendAty, map_user["avatar"], ivHead,maxW,maxW)
                            tvName.text = map_user["nick"]
                            if (map_user["is_friend"] == "1") {
                                imgvAtten.setImageResource(R.mipmap.ic_66)
                            } else {
                                imgvAtten.setImageResource(R.mipmap.ic_10)
                            }

                            tvContent.text = list_per[position]["intro"]
                            MyUtils3.setVipLevel(map_user["vip_level"],mBinding.imgvVipLevel,0)

                            when (map_user["gender"]) {
                                "1" -> {
                                    mBinding.imgvGender.visibility = View.VISIBLE
                                    mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
                                }
                                "0" -> {
                                    mBinding.imgvGender.visibility = View.VISIBLE
                                    mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
                                }
                                "10" -> {
                                    mBinding.imgvGender.visibility = View.VISIBLE
                                    mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
                                }
                                else -> {
                                    mBinding.imgvGender.visibility = View.GONE
                                }
                            }

                            if (map_user["is_creator"]=="1"){
                                mBinding.imgvCreator.visibility = View.VISIBLE
                            }else{
                                mBinding.imgvCreator.visibility = View.GONE
                            }

                            relayTop.setOnClickListener {
                                if (position==list_per.size-1)return@setOnClickListener
                                setEditextQ(map_user["nick"]!!,"@")
                                mapAttenSearchCheck[map["nick"]!!]= map["code"]!!
                            }

                        }


                        if (position == list_per.size-1){
                            relayBottom.visibility = View.VISIBLE
                            relayTop.visibility = View.GONE
                        }else{
                            relayBottom.visibility = View.GONE
                            relayTop.visibility = View.VISIBLE
                        }


                        relayBottom.setOnClickListener {
                            startActivityForResult(PostSearchPerAty::class.java,123)
                        }

                    }
                }


            }

        }

        inner class fGoldViewHolder(binding: ItemPostSendPerBinding) : RecyclerView.ViewHolder(binding.root) {

            var mBinding: ItemPostSendPerBinding = binding

            init {
                AutoUtils.autoSize(binding.root)
            }
        }

    }

    inner class GoldRecyclerAdapterPost : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemPostSendPostBinding.inflate(LayoutInflater.from(this@PostSendAty)))
        }

        override fun getItemCount(): Int = list_post.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                        var map = list_post[position]

                        if (position!=list_post.size-1){
                            tvName.text = "#"+list_post[position]["title"]
                        }

                        relayTop.setOnClickListener {
                            setEditextQ(list_post[position]["title"]!!,"#")
                            mapTopic[map["title"]!!] = map["id"]!!
                        }

                        if (position == list_post.size-1){
                            relayBottom.visibility = View.VISIBLE
                            relayTop.visibility = View.GONE
                        }else{
                            relayBottom.visibility = View.GONE
                            relayTop.visibility = View.VISIBLE
                        }

                        relayBottom.setOnClickListener {
                            startActivityForResult(PostSearchTopicAty::class.java,124)
                        }

                    }

                }

            }

        }

        inner class fGoldViewHolder(binding: ItemPostSendPostBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemPostSendPostBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != RESULT_OK) {
            return
        }
        when (requestCode) {

            123->{
                var list = data?.getStringArrayListExtra("data")
                setEditextQ(list!![0],"@")
                mapAttenSearchCheck[list!![0]]= list!![1]

            }

            124->{
                var list = data?.getStringArrayListExtra("data")
                setEditextQ(list!![0],"#")
                mapTopic[list!![0]] = list!![1]

            }

            300 -> {
                var type = data?.getStringExtra("type")

                when (type) {
                    "img_photo" -> {
                        var mlist = data?.getStringArrayListExtra("data")
                        for (index in mlist!!.indices){
                            var map = HashMap<String, String>()
                            map["type"] = "img"
                            map["path"] = mlist!![index]
                            list.add(0, map)
                        }

                        mAdapter2.notifyDataSetChanged()
                    }
                }

            }
            400 -> {
                var path = data?.getStringExtra("data")
                var size = FileSizeUtil.getFileOrFilesSize(path, FileSizeUtil.SIZETYPE_MB)

                if (size >= 200) {
                    showToastS("视频不能大于100M")
                    return
                }
                var map = HashMap<String, String>()
                map["type"] = "video"
                map["path"] = path!!
                list.add(0, map)
                mAdapter2.notifyDataSetChanged()

            }
        }
    }


}