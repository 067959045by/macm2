package com.mail.myapplication.ui.mine

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.zhy.autolayout.utils.AutoUtils

class AttentionAty : BaseXAty() {

    lateinit var mBinding: AtyAttentionBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2

    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtyAttentionBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "關注"

            var mLayoutManager2 = GridLayoutManager(this@AttentionAty,1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager =mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    swipeRefreshLayout.finishLoadmore()
                    swipeRefreshLayout.finishRefreshing()
                }

                override fun loadMoreStart() {
                    swipeRefreshLayout.finishLoadmore()
                    swipeRefreshLayout.finishRefreshing()
                }

            })


        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
        }
    }


    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemAttentionBinding.inflate(LayoutInflater.from(this@AttentionAty)))
        }

        override fun getItemCount(): Int = 3

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {

                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemAttentionBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemAttentionBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }




}