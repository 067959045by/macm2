package com.mail.myapplication.ui.lar

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.zhy.autolayout.utils.AutoUtils

class AreaListAty : BaseXAty() {

    lateinit var mBinding: AtyAreaBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2
    var list = ArrayList<MutableMap<String, String>>()
    var lar = Lar()

    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        lar.b5(this)

    }

    fun requestData2() {
        lar.b5(this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyAreaBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = ""
            var mLayoutManager2 = GridLayoutManager(this@AreaListAty,1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager =mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    requestData2()
                }

                override fun loadMoreStart() {
                }

            })

            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }

            })


        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type== "data"){

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if(map["code"]=="200"){
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var maps =JSONUtils.parseKeyAndValueToMap(str)
                var list2 =JSONUtils.parseKeyAndValueToMapList(maps["country_data"])
                list.clear()
                list.addAll(list2)
                mAdapter2?.notifyDataSetChanged()

            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type== "data"){
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }
    }

    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemAreaBinding.inflate(LayoutInflater.from(this@AreaListAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    mBinding.tvName.text = list[position]["country_name"]
                    mBinding.tvName2.text = list[position]["area_code"]

                    itemView.setOnClickListener {
                        var intent= Intent()
                        intent.putExtra("area_code",list[position]["area_code"])
                        intent.putExtra("country_name",list[position]["country_name"])
                        setResult(RESULT_OK,intent)
                        finish()
                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemAreaBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemAreaBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }




}