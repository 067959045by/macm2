package com.mail.myapplication.ui.wallet

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.dg.CodeDialog
import com.mail.myapplication.ui.utils.MyUtils3
import org.xutils.common.util.LogUtil

class DuiCodeAty : BaseXAty() {

    var home = Home()
    var codeDialog:CodeDialog?=null

    var type ="" //yqm

    lateinit var mBinding: AtyCodeBinding

    override fun getLayoutId(): Int = 0

    override fun initView() {
        if (!intent.hasExtra("type")){
            return
        }
        type = intent.getStringExtra("type").toString()
    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtyCodeBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTranslanteBar()
        setAndroidNativeLightStatusBar(true)

        with(mBinding) {

            if (type=="yqm"){
                tvName.text = "請輸入要綁定的邀請碼"
                tvName2.text = "推廣綁定说明："
                tvName3.visibility = View.GONE
               var info_parent_code = MyUtils.getInfoDetails(this@DuiCodeAty,"info_parent_code")
                if (!TextUtils.isEmpty(info_parent_code)){
                    editCode.setText(info_parent_code)
                    editCode.isEnabled = false
                }
                linlayType02.visibility  =View.GONE
                linlayType03.visibility  =View.GONE
                linlayType01.visibility  =View.VISIBLE

            }else{
                tvName.text = "請輸入VIP兌換碼"
                tvName2.text = "兌換說明"
                tvName3.visibility = View.VISIBLE
                linlayType02.visibility  =View.VISIBLE
                linlayType03.visibility  =View.VISIBLE
                linlayType01.visibility  =View.GONE
            }
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_ok -> {
                if (type == "yqm"){

                    if (TextUtils.isEmpty(mBinding.editCode.text.toString())) {
                        showToastS("请输入邀請碼")
                        return
                    }
                    startProgressDialog()
                    home.a39(mBinding.editCode.text.toString(), this)

                }else{
                    if (TextUtils.isEmpty(mBinding.editCode.text.toString())) {
                        showToastS("请输入兌換碼")
                        return
                    }
                    startProgressDialog()
                    home.a5(mBinding.editCode.text.toString(), this)

                }

            }

        }
    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        stopProgressDialog()


        if (type == "bind/yqm"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                showToastS("绑定成功")
                MyUtils.saveInfoDetails(this,"info_parent_code",mBinding.editCode.text.toString())
                finish()
            } else {
                showToastS(map["message"])
            }
        }

        if (type == "code/exchange"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                showToastS("兑换成功")
            } else {
                showToastS(map["message"])
            }
        }


        if (type == "code/info") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map = JSONUtils.parseKeyAndValueToMap(str)
                if (codeDialog==null){
                    codeDialog = CodeDialog(this)
                    codeDialog?.setRechargeListen(object: CodeDialog.RechargeListen{
                        override fun onclik01() {
                            startProgressDialog()
                            home.a19(mBinding.editCode.text.toString(),this@DuiCodeAty)
                        }
                    })
                }
                codeDialog?.show()
                codeDialog?.setData(map["remark"]!!)

            } else {
                showToastS(map["message"])
            }
        }
    }


    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        showToastS("网络异常")
    }

}