package com.mail.myapplication.ui.msg

import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.app.BaseApp
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyKefuListBinding
import com.mail.myapplication.databinding.ItemKefuBinding
import com.mail.myapplication.databinding.ItemZanListBinding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.home.HistoryActiveListAty
import com.mail.myapplication.ui.home.PostDetailsAty
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.utils.ClickListener
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils

class CustomerServiceAty : BaseXAty() {

    lateinit var mBinding: AtyKefuListBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2
    var home = Home()
    var list = ArrayList<MutableMap<String, String>>()

    var to_user_code = ""
    var to_nick = ""
    var to_avatar = ""

    override fun getLayoutId(): Int = 0

    override fun initView() {
        to_user_code = intent.getStringExtra("code").toString()
        to_nick = intent.getStringExtra("nick").toString()
        to_avatar = intent.getStringExtra("avatar").toString()
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        home.a51( this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyKefuListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "官方客服"

            var mLayoutManager2 = GridLayoutManager(this@CustomerServiceAty,1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager =mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }

            })

        }

    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type == "question/answer") {

            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                list.clear()
                list.addAll(mList)
                mAdapter2?.notifyDataSetChanged()

            } else {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type== "question/answer"){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)

        }
    }
    fun mainClick(v: View) {

        when (v.id) {
            R.id.relay_back -> {
                finish()
            }
            R.id.tv_ok -> {

                var bundle = Bundle()
                bundle.putString("code",to_user_code)
                bundle.putString("nick",to_nick)
                bundle.putString("avatar",to_avatar)
                startActivity(ChatAty::class.java,bundle)
            }

            R.id.tv_ok01 -> {
                var link = BaseApp.instance?.getOneMapData("tg_url")
                if (link==null)return
                MyUtils3.openBrowser(this,link)

            }
        }

    }

    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(ItemKefuBinding.inflate(LayoutInflater.from(this@CustomerServiceAty)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {


                with(holder){

                    with(mBinding){

                        tvNum.text = "Q${position}"
                        tvTitle.text = list[position]["title"]
                        tvContent.text = list[position]["content"]

                    }

                }
            }

        }

        inner class fGoldViewHolder(binding: ItemKefuBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemKefuBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }


}