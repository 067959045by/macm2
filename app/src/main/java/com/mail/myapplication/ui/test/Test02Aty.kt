package com.mail.myapplication.ui.test

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.annotations.SerializedName
//import com.youth.banner.view.Banner
import kotlinx.coroutines.*

class Test02Aty {


//    class BannerViewModel : ViewModel() {
//        val mBannerLiveData = MutableLiveData<List<Banner>>()
//        fun getBanner() {
//            viewModelScope.launch {
//                val res = apiCall { Api.get().getBanner() }
//                if (res.errorCode == 0 && res.data != null) {
//                    mBannerLiveData.postValue(res.data)
//                } else {
//                    // 报错
//                }
//            }
//        }
//
//    }

    suspend fun getBanner() {
        GlobalScope.launch {
            val res = apiCall {
                getBanner2()
            }


//            if (res.errorCode == 0 && res.data != null) {
//                mBannerLiveData.postValue(res.data)
//            } else {
//                // 报错
//            }
        }
    }

    suspend fun getBanner2(): String{
        return ""
    }

    suspend inline fun  apiCall(crossinline call: suspend CoroutineScope.() -> String):String {
        return withContext(Dispatchers.IO) {
            val res: String
            try {
                res = call()
            } catch (e: Throwable) {
                Log.e("ApiCaller", "request error", e)
                // 请求出错，将状态码和消息封装为 ResponseResult
//                return@withContext Test01Aty.ApiException.build(e).toResponse<T>()
                return@withContext ""
            }
//            if (res.code == ApiException.CODE_AUTH_INVALID) {
//                Log.e("ApiCaller", "request auth invalid")
//                // 登录过期，取消协程，跳转登录界面
//                // 省略部分代码
//                cancel()
//            }
            return@withContext ""
        }
    }


    data class ResponseResult<T>(
        @SerializedName("errorCode") var errorCode: Int = -1,
        @SerializedName("errorMsg") var errorMsg: String? = "",
        @SerializedName("data") var data: T? = null
    )

}