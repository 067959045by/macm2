//package com.mail.myapplication.ui.test
//
//import android.os.Bundle
//import android.util.Log
//import androidx.appcompat.app.AppCompatActivity
//import androidx.lifecycle.MutableLiveData
//import androidx.lifecycle.ViewModel
//import androidx.lifecycle.viewModelScope
//import com.google.gson.JsonParseException
//import com.google.gson.annotations.SerializedName
//import com.lzy.okgo.exception.HttpException
//import com.mail.myapplication.R
//import com.youth.banner.view.Banner
//import kotlinx.coroutines.*
//import okhttp3.OkHttpClient
//import okhttp3.Request
//import okhttp3.Response
//import org.apache.http.conn.ConnectTimeoutException
//import org.json.JSONException
//import java.io.IOException
//import java.net.SocketTimeoutException
//import java.net.UnknownHostException
//import java.util.concurrent.TimeUnit
//import kotlin.coroutines.resumeWithException
//
//class Test01Aty : AppCompatActivity(), CoroutineScope by MainScope() {
//
//    private lateinit var client: OkHttpClient
//    private lateinit var builder: OkHttpClient.Builder
//    private val TAG = Test01Aty::class.java.simpleName
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.aty_splash)
//
//        builder = OkHttpClient.Builder()
//            .readTimeout(5000, TimeUnit.MILLISECONDS)
//            .writeTimeout(10000, TimeUnit.MILLISECONDS)
//
//        client = builder
//            .build()
//    }
//
//    private fun normalRequest() {
//        //请求公众号列表
//        val request = Request.Builder()
//            .url("https://wanandroid.com/wxarticle/chapters/json")
//            .build()
//
//        client.newCall(request).enqueue(object : okhttp3.Callback {
//
//            override fun onFailure(call: okhttp3.Call, e: IOException) {
//                Log.d(TAG, "onFailure: ${e.message}")
//            }
//
//            override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
//                val string = response.body()?.string()
//                Log.d(TAG, "正常请求 onResponse: $string")
//                //切换到主线程
//                runOnUiThread {
////                    tvResult.text = "正常请求 onResponse: $string"
//                }
//            }
//        })
//    }
//
//
//    suspend fun okhttp3.Call.awaitResponse(): okhttp3.Response {
//
//        return suspendCancellableCoroutine {
//
//            it.invokeOnCancellation {
//                //当协程被取消的时候，取消网络请求
//                cancel()
//            }
//
//            enqueue(object : okhttp3.Callback {
//                override fun onFailure(call: okhttp3.Call, e: IOException) {
//                    it.resumeWithException(e)
//                }
//
//                override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
//                    it.resume(response)
//                }
//            })
//        }
//    }
//
//    //注释1处
//    val exceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
//        Log.d(TAG, "coroutine: error ${throwable.message}")
//    }
//
//    private fun coroutineRequest() {
//        //请求公众号列表
//        val request1 = Request.Builder()
//            .url("https://wanandroid.com/wxarticle/chapters/json")
//            .build()
//
//        //使用exceptionHandler
//        launch(exceptionHandler) {
//            //注释2处
//            val response = client.newCall(request1).awaitResponse()
//            //注释3处
//            val string = getString(response)
//            //合并两次请求的结果更新UI
////            tvResult.text = "协程请求 onResponse: $string"
//        }
//    }
//
//    private suspend fun getString(response: Response): String {
//        return withContext(Dispatchers.IO) {
//            response.body()?.string() ?: "empty string"
//        }
//    }
//
//    private fun coroutineRequest2() {
//        val request1 = Request.Builder()
//            .url("https://wanandroid.com/wxarticle/chapters/json")
//            .build()
//
//        launch(exceptionHandler) {
//            //第一个网络请求
//            val response1 = client.newCall(request1).awaitResponse()
//            val string1 = getString(response1)
////            val wxArticleResponse = JsonUtilKt.instance.toObject(string1, WxArticleResponse::class.java)
//
////            //第二个网络请求依赖于第一个网络请求结果
////            val firstWxId = wxArticleResponse?.data?.get(0)?.id ?: return@launch
////            //第二个网络请求
////            val request2 = Request.Builder()
////                .url("https://wanandroid.com/wxarticle/list/${firstWxId}/1/json")
////                .build()
////            val response2 = client.newCall(request2).awaitResponse()
////            val string2 = getString(response2)
//
////            tvResult.text = "协程请求 onResponse: ${string2}"
//        }
//    }
//
//
//    data class ResponseResult<T>(
//        @SerializedName("errorCode") var errorCode: Int = -1,
//        @SerializedName("errorMsg") var errorMsg: String? = "",
//        @SerializedName("data") var data: T? = null
//    )
//
//
//    class BannerViewModel : ViewModel() {
//        val mBannerLiveData = MutableLiveData<List<Banner>>()
//        fun getBanner() {
//            viewModelScope.launch {
//                val res = apiCall { Api.get().getBanner() }
//                if (res.errorCode == 0 && res.data != null) {
//                    mBannerLiveData.postValue(res.data)
//                } else {
//                    // 报错
//                }
//            }
//        }
//
//        suspend inline fun <T> apiCall(crossinline call: suspend CoroutineScope.() -> ResponseResult<T>): ResponseResult<T> {
//            return withContext(Dispatchers.IO) {
//                val res: ResponseResult<T>
//                try {
//                    res = call()
//                } catch (e: Throwable) {
//                    Log.e("ApiCaller", "request error", e)
//                    // 请求出错，将状态码和消息封装为 ResponseResult
//                    return@withContext ApiException.build(e).toResponse<T>()
//                }
//                if (res.code == ApiException.CODE_AUTH_INVALID) {
//                    Log.e("ApiCaller", "request auth invalid")
//                    // 登录过期，取消协程，跳转登录界面
//                    // 省略部分代码
//                    cancel()
//                }
//                return@withContext res
//            }
//        }
//    }
//
//
//    // 网络、数据解析错误处理
//    class ApiException(
//        val code: Int,
//        override val message: String?,
//        override val cause: Throwable? = null
//    ) : RuntimeException(message, cause) {
//        companion object {
//            // 网络状态码
//            const val CODE_NET_ERROR = 4000
//            const val CODE_TIMEOUT = 4080
//            const val CODE_JSON_PARSE_ERROR = 4010
//            const val CODE_SERVER_ERROR = 5000
//
//            // 业务状态码
//            const val CODE_AUTH_INVALID = 401
//
//            fun build(e: Throwable): ApiException {
//                return if (e is HttpException) {
//                    ApiException(CODE_NET_ERROR, "网络异常(${e.code()},${e.message()})")
//                } else if (e is UnknownHostException) {
//                    ApiException(CODE_NET_ERROR, "网络连接失败，请检查后再试")
//                } else if (e is ConnectTimeoutException || e is SocketTimeoutException) {
//                    ApiException(CODE_TIMEOUT, "请求超时，请稍后再试")
//                } else if (e is IOException) {
//                    ApiException(CODE_NET_ERROR, "网络异常(${e.message})")
//                } else if (e is JsonParseException || e is JSONException) {
//                    // Json解析失败
//                    ApiException(CODE_JSON_PARSE_ERROR, "数据解析错误，请稍后再试")
//                } else {
//                    ApiException(CODE_SERVER_ERROR, "系统错误(${e.message})")
//                }
//            }
//        }
//
//        fun <T> toResponse(): ResponseResult<T> {
//            return ResponseResult(code, message)
//        }
//    }
//
//
//}