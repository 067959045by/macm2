package com.mail.myapplication.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import com.mail.comm.base.BaseViewModel
import com.mail.comm.base.ConstantModel
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.myapplication.interfaces.Home

class WithdrawViewModel : BaseViewModel() {

    var list :MutableLiveData<ArrayList<MutableMap<String, String>>> = MutableLiveData()
    var home = Home()

    init {
        list.postValue(ArrayList())
    }

    fun requestData(isLoadMore:Boolean = false,isShowLoadVew:Boolean = false) {
        if (isLoadMore){
            page++
        }else{
            page = 1
        }
        if (isShowLoadVew){
            type_load.postValue(ConstantModel.LOADVIEW_LOADING)
        }
        home.a35(page, this)
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        var map = JSONUtils.parseKeyAndValueToMap(var2)

        if (type == "recharge/list") {
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)

                var mList2 = this.list.value

                if (page ==1){
                    mList2?.clear()
                    mList2?.addAll(mList)
                }else{
                    mList2?.addAll(mList)
                }

                if (page==1&&(mList==null||mList.size==0)){
                    type_load.postValue(ConstantModel.LOADVIEW_EMPTY)
                }else{
                    list.postValue(mList2)
                    type_load.postValue(ConstantModel.LOADVIEW_FINISH)
                }

            }else{

                if (page ==1){
                    type_load.postValue(ConstantModel.LOADVIEW_EMPTY)
                }
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type == "recharge/list"){
            if (page ==1){
                type_load.postValue(ConstantModel.LOADVIEW_ERROR)
            }
        }
    }

}

