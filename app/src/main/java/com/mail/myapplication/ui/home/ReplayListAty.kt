package com.mail.myapplication.ui.home

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.TimeUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.mine.PersonOtherDetailsAty
import com.mail.myapplication.ui.mine.PostSearchPerAty
import com.mail.myapplication.ui.utils.ClickListener
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils

class ReplayListAty : BaseXAty() {

    lateinit var mBinding: AtyReplayListBinding
    lateinit var mAdapter: GoldRecyclerAdapter
    var home = Home()
    var page = 1
    var list = ArrayList<MutableMap<String, String>>()
    var id_s = ""
    var parent_id_s = ""
    var parent_id = ""
    var mapAttenSearchCheck = HashMap<String, String>()
    var index_zan = -1
    var info_code = ""


    override fun getLayoutId(): Int = 0

    override fun initView() {
        id_s = intent.getStringExtra("id").toString()
        parent_id_s = intent.getStringExtra("parent_id").toString()
        parent_id = intent.getStringExtra("parent_id").toString()
        info_code = MyUtils.getInfoDetails(this,"info_code")
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData2()
    }

    fun requestData2() {
        home.a132(page, id_s, parent_id_s, this)
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "comment/zan"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                if (index_zan == -1)return
                var index =index_zan
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var map_data =JSONUtils.parseKeyAndValueToMap(str)
                var mapList = list[index]
                mapList["is_like"] = map_data["id"]!!
                var total_zan_num = mapList["total_support"]!!.toInt()
                if (map_data["id"]=="1"){
                    total_zan_num += 1
                }else{
                    total_zan_num -= 1
                    if (total_zan_num<0){
                        total_zan_num = 0
                    }
                }
                mapList["total_support"]= (total_zan_num).toString()
                list[index]=mapList
                mAdapter.notifyItemChanged(index)
                index_zan = -1

            }else{
                showToastS(map["message"])
            }
        }

        if (type == "comment/write") {
            stopProgressDialog()
            mBinding.editChat.hint = "有爱评论，说点好听的～"
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            showToastS(map["message"])
            if (map["code"] == "200") {
                page = 1
                requestData2()
                mBinding.editChat.setText("")
            }
        }

        if (type == "relay/list") {
            with(mBinding) {
                swipeRefreshLayout.finishRefreshing()
                swipeRefreshLayout.finishLoadmore()
                loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                var map = JSONUtils.parseKeyAndValueToMap(var2)
                if (map["code"] == "200") {
                    var str = AESCBCCrypt.aesDecrypt(map["data"])
                     include.tvTitle.text ="${map["total"]}条回复"
//                    tvTotal.text = map["total"] + "条评论"
                    var mList = JSONUtils.parseKeyAndValueToMapList(str)

//                   var  mList = JSONUtils.parseKeyAndValueToMapList(mList2[0]["comment_reply_user_data"])
                    if (page == 1) {
                        list.clear()
                        list.addAll(mList)
                    } else {
                        list.addAll(mList)
                    }

                    if (page == 1 && mList.size == 0) {
                        include.tvTitle.text = "暂无回复"
                        loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                    } else {
                        mAdapter?.notifyDataSetChanged()
                    }
                } else {
                    if (page == 1) {
                        loading.setLoadingTip(XLoadTip.LoadStatus.error)
                    }
                }
            }

        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type == "relay/list") {
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            if (page == 1 && list.size == 0) {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            } else {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
        }
    }


    override fun getLayoutView(): View {
        mBinding = AtyReplayListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusColor(resources.getString(R.string.main_02))

        with(mBinding) {
            initTopview2(include.relayTopBg, resources.getString(R.string.main_02))
            include.tvTitle.text = ""
            loading.setBackgrouneColorsX("#F7F9FA")
            var mLayoutManager = GridLayoutManager(this@ReplayListAty, 1)
            mLayoutManager.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager = mLayoutManager
            mAdapter = GoldRecyclerAdapter(this@ReplayListAty)
            recyclerview.adapter = mAdapter

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page++
                    requestData2()
                }

                override fun loadMoreStart() {
                    page--
                    requestData2()
                }

            })

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    requestData()
                }
            })


        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.imgv_q ->{
                startActivityForResult(PostSearchPerAty::class.java,123)
            }

            R.id.relay_back -> {
                finish()
            }
            R.id.tv_send -> {

                if (TextUtils.isEmpty(mBinding.editChat.text.toString())) {
                    showToastS("回复不能为空！")
                    return
                }
                startProgressDialog()
                var list_post = ArrayList<MutableMap<String, String>>()

                var ss = mBinding.editChat.spDatas
                var strE = mBinding.editChat.text.toString()
                if (ss.size > 0) {
                    for (i in ss.indices) {
                        var cruS = ss[i].start
                        var cruE = ss[i].end
                        if (i == 0 && cruS != 0) {
                            var s01 = strE.substring(0, cruS)
                            var map = HashMap<String, String>()
                            map["content"] = s01
                            map["code"] = ""
                            list_post.add(map)
                        }
                        if (i != ss.size - 1) {
                            var nextS = ss[i + 1].start
                            var s02 = ss[i].showContent.toString()
                                .substring(3, ss[i].showContent.toString().length - 2)
                            var map = HashMap<String, String>()
                            map["content"] = s02
                            map["code"] = mapAttenSearchCheck[s02]!!
                            list_post.add(map)

                            if (cruE != nextS) {
                                var s03 = strE.substring(cruE, nextS)
                                var map = HashMap<String, String>()
                                map["content"] = s03
                                map["code"] = ""
                                list_post.add(map)
                            }

                        } else {
                            var s02 = ss[i].showContent.toString()
                                .substring(3, ss[i].showContent.toString().length - 2)

                            var map = HashMap<String, String>()
                            map["content"] = s02
                            map["code"] = mapAttenSearchCheck[s02]!!
                            list_post.add(map)

                            var cruE = ss[i].end

                            if (cruE != strE.length) {
                                var s04 = strE.substring(cruE, strE.length)
                                var map = HashMap<String, String>()
                                map["content"] = s04
                                map["code"] = ""
                                list_post.add(map)
                            }
                        }
                    }
                } else {
                    var map = HashMap<String, String>()
                    map["content"] = mBinding.editChat.text.toString()
                    map["code"] = ""
                    list_post.add(map)
                }
                val jsonString = Gson().toJson(list_post)
                home.a14(id_s, jsonString, parent_id, this)
            }

        }
    }

    fun setEditextQ(key:String){
        if (!mBinding.editChat.text.toString().contains(key)) {
            mBinding.editChat.insertSpecialStr("  @" + key + "  ", false, 0, ForegroundColorSpan(Color.parseColor(resources.getString(R.string.main_05))));
            mBinding.editChat.requestFocus()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != RESULT_OK) {
            return
        }

        when (requestCode) {
            123->{
                var list = data?.getStringArrayListExtra("data")
                setEditextQ(list!![0])
                mapAttenSearchCheck[list!![0]]= list!![1]
            }

        }
    }

    inner class GoldRecyclerAdapter(context: Context) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {


        private val inflater: LayoutInflater = LayoutInflater.from(context)


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(inflater.inflate(R.layout.item_comment_replay_list, parent, false))
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {

                    var maxW = AutoUtils.getPercentWidthSizeBigger(200)

                    ImageLoader.loadImageAes(this@ReplayListAty, list[position]["avatar"], imgv_head,maxW,maxW)

//                    if (list[position]["parent_user_id"]==id){
//                        tv_name?.text = list[position]["nick"]
//
//                    }else{
//                        tv_name?.text = list[position]["nick"]+" 回复 "+list[position]["parent_user_nick"]
//
//                    }

                    tv_name?.text = list[position]["nick"]+" 回复 "+list[position]["parent_user_nick"]


                    var index =position

                    if (list[index]["is_like"]=="1"){
                        imgv_xin?.setImageResource(R.mipmap.ic_74)
                    }else{
                        imgv_xin?.setImageResource(R.mipmap.ic_12)
                    }

                    if (list[position]["content"]!!.contains("[")){
                        var listContent = JSONUtils.parseKeyAndValueToMapList(list[position]["content"])
                        var str_list =  ArrayList<String>();
                        var color_list =  ArrayList<Int>()
                        var text_size_list =  ArrayList<Float>()

                        for ( i in listContent.indices){
                            var sp = ""
                            if (i!=0){
                                sp = "  "
                            }

                            text_size_list.add(AutoUtils.getPercentHeightSize(40).toFloat())
                            if (TextUtils.isEmpty(listContent[i]["code"])){
                                color_list.add(Color.BLACK)
                                str_list.add(sp + listContent[i]["content"]!! + " ")
                            }else{
                                color_list.add(Color.parseColor(resources.getString(R.string.main_05)))
                                str_list.add(sp+"@" + listContent[i]["content"]!!)
                            }
                        }

                        MyUtils3.setText(this@ReplayListAty, tv_content, str_list, color_list, text_size_list, object :
                            ClickListener {
                            override fun click(position: Int) {

                                if (TextUtils.isEmpty(listContent[position]["code"])) {
                                    this@ReplayListAty.mBinding.editChat.hint = "回复"+list[index]["nick"]
                                    parent_id = list[index]["id"]!!
                                    this@ReplayListAty.mBinding.editChat.requestFocus()
                                    return
                                }

                                var bundle = Bundle()
                                if (listContent[position]["code"]==info_code){
                                    bundle.putString("typePage","my")
                                    startActivity(PersonDetailsAty::class.java,bundle)
                                }else{
                                    bundle.putString("user_id",listContent[position]["code"])
                                    startActivity(PersonOtherDetailsAty::class.java,bundle)
                                }
                            }
                        })

                    }else{
                        tv_content?.text = list[position]["content"]
                    }


                    var time = (TimeUtils.getBeijinTime().toDouble() - TimeUtils.getTime(list[position]["created_at"]).toDouble())/1000
                    tv_time?.text = TimeUtils.formatSecond3(time)+"        回复"

                    tv_num?.text = list[position]["total_support"]

                    if (list[position]["is_author"]=="1"){
                        tv_name2?.visibility = View.VISIBLE
                    }else{
                        tv_name2?.visibility = View.GONE
                    }
                    linlay_zan?.setOnClickListener {
                        index_zan = position
                        startProgressDialog()
                        home?.a16(list[position]["id"]!!, this@ReplayListAty)
                    }

                    tv_time?.setOnClickListener {
                        this@ReplayListAty.mBinding.editChat.hint = "回复"+list[position]["nick"]
                        parent_id = list[position]["id"]!!
                        this@ReplayListAty.mBinding.editChat.requestFocus()
                    }

                    imgv_head?.setOnClickListener {
                        var info_code = MyUtils.getInfoDetails(this@ReplayListAty,"info_code")
                        var bundle = Bundle()
                        if (list[position]["user_id"]==info_code){
                            bundle.putString("typePage","my")
                            startActivity(PersonDetailsAty::class.java,bundle)
                        }else{
                            bundle.putString("user_id",list[position]["user_id"])
                            startActivity(PersonOtherDetailsAty::class.java,bundle)
                        }
//                        var bundle = Bundle()
//                        bundle.putString("userId", list[position]["user_id"])
//                        startActivity(UserCenterActivity::class.java, bundle)
                    }

                    itemView.setOnClickListener {
                        parent_id = "0"
                        this@ReplayListAty.mBinding.editChat.hint = "有爱评论，说点好听的～"
                        this@ReplayListAty.mBinding.editChat.setText("")
                        this@ReplayListAty.mBinding.editChat.clearFocus()
                    }
                }

            }
        }

        override fun getItemCount(): Int = list.size


        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv_head: ImageView? = null
            var imgv_xin: ImageView? = null
            var tv_name: TextView? = null
            var tv_name2: TextView? = null
            var tv_content: TextView? = null
            var tv_time: TextView? = null
            var tv_num: TextView? = null
            var linlay_zan: LinearLayout? = null

            init {
                AutoUtils.autoSize(itemView)
                imgv_head = itemView.findViewById(R.id.imgv_head)
                imgv_xin = itemView.findViewById(R.id.imgv_xin)
                tv_name = itemView.findViewById(R.id.tv_name)
                tv_name2 = itemView.findViewById(R.id.tv_name2)
                tv_content = itemView.findViewById(R.id.tv_content)
                tv_time = itemView.findViewById(R.id.tv_time)
                tv_num = itemView.findViewById(R.id.tv_num)
                linlay_zan = itemView.findViewById(R.id.linlay_zan)
            }
        }

    }


}