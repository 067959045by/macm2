package com.mail.myapplication.ui.lar

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.view.AppCountdown
import com.mail.comm.view.load.XLoadTip
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyRegisterBinding
import com.mail.myapplication.interfaces.Lar
import com.rey.material.widget.Spinner
import org.xutils.common.util.LogUtil
import org.xutils.http.RequestParams
import java.util.ArrayList

class RegisterAty : BaseXAty() {

    private var list =ArrayList<MutableMap<String, String>>()
    lateinit var mBinding: AtyRegisterBinding
    lateinit var apCountdown: AppCountdown
    var lar = Lar()
    var area_code = ""
    var phone_send = ""
    var area_code_send = ""
    var config_data = ""
    var index_check =0

    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        lar.b5(this)
//         config_data = PreferencesUtils.getString(this, "config_data", "")
//        if (TextUtils.isEmpty(config_data)) {
//            startProgressDialog()
//            lar.b5(this)
//        }else{
//            var maps =JSONUtils.parseKeyAndValueToMap(config_data)
//            list =JSONUtils.parseKeyAndValueToMapList(maps["country_data"])
//            initView2()
//        }
    }

    override fun getLayoutView(): View {
        mBinding = AtyRegisterBinding.inflate(layoutInflater);
        return mBinding.root
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding){
            initTopview2(include.relayTopBg)

            apCountdown = AppCountdown.getInstance()
            apCountdown?.play(tvCode)

            editPhone.addTextChangedListener(object: TextWatcher{
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {

                    if (TextUtils.isEmpty(editPhone.text.toString())){
                        relayPhone.setBackgroundResource(R.drawable.shape_5)
                        tvOk.setBackgroundResource(R.drawable.shape_6)
                    }else{
                        relayPhone.setBackgroundResource(R.drawable.shape_7)
                        tvOk.setBackgroundResource(R.drawable.shape_8)
                    }
                }

            })

            spinnerLabel.setOnItemSelectedListener(object : Spinner.OnItemSelectedListener{
                override fun onItemSelected(parent: Spinner?, view: View?, position: Int, id: Long) {
//                    showToastS(position.toString())
                    index_check = position
                }

            })
            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    requestData()
                }

            })
        }

    }

    fun initView2(){
        val items = arrayOfNulls<String>(list.size)
        for (i in list.indices){
            items[i]=list[i]["country_name"]+" "+list[i]["area_code"]
        }
        index_check = 0
        area_code = list[0]["area_code"]!!
        val adapter = ArrayAdapter(this, R.layout.row_spn, items)
        adapter.setDropDownViewResource(R.layout.row_spn_dropdown)
        mBinding.spinnerLabel.adapter = adapter
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type=="data"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
//            stopProgressDialog()
            if (map["code"] == "200") {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)

                var str = AESCBCCrypt.aesDecrypt(map["data"])
                PreferencesUtils.putString(this,"config_data",str)
                var maps =JSONUtils.parseKeyAndValueToMap(str)
                list =JSONUtils.parseKeyAndValueToMapList(maps["country_data"])
                initView2()
            }else{
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }
        }
        if (type=="sendmsg"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            LogUtil.e("========="+var2)
            if (map["code"] == "200") {
                apCountdown.start()
                phone_send = mBinding.editPhone.text.toString()
                area_code_send = area_code
                var bundle = Bundle()
                bundle.putString("phone",phone_send)
                bundle.putString("area_code",area_code_send)
                startActivity(RegisterCodeAty::class.java,bundle)
            }else{
                showToastS(map["message"])
            }

        }
    }


    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type=="sendmsg"){
            stopProgressDialog()
            showToastS("网络异常")

        }
        if (type == "data"){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_ok -> {

                if (TextUtils.isEmpty(mBinding.editPhone.text.toString())){
                    showToastS("手机号不能为空")
                    return
                }

                if (apCountdown.time>0){
//                  showToastS(apCountdown.time.toString()+"s后可以发送")
                    var bundle = Bundle()
                    bundle.putString("phone",phone_send)
                    bundle.putString("area_code",area_code_send)
                    startActivity(RegisterCodeAty::class.java,bundle)
                    return
                }

                startProgressDialog()
                lar.b2(mBinding.editPhone.text.toString(),area_code,this)
//                startActivity(RegisterCodeAty::class.java)
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        apCountdown.reSet()
    }

}