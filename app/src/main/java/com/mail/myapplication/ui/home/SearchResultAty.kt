package com.mail.myapplication.ui.home

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.zhy.autolayout.utils.AutoUtils

class SearchResultAty : BaseXAty() {

    lateinit var mBinding: AtySearchResultBinding
    lateinit var list_tv: Array<TextView>
    lateinit var list_v: Array<View>

    var keywords =""
    override fun getLayoutId(): Int = 0

    override fun initView() {
        keywords = intent.getStringExtra("keywords").toString()
        with(mBinding){
            list_tv = arrayOf(tv01,tv02)
            list_v = arrayOf(v01,v02)
        }
    }

    override fun requestData() {
    }

    private fun setSelector(tv: TextView) {
        for (i in list_tv.indices) {
            if (tv === list_tv[i]) {
                list_tv[i].setTextColor(Color.parseColor("#333438"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗
                list_tv[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentWidthSizeBigger(55).toFloat())
                list_v[i].visibility = View.VISIBLE
            } else {
                list_tv[i].setTextColor(Color.parseColor("#65666D"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));//加粗
                list_tv[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentWidthSizeBigger(45).toFloat())
                list_v[i].visibility = View.INVISIBLE
            }
        }
    }

    override fun getLayoutView(): View {
        mBinding = AtySearchResultBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding) {
            linlay01.performClick()
            var list =  ArrayList<Fragment>()
            list.add(HomeListFrg.create3("search_keywords",keywords))
            list.add(SearchUserFrg.create(keywords))
            viewPager.adapter = HomeListAdataper(supportFragmentManager, this@SearchResultAty.lifecycle, list)
            viewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    when(position){
                        0 -> {
                            setSelector(tv01)
                        }
                        1 -> {
                            setSelector(tv02)
                        }

                    }
                }
            })

            editSearch.text = keywords

        }
    }

    inner class HomeListAdataper(fa: FragmentManager, lifecycle: Lifecycle, val list : MutableList<Fragment>) :

        FragmentStateAdapter(fa, lifecycle) {

        override fun getItemCount(): Int =list.size

        override fun createFragment(position: Int): Fragment = list[position]

    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_top,R.id.relay_back -> {
                finish()
            }
            R.id.linlay_01 -> {
                mBinding.viewPager.setCurrentItem(0,true)
            }
            R.id.linlay_02 -> {
                mBinding.viewPager.setCurrentItem(1,true)

            }
        }
    }


}