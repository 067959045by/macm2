package com.mail.myapplication.ui.msg


import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.mail.comm.app.AppManager
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.ToastUitl
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyChatSetBinding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.ui.dg.ChatLaHeiDgFrg
import com.mail.myapplication.ui.dg.ClearCacheDgFrg
import com.mail.myapplication.ui.dg.QuitLoginDgFrg
import com.mail.myapplication.ui.mine.BlackListAty
import org.xutils.common.util.LogUtil

class ChatSetAty:BaseXAty() {

    lateinit var mBinding: AtyChatSetBinding
    var to_user_code = ""
    var home = Home()

    override fun getLayoutId(): Int = 0

    override fun initView() {
        to_user_code = intent.getStringExtra("to_user_code").toString()
    }

    override fun requestData() {
    }


    override fun getLayoutView(): View {
        mBinding = AtyChatSetBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "聊天設置"
        }
    }

    fun blakFinish(){
        startProgressDialog()
        home.a28("2",to_user_code,this)
//        ToastUitl.showToast(this,"拉黑成功",Toast.LENGTH_SHORT)
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        stopProgressDialog()
        if (type == "post/block"){

            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                showToastS("已拉黑")
                AppManager.getInstance().killActivity(ChatAty::class.java)
                finish()

            }else{
                showToastS(map["message"])
            }

        }
    }


    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        showToastS("网络异常")
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }


            R.id.relay_jubao -> {
                var bundle = Bundle()
                bundle.putString("to_user_code",to_user_code)
                startActivity(ChatReportAty::class.java,bundle)
            }

            R.id.relay_lahei -> {
                var dg = ChatLaHeiDgFrg()
//                startProgressDialog()
//                home.a28("1",map_user["code"]!!,this@HomeListFrg)
                dg.show(supportFragmentManager,"ChatLaHeiDgFrg")
            }

            R.id.relay_private -> {
            }
        }
    }
}