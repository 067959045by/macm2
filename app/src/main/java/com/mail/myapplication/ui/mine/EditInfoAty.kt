package com.mail.myapplication.ui.mine

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.function.chooseimg.ChooseImgAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyEditInfoBinding
import com.mail.myapplication.databinding.ItemRegisterLikeBinding
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.lar.*
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil

class EditInfoAty : BaseXAty() {

    var lar = Lar()

    var info_nick = ""
    var info_intro = ""
    var info_gender = ""
    var info_gender2 = ""
    var info_birth = ""
    var info_avatar = ""
    var info_city = ""
    var info_job = ""
    var info_habit = ""
    var path_head = ""
    var info_mobile = ""
    var list_like = ArrayList<String>()

    lateinit var mBinding: AtyEditInfoBinding

    override fun getLayoutId(): Int = 0

    override fun initView() {}

    override fun requestData() {
        lar.b5(this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyEditInfoBinding.inflate(layoutInflater);
        return mBinding.root
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PreferencesUtils.remove(this,"config_data")

        initMyInfo()
        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvTitle.text = "編輯個人資料"
            include.tvRight.text = "完成"
            include.tvRight.visibility = View.VISIBLE

            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setIsPinContentView(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    requestData()
                }

                override fun loadMoreStart() {
                }

            })
        }


    }


    fun initMyInfo() {
        info_nick = PreferencesUtils.getString(this, "info_nick")
        var info_code = PreferencesUtils.getString(this, "info_code")
        var info_flow = PreferencesUtils.getString(this, "info_flow")
        var info_fans = PreferencesUtils.getString(this, "info_fans")
        info_avatar = PreferencesUtils.getString(this, "info_avatar")
        info_intro = PreferencesUtils.getString(this, "info_intro")
        info_city = PreferencesUtils.getString(this, "info_city")
        var info_years = PreferencesUtils.getString(this, "info_years")
        info_job = PreferencesUtils.getString(this, "info_job")
        info_gender = PreferencesUtils.getString(this, "info_gender")
        info_birth = PreferencesUtils.getString(this, "info_birth")
        info_habit = PreferencesUtils.getString(this, "info_habit")
        info_mobile = PreferencesUtils.getString(this, "info_mobile")
        info_gender2 = info_gender


        var maxW = AutoUtils.getPercentWidthSizeBigger(300)
        ImageLoader.loadImageAes(this, info_avatar, mBinding.ivHead,maxW,maxW)
        mBinding.editName.setText(info_nick)
        mBinding.editSign.setText(info_intro)
        if (!TextUtils.isEmpty(info_birth)){
            mBinding.tvYear.setText(info_birth)
        }
        mBinding.tvJob.setText(info_job)
        mBinding.tvCity.text = info_city

        mBinding.tvPhone.text = info_mobile
        if (TextUtils.isEmpty(info_mobile)){
            mBinding.tvPhoneType.text = "未設置"
            mBinding.imgvPhoneType.visibility = View.VISIBLE
        }else{
            mBinding.tvPhoneType.text = "更換手機號"
            mBinding.imgvPhoneType.visibility = View.GONE
        }
        //1男,2女,3机器人
        initGender()
    }

    fun initGender() {
        var str = info_habit.split(",")

//        LogUtil.e("info_habit=="+str.toString())
//        LogUtil.e("info_habit2=="+info_habit)
        if (str == null || str.size == 0 || info_habit.length == 0 || TextUtils.isEmpty(info_habit)) {
            mBinding.tvLike01.visibility = View.GONE
            mBinding.tvLike02.visibility = View.GONE
            mBinding.tvLike03.visibility = View.GONE
            mBinding.tvLike.visibility = View.VISIBLE
            mBinding.linlayLike.visibility = View.GONE
        } else if (str.size == 1) {
            mBinding.tvLike01.visibility = View.VISIBLE
            mBinding.tvLike02.visibility = View.GONE
            mBinding.tvLike03.visibility = View.GONE
            mBinding.tvLike.visibility = View.GONE
            mBinding.linlayLike.visibility = View.VISIBLE
            mBinding.tvLike01.text = str[0]
        } else if (str.size == 2) {
            mBinding.tvLike01.visibility = View.VISIBLE
            mBinding.tvLike02.visibility = View.VISIBLE
            mBinding.tvLike03.visibility = View.GONE
            mBinding.tvLike.visibility = View.GONE
            mBinding.linlayLike.visibility = View.VISIBLE
            mBinding.tvLike01.text = str[0]
            mBinding.tvLike02.text = str[1]
        } else if (str.size == 3) {
            mBinding.tvLike01.visibility = View.VISIBLE
            mBinding.tvLike02.visibility = View.VISIBLE
            mBinding.tvLike03.visibility = View.VISIBLE
            mBinding.tvLike.visibility = View.GONE
            mBinding.linlayLike.visibility = View.VISIBLE
            mBinding.tvLike01.text = str[0]
            mBinding.tvLike02.text = str[1]
            mBinding.tvLike03.text = str[2]
        }

        when (info_gender) {
            "1" -> {
                mBinding.tvGender01.setBackgroundResource(R.drawable.shape_15)
                mBinding.tvGender02.setBackgroundResource(R.drawable.shape_14)
                mBinding.tvGender03.setBackgroundResource(R.drawable.shape_14)
            }
            "0" -> {
                mBinding.tvGender01.setBackgroundResource(R.drawable.shape_14)
                mBinding.tvGender02.setBackgroundResource(R.drawable.shape_15)
                mBinding.tvGender03.setBackgroundResource(R.drawable.shape_14)
            }
            "10" -> {
                mBinding.tvGender01.setBackgroundResource(R.drawable.shape_14)
                mBinding.tvGender02.setBackgroundResource(R.drawable.shape_14)
                mBinding.tvGender03.setBackgroundResource(R.drawable.shape_15)
            }
            else -> {
                mBinding.tvGender01.setBackgroundResource(R.drawable.shape_14)
                mBinding.tvGender02.setBackgroundResource(R.drawable.shape_14)
                mBinding.tvGender03.setBackgroundResource(R.drawable.shape_14)
            }

        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_right -> {
                info_nick = mBinding.editName.text.toString()
                info_intro = mBinding.editSign.text.toString()
                info_birth = mBinding.tvYear.text.toString()
                info_city = mBinding.tvCity.text.toString()
                info_job = mBinding.tvJob.text.toString()
                startProgressDialog()
                if (TextUtils.isEmpty(path_head)) {
                    lar.b4(
                        info_nick,
                        info_intro,
                        info_gender,
                        info_birth,
                        info_avatar,
                        info_city,
                        info_job,
                        info_habit,
                        "",
                        this
                    )
                } else {
                    lar.b7(path_head, this)
                }
            }

            R.id.relay_gender -> {
                if (info_gender2 != "2") {
                    showToastS("不可更改性别")
                    return
                }
                var bundle = Bundle()
                bundle.putString("type", "edit")
                bundle.putString("info_gender", info_gender)
                startActivityForResult(RegisterSexAty::class.java, bundle, 100)
            }

            R.id.tv_year -> {
                var bundle = Bundle()
                bundle.putString("type", "edit")
                startActivityForResult(RegisterBirthdayAty::class.java, bundle, 200)
            }

            R.id.tv_job -> {
                if (isEmptyConfigData()) return
                var bundle = Bundle()
                bundle.putString("type", "edit")
                startActivityForResult(RegisterJobAty::class.java, bundle, 400)
            }

            R.id.relay_like -> {
                if (isEmptyConfigData()) return
                var bundle = Bundle()
                bundle.putString("type", "edit")
                startActivityForResult(RegisterLikeAty::class.java, bundle, 500)
            }
            R.id.relay_phone -> {
                if (isEmptyConfigData()) return
                var bundle = Bundle()
                startActivity(BindPhoneAty::class.java, bundle)
            }

            R.id.relay_city -> {
                var bundle = Bundle()
                bundle.putString("type", "edit")
                startActivityForResult(CityListAty::class.java, bundle, 600)
            }

            R.id.framlay_head -> {
                var bundle = Bundle()
                bundle.putInt("max_num", 1)
                bundle.putString("ratio", "16,16")
                startActivityForResult(ChooseImgAty::class.java, bundle, 300)
            }
        }
    }

    fun isEmptyConfigData(): Boolean {
        var config_data = PreferencesUtils.getString(this, "config_data", "")
        if (TextUtils.isEmpty(config_data)) {
            startProgressDialog()
            lar.b5(this)
            return true
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != RESULT_OK) {
            return
        }
        when (requestCode) {
            100 -> {
                info_gender = data?.getStringExtra("data")!!
                initGender()
            }
            200 -> {
                info_birth = data?.getStringExtra("data")!!
                info_birth =info_birth.replace("\\s".toRegex(), "")
                mBinding.tvYear.setText(info_birth)
            }
            400 -> {
                info_job = data?.getStringExtra("data")!!
                mBinding.tvJob.setText(info_job)
            }
            500 -> {
                info_habit = data?.getStringExtra("data")!!
                initGender()
            }
            600 -> {
                info_city = data?.getStringExtra("data")!!
                mBinding.tvCity.text = info_city
            }
            300 -> {
                var type = data?.getStringExtra("type")

                when (type) {
                    "img_photo" -> {
                        var list = data?.getStringArrayListExtra("data")
                        path_head = list!![0]
                        ImageLoader.loadImage(this, list!![0], mBinding.ivHead)
                    }
                }

            }
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        mBinding.swipeRefreshLayout.finishRefreshing()
        mBinding.swipeRefreshLayout.finishLoadmore()
        if (type=="data"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            stopProgressDialog()
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                PreferencesUtils.putString(this,"config_data",str)
            }
        }

        if (type == "update") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                saveInfo()
                showToastS("编辑成功")
                finish()
            } else {
                showToastS(map["message"])
            }
        }

        if (type == "file/upload") {
            stopProgressDialog()

            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "0") {
                var dataMap = JSONUtils.parseDataToMap(var2)
                info_avatar = dataMap["url"]!!
                var info_avatar2 = dataMap["file_path"]!!
                lar.b4(
                    info_nick,
                    info_intro,
                    info_gender,
                    info_birth,
                    info_avatar2,
                    info_city,
                    info_job,
                    info_habit,
                    "",
                    this
                )
            } else {
                showToastS(map["message"])
            }
        }
    }

    fun saveInfo() {
        PreferencesUtils.putString(this, "info_nick", info_nick)
        PreferencesUtils.putString(this, "info_avatar", info_avatar)
        PreferencesUtils.putString(this, "info_intro", info_intro)
        PreferencesUtils.putString(this, "info_city", info_city)
        PreferencesUtils.putString(this, "info_job", info_job)
        PreferencesUtils.putString(this, "info_gender", info_gender)
        PreferencesUtils.putString(this, "info_birth", info_birth)
        PreferencesUtils.putString(this, "info_habit", info_habit)
    }


    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        mBinding.swipeRefreshLayout.finishRefreshing()
        mBinding.swipeRefreshLayout.finishLoadmore()
        stopProgressDialog()
        if (type == "update") {
            stopProgressDialog()
            showToastS("网络异常")
        }
    }

    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemRegisterLikeBinding.inflate(LayoutInflater.from(this@EditInfoAty)))
        }

        override fun getItemCount(): Int = list_like.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

            }

        }

        inner class fGoldViewHolder(binding: ItemRegisterLikeBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemRegisterLikeBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }



}