package com.mail.myapplication.ui.video

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultAllocator
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.mail.comm.R
import com.mail.comm.image.ImageLoader
import com.mail.comm.video.exo.CacheDataSourceFactory
import com.mail.comm.view.load.XLoadTip
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.databinding.FrgPlayList2Binding
import com.mail.myapplication.ui.utils.MyUtils3
import org.xutils.common.util.LogUtil

class PlayList2Frg : BaseXFrg() {

    lateinit var mBinding: FrgPlayList2Binding
    var simpleExoPlayer: SimpleExoPlayer? = null
    var img = ""
    var video = ""
    var type = ""

    override fun getLayoutView(): View {
        mBinding = FrgPlayList2Binding.inflate(layoutInflater);
        return mBinding.root
    }

    companion object {
        fun create(img: String, video: String): PlayList2Frg {
            val fragment = PlayList2Frg()
            val bundle = Bundle()
            bundle.putString("img", img)
            bundle.putString("video", video)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        img = arguments?.getString("img").toString()
        video = arguments?.getString("video").toString()
        animation2 = AnimationUtils.loadAnimation(context, R.anim.loading_dialog_rotate)
        animation2?.interpolator = LinearInterpolator()

        if (TextUtils.isEmpty(video)){
            mBinding.relayImg.visibility = View.VISIBLE
            mBinding.flayVideo.visibility = View.GONE
            LogUtil.e("PlayList2Frg img="+img)
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
//            ImageLoader.loadImageAes3(img,mBinding.imgv)

            loadImg()
            mBinding.loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    loadImg()
                }
            })

        }else{

            mBinding
            mBinding.relayImg.visibility = View.GONE
            mBinding.flayVideo.visibility = View.VISIBLE
            initPlayer()
        }

        mBinding.relayDown.setOnClickListener {

            startProgressDialog()
            ImageLoader.loadImageAesCallback(activity,img,object :
                ImageLoader.BimapCallback {
                override fun onLoadSuccess(bitmap: Bitmap?) {
                    stopProgressDialog()
                    MyUtils3.saveBimap(activity,bitmap,".lvban_poster2.jpg")
//                  mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
//                  mBinding.imgv.setImageBitmap(bitmap)
                }

                override fun onLoadFailed() {
                    stopProgressDialog()
//                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }

            })

        }
    }


    fun loadImg(){
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        ImageLoader.loadImageAesCallback(activity,img,object :
            ImageLoader.BimapCallback {
            override fun onLoadSuccess(bitmap: Bitmap?) {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
                mBinding.imgv.setImageBitmap(bitmap)
            }

            override fun onLoadFailed() {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            }

        })
    }

    fun initPlayer() {
        var videoTrackSelectionFactory = AdaptiveTrackSelection.Factory()

        //轨道选择器
        var trackSelector = DefaultTrackSelector(requireActivity(), videoTrackSelectionFactory)

        //用于控制MediaSource何时缓冲更多的媒体资源以及缓冲多少媒体资源
        var loadControl = DefaultLoadControl.Builder()
            .setAllocator(DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE))
            .setBufferDurationsMs(360000, 600000, 1000, 5000)
            .setPrioritizeTimeOverSizeThresholds(false)
            .setTargetBufferBytes(C.LENGTH_UNSET)
            .createDefaultLoadControl()

        //Provides estimates of the currently available bandwidth.
        var bandwidthMeter = DefaultBandwidthMeter.Builder(requireActivity()).build()

        //渲染器，用于渲染媒体文件。
        var renderersFactory = DefaultRenderersFactory(requireActivity())

//        val cache: Cache = VideoCache.getInstance()

        simpleExoPlayer = SimpleExoPlayer.Builder(requireActivity(), renderersFactory)
            .setTrackSelector(trackSelector)
            .setLoadControl(loadControl)
            .setBandwidthMeter(bandwidthMeter)
            .build()

//        var dataSourceFactory = DefaultDataSourceFactory(requireContext(), Util.getUserAgent(requireContext(), "壹同"))

        var dataSourceCacheFactory = CacheDataSourceFactory(requireContext(), 1024 * 1024 * 1024, 5 * 1024 * 1024,"壹同")

        var currUrl = video

        LogUtil.e("currUrl=="+currUrl)

        //播放的媒体文件
        var videoSource: MediaSource
        if (currUrl.contains(".m3u8")) {
            videoSource = HlsMediaSource.Factory(dataSourceCacheFactory)
                .createMediaSource(Uri.parse(currUrl));
            //addEventListener 这里只有两个参数都要传入值才可以成功设置
            // 否者会被断言 Assertions.checkArgument(handler != null && eventListener != null);
            // 并且报错  IllegalArgumentException()  所以不需要添加监听器时 注释掉
            //      videoSource .addEventListener( handler, null);
        } else {
            videoSource = ProgressiveMediaSource.Factory(dataSourceCacheFactory)
                .createMediaSource(Uri.parse(currUrl));
        }

//        var audioSource =  ExtractorMediaSource(Uri.parse(url), CacheDataSourceFactory(context, 100 * 1024 * 1024, 5 * 1024 * 1024),  DefaultExtractorsFactory(), null, null);

        simpleExoPlayer?.addListener(object : Player.EventListener {

            override fun onPlayerError(error: ExoPlaybackException) {
                super.onPlayerError(error)
//                showToastS("播放异常")
            }

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
//                Log.e("ExoPlayer","playWhenReady: $playWhenReady  +$playbackState")
                when (playbackState) {
                    Player.STATE_BUFFERING -> {
                        // showToastS("加载中")
                        mBinding.downLoading.startAnimation(animation2)
                        mBinding.linlayLoad.visibility = View.VISIBLE
                    }
                    Player.STATE_READY -> {
                        //showToastS("播放中")
                        mBinding.downLoading.clearAnimation()
                        mBinding.linlayLoad.visibility = View.GONE

                    }
                    Player.STATE_ENDED -> {
                        //showToastS("播放结束")
                        mBinding.downLoading.clearAnimation()
                        mBinding.linlayLoad.visibility = View.GONE
                    }
                }
            }
        })

        simpleExoPlayer?.prepare(videoSource)
        simpleExoPlayer?.playWhenReady = true  //自动播放
        simpleExoPlayer?.repeatMode = Player.REPEAT_MODE_ONE   //循环播放
        mBinding.playerView.player = simpleExoPlayer;

    }


    var animation2: Animation? = null

    override fun onResume() {
        super.onResume()
        simpleExoPlayer?.play()
        LogUtil.e("PlayListFrg onResume type=" + type.toString())
    }

    override fun onPause() {
        super.onPause()
        simpleExoPlayer?.pause()
        mBinding.downLoading?.clearAnimation()
        LogUtil.e("PlayListFrg onPause type=" + type.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        simpleExoPlayer?.release()
        LogUtil.e("PlayListFrg onDestroy type=" + type.toString())
    }

    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
    }


}