package com.mail.myapplication.ui

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.GravityCompat
import com.lzy.okgo.utils.HttpUtils
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.MyApp
import com.mail.myapplication.R
import com.mail.myapplication.ui.home.HomeFrg
import com.mail.myapplication.databinding.AtyMainBinding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.dg.NoticeDialog
import com.mail.myapplication.ui.dg.UpdateDialog
import com.mail.myapplication.ui.home.HistoryActiveAty
import com.mail.myapplication.ui.lar.BindPhoneAty
import com.mail.myapplication.ui.mine.EditInfoAty
import com.mail.myapplication.ui.mine.PersonDetailsAty
import com.mail.myapplication.ui.mine.Set2Aty
import com.mail.myapplication.ui.mine.apply.ApplyAty
import com.mail.myapplication.ui.msg.*
import com.mail.myapplication.ui.msg.database.ChatUtils
import com.mail.myapplication.ui.utils.MyUtils3
import com.mail.myapplication.ui.wallet.DuiCodeAty
import com.zhy.autolayout.utils.AutoUtils
import io.agora.rtm.*
import org.xutils.common.util.LogUtil
import java.util.HashMap

class MainAty : BaseXAty(), RtmClientListener {

    var position: Int = 0

    lateinit var mBinding: AtyMainBinding

    var map_info: MutableMap<String, String>? = null

    var str_info = ""

    var list_tv: Array<TextView>? = null

    var list_imgv: Array<ImageView>? = null

    var lar = Lar()

    var home = Home()

    var creator_status = ""

    var noticeDialog: NoticeDialog? = null

    var updateDialog: UpdateDialog? = null

    var isChatLogin = false

    var info_code = ""

    lateinit var mRtmClient: RtmClient

    lateinit var mChatManager: ChatManager

    var map_service_info: MutableMap<String, String>? = null

    var  lastClickTime = 0L

    override fun getLayoutId(): Int = 0

    override fun getLayoutView(): View {
        mBinding = AtyMainBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun getFragmentContainerId(): Int = R.id.fralay_content

    override fun initView() {
        setBackTwo(true)
        list_tv = arrayOf(mBinding.tv01, mBinding.tv02, mBinding.tv03, mBinding.tv04)
        list_imgv = arrayOf(mBinding.imgv01, mBinding.imgv02, mBinding.imgv03, mBinding.imgv04)
        var service_info = PreferencesUtils.getString(this, "service_info")
        map_service_info = JSONUtils.parseKeyAndValueToMap(service_info)
    }

    fun setUnReadNum(num: String) {
        if (TextUtils.isEmpty(num) || num == "0") {
            mBinding.tvNum.visibility = View.GONE
        } else {
            mBinding.tvNum.visibility = View.VISIBLE
            mBinding.tvNum.text = num
        }
    }

    override fun requestData() {
        home.a42(this)
    }

    override fun onResume() {
        super.onResume()
        lar.b6(this)
        registerChatListen()
    }

    override fun onPause() {
        super.onPause()
        (application as MyApp).getChatManager().unregisterListener(this)
    }

    fun requestMyInfo() {
        startProgressDialog()
        lar.b6(this)
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "version/update") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var map = JSONUtils.parseKeyAndValueToMap(map["data"])
                if (updateDialog == null) {
                    updateDialog = UpdateDialog(this)
                }
                updateDialog?.show()
                updateDialog?.setData(map["remark"]!!, map["force"]!!, map["package_path"]!!)
//                showBuilder(map["remark"]!!, map["package_p
                //                ath"]!!,map["force"]!!)
            }
        }

        if (type == "user/info") {
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                str_info = AESCBCCrypt.aesDecrypt(map["data"])
                map_info = JSONUtils.parseKeyAndValueToMap(str_info)

                var maxW = AutoUtils.getPercentWidthSizeBigger(300)

                ImageLoader.loadImageAes(this, map_info!!["avatar"], mBinding.imgvHead,maxW,maxW)
                mBinding.tvName.text = map_info!!["nick"]
                mBinding.tvId.text = "ID: " + map_info!!["code"]
                mBinding.tvAttenNum.text = map_info!!["flow"]
                mBinding.tvFansNum.text = map_info!!["fans"]

                if (TextUtils.isEmpty(map_info!!["parent_code"])) {
                    mBinding.tvYqm.text = "未綁定"
                } else {
                    mBinding.tvYqm.text = "已綁定"
                }

                if (TextUtils.isEmpty(map_info!!["mobile"])) {
                    mBinding.tvPhone.text = "未綁定手機號"
                } else {
                    mBinding.tvPhone.text = map_info!!["mobile"]
                }

                MyUtils3.setVipLevel2(map_info!!["vip_level"],  mBinding.imgvVipLevel,mBinding.linlayVip,0)

                when (map_info!!["gender"]) {
                    "1" -> {
                        mBinding.imgvGender.visibility = View.VISIBLE
                        mBinding.imgvGender.setImageResource(R.mipmap.ic_60)
                    }
                    "0" -> {
                        mBinding.imgvGender.visibility = View.VISIBLE
                        mBinding.imgvGender.setImageResource(R.mipmap.ic_61)
                    }
                    "10" -> {
                        mBinding.imgvGender.visibility = View.VISIBLE
                        mBinding.imgvGender.setImageResource(R.mipmap.ic_18)
                    }
                    else -> {
                        mBinding.imgvGender.visibility = View.GONE
                    }
                }

                if (!TextUtils.isEmpty(map_info!!["intro"])) {
                    mBinding.tvIntro.text = map_info!!["intro"]
                } else {
                    mBinding.tvIntro.text = "暂无签名"
                }

//                创作者申请状态 (1=已通过; 2=认证中;0=已拒绝 10=未申请)
                creator_status = map_info!!["creator_status"]!!
                when (creator_status) {
                    "1" -> {
                        mBinding.imgvCreator.visibility = View.VISIBLE
                        mBinding.tvApplay.text = "已認證"
                    }
                    "2" -> {
                        mBinding.imgvCreator.visibility = View.GONE
                        mBinding.tvApplay.text = "認證中"
                    }
                    "0" -> {
                        mBinding.imgvCreator.visibility = View.GONE
                        mBinding.tvApplay.text = "已拒絕"
                    }
                    "10" -> {
                        mBinding.imgvCreator.visibility = View.GONE
                        mBinding.tvApplay.text = "未認證"
                    }
                    else -> {
                        mBinding.imgvCreator.visibility = View.GONE
                        mBinding.tvApplay.text = "未認證"
                    }
                }

                MyUtils.saveMyInfo(this, map_info as HashMap<String, String>)

                if (!isChatLogin) {
                    loginChat()
                }
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type == "user/info") {
            stopProgressDialog()
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addFragment(HomeFrg::class.java, null)
        with(mBinding) {
            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setIsPinContentView(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    lar.b6(this@MainAty)
                }

                override fun loadMoreStart() {
                }

            })
        }

        var notice = PreferencesUtils.getString(this, "notice")
        var app_notice = PreferencesUtils.getString(this, "app_notice")
        if (notice == "1") {
            if (noticeDialog == null) {
                noticeDialog = NoticeDialog(this)
            }
            noticeDialog?.show()
            noticeDialog?.setData(app_notice)
        }

        var myApp = application as MyApp
        mChatManager = myApp.getChatManager()
        mChatManager.enableOfflineMessage(true)
        mChatManager.enableHistoricalMessage(true)
    }

    fun registerChatListen() {
        mChatManager.registerListener(this)
    }

    fun mainClick(v: View) {
        var time =0L
        if(v.id!=R.id.linlay_01&&v.id!=R.id.linlay_02&&v.id!=R.id.linlay_03&&v.id!=R.id.linlay_04){
            if (!MyUtils3.isFastClick()){
                return
            }
            time=300L
        }
        closeide()
        Handler().postDelayed({
            run {
//              showToastS(registrationId)
                when (v.id) {
                    R.id.linlay_01 -> {
                        if (position == 0){
                            val curClickTime = System.currentTimeMillis()
                            if (curClickTime - lastClickTime < 500) {

                                var frg = supportFragmentManager.findFragmentByTag(HomeFrg::class.java.toString())
                                if (frg is HomeFrg) {
                                    frg.refreshData()
                                }
                            }else{
                                lastClickTime = curClickTime
                            }

                            return@run
                        }
                        position = 0
                        list_tv?.let { t1 -> list_imgv?.let { t2 -> setSelector(mBinding.tv01, t1, t2) } }
                        addFragment(HomeFrg::class.java, null)
                    }

                    R.id.linlay_02 -> {
                        position = 1

                        startActivityForResult(MainFindAty::class.java, null, 2, false)
                        list_tv?.let { t1 -> list_imgv?.let { t2 -> setSelector(mBinding.tv02, t1, t2) } }

                    }

                    R.id.linlay_03 -> {
                        if (position == 2) return@run
                        position = 2
                        list_tv?.let { t1 -> list_imgv?.let { t2 -> setSelector(mBinding.tv03, t1, t2) } }
                        addFragment(MsgFrg::class.java, null)
                    }

                    R.id.relay_yqm -> {
                        if (TextUtils.isEmpty(str_info)) {
                            requestMyInfo()
                            return@run
                        }
                        var bundle = Bundle()
                        bundle.putString("type", "yqm")
                        startActivity(DuiCodeAty::class.java, bundle)
                    }

                    R.id.linlay_04 -> {
                        position = 4
                        if (TextUtils.isEmpty(str_info)) {
                            requestMyInfo()
                            return@run
                        }
                        startActivityForResult(MainWalletAty::class.java, null, 1, false)
                    }

                    R.id.relay_set -> {
                        startActivity(Set2Aty::class.java)
                    }

                    R.id.relay_apply -> {
                        if (TextUtils.isEmpty(str_info)) {
                            requestMyInfo()
                            return@run
                        }
                        var bundle = Bundle()
                        bundle.putString("creator_status", creator_status)
                        startActivity(ApplyAty::class.java, bundle)
                    }

                    R.id.linlay_atten -> {
                        var bundle = Bundle()
                        bundle.putString("type", "favorite")
                        startActivity(FansAty::class.java, bundle)
//                startActivity(AttentionAty::class.java)
                    }

                    R.id.linlay_fans -> {
                        var bundle = Bundle()
                        bundle.putString("type", "fans")
                        startActivity(FansAty::class.java, bundle)
                    }

                    R.id.relay_phone -> {
                        startActivity(BindPhoneAty::class.java)
                    }

                    R.id.relay_mine -> {
                        if (TextUtils.isEmpty(str_info)) {
                            requestMyInfo()
                            return@run
                        }
                        var bundle = Bundle()
                        bundle.putString("typePage", "my")
                        startActivity(PersonDetailsAty::class.java, bundle)
                    }

                    R.id.relay_info -> {
                        if (TextUtils.isEmpty(str_info)) {
                            requestMyInfo()
                            return@run
                        }
                        startActivity(EditInfoAty::class.java)
                    }

                    R.id.relay_active -> {
                        startActivity(HistoryActiveAty::class.java)
                    }

                    R.id.relay_kefu -> {

                        var bundle = Bundle()
                        bundle.putString("code", map_service_info!!["code"])
                        bundle.putString("nick", map_service_info!!["nick"])
                        bundle.putString("avatar", map_service_info!!["avatar"])
                        startActivity(CustomerServiceAty::class.java, bundle)
//
                    }
                }
            }
        }, time)
//      closeide()
    }

    override fun onBackPressed() {

        if (mBinding.rootview.isDrawerOpen(mBinding.swipeRefreshLayout)){
            closeide()
        }else{
            super.onBackPressed()
        }

    }

    fun openSide() {
        mBinding.rootview.openDrawer(GravityCompat.START)
    }

    fun closeide() {
        mBinding.rootview.closeDrawer(GravityCompat.START)
    }

    private fun setSelector(tv: TextView, list_tv: Array<TextView>, list_imgv: Array<ImageView>) {
        for (i in list_tv.indices) {
            if (tv === list_tv[i]) {
//                if(tv==mBinding.tv02){
//                    list_tv[i].setTextColor(resources.getColor(R.color.white))
//                    setStatusColor("#010D13")
//                } else{
//                    list_tv[i].setTextColor(resources.getColor(R.color.main_color))
//                    setStatusColor("#ffffff")
//                }
                list_tv[i].setTextColor(resources.getColor(R.color.main_01))
                when (i) {
                    0 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab1_checked)
                    1 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab2_checked)
                    2 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab3_checked)
                    3 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab4_checked)
                }
            } else {
                list_tv[i].setTextColor(resources.getColor(R.color.mainnocheck))
                when (i) {
                    0 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab1_normal)
                    1 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab2_normal)
                    2 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab3_normal)
                    3 -> list_imgv[i].setImageResource(R.drawable.ic_main_tab4_normal)
                }
            }
        }
    }

    fun loginChat() {
        info_code = PreferencesUtils.getString(this, "info_code")
        var myApp = application as MyApp
        myApp.getChatManager().enableOfflineMessage(true)
        mRtmClient = myApp.getChatManager().rtmClient
        mRtmClient.logout(null)
        mRtmClient.login(null, info_code, object : ResultCallback<Void> {
            override fun onSuccess(p0: Void?) {
                isChatLogin = true
                LogUtil.e("ErrorInfo2=")
                HttpUtils.runOnUiThread {
//                    showToastS("聊天服务器登陆成功")
                }
            }

            override fun onFailure(p0: ErrorInfo?) {
//                showToastS("登陆失败")
                LogUtil.e("ErrorInfo=" + p0.toString())
                runOnUiThread {

                    when (p0?.errorCode) {

                        //0登陆成功
                        RtmStatusCode.LoginError.LOGIN_ERR_OK -> {

                        }
                        //1登录失败。原因未知
                        RtmStatusCode.LoginError.LOGIN_ERR_UNKNOWN -> {
                            showToastS("登录失败。原因未知")
                        }

                        //2登录被服务器拒绝。
                        RtmStatusCode.LoginError.LOGIN_ERR_REJECTED -> {
                            showToastS("登录被服务器拒绝。")
                        }

                        //3无效的登录参数。
                        RtmStatusCode.LoginError.LOGIN_ERR_INVALID_ARGUMENT -> {
                            showToastS("无效的登录参数")
                        }

                        //4: App ID 无效。。
                        RtmStatusCode.LoginError.LOGIN_ERR_INVALID_APP_ID -> {
                            showToastS("App ID 无效")

                        }
                        //4: App ID 无效。。
                        RtmStatusCode.LoginError.LOGIN_ERR_INVALID_TOKEN -> {
                            showToastS("App ID 无效")

                        }

                        //6: token 已过期，登录被拒绝
                        RtmStatusCode.LoginError.LOGIN_ERR_TOKEN_EXPIRED -> {
                            showToastS(" token 已过期")
                        }

                        //7: Token 验证失败。
                        RtmStatusCode.LoginError.LOGIN_ERR_NOT_AUTHORIZED -> {
                            showToastS(" Token 验证失败。")
                        }

                        //8: 用户已登录，或已正在登录 Agora RTM 系统，或未调用 logout 方法退出 CONNECTION_STATE_ABORTED 状态。
                        RtmStatusCode.LoginError.LOGIN_ERR_ALREADY_LOGIN -> {
                            isChatLogin = true
//                        showToastS("聊天服务器登陆成功")
                        }

                        //9 登录超时。目前的超时设置为 12 秒。你需要再次登录。。
                        RtmStatusCode.LoginError.LOGIN_ERR_TIMEOUT -> {
                            showToastS(" 登录超时。")
                        }
                    }

                }
            }
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            var type = data?.getStringExtra("type")
            when (type) {

                "home_01" -> {
                    mBinding.linlay01.performClick()
                }
                "home_02" -> {
                    mBinding.linlay02.performClick()
                }
                "home_03" -> {
                    mBinding.linlay03.performClick()
                }

                "home_04" -> {
                    mBinding.linlay04.performClick()
                }
//                "home_set"->{
//                    mBinding.relaySet.performClick()
//                }
//                "home_applay"->{
//                    mBinding.relayApply.performClick()
//                }
//                "home_atten"->{
//                    mBinding.linlayAtten.performClick()
//                }
//                "home_fans"->{
//                    mBinding.linlayFans.performClick()
//                }
//                "home_fans"->{
//                    mBinding.linlayFans.performClick()
//                }
            }

        }
    }

    override fun onConnectionStateChanged(p0: Int, p1: Int) {
    }

    override fun onMessageReceived(message: RtmMessage?, p1: String?) {
        ChatUtils.saveChatpannel(message!!.text, false)
        runOnUiThread {
            var fg = supportFragmentManager.findFragmentByTag(MsgFrg::class.java.toString())
            if (fg is MsgFrg) {
                fg.onResume()
            }
        }
    }

    override fun onImageMessageReceivedFromPeer(p0: RtmImageMessage?, p1: String?) {
    }

    override fun onFileMessageReceivedFromPeer(p0: RtmFileMessage?, p1: String?) {
    }

    override fun onMediaUploadingProgress(p0: RtmMediaOperationProgress?, p1: Long) {
    }

    override fun onMediaDownloadingProgress(p0: RtmMediaOperationProgress?, p1: Long) {
    }

    override fun onTokenExpired() {
    }

    override fun onPeersOnlineStatusChanged(p0: MutableMap<String, Int>?) {
    }


}