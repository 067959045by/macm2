package com.mail.myapplication.ui.mine

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.TextUtils
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.mail.comm.image.ImageLoader
import com.mail.comm.utils.MyUtils2
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.utils.TimeUtils
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyPerDetailsBinding
import com.mail.myapplication.databinding.ItemFrgHome02Binding
import com.mail.myapplication.databinding.ItemFrgHome03Binding
import com.mail.myapplication.ui.home.HomeListFrg
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils
import kotlin.math.abs

class PersonDetailsAty : BaseXAty() {

    lateinit var mBinding: AtyPerDetailsBinding
    lateinit var list_tv: Array<TextView>
    lateinit var list_v: Array<View>
    var typePage = "my"

    override fun getLayoutId(): Int = 0

    override fun getLayoutView(): View {
        mBinding = AtyPerDetailsBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun initView() {
        typePage =intent.getStringExtra("typePage")!!

        with(mBinding){
            list_tv = arrayOf(tv01,tv02, tv03)
            list_v = arrayOf(v01,v02, v03)
            if (typePage == "my"){
                linlayAtten.visibility = View.GONE
                imgvAddAttention.visibility = View.GONE
                relayReport.visibility = View.GONE
                relayEdit.visibility = View.VISIBLE
            }else{
                linlayAtten.visibility = View.VISIBLE
                relayEdit.visibility = View.GONE
            }
        }
        initLayout()

    }

    override fun requestData() {}

    override fun onResume() {
        super.onResume()
        initMyInfo()
    }


   fun initMyInfo(){
       with(mBinding){

           var info_nick =PreferencesUtils.getString(this@PersonDetailsAty,"info_nick")
           var info_flow =PreferencesUtils.getString(this@PersonDetailsAty,"info_flow")
           var info_fans =PreferencesUtils.getString(this@PersonDetailsAty,"info_fans")
           var info_avatar =PreferencesUtils.getString(this@PersonDetailsAty,"info_avatar")
           var info_intro =PreferencesUtils.getString(this@PersonDetailsAty,"info_intro")
           var info_city =PreferencesUtils.getString(this@PersonDetailsAty,"info_city")
           var info_years =PreferencesUtils.getString(this@PersonDetailsAty,"info_years")
           var info_job =PreferencesUtils.getString(this@PersonDetailsAty,"info_job")
           var info_gender =PreferencesUtils.getString(this@PersonDetailsAty,"info_gender")
           var info_code =PreferencesUtils.getString(this@PersonDetailsAty,"info_code")
           var info_vip_level =PreferencesUtils.getString(this@PersonDetailsAty,"info_vip_level")
           var info_is_creator =PreferencesUtils.getString(this@PersonDetailsAty,"info_is_creator")
           var info_vip_expired =PreferencesUtils.getString(this@PersonDetailsAty,"info_vip_expired")

           var maxW = AutoUtils.getPercentWidthSizeBigger(300)

           ImageLoader.loadImageAes(this@PersonDetailsAty,info_avatar,ivHead,maxW,maxW)
           mBinding.tvName.text = info_nick
           mBinding.tvName2.text = info_nick
           mBinding.tvIntro.text =info_intro
           mBinding.tvAttenNum.text =info_flow
           mBinding.tvFansNum.text =info_fans
           mBinding.tvCity.text =info_city
           mBinding.tvJob.text =info_years+"岁  "+info_job
           mBinding.tvId.text = "ID: ${info_code}"

           if (TextUtils.isEmpty(info_intro)){
               mBinding.tvIntro.text ="暂无签名"
           }

           when (info_gender) {

               "1" -> {
                   imgvGender.visibility = View.VISIBLE
                   imgvGender.setImageResource(R.mipmap.ic_60)
               }
               "0" -> {
                   imgvGender.visibility = View.VISIBLE
                   imgvGender.setImageResource(R.mipmap.ic_61)
               }
               "10" -> {
                   imgvGender.visibility = View.VISIBLE
                   imgvGender.setImageResource(R.mipmap.ic_18)
               }
               else -> {
                   imgvGender.visibility = View.GONE
               }

           }

           if (info_is_creator== "1") {
               mBinding.imgvCreator.visibility = View.VISIBLE
           } else {
               mBinding.imgvCreator.visibility = View.GONE
           }

           if(!TextUtils.isEmpty(info_vip_expired)){
               var time =  (TimeUtils.getTime(info_vip_expired).toDouble()
                       - TimeUtils.getBeijinTime().toDouble())/1000
               mBinding.tvVipTime.text = TimeUtils.formatSecond3(time).replace("前","后")+"到期"
           }

           MyUtils3.setVipLevel(info_vip_level,  mBinding.imgvVipLevel,0)

       }

       var info_habit =PreferencesUtils.getString(this@PersonDetailsAty,"info_habit")
       var str = info_habit.split(",")

//        LogUtil.e("info_habit=="+str.toString())
//        LogUtil.e("info_habit2=="+info_habit)
       if (str == null || str.size == 0 || info_habit.length == 0 || TextUtils.isEmpty(info_habit)) {
           mBinding.tvLike01.visibility = View.GONE
           mBinding.tvLike02.visibility = View.GONE
           mBinding.tvLike03.visibility = View.GONE
       } else if (str.size == 1) {
           mBinding.tvLike01.visibility = View.VISIBLE
           mBinding.tvLike02.visibility = View.GONE
           mBinding.tvLike03.visibility = View.GONE
           mBinding.tvLike01.text = str[0]
       } else if (str.size == 2) {
           mBinding.tvLike01.visibility = View.VISIBLE
           mBinding.tvLike02.visibility = View.VISIBLE
           mBinding.tvLike03.visibility = View.GONE
           mBinding.tvLike01.text = str[0]
           mBinding.tvLike02.text = str[1]
       } else if (str.size == 3) {
           mBinding.tvLike01.visibility = View.VISIBLE
           mBinding.tvLike02.visibility = View.VISIBLE
           mBinding.tvLike03.visibility = View.VISIBLE
           mBinding.tvLike01.text = str[0]
           mBinding.tvLike02.text = str[1]
           mBinding.tvLike03.text = str[2]
       }
   }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTranslanteBar()
        setAndroidNativeLightStatusBar(true)
        initMyInfo()
        with(mBinding){

            var mLayoutManager2 = GridLayoutManager(this@PersonDetailsAty,1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL

            var list =  ArrayList<String>()
            list.add("mine_like")
            list.add("mine_my")
            list.add("mine_buy")

            viewPager.adapter = PersonListAdataper(supportFragmentManager, this@PersonDetailsAty.lifecycle, list)

            appbar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                val Offset = abs(verticalOffset).toFloat()
                val totalScrollRange = appBarLayout?.totalScrollRange
                var a =(Offset/ totalScrollRange!!)
                linlayName.alpha= a
                relayReport.alpha= a
                imgvAddAttention.alpha= a
            })

            linlay01.setOnClickListener {
                viewPager.setCurrentItem(0,true)
            }
            linlay02.setOnClickListener {
                viewPager.setCurrentItem(1,true)

            }
            linlay03.setOnClickListener {
                viewPager.setCurrentItem(2,true)
            }


            viewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    when(position){
                        0 -> {
                            setSelector(tv01)
                        }
                        1 -> {
                            setSelector(tv02)
                        }
                        2 -> {
                            setSelector(tv03)
                        }

                    }
                }
            })

            viewPager.setCurrentItem(1,false)
        }
    }

    inner class PersonListAdataper(fa: FragmentManager, lifecycle: Lifecycle, val docs : MutableList<String>) :

        FragmentStateAdapter(fa, lifecycle) {

        override fun getItemCount(): Int =docs.size

        override fun createFragment(position: Int): Fragment = HomeListFrg.create((docs[position]))

    }

    private fun setSelector(tv: TextView) {
        for (i in list_tv.indices) {
            if (tv === list_tv[i]) {
                list_tv[i].setTextColor(Color.parseColor("#333438"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗


                list_tv[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentWidthSizeBigger(55).toFloat())
                list_v[i].visibility = View.VISIBLE
            } else {
                list_tv[i].setTextColor(Color.parseColor("#65666D"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));//加粗
                list_tv[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentWidthSizeBigger(45).toFloat())
                list_v[i].visibility = View.INVISIBLE
            }
        }
    }

    fun initLayout(){
        with(mBinding){
            var params =toolbar.layoutParams  as CollapsingToolbarLayout.LayoutParams
            var StatusBarHeight = MyUtils2.getStateBar(this@PersonDetailsAty)
            if (StatusBarHeight <= 0) {
                StatusBarHeight = MyUtils2.dip2px(this@PersonDetailsAty, 20F)
            }
            params.topMargin = StatusBarHeight
            params.height = AutoUtils.getPercentHeightSizeBigger(120)
            toolbar.setPadding(0,0,0,0)
            toolbar.layoutParams =params
        }
        initLayout2()
    }

    fun initLayout2(){
        with(mBinding){
            var params =linlayTop.layoutParams  as LinearLayout.LayoutParams
            var StatusBarHeight = MyUtils2.getStateBar(this@PersonDetailsAty)
            if (StatusBarHeight <= 0) {
                StatusBarHeight = MyUtils2.dip2px(this@PersonDetailsAty, 20F)
            }
            params.topMargin = StatusBarHeight+AutoUtils.getPercentHeightSizeBigger(150)
            linlayTop.layoutParams =params
        }
    }


    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.relay_edit -> {
                startActivity(EditInfoAty::class.java)
            }

            R.id.framlay_head -> {
//                startActivity(MyHeadAty::class.java)
            }

            R.id.imgv_post -> {
//                startActivity(MyHeadAty::class.java)

                startActivity(PostSendAty::class.java)

            }

        }
    }






}