package com.mail.myapplication.ui.dg

import android.os.Bundle
import android.view.*
import com.lzy.okgo.utils.HttpUtils.runOnUiThread
import com.mail.comm.base.BaseDgFrg
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.WheelView.OnWheelViewListener
import com.mail.myapplication.R
import com.mail.myapplication.databinding.DgDateSelectBinding
import com.mail.myapplication.ui.lar.RegisterBirthdayAty
import org.xutils.common.util.LogUtil


class DateSelectDgFrg : BaseDgFrg() {

    lateinit var mBinding: DgDateSelectBinding

    override fun getLayoutId(): Int = 0

    override fun getDialogStyle(): Int = R.style.dialogFullscreen2

    override fun getLayoutView(): View {
        mBinding = DgDateSelectBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun canCancel(): Boolean = true

    var listDate = ArrayList<String>()

    override fun setWindowAttributes(window: Window?) {
//
    }

    var str_year = ""
    var str_moon = ""
    var str_day = ""

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(mBinding) {
            var listYear = getListYear()
            var listMoon = getListMoon()
            str_moon = listMoon[listMoon.size - 6].replace("月","")
            str_year = listYear[listYear.size - 25].replace("年","")

            whellviewYear.setOffset(1)
            whellviewYear.setItems(listYear)
            whellviewYear.setSeletion(listYear.size - 25)
            whellviewYear.setOnWheelViewListener(object : OnWheelViewListener() {
                override fun onSelected(selectedIndex: Int, item: String) {
                    str_year = listYear[selectedIndex-1].replace("年","")

                    runOnUiThread {

                        listDate.clear()
                        listDate.addAll(getListDate(str_year.toInt(),str_moon.toInt()))
                        whellviewDay.setItems(listDate)
                        whellviewDay.setSeletion(2)
                        str_day = listDate[2].replace("日","")

                    }

                }
            })

            whellviewMoon.setOffset(1)
            whellviewMoon.setItems(listMoon)
            whellviewMoon.setSeletion(listMoon.size - 6)
            whellviewMoon.setOnWheelViewListener(object : OnWheelViewListener() {
                override fun onSelected(selectedIndex: Int, item: String) {

                    runOnUiThread {
                        str_moon = listMoon[selectedIndex-1].replace("月","")
                        listDate.clear()
                        listDate.addAll(getListDate(str_year.toInt(),str_moon.toInt()))
                        whellviewDay.setItems(listDate)
                        whellviewDay.setSeletion(2)
                        str_day = listDate[2].replace("日","")

                    }


                }
            })

            listDate = getListDate(str_year.toInt(),str_moon.toInt())
            whellviewDay.setOffset(1)
            whellviewDay.setItems(listDate)
            whellviewDay.setSeletion(2)
            whellviewDay.setOnWheelViewListener(object : OnWheelViewListener() {
                override fun onSelected(selectedIndex: Int, item: String) {
                    str_day = item.replace("日","")
                }
            })
            str_day = "3"

            tvOk.setOnClickListener {
                submitData()
                dismiss()
            }

            imgvCancel.setOnClickListener {
                dismiss()
            }
        }
    }


    fun submitData() {
        var aty = activity
        if (aty is RegisterBirthdayAty) {
            aty.submitData(str_year,str_moon,str_day)
        }
    }

    fun getListYear(): ArrayList<String> {
        var listYear = ArrayList<String>()
        for (i in 1949..2021) {
            listYear.add(i.toString() + "年")
        }
        return listYear
    }


    fun getListMoon(): ArrayList<String> {
        var listYear = ArrayList<String>()
        for (i in 1..12) {
            listYear.add(i.toString() + "月")
        }
        return listYear
    }

    fun getListDate(year:Int,montn:Int): ArrayList<String> {
        var days =MyUtils.getMonthOfDays(year,montn)
        var listYear = ArrayList<String>()
        for (i in 1..days) {
            listYear.add(i.toString() + "日")
        }
        return listYear
    }


}