package com.mail.myapplication.ui.msg

import android.os.Bundle
import android.view.View
import com.mail.comm.image.ImageLoader
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.databinding.FrgBigimgBinding
import com.zhy.autolayout.config.AutoLayoutConifg
import com.zhy.autolayout.utils.AutoUtils

class BigImgFrg : BaseXFrg() {

    lateinit var mBinding: FrgBigimgBinding
    var img = ""

    override fun getLayoutView(): View {
        mBinding = FrgBigimgBinding.inflate(layoutInflater);
        return mBinding.root
    }

    companion object {
        fun create(img: String): BigImgFrg {
            val fragment = BigImgFrg()
            val bundle = Bundle()
            bundle.putString("img", img)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        img = arguments?.getString("img").toString()

        var maxW = AutoLayoutConifg.getInstance().screenWidth
        var maxH = AutoLayoutConifg.getInstance().screenWidth
        ImageLoader.loadImageAes(context=null,img,mBinding.imgv,maxW,maxH)
    }

    override fun getLayoutId(): Int = 0

    override fun initView() {}

    override fun requestData() {}


}