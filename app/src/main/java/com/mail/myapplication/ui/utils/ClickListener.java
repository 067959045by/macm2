package com.mail.myapplication.ui.utils;

public interface ClickListener {

    void click(int position);
}
