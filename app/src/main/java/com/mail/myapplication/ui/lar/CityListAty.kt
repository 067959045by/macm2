package com.mail.myapplication.ui.lar

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.MyUtils2
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil

class CityListAty : BaseXAty() {

    lateinit var mBinding: AtyCityListBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2

    var listNation = ArrayList<MutableMap<String, String>>()
    override fun getLayoutId(): Int = 0

    var list_tv: Array<TextView>? = null
    var list_v: Array<View>? = null

    var str_nation = ""
    var str_province = ""
    var str_city = ""
    var type = "nation"
    var typePage = ""
    var login_job = ""
    var login_gender = ""
    var login_like = ""
    var login_birth = ""
    var area_json_url=""
    var new_area_json_url=""
    var resource_cdn=""
    var lar = Lar()

    override fun initView() {
        typePage = intent.getStringExtra("type")!!
        area_json_url = PreferencesUtils.getString(this,"area_json_url","")
        new_area_json_url = PreferencesUtils.getString(this,"new_area_json_url","")
        resource_cdn = PreferencesUtils.getString(this,"resource_cdn","")

        if (intent.hasExtra("login_job")){
            login_job = intent.getStringExtra("login_job")!!
        }
        if (intent.hasExtra("login_gender")){
            login_gender = intent.getStringExtra("login_gender")!!
        }
        if (intent.hasExtra("login_like")){
            login_like = intent.getStringExtra("login_like")!!
        }
        if (intent.hasExtra("login_birth")){
            login_birth = intent.getStringExtra("login_birth")!!
        }

        list_tv = arrayOf(mBinding.tv01, mBinding.tv02, mBinding.tv03)
        list_v = arrayOf(mBinding.v01, mBinding.v02, mBinding.v03)
    }

    override fun requestData() {
    }

    fun initCityJson(){

        if (new_area_json_url!=area_json_url){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
            lar.b18(new_area_json_url,resource_cdn,this)
        }else{

            var area_json= PreferencesUtils.getString(this,"area.json","")
            if (TextUtils.isEmpty(area_json)){
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
                lar.b18(new_area_json_url,resource_cdn,this)

            }else{
                var str = JSONUtils.parseKeyAndValueToMap(area_json)
                listNation = JSONUtils.parseKeyAndValueToMapList(str["nation"])
                mAdapter2.setData(listNation)
            }
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        if (type=="area/json"){

//            showToastS("sss")
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var str = JSONUtils.parseKeyAndValueToMap(var2)
            listNation = JSONUtils.parseKeyAndValueToMapList(str["nation"])
            mAdapter2.setData(listNation)
            PreferencesUtils.putString(this,"area_json_url",new_area_json_url)
            PreferencesUtils.putString(this,"area.json",var2)
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        if (type == "area/json"){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }
    }

    override fun getLayoutView(): View {
        mBinding = AtyCityListBinding.inflate(layoutInflater);
        return mBinding.root
    }

    private fun setSelector(tv: TextView) {
        for (i in list_tv!!.indices) {
            if (tv === list_tv!![i]) {
                list_tv!![i].setTextColor(resources.getColor(R.color.black))
                list_v!![i].visibility = View.VISIBLE
            } else {
                list_tv!![i].setTextColor(Color.parseColor("#FF65666D"))
                list_v!![i].visibility = View.GONE
            }
        }
    }

    fun setSelector2() {

        when (type) {

            "nation" -> {
                setSelector(mBinding.tv01)
            }
            "province" -> {
                setSelector(mBinding.tv02)
            }
            "city" -> {
                setSelector(mBinding.tv03)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvTitle.text = ""
            include.tvRight.text = "跳過"
            include.tvRight.visibility = View.VISIBLE
            if (typePage == "edit") {
                include.tvRight.visibility = View.GONE
            } else {
                include.tvRight.visibility = View.VISIBLE
            }
//            var json = MyUtils3.getJson("area.json", this@CityListAty)
//
//            var str = JSONUtils.parseKeyAndValueToMap(json)
//
//            listNation = JSONUtils.parseKeyAndValueToMapList(str["nation"])

            var mLayoutManager2 = GridLayoutManager(this@CityListAty, 1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager = mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2
            initCityJson()

            loading.setLoadingTipXReloadCallback(object: XLoadTip.LoadingTipXReloadCallback{
                override fun reload() {
                    initCityJson()
                }

            })
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
            R.id.linlay_01->{
                type = "nation"
                setSelector2()
                mAdapter2.setData(listNation)
                mBinding.tvContent.text = ""
                str_nation=""
                str_province=""
                str_city=""
                this@CityListAty.mBinding.tvOk.visibility = View.INVISIBLE
            }

            R.id.tv_ok->{
                if ( this@CityListAty.mBinding.tvOk.visibility==View.VISIBLE){
                    if (typePage == "edit") {
                        var intent = Intent()
                        intent.putExtra("data", mBinding.tvContent.text.toString())
                        setResult(RESULT_OK, intent)
                        finshPage("2")
                    }else{
                        var bundle = Bundle()
                        bundle.putString("type","login")
                        bundle.putString("login_job",login_job)
                        bundle.putString("login_gender",login_gender)
                        bundle.putString("login_like",login_like)
                        bundle.putString("login_birth", login_birth)
                        bundle.putString("login_city", mBinding.tvContent.text.toString())
                        startActivity(RegisterHeadAty::class.java,bundle)
                    }
                }
            }
            R.id.tv_right -> {
                var bundle = Bundle()
                bundle.putString("type","login")
                bundle.putString("login_job",login_job)
                bundle.putString("login_gender",login_gender)
                bundle.putString("login_like",login_like)
                bundle.putString("login_birth", login_birth)
                bundle.putString("login_city","")
                startActivity(RegisterHeadAty::class.java,bundle)
            }
        }
    }


    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        var list = ArrayList<MutableMap<String, String>>()
        var list2 = ArrayList<String>()


        fun setData(list: ArrayList<MutableMap<String, String>>) {
            this@GoldRecyclerAdapter2.list = list
            notifyDataSetChanged()
        }

        fun setData2(list: ArrayList<String>) {
            this@GoldRecyclerAdapter2.list2 = list
            notifyDataSetChanged()
        }

        fun clearData() {
            list.clear()
            list2.clear()
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            return fGoldViewHolder(ItemCityBinding.inflate(LayoutInflater.from(this@CityListAty)))
        }

        override fun getItemCount(): Int {
            if (type == "city") {
                return list2.size
            } else {
                return list.size
            }
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {

                    with(mBinding) {

                        itemView.setOnClickListener {

                            when (type) {
                                "nation" -> {
                                    str_nation=list[position]["name"]!!
                                    if (list[position].containsKey("provinces")) {
                                        type = "province"
                                        var mList = JSONUtils.parseKeyAndValueToMapList(list[position]["provinces"])
                                        setData(mList)
                                        if ( this@CityListAty.mBinding.tvOk.visibility==View.VISIBLE){
                                            this@CityListAty.mBinding.tvOk.visibility = View.INVISIBLE
                                        }

                                    } else {
                                        this@CityListAty.mBinding.tvOk.visibility = View.VISIBLE
                                    }
                                }

                                "province" -> {
                                    str_province=list[position]["province"]!!
                                    if (list[position].containsKey("cities")) {
                                        type = "city"
                                        var mList = JSONUtils.parseKeyAndValueToListString(list[position]["cities"])
                                        setData2(mList)
                                    }
                                }
                                "city" -> {
                                    str_city=list2[position]!!
                                    this@CityListAty.mBinding.tvOk.visibility = View.VISIBLE
                                }
                            }
                            if (TextUtils.isEmpty(str_province)){

                            }
                            this@CityListAty.mBinding.tvContent.text = str_nation+getString01(str_province)+getString01(str_city)
                            setSelector2()
                        }

                        when (type) {
                            "nation" -> {
                                tvName.text = list[position]["name"]
                            }
                            "province" -> {
                                tvName.text = list[position]["province"]
                            }
                            "city" -> {
                                tvName.text = list2[position]
                            }
                        }


                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemCityBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemCityBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }

    fun  getString01 (str:String):String{

        if (TextUtils.isEmpty(str)){
            return ""
        }else{
            return "-"+str
        }
    }


}