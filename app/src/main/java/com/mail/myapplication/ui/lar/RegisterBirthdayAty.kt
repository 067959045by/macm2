package com.mail.myapplication.ui.lar

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.ui.dg.DateSelectDgFrg
import com.zhy.autolayout.utils.AutoUtils

class RegisterBirthdayAty : BaseXAty() {

    lateinit var mBinding: AtyRegisterBirthdayBinding
    var str_year = "1997"
    var str_moon = "7"
    var str_day = "3"
    var type = ""

    var login_job = ""
    var login_gender = ""
    var login_like = ""

    override fun getLayoutId(): Int = 0

    override fun initView() {
        type = intent.getStringExtra("type")!!

        if (intent.hasExtra("login_job")){
            login_job = intent.getStringExtra("login_job")!!
        }
        if (intent.hasExtra("login_gender")){
            login_gender = intent.getStringExtra("login_gender")!!
        }
        if (intent.hasExtra("login_like")){
            login_like = intent.getStringExtra("login_like")!!
        }
    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtyRegisterBirthdayBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvRight.text = "跳過"
            if (type == "edit") {
                include.tvRight.visibility = View.GONE
            } else {
                include.tvRight.visibility = View.VISIBLE
            }
        }
        mBinding.tvDate.text = str_year+" - "+str_moon+" - "+str_day

    }
    //http://cdnimg.yt.xingtaole.com/ig/images/080420/841482beb9f8bb50.ceb

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_ok -> {
                if (type == "edit") {

                    var intent = Intent()
                    intent.putExtra("data", mBinding.tvDate.text.toString())
                    setResult(RESULT_OK, intent)
                    finshPage("2")
                }else{

                    var bundle = Bundle()
                    bundle.putString("type","login")
                    bundle.putString("login_job",login_job)
                    bundle.putString("login_gender",login_gender)
                    bundle.putString("login_like",login_like)
                    bundle.putString("login_birth", mBinding.tvDate.text.toString())
                    startActivity(CityListAty::class.java,bundle)
                }
//                startActivity(CityListAty::class.java)
            }

            R.id.tv_right -> {
                var bundle = Bundle()
                bundle.putString("type","login")
                bundle.putString("login_job",login_job)
                bundle.putString("login_gender",login_gender)
                bundle.putString("login_like",login_like)
                bundle.putString("login_birth", "")
                startActivity(CityListAty::class.java,bundle)
//                startActivity(RegisterCodeAty::class.java)
            }

            R.id.tv_date -> {
                var dg = DateSelectDgFrg()
                dg.show(supportFragmentManager,"DateSelectDgFrg")
//                startActivity(RegisterCodeAty::class.java)
            }

        }
    }

    fun submitData(strYear: String, strMoon: String, strDay: String) {
        str_year = strYear
        str_moon = strMoon
        str_day = strDay
        mBinding.tvDate.text = str_year+" - "+strMoon+" - "+strDay
    }





}