package com.mail.myapplication.ui.mine.apply

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.mail.comm.app.AppManager
import com.mail.comm.function.chooseimg.ChooseImgAty
import com.mail.comm.function.choosevideo.ChooseVideoAty
import com.mail.comm.image.ImageLoader
import com.mail.comm.utils.FileSizeUtil
import com.mail.comm.utils.JSONUtils
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyApplyImgVideoBinding
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.dg.UploadDialog
import com.mail.myapplication.ui.video.PlayListVieoAty
import com.zhy.autolayout.utils.AutoUtils

class ApplyImgVideoAty : BaseXAty() {

    var videoPath =""
    var imgPath =""
    var imgPath2 =""
    var home = Home()
    var lar = Lar()
    var creator_status = ""
    var str =""
    var selfie =""
    var video_selfie =""
    var uploadDialog: UploadDialog?=null

    var map:HashMap<String,String>?=null

    lateinit var mBinding: AtyApplyImgVideoBinding

    override fun getLayoutId(): Int = 0

    override fun initView() {
        creator_status = intent.getStringExtra("creator_status").toString()
        str = intent.getStringExtra("data").toString()
    }

    override fun requestData() {}

    override fun getLayoutView(): View {
        mBinding = AtyApplyImgVideoBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(mBinding){
            initTopview2(include.relayTopBg)
            include.tvTitle.text = ""

            if (!TextUtils.isEmpty(str)){
                 map = JSONUtils.parseKeyAndValueToMap(str) as HashMap<String, String>?
                 selfie = map!!["selfie"]!!
                 video_selfie = map!!["video_selfie"]!!
            }
            var maxW = AutoUtils.getPercentWidthSizeBigger(500)

            when(creator_status){
                "1"->{
                    mBinding.tvOk.text = "已認證"
                    if (map!=null){
                        ImageLoader.loadImageAes(this@ApplyImgVideoAty,map!!["selfie"],ivHead,maxW,maxW)
                        ImageLoader.loadImageAes(this@ApplyImgVideoAty,map!!["cover"],ivVideo,maxW,maxW)
                        mBinding.imgvVideo.visibility = View.VISIBLE
                    }
                }
                "2"->{
                    mBinding.tvOk.text = "認證中"
                    if (map!=null){
                        ImageLoader.loadImageAes(this@ApplyImgVideoAty,map!!["selfie"],ivHead,maxW,maxW)
                        ImageLoader.loadImageAes(this@ApplyImgVideoAty,map!!["cover"],ivVideo,maxW,maxW)
                        mBinding.imgvVideo.visibility = View.VISIBLE
                    }
                }
                "0"->{
                    mBinding.tvOk.text = "已拒絕"
                    if (map!=null){
                        ImageLoader.loadImageAes(this@ApplyImgVideoAty,map!!["selfie"],ivHead,maxW,maxW)
                        ImageLoader.loadImageAes(this@ApplyImgVideoAty,map!!["cover"],ivVideo,maxW,maxW)
                        mBinding.imgvVideo.visibility = View.VISIBLE
                    }
                }
                "10"->{
                    mBinding.tvOk.text = "認證"
                }
                else ->{
                    mBinding.tvOk.text = "認證"
                }
            }
        }
    }

    override fun onLoading(type: String, total: Long, current: Long, isDownloading: Boolean) {
        super.onLoading(type, total, current, isDownloading)

        if (type!!.startsWith("video")){
            runOnUiThread {
                if (isFinishing){
                    return@runOnUiThread
                }
                if (Math.abs(total-current)<50000){
                    startProgressDialog()
                    uploadDialog?.dismiss()
                    uploadDialog = null
                    return@runOnUiThread
                }

                stopProgressDialog()
                var c = FileSizeUtil.FormetFileSize(current)
                var t = FileSizeUtil.FormetFileSize(total)
                if (uploadDialog == null){
                    uploadDialog = UploadDialog(this)
                    uploadDialog?.show()
                }
//                if (!uploadDialog!!.isShowing){
//                    uploadDialog?.show()
//                }
                uploadDialog?.setData("${c}/${t}","视频上传中..")
            }
        }

    }


    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.relay_img -> {
                if (creator_status =="1"||creator_status=="2"){
                    return
                }

                var bundle = Bundle()
                bundle.putInt("max_num", 1)
                bundle.putString("ratio", "0,0")
                startActivityForResult(ChooseImgAty::class.java, bundle, 300)
            }

            R.id.relay_video -> {
                if (creator_status =="1"||creator_status=="2"){
                    var list = ArrayList<String>()
                    list.add(map!!["video_selfie"]!!)
                    var bundle = Bundle()
                    bundle.putStringArrayList("data",list)
                    startActivity(PlayListVieoAty::class.java,bundle)
                    return
                }
                var bundle = Bundle()
                startActivityForResult(ChooseVideoAty::class.java, bundle, 400)
            }

            R.id.tv_ok -> {
                if (creator_status =="1"||creator_status=="2"){
                    return
                }
//                startActivity(ApplyFinishAty::class.java)
                if (TextUtils.isEmpty(videoPath)&&TextUtils.isEmpty(video_selfie)){
                    showToastS("请上传视频")
                    return
                }
                if (TextUtils.isEmpty(imgPath)&&TextUtils.isEmpty(selfie)){
                    showToastS("请上图片")
                    return
                }

                startProgressDialog("",false)
                if (TextUtils.isEmpty(selfie)){
                    lar.b72(imgPath, "img", this)
                    return
                }

                if (TextUtils.isEmpty(video_selfie)){
                    lar.b72(videoPath, "video", this)
                    return
                }

                home.a33(selfie,video_selfie,this)

//                if (creator_status == "0"){
//
//                }
            }
        }
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type=="img") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "0") {
                var dataMap = JSONUtils.parseDataToMap(var2)
                selfie = dataMap["file_path"]!!
//                lar.b72(videoPath, "video", this)
                if (TextUtils.isEmpty(video_selfie)){
                    lar.b72(videoPath, "video", this)
                    return
                }

                home.a33(selfie,video_selfie,this)

            } else {
                stopProgressDialog()
                showToastS(map["message"])
            }
        }

        if (type=="video") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "0") {
                var dataMap = JSONUtils.parseDataToMap(var2)
                var videoPath2 = dataMap["file_path"]!!
                home.a33(selfie,videoPath2,this)

            } else {
                stopProgressDialog()
                showToastS(map["message"])
            }
        }

        if (type == "creator/auth"){
            stopProgressDialog()
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                showToastS("已提交")
                finish()
                AppManager.getInstance().killActivity(ApplyAty::class.java)
                var bundle = Bundle()
                bundle.putString("type","2")
                startActivity(ApplyFinishAty::class.java)
            }else{
                showToastS(map["message"])
            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
        if (type =="img"||type=="video"){
            showToastS("网络异常")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != RESULT_OK) {
            return
        }
        when (requestCode) {
            300 -> {
                var type = data?.getStringExtra("type")

                when (type) {
                    "img_photo" -> {
                        selfie =""
                        var mlist = data?.getStringArrayListExtra("data")
                        imgPath = mlist!![0]
                        ImageLoader.loadImage(this, imgPath, mBinding.ivHead)

                    }
                }

            }
            400 -> {
                video_selfie = ""
                videoPath = data?.getStringExtra("data")!!
                ImageLoader.loadImage(this, videoPath, mBinding.ivVideo)
                mBinding.imgvVideo.visibility = View.VISIBLE

//                var size = FileSizeUtil.getFileOrFilesSize(path, FileSizeUtil.SIZETYPE_MB)

//                if (size >= 200) {
//                    showToastS("视频不能大于100M")
//                    return
//                }



            }
        }
    }


}