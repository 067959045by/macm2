package com.mail.myapplication.ui.wallet

import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.R
import com.mail.myapplication.databinding.FrgWindrawLogBinding
import com.mail.myapplication.databinding.ItemFrgHome02Binding
import com.mail.myapplication.databinding.ItemWindrawBinding
import com.mail.myapplication.interfaces.Home
import com.zhy.autolayout.utils.AutoUtils

class WithdrawLogFrg : BaseXFrg() {

    lateinit var mBinding: FrgWindrawLogBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2
    var list = ArrayList<MutableMap<String, String>>()
    var home = Home()
    var page = 1
    var type = ""

    override fun getLayoutId(): Int = 0

    override fun initView() {
        type = arguments?.getString("type").toString()
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData2()
    }

    fun requestData2() {
        when (type) {

            "1"->{
                home.a35(page,this)
            }
            "2"->{
                home.a36(page,this)
            }
            "3"->{
                home.a37(page,this)
            }

        }
    }

    companion object {

        fun create(type: String): WithdrawLogFrg {
            val fragment = WithdrawLogFrg()
            val bundle = Bundle()
            bundle.putString("type", type)
            fragment.arguments = bundle
            return fragment
        }

    }


    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)

        if (type == "recharge/list") {

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                if (page == 1) {
                    list.clear()
                    list.addAll(mList)

                } else {
                    list.addAll(mList)
                }

                if (page == 1 && mList.size == 0) {
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                } else {
                    if(mList!=null&&mList.size>0){
                        mAdapter2?.notifyDataSetChanged()
                    }
                }
            } else {
                if (page == 1) {
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }
        if (type == "profit/list") {

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                if (page == 1) {
                    list.clear()
                    list.addAll(mList)

                } else {
                    list.addAll(mList)
                }

                if (page == 1 && mList.size == 0) {
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                } else {
                    if(mList!=null&&mList.size>0){
                        mAdapter2?.notifyDataSetChanged()
                    }
                }
            } else {
                if (page == 1) {
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }

        if (type == "withdraw/list") {

            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                var str = AESCBCCrypt.aesDecrypt(map["data"])
                var mList = JSONUtils.parseKeyAndValueToMapList(str)
                if (page == 1) {
                    list.clear()
                    list.addAll(mList)

                } else {
                    list.addAll(mList)
                }

                if (page == 1 && mList.size == 0) {
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.empty)
                } else {
                    if(mList!=null&&mList.size>0){
                        mAdapter2?.notifyDataSetChanged()
                    }
                }
            } else {
                if (page == 1) {
                    mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
                }
            }
        }
    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        stopProgressDialog()
//        if (type == "recharge/list") {
            mBinding.swipeRefreshLayout.finishRefreshing()
            mBinding.swipeRefreshLayout.finishLoadmore()
            if (page == 1 && list.size == 0) {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
            } else {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)
            }
//        }
    }

    override fun getLayoutView(): View {
        mBinding = FrgWindrawLogBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(mBinding) {

            var mLayoutManager2 = GridLayoutManager(activity, 1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager = mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.setItemViewCacheSize(200)
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)

            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    page = 1
                    requestData2()
                }

                override fun loadMoreStart() {
                    page++
                    requestData2()
                }

            })

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    requestData()
                }

            })
        }
    }

    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            return fGoldViewHolder(ItemWindrawBinding.inflate(LayoutInflater.from(activity)))
        }

        override fun getItemCount(): Int = list.size


        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {

                    with(mBinding){


                       when(this@WithdrawLogFrg.type){
                           "1"->{
                               tvName.text = list[position]["pay_gate_type"]
                               tvTime.text = list[position]["created_at"]
                               tvNum.text = "+"+list[position]["real_amount"]

                               tvStatus.visibility = View.VISIBLE
                               when(list[position]["status"]){
                                   "0"->{
                                       tvStatus.text = "待支付"
                                       tvStatus.setTextColor(Color.parseColor("#FFA4A4A4"))

                                   }
                                   "1"->{
                                       tvStatus.text = "已支付"
                                   }
                                   "2"->{
                                       tvStatus.text = "已取消"
                                       tvStatus.setTextColor(Color.parseColor("#FFA4A4A4"))

                                   }
                               }
                           }
                           "2"->{
                               tvStatus.visibility = View.GONE
                               tvName.text = "钻石收益"
                               tvTime.text = list[position]["created_at"]
                               tvNum.text = "+"+list[position]["coins"]
                           }
                           "3"->{
                               if (TextUtils.isEmpty(list[position]["bank_name"])){
                                   tvName.text = "提现到USDT"

                               }else{
                                   tvName.text = "提现到银行卡"
                               }
                               tvStatus.visibility = View.VISIBLE

                               //状态1成功2失败3提现中
                               when(list[position]["status"]){
                                   "1"->{
                                       tvStatus.text = "提现成功"
                                       tvStatus.setTextColor(resources.getColor(R.color.main_03))
                                   }
                                   "2"->{
                                       tvStatus.text ="（"+list[position]["message"]+"） 提现失败"
                                       tvStatus.setTextColor(Color.parseColor("#FFA4A4A4"))

                                   }
                                   "3"->{
                                       tvStatus.text = "审核中"
                                       tvStatus.setTextColor(Color.parseColor("#FFFF3D77"))

                                   }
                               }

                               tvTime.text = list[position]["created_at"]
                               tvNum.text = "-"+list[position]["withdraw_num"]
                           }
                       }

                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemWindrawBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemWindrawBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }

}