package com.mail.myapplication.ui.wallet

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.TextUtils
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.image.ImageLoader
import com.mail.comm.net.AESCBCCrypt
import com.mail.comm.utils.JSONUtils
import com.mail.comm.utils.PreferencesUtils
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.*
import com.mail.myapplication.interfaces.Home
import com.mail.myapplication.interfaces.Lar
import com.mail.myapplication.ui.MainWalletAty
import com.mail.myapplication.ui.msg.ChatAty
import com.mail.myapplication.ui.msg.CustomerServiceAty
import com.mail.myapplication.ui.utils.MyUtils3
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.http.RequestParams

class WithdrawAty : BaseXAty() {

    lateinit var mBinding: AtyWithdrawBinding
    var home = Home()
    var page = 1
    var list = ArrayList<MutableMap<String, String>>()
    lateinit var list_tv: Array<TextView>
    lateinit var list_v: Array<View>
    var lar = Lar()

    var withdraw_num =""
    var type ="1"
    var bank_address =""
    var bank_name =""
    var bank_no =""
    var real_name =""
    var usdt_name =""
    var usdt_address =""
    var map_service_info:MutableMap<String,String>? = null

    override fun getLayoutId(): Int = 0

    override fun initView() {
        with(mBinding) {
            list_tv = arrayOf(tv01, tv02)
            list_v = arrayOf(v01, v02)
        }
        var service_info = PreferencesUtils.getString(this,"service_info")
        map_service_info = JSONUtils.parseKeyAndValueToMap(service_info)
    }

    override fun requestData() {
        mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        requestData2()
    }

    fun requestData2() {
        lar.b10(this)
    }

    override fun getLayoutView(): View {
        mBinding = AtyWithdrawBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onComplete(var2: String?, type: String?) {
        super.onComplete(var2, type)
        mBinding.swipeRefreshLayout.finishLoadmore()
        mBinding.swipeRefreshLayout.finishRefreshing()
        stopProgressDialog()

        if(type == "withdraw"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            showToastS(map["message"])
            if (map["code"] == "200") {
                finish()
            }
        }

        if (type == "wallet/info") {
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"] == "200") {
                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.finish)

                var str_wallet = AESCBCCrypt.aesDecrypt(map["data"])
                var map_info = JSONUtils.parseKeyAndValueToMap(str_wallet)

                //1鑽石=0.99人民幣
                mBinding.tvZuan.text = map_info["diamonds"]
                mBinding.tvTi.text = "累計提現：" + map_info["withdraw_count"]
                mBinding.tvFee.text = map_info["withdraw_fee_scale"]!!+"%"
                mBinding.tvFee3.text = map_info["withdraw_fee_scale"]!!+"%"
                mBinding.tvFee2.text = map_info["coin_diamonds_scale"]!!+"鑽石=1人民幣"
//                mBinding.tvInfo.text = map_info["withdraw_intro"]

                var str_list = ArrayList<String>();
                var color_list = ArrayList<Int>()
                var text_size_list = ArrayList<Float>()
                str_list.add(map_info["withdraw_intro"]!!)
                str_list.add("在線客服")
                color_list.add(resources.getColor(R.color.main_08))
                color_list.add(resources.getColor(R.color.main_03))
                text_size_list.add(AutoUtils.getPercentHeightSize(38).toFloat())
                text_size_list.add(AutoUtils.getPercentHeightSize(42).toFloat())

                MyUtils3.setText(this,  mBinding.tvInfo, str_list, color_list, text_size_list) { position->

                    if (position==1){
                        var bundle = Bundle()
                        bundle.putString("code",map_service_info!!["code"])
                        bundle.putString("nick",map_service_info!!["nick"])
                        bundle.putString("avatar",map_service_info!!["avatar"])
                        startActivity(CustomerServiceAty::class.java, bundle)
                    }

                }

            } else {

                mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)

            }
        }

    }

    override fun onExceptionType(type: String?) {
        super.onExceptionType(type)
        mBinding.swipeRefreshLayout.finishLoadmore()
        mBinding.swipeRefreshLayout.finishRefreshing()
        stopProgressDialog()
        if (type == "wallet/info"){
            mBinding.loading.setLoadingTip(XLoadTip.LoadStatus.error)
        }
    }

    private fun setSelector(tv: TextView) {
        for (i in list_tv.indices) {
            if (tv === list_tv[i]) {
                list_tv[i].setTextColor(Color.parseColor("#333438"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗


                list_tv[i].setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    AutoUtils.getPercentWidthSizeBigger(55).toFloat()
                )
                list_v[i].visibility = View.VISIBLE
            } else {
                list_tv[i].setTextColor(Color.parseColor("#65666D"))
                list_tv[i].setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));//加粗
                list_tv[i].setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    AutoUtils.getPercentWidthSizeBigger(45).toFloat()
                )
                list_v[i].visibility = View.INVISIBLE
            }
        }
    }


    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_windraw_log -> {
                var bundle = Bundle()
                bundle.putInt("type_index",3)
                startActivity(WithdrawLogAty::class.java,bundle)
            }

            R.id.tv_ok -> {
                withdraw_num = mBinding.editMoney.text.toString()
                bank_address = mBinding.editCardAddress.text.toString()
                bank_no = mBinding.editCardNo.text.toString()
                bank_name = mBinding.editCardBank.text.toString()
                real_name = mBinding.editCardName.text.toString()

                usdt_name = mBinding.editUsdtName.text.toString()
                usdt_address = mBinding.editUsdtAddress.text.toString()

                if (TextUtils.isEmpty(withdraw_num)){
                    showToastS("请输入提现金额")
                    return
                }
                if (mBinding.linlayCard.visibility == View.VISIBLE){

                    if (TextUtils.isEmpty(real_name)){
                        showToastS("请输入姓名")
                        return
                    }
                    if (TextUtils.isEmpty(bank_address)){
                        showToastS("请输入开户行")
                        return
                    }
                    if (TextUtils.isEmpty(bank_name)){
                        showToastS("请输入银行名称")
                        return
                    }
                    if (TextUtils.isEmpty(bank_no)){
                        showToastS("请输入卡号")
                        return
                    }

                    startProgressDialog()
                    home.a34(withdraw_num,type,bank_address,bank_name,bank_no,real_name,"","",this)
                }else{

                    if (TextUtils.isEmpty(usdt_name)){
                        showToastS("錢包名稱")
                        return
                    }
                    if (TextUtils.isEmpty(usdt_address)){
                        showToastS("錢包地址")
                        return
                    }

                    startProgressDialog()
                    home.a34(withdraw_num,type,"","","","",usdt_name,usdt_address,this)

                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(mBinding) {
            initTopview2(include.relayTopBg)
            include.tvTitle.text = ""

            linlay01.setOnClickListener {
                setSelector(tv01)
                linlayCard.visibility = View.VISIBLE
                linlayUsdt.visibility = View.GONE

                tvTypeName.text = "銀行卡資料"
            }

            linlay02.setOnClickListener {
                setSelector(tv02)
                linlayCard.visibility = View.GONE
                linlayUsdt.visibility = View.VISIBLE

                tvTypeName.text = "USDT資料"

            }

            linlay01.performClick()

            swipeRefreshLayout.setEnableLoadmore(false)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setIsPinContentView(true)

            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    requestData2()
                }

                override fun loadMoreStart() {
                }

            })

            loading.setLoadingTipXReloadCallback(object : XLoadTip.LoadingTipXReloadCallback {
                override fun reload() {
                    requestData()
                }

            })

        }


    }
}





