package com.mail.myapplication.ui.dg

import android.os.Bundle
import android.view.*
import com.lzy.okgo.utils.HttpUtils.runOnUiThread
import com.mail.comm.base.BaseDgFrg
import com.mail.comm.utils.MyUtils
import com.mail.comm.utils.ToastUitl
import com.mail.comm.view.WheelView.OnWheelViewListener
import com.mail.myapplication.R
import com.mail.myapplication.databinding.DgCodeBinding
import com.mail.myapplication.databinding.DgDateSelectBinding
import com.mail.myapplication.databinding.DgRechargeBinding
import com.mail.myapplication.ui.lar.RegisterBirthdayAty
import org.xutils.common.util.LogUtil


class CodeoDgFrg : BaseDgFrg() {

    lateinit var mBinding: DgCodeBinding

    override fun getLayoutId(): Int = 0

    override fun getDialogStyle(): Int = R.style.dialogFullscreen2

    override fun getLayoutView(): View {
        mBinding = DgCodeBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun canCancel(): Boolean = true


    override fun setWindowAttributes(window: Window?) {
//
    }

    fun  setContent(str:String){
        mBinding.tvContent.text = str
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        with(mBinding){
            tvOk.setOnClickListener { dismiss() }
            imgvCancel.setOnClickListener { dismiss() }
        }
    }




}