package com.mail.myapplication.ui.home

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyActiveBinding
import com.mail.myapplication.databinding.AtyPerDetailsBinding
import com.mail.myapplication.databinding.ItemFrgHome02Binding
import com.mail.myapplication.databinding.ItemFrgHome03Binding
import com.mail.myapplication.ui.wallet.ImageAdapter
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.util.BannerUtils
import com.zhy.autolayout.utils.AutoUtils

class ActiveAty : BaseXAty() {

    lateinit var mBinding: AtyActiveBinding
    lateinit var mAdapter2: GoldRecyclerAdapter2

    override fun getLayoutId(): Int = 0

    override fun initView() {
    }

    override fun requestData() {
    }

    override fun getLayoutView(): View {
        mBinding = AtyActiveBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTranslanteBar()
        setAndroidNativeLightStatusBar(true)

        with(mBinding){
            var mLayoutManager2 = GridLayoutManager(this@ActiveAty,1)
            mLayoutManager2.orientation = RecyclerView.VERTICAL
            recyclerview2.layoutManager =mLayoutManager2
            mAdapter2 = GoldRecyclerAdapter2()
            recyclerview2.adapter = mAdapter2

            swipeRefreshLayout.setEnableLoadmore(true)
            swipeRefreshLayout.setEnableRefresh(true)
            swipeRefreshLayout.setXRefreshAndLoadListen(object : XRefreshInterface {
                override fun refreshStart() {
                    swipeRefreshLayout.finishLoadmore()
                    swipeRefreshLayout.finishRefreshing()
                }

                override fun loadMoreStart() {
                    swipeRefreshLayout.finishLoadmore()
                    swipeRefreshLayout.finishRefreshing()
                }

            })

            var list_img = ArrayList<String>()
            list_img.add("")
            list_img.add("")
            list_img.add("")
            var adapter = ImageActiveAdapter(list_img)
            banner1.setAdapter(adapter)
//            banner1.setIndicator(CircleIndicator(this@ActiveAty))
            banner1.addBannerLifecycleObserver(this@ActiveAty)

            banner1.setIndicator(indicator, false);
            banner1.setIndicatorSelectedWidth(AutoUtils.getPercentWidthSizeBigger(30))
            banner1.setIndicatorNormalWidth(AutoUtils.getPercentWidthSizeBigger(20))
            banner1.setIndicatorSpace(AutoUtils.getPercentWidthSizeBigger(10))
            banner1.setIndicatorSelectedColor(Color.parseColor("#00FFA5"))
            banner1.setIndicatorNormalColor(Color.parseColor("#ffffff"))
            banner1.setIndicatorHeight(AutoUtils.getPercentWidthSizeBigger(10))
            banner1.setBannerRound(AutoUtils.getPercentWidthSizeBigger(20).toFloat())
        }
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
        }
    }


    inner class GoldRecyclerAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemFrgHome02Binding.inflate(LayoutInflater.from(this@ActiveAty)))
        }

        override fun getItemCount(): Int = 12

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {
                        framlayHead.setOnClickListener {
                            startActivity(ActiveAty::class.java)
                        }
                    }

                    when (position) {
                        1 -> {
                            var list = ArrayList<MutableMap<String,String>>()
                            list.add(HashMap<String,String>())
                            var mLayoutManager2 = GridLayoutManager(this@ActiveAty, 1)
                            mLayoutManager2.orientation = RecyclerView.VERTICAL
                            mBinding.recyclerview.layoutManager = mLayoutManager2
                            mBinding.recyclerview?.isNestedScrollingEnabled = false
                            mBinding.recyclerview.setItemViewCacheSize(200)
                            mBinding.recyclerview.setHasFixedSize(true)
                            var mAdapter = GoldRecyclerAdapter3(list)
                            mBinding.recyclerview.adapter = mAdapter
                        }
                        2->{
                            var list = ArrayList<MutableMap<String,String>>()
                            list.add(HashMap<String,String>())
                            list.add(HashMap<String,String>())
                            var mLayoutManager2 = GridLayoutManager(this@ActiveAty, 2)
                            mLayoutManager2.orientation = RecyclerView.VERTICAL
                            mBinding.recyclerview.layoutManager = mLayoutManager2
                            mBinding.recyclerview?.isNestedScrollingEnabled = false
                            mBinding.recyclerview.setItemViewCacheSize(200)
                            mBinding.recyclerview.setHasFixedSize(true)
                            var mAdapter = GoldRecyclerAdapter3(list)
                            mBinding.recyclerview.adapter = mAdapter
                        }
                        4->{
                            var list = ArrayList<MutableMap<String,String>>()
                            list.add(HashMap<String,String>())
                            list.add(HashMap<String,String>())
                            list.add(HashMap<String,String>())
                            list.add(HashMap<String,String>())
                            var mLayoutManager2 = GridLayoutManager(this@ActiveAty, 2)
                            mLayoutManager2.orientation = RecyclerView.VERTICAL
                            mBinding.recyclerview.layoutManager = mLayoutManager2
                            mBinding.recyclerview?.isNestedScrollingEnabled = false
                            mBinding.recyclerview.setItemViewCacheSize(200)
                            mBinding.recyclerview.setHasFixedSize(true)
                            var mAdapter = GoldRecyclerAdapter3(list)
                            mBinding.recyclerview.adapter = mAdapter
                        }
                        else -> {
                            var list = ArrayList<MutableMap<String,String>>()
                            for ( i in 1..9){
                                list.add(HashMap<String,String>())
                            }

                            var mLayoutManager2 = GridLayoutManager(this@ActiveAty, 3)
                            mLayoutManager2.orientation = RecyclerView.VERTICAL
                            mBinding.recyclerview.layoutManager = mLayoutManager2
                            mBinding.recyclerview?.isNestedScrollingEnabled = false
                            mBinding.recyclerview.setItemViewCacheSize(200)
                            mBinding.recyclerview.setHasFixedSize(true)
                            var mAdapter = GoldRecyclerAdapter3(list)
                            mBinding.recyclerview.adapter = mAdapter
                        }
                    }

                }

            }

        }

        inner class fGoldViewHolder(binding: ItemFrgHome02Binding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFrgHome02Binding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }

    inner class GoldRecyclerAdapter3(list:ArrayList<MutableMap<String,String>>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        var list = ArrayList<MutableMap<String,String>>()

        init {
            this.list = list

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(ItemFrgHome03Binding.inflate(LayoutInflater.from(this@ActiveAty)))
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder){
                    when(list.size){
                        1->{
                            var  mlayoutParams =  mBinding.framlayImgv.layoutParams
                            mlayoutParams.height = AutoUtils.getPercentWidthSize(1040)
                            mBinding.framlayImgv.layoutParams =mlayoutParams
                        }
                        2->{
                            var  mlayoutParams =  mBinding.framlayImgv.layoutParams
                            mlayoutParams.height = AutoUtils.getPercentWidthSize(520)
                            mBinding.framlayImgv.layoutParams =mlayoutParams
                        }
                        4->{
                            var  mlayoutParams =  mBinding.framlayImgv.layoutParams
                            mlayoutParams.height = AutoUtils.getPercentWidthSize(520)
                            mBinding.framlayImgv.layoutParams =mlayoutParams
                        }
                        else->{
                            var  mlayoutParams =  mBinding.framlayImgv.layoutParams
                            mlayoutParams.height = AutoUtils.getPercentWidthSize(330)
                            mBinding.framlayImgv.layoutParams =mlayoutParams
                        }
                    }
                }
            }

        }

        inner class fGoldViewHolder(binding: ItemFrgHome03Binding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemFrgHome03Binding = binding
            init {
                AutoUtils.autoSize(binding.root)
            }
        }

    }



}