package com.mail.myapplication.ui.dg

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.mail.myapplication.R
import com.mail.myapplication.databinding.DgPayBinding
import com.mail.myapplication.databinding.ItemDgPayBinding
import com.mail.myapplication.interfaces.Lar
import com.zhy.autolayout.utils.AutoUtils

class PayDialog(context: BaseAty) : Dialog(context) {

    private var baseAty = context
    lateinit var mBinding: DgPayBinding
    var mAdapter: GoldRecyclerAdapter? = null
    var listen: PayListen? = null

    var list = ArrayList<MutableMap<String, String>>()

    var lar = Lar()
    var index_check = 0

    fun setData(list: ArrayList<MutableMap<String, String>>) {
        if (isShowing){
            this.list = list
            index_check = 0
            mAdapter?.notifyDataSetChanged()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DgPayBinding.inflate(layoutInflater);
        setContentView(mBinding.root)
        window?.setWindowAnimations(R.style.dialogFullscreen3)
        val dialogWindow = window
        dialogWindow!!.setBackgroundDrawable(null)
        dialogWindow.setGravity(Gravity.BOTTOM)
        setCanceledOnTouchOutside(true)
        val m = baseAty.windowManager
        val d = m.defaultDisplay// 获取屏幕宽、高
        val p = dialogWindow.attributes
        window?.getDecorView()?.setPadding(0, 0, 0, 0)
//        p.height =  (d.height * 1) // 高度设置为屏幕的0.6
        p.width = (d.width * 1) // 宽度设置为屏幕的0.85
        dialogWindow.attributes = p

        with(mBinding) {
            var mLayoutManager = GridLayoutManager(baseAty, 1)
            mLayoutManager.orientation = RecyclerView.VERTICAL
            recyclerview.layoutManager = mLayoutManager
            mAdapter = GoldRecyclerAdapter()
            recyclerview.adapter = mAdapter

            tvOk.setOnClickListener {
                  if (list.size == null||list.size==0){
                      return@setOnClickListener
                  }
                  listen?.toPay( list!![index_check]["id"]!!)
//                baseAty.startProgressDialog()
//                lar.b11(",", list!![index_check]["id"]!!,id,this@PayDialog)
            }

            relayCancel.setOnClickListener {
                dismiss()
            }
        }
    }

    interface  PayListen{
        fun toPay(payment_id:String)
    }

    fun setPayListen(listener:PayListen){
        this.listen =listener
    }


    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


            return fGoldViewHolder(ItemDgPayBinding.inflate(LayoutInflater.from(context)))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                with(holder) {
                    with(mBinding) {


                        var maxW = AutoUtils.getPercentWidthSizeBigger(300)
                        ImageLoader.loadImageAes(context, list[position]["img"], imgvType,maxW,maxW)
                        tvName.text = list[position]["name"]

                        if (position==index_check){
                            imgvCheck.setImageResource(R.mipmap.ic_68)
                        } else{
                            imgvCheck.setImageResource(R.mipmap.ic_67)
                        }

                        itemView.setOnClickListener {
                            if (position == index_check){
                                return@setOnClickListener
                            }
                            index_check  = position
                            notifyDataSetChanged()

                        }
                    }
                }

            }

        }

        inner class fGoldViewHolder(binding: ItemDgPayBinding) :
            RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemDgPayBinding = binding

            init {
                AutoUtils.autoSize(binding.root)

            }
        }

    }

}