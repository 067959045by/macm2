package com.mail.myapplication.ui.lar

import android.os.Bundle
import android.util.Log
import android.view.View
import com.andrognito.patternlockview.PatternLockView
import com.andrognito.patternlockview.PatternLockView.Dot
import com.andrognito.patternlockview.listener.PatternLockViewListener
import com.andrognito.patternlockview.utils.PatternLockUtils
import com.andrognito.patternlockview.utils.ResourceUtils
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyPatternBinding

class PatternAty:BaseXAty() {

    lateinit var mBinding: AtyPatternBinding


    override fun getLayoutId(): Int = 0

    override fun getLayoutView(): View {
        mBinding = AtyPatternBinding.inflate(layoutInflater);
        return mBinding.root
    }

    override fun initView() {
    }

    override fun requestData() {
    }


    private val mPatternLockViewListener: PatternLockViewListener =
        object : PatternLockViewListener {
            override fun onStarted() {
                Log.d(javaClass.name, "Pattern drawing started")
            }

            override fun onProgress(progressPattern: List<Dot>) {
                Log.d(
                    javaClass.name, "Pattern progress: " +
                            PatternLockUtils.patternToString(mBinding.patterLockView, progressPattern)
                )
            }

            override fun onComplete(pattern: List<Dot>) {
                Log.d(
                    javaClass.name, "Pattern complete: " +
                            PatternLockUtils.patternToString(mBinding.patterLockView, pattern)
                )
            }

            override fun onCleared() {
                Log.d(javaClass.name, "Pattern has been cleared")
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStatusColor("#303030")

        with(mBinding) {

            patterLockView.setDotCount(3)
            patterLockView.setDotNormalSize(
                ResourceUtils.getDimensionInPx(this@PatternAty, R.dimen.pattern_lock_dot_size)
                    .toInt()
            )
            patterLockView.setDotSelectedSize(
                ResourceUtils.getDimensionInPx(this@PatternAty, R.dimen.pattern_lock_dot_selected_size)
                    .toInt()
            )
            patterLockView.setPathWidth(
                ResourceUtils.getDimensionInPx(this@PatternAty, R.dimen.pattern_lock_path_width)
                    .toInt()
            )
            patterLockView.setAspectRatioEnabled(true)
            patterLockView.setAspectRatio(PatternLockView.AspectRatio.ASPECT_RATIO_HEIGHT_BIAS)
            patterLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT)
            patterLockView.setDotAnimationDuration(150)
            patterLockView.setPathEndAnimationDuration(100)
            patterLockView.setCorrectStateColor(ResourceUtils.getColor(this@PatternAty, R.color.white))
            patterLockView.setInStealthMode(false)
            patterLockView.setTactileFeedbackEnabled(true)
            patterLockView.setInputEnabled(true)
            patterLockView.addPatternLockListener(mPatternLockViewListener)
//
//            RxPatternLockView.patternComplete(patterLockView)
//                .subscribe(object : Consumer<PatternLockCompleteEvent> {
//                    @Throws(Exception::class)
//                    override fun accept(patternLockCompleteEvent: PatternLockCompleteEvent) {
//                        Log.d(
//                            javaClass.name,
//                            "Complete: " + patternLockCompleteEvent.pattern.toString()
//                        )
//                    }
//                })
//
//            RxPatternLockView.patternChanges(patterLockView)
//                .subscribe(object : Consumer<PatternLockCompoundEvent> {
//                    @Throws(Exception::class)
//                    override fun accept(event: PatternLockCompoundEvent) {
//                        if (event.eventType == PatternLockCompoundEvent.EventType.PATTERN_STARTED) {
//                            Log.d(javaClass.name, "Pattern drawing started")
//                        } else if (event.eventType == PatternLockCompoundEvent.EventType.PATTERN_PROGRESS) {
//                            Log.d(javaClass.name, "Pattern progress: " + PatternLockUtils.patternToString(patterLockView, event.pattern))
//                        } else if (event.eventType == PatternLockCompoundEvent.EventType.PATTERN_COMPLETE) {
//                            Log.d(
//                                javaClass.name, "Pattern complete: " +
//                                        PatternLockUtils.patternToString(
//                                            patterLockView,
//                                            event.pattern
//                                        )
//                            )
//                        } else if (event.eventType == PatternLockCompoundEvent.EventType.PATTERN_CLEARED) {
//                            Log.d(javaClass.name, "Pattern has been cleared")
//                        }
//                    }
//                })
        }


    }
}