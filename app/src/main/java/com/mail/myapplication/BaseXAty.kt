package com.mail.myapplication

import android.graphics.Color
import android.os.Bundle
import android.widget.RelativeLayout
import com.mail.comm.base.BaseAty
import com.mail.comm.utils.PreferencesUtils
import com.mail.myapplication.ui.dg.BindPhoneDialog
import com.mail.myapplication.ui.lar.BindPhoneAty
import com.zhy.autolayout.utils.AutoUtils

abstract  class BaseXAty :BaseAty() {


    fun initTopview2(relay: RelativeLayout?, bgColor: String = "#ffffff") {
        val lp = relay?.layoutParams as RelativeLayout.LayoutParams
        lp.height = AutoUtils.getPercentHeightSize(150)
        relay.layoutParams = lp
        relay.setBackgroundColor(Color.parseColor(bgColor))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun setLoginStatus(isLogin:Boolean){
        PreferencesUtils.putBoolean(this,"isLogin",isLogin)
    }

    fun isLogin():Boolean{
         var isLogin =PreferencesUtils.getBoolean(this,"isLogin",false)
         return isLogin
    }

    fun exitLogin(){
         setLoginStatus(false)
    }


}