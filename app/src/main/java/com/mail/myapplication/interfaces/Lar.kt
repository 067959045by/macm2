package com.mail.myapplication.interfaces

import android.text.TextUtils
import com.mail.comm.app.AppConfig
import com.mail.comm.net.ApiListener
import com.mail.comm.net.ApiTool
import com.mail.comm.net.DataManager
import com.mail.comm.utils.MyUtils2
import org.xutils.http.RequestParams
import org.xutils.x
import java.io.File

class Lar {


    /**
     * 轮训检查ip
     */
    fun a(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/ping")
        ApiTool().getApi2(params, apiListener, "ping")
    }

    /**
     * 游客login
     */
    fun b(code: String, channel: String,apiListener: ApiListener, device_data:String ="") {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/auth/login/device")
        if (!TextUtils.isEmpty(code)) {
            params.addBodyParameter("code", code)
        }
        if (!TextUtils.isEmpty(channel)) {
            params.addBodyParameter("channel", channel)
        }
        if (TextUtils.isEmpty(MyUtils2.getToken())) {
            params.addBodyParameter("token", MyUtils2.getToken());
        }
        params.addBodyParameter("device_type", "A")
        if (!TextUtils.isEmpty(device_data)){
            params.addBodyParameter("device_data", device_data)
        }
        params.addBodyParameter("device_no", MyUtils2.getAndroidId(x.app()))
        params.addBodyParameter("version", MyUtils2.getAppVersionName());
        ApiTool().postApi2(params, apiListener, "login",false)
    }

    fun b2(phone: String, area_code: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/sendmsg")
        params.addBodyParameter("phone", phone)
        params.addBodyParameter("area_code", area_code)
        ApiTool().postApi2(params, apiListener, "sendmsg")
    }

    fun b3(phone: String, code: String, area_code:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/bindphone")
        params.addBodyParameter("phone", phone)
        params.addBodyParameter("code", code)
        params.addBodyParameter("area_code", area_code)
        ApiTool().postApi2(params, apiListener, "bindphone")
    }

    fun b4(
        nick: String, intro: String,
        gender: String, birth: String,
        avatar: String, city: String,
        job: String, habit: String,
        login_type: String,
        apiListener: ApiListener
    ) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/update")
        if (!TextUtils.isEmpty(nick)) {
            params.addBodyParameter("nick", nick)
        }
        if (!TextUtils.isEmpty(intro)) {
            params.addBodyParameter("intro", intro)
        }
        if (!TextUtils.isEmpty(gender) && gender != "2") {
            params.addBodyParameter("gender", gender)
        }
        if (!TextUtils.isEmpty(birth)) {
            params.addBodyParameter("birth", birth)
        }
        if (!TextUtils.isEmpty(avatar)) {
            params.addBodyParameter("avatar", avatar)
        }
        if (!TextUtils.isEmpty(city)) {
            params.addBodyParameter("city", city)
        }
        if (!TextUtils.isEmpty(job)) {
            params.addBodyParameter("job", job)
        }
        if (!TextUtils.isEmpty(habit)) {
            params.addBodyParameter("habit", habit)
        }
        if (!TextUtils.isEmpty(login_type)) {
            params.addBodyParameter("login_type", login_type)
        }
        ApiTool().postApi2(params, apiListener, "update")
    }

    fun b5(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/get/select/data")
        ApiTool().postApi2(params, apiListener, "data")
    }

    fun b6(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/info")
        ApiTool().postApi2(params, apiListener, "user/info")
    }

    //文件上传
    fun b7(path: String, apiListener: ApiListener) {
        val timeStamp: String = (System.currentTimeMillis() / 1000).toString()
        val singPostBody = DataManager.singFileBody(timeStamp + "")
        var url =
            AppConfig.upload_url + "/upload?" + "platform=A&timestamp=" + timeStamp + "&dir=user&sign=" + singPostBody
        val params = RequestParams(url)
        params.isMultipart = true
        params.addBodyParameter("file", File(path), "multipart/form-data")
        ApiTool().postApi(params, apiListener, "file/upload")
    }

    //文件上传
    fun b72(path: String, path_index: String, apiListener: ApiListener) {
        val timeStamp: String = (System.currentTimeMillis() / 1000).toString()
        val singPostBody = DataManager.singFileBody(timeStamp + "")
        var url = AppConfig.upload_url + "/upload?" + "platform=A&timestamp=" + timeStamp + "&dir=user&sign=" + singPostBody
        val params = RequestParams(url)
        params.isMultipart = true
        params.readTimeout = 1000*60*5
        params.addBodyParameter("file", File(path), "multipart/form-data")
        ApiTool().postApiFile(params, apiListener, path_index)
    }

    //发帖
    fun b8(
        content: String,
        type: String,
        mulit_pic: String,
        mulit_video: String,
        coins: String,
        apiListener: ApiListener
    ) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/forum/create")
        if (!TextUtils.isEmpty(content)) {
            params.addBodyParameter("content", content)

        }
        //类型(1图文2视频3图文和视频)
        params.addBodyParameter("type", type)
        if (!TextUtils.isEmpty(mulit_pic)) {
            params.addBodyParameter("mulit_pic", mulit_pic)

        }
        if (!TextUtils.isEmpty(mulit_video)) {
            params.addBodyParameter("mulit_video", mulit_video)
        }
        if (!TextUtils.isEmpty(coins)) {
            params.addBodyParameter("coins", coins)
        }

        ApiTool().postApi2(params, apiListener, "send/post")
    }

    //关注
    fun b9(to_user_id: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/relation/do")
        params.addBodyParameter("to_user_id", to_user_id)
        ApiTool().postApi2(params, apiListener, "attention")
    }

    //金币列表/VIP套餐页面
    fun b10(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/orders/user/purchase/list")
        ApiTool().postApi2(params, apiListener, "wallet/info")
    }

    //去支付
    fun b11(coin_id: String, payment_id: String, vip_id: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/orders/user/purchase")
        if (!TextUtils.isEmpty(coin_id)) {
            params.addBodyParameter("coin_id", coin_id)
        }
        params.addBodyParameter("payment_id", payment_id)
        if (!TextUtils.isEmpty(vip_id)) {
            params.addBodyParameter("vip_id", vip_id)
        }
        ApiTool().postApi2(params, apiListener, "pay")
    }

    //购买帖子
    fun b12(forum_id: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/forum/buy")
        params.addBodyParameter("forum_id", forum_id)
        ApiTool().postApi2(params, apiListener, "post/buy")
    }

    fun b13(page: Int, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/list/friend")
        params.addBodyParameter("page", page.toString())
        params.addBodyParameter("page_size", "20")
        ApiTool().postApi2(params, apiListener, "friend/list")
    }

    fun b14(phone: String, phone_code:String,area_code:String,login_type:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/login/type")
        if (!TextUtils.isEmpty(phone)){
            params.addBodyParameter("phone", phone)
        }
        if (!TextUtils.isEmpty(phone_code)){
            params.addBodyParameter("phone_code", phone_code)
        }
        if (!TextUtils.isEmpty(area_code)){
            params.addBodyParameter("area_code", area_code)
        }
        params.addBodyParameter("login_type", login_type)
        ApiTool().postApi2(params, apiListener, "login/type")
    }

    //支付结果返回状态
    fun b15(order_sn: String,order_type:String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/orders/pay/result/status")
        params.addBodyParameter("order_sn", order_sn)
        params.addBodyParameter("order_type", order_type)
        ApiTool().postApi2(params, apiListener, "pay/result")
    }

 //支付结果返回状态
    fun b16(id: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/ad/count")
        params.addBodyParameter("id", id)
        ApiTool().postApi2(params, apiListener, "ad/count")
    }

    //退出登陆
    fun b17(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/login/out")
        ApiTool().postApi2(params, apiListener, "login/out")
    }

    //地区json数据
    fun b18(url:String,url2:String,apiListener: ApiListener) {
        var url3 = url
        if (!url3.startsWith("http")){
            url3 = url2+url3
        }
        val params = RequestParams(url3)
        ApiTool().getApi(params, apiListener, "area/json")
    }

    //关注
    fun b19(to_user_id: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/relation/do/follow")
        params.addBodyParameter("to_user_id", to_user_id)
        ApiTool().postApi2(params, apiListener, "follow")
    }

    //关注
    fun b20(to_user_id: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/relation/do/cancel")
        params.addBodyParameter("to_user_id", to_user_id)
        ApiTool().postApi2(params, apiListener, "cancel/follow")
    }




}