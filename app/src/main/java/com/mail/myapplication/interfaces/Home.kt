package com.mail.myapplication.interfaces

import android.text.TextUtils
import com.mail.comm.app.AppConfig
import com.mail.comm.net.ApiListener
import com.mail.comm.net.ApiTool
import com.mail.comm.netretrofit.ApiRetrofit
import com.mail.comm.utils.MyUtils2
import org.xutils.http.RequestParams

class Home {

    fun a(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url)
        ApiTool().postApi(params, apiListener, "home/list")
    }

    fun a2(page: Int, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/message/like/lists")
        params.addBodyParameter("page", page.toString())
        params.addBodyParameter("page_size", "20")
        ApiTool().postApi2(params, apiListener, "zan/lists")
    }

    fun a3(page: Int, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/message/comment/lists")
        params.addBodyParameter("page", page.toString())
        params.addBodyParameter("page_size", "5")
        ApiTool().postApi2(params, apiListener, "comment/lists")
    }

    fun a4(page: Int, type:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/relation/list")
        params.addBodyParameter("page", page.toString())
        params.addBodyParameter("page_size", "20")
        params.addBodyParameter("type", type)
        ApiTool().postApi2(params, apiListener, "fans/lists")
    }

    //兑换码信息
    fun a5(code: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/code/info")
        params.addBodyParameter("code", code)
        ApiTool().postApi2(params, apiListener, "code/info")
    }

    //兑换
    fun a6(code: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/code/exchange")
        params.addBodyParameter("code", code)
        ApiTool().postApi2(params, apiListener, "code/exchange")
    }

    //用户喜欢的帖子
    fun a8(page: Int,user_id: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/list/forum/like")
        params.addBodyParameter("page", page.toString())
        params.addBodyParameter("page_size", "20")
        if (!TextUtils.isEmpty(user_id)) {
            params.addBodyParameter("user_id", user_id)
        }
        ApiTool().postApi2(params, apiListener, "home/list")
    }

    //首页帖子列表
    fun a7(page: Int, topic_id:String,is_new:String,is_hot:String,
           keywords:String,is_recommend:String,is_follow:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/forum/lists")
        params.addBodyParameter("page", page.toString())
        params.addBodyParameter("page_size", "20")

        //话题ID
        if (!TextUtils.isEmpty(topic_id)) {
            params.addBodyParameter("topic_id", topic_id)
        }

        //是否最新 1是0否
        if (!TextUtils.isEmpty(is_new)) {
            params.addBodyParameter("is_new", is_new)
        }

        //是否最火  1是0否
        if (!TextUtils.isEmpty(is_hot)) {
            params.addBodyParameter("is_hot", is_hot)
        }

        //关键字  搜索时传参
        if (!TextUtils.isEmpty(keywords)) {
            params.addBodyParameter("keywords", keywords)
        }

        //是否推荐 1是0否
        if (!TextUtils.isEmpty(is_recommend)) {
            params.addBodyParameter("is_recommend", is_recommend)
        }

        //是否关注 1是0否
        if (!TextUtils.isEmpty(is_follow)) {
            params.addBodyParameter("is_follow", is_follow)
        }

        ApiTool().postApi2(params, apiListener, "home/list")
    }

//    //帖子详情(鉴权)-详情页面
//    fun a11(id: String, apiListener: ApiListener) {
//        val params = RequestParams(AppConfig.Host_Url + "/app/api/forum/info")
//        params.addBodyParameter("id", id)
//        ApiTool().postApi2(params, apiListener, "post/details_${id}_details_0")
//    }

    //帖子详情(鉴权)-非详情页面
    fun a10(id: String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/forum/info")
        params.addBodyParameter("id", id)
        ApiTool().postApi2(params, apiListener, "post/details")
    }

    //推荐用户列表
    fun a9(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/recommend")
        ApiTool().postApi2(params, apiListener, "home/user/list")
    }

    //我的帖子
    fun a12(user_id:String,page: Int, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/list/forum")
        params.addBodyParameter("page", page.toString())
        params.addBodyParameter("page_size", "20")
        if (!TextUtils.isEmpty(user_id)){
            params.addBodyParameter("user_id", user_id)
        }
        ApiTool().postApi2(params, apiListener, "home/list")
    }

    //已购帖子
    fun a122(user_id:String,page: Int, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/list/forum/buy")
        params.addBodyParameter("page", page.toString())
        params.addBodyParameter("page_size", "20")
        if (!TextUtils.isEmpty(user_id)){
            params.addBodyParameter("user_id", user_id)
        }
        ApiTool().postApi2(params, apiListener, "home/list")
    }


    //评论列表
    fun a13(page:Int,id: String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/comment/lists")
        params.addBodyParameter("page", page)
        params.addBodyParameter("page_size", "20")
        params.addBodyParameter("module", "forum")
        params.addBodyParameter("svalue", id)
        params.addBodyParameter("parent_id", "0")
        ApiTool().postApi2(params, apiListener, "comment/list")
    }

    //回复列表
    fun a132(page:Int,id: String,parent_id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/comment/lists")
        params.addBodyParameter("page", page)
        params.addBodyParameter("page_size", "20")
        params.addBodyParameter("module", "forum")
        params.addBodyParameter("svalue", id)
        params.addBodyParameter("parent_id", parent_id)
        ApiTool().postApi2(params, apiListener, "relay/list")
    }

    fun a14(svalue:String,content:String,parent_id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/comment/write")
        params.addBodyParameter("module","forum")
        params.addBodyParameter("svalue",svalue)
        params.addBodyParameter("content",content)
        params.addBodyParameter("parent_id",parent_id)
        ApiTool().postApi2(params, apiListener, "comment/write")
    }


    //帖子点赞
    fun a15(id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/forum/support")
        params.addBodyParameter("id",id)
        ApiTool().postApi2(params, apiListener, "post/zan")
    }


    //评论点赞
    fun a16(id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/comment/support")
        params.addBodyParameter("id",id)
        ApiTool().postApi2(params, apiListener, "comment/zan")
    }

    //用户资料(查看别人的)
    fun a17(code:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/data")
        params.addBodyParameter("code",code)
        ApiTool().postApi2(params, apiListener, "other/info")
    }

    //话题列表
    fun a18(keywords:String,apiListener: ApiListener,page:Int=1) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/topic/lists")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("page_size","10")
        if (!TextUtils.isEmpty(keywords)){
            params.addBodyParameter("keywords",keywords)
        }
        ApiTool().postApi2(params, apiListener, "topic/list")
    }

    //话题搜索列表
    fun a182(keywords:String,apiListener: ApiListener,page:Int=1) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/topic/lists")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("page_size","20")
        if (!TextUtils.isEmpty(keywords)){
            params.addBodyParameter("keywords",keywords)
        }
        ApiTool().postApi2(params, apiListener, "topic/search/list")
    }

    //兑换
    fun a19(code:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/code/exchange")
        params.addBodyParameter("code",code)
        ApiTool().postApi2(params, apiListener, "code/exchange")
    }

    //热门搜索推荐用户
    fun a20(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/search/list/user")
        ApiTool().postApi2(params, apiListener, "user/list")
    }

    //热门搜索推荐关键字
    fun a21(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/search/keywords/list")
        ApiTool().postApi2(params, apiListener, "keywords/list")
    }

    //热门搜索推荐帖子
    fun a22(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/forum/recommend")
        ApiTool().postApi2(params, apiListener, "home/list")
    }

    //热门搜索-搜索用户结果
    fun a23(page:Int,keywords:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/list/search")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("page_size","20")
        params.addBodyParameter("keywords",keywords)
        ApiTool().postApi2(params, apiListener, "search/list")
    }

    //历史活动
    fun a24(page:Int,keywords:String,from:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/topic/lists")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("page_size","20")
        params.addBodyParameter("keywords",keywords)
        params.addBodyParameter("from",from)
        ApiTool().postApi2(params, apiListener, "topic/list")
    }

  //历史活动
    fun a25(id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/topic/info")
        params.addBodyParameter("id",id)
        ApiTool().postApi2(params, apiListener, "topic/info")
    }

    //邀请规则
    fun a26(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/invite/rule")
        ApiTool().postApi2(params, apiListener, "invite/rule")
    }

    //帖子举报
    fun a27(id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/forum/report")
        params.addBodyParameter("id",id)
        ApiTool().postApi2(params, apiListener, "post/report")
    }

    //用户拉黑/屏蔽
    fun a28(type:String,to_user_id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/relation/do/block")
        params.addBodyParameter("type",type)
        params.addBodyParameter("to_user_id",to_user_id)
        ApiTool().postApi2(params, apiListener, "post/block")
    }

    //用户拉黑/屏蔽
    fun a29(page:Int,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/relation/block/list")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("page_size","20")
        ApiTool().postApi2(params, apiListener, "block/list")
    }

    //用户拉黑/屏蔽
    fun a30(to_user_id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/relation/user/delete")
        params.addBodyParameter("to_user_id",to_user_id)
        ApiTool().postApi2(params, apiListener, "user/delete")
    }

    //用户拉黑/屏蔽
    fun a31(id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/forum/delete")
        params.addBodyParameter("id",id)
        ApiTool().postApi2(params, apiListener, "post/delete")
    }

    //认证info
    fun a32(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/creator/info")
        ApiTool().postApi2(params, apiListener, "creator/info")
    }

    //去认证
    fun a33(selfie:String,video_selfie:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/creator/auth")
        params.addBodyParameter("selfie",selfie)
        params.addBodyParameter("video_selfie",video_selfie)
        params.addBodyParameter("type","1")
        ApiTool().postApi2(params, apiListener, "creator/auth")
    }

    //提现
    fun a34(withdraw_num:String,type:String,bank_address:String,bank_name:String,
            bank_no:String, real_name:String,usdt_name:String,usdt_address:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/withdraw")

        params.addBodyParameter("withdraw_num",withdraw_num)
        params.addBodyParameter("type",type)

        if (!TextUtils.isEmpty(bank_address)){
            params.addBodyParameter("bank_address",bank_address)
        }
        if (!TextUtils.isEmpty(bank_name)){
            params.addBodyParameter("bank_name",bank_name)
        }
        if (!TextUtils.isEmpty(bank_no)){
            params.addBodyParameter("bank_no",bank_no)
        }
        if (!TextUtils.isEmpty(real_name)){
            params.addBodyParameter("real_name",real_name)
        }
        if (!TextUtils.isEmpty(usdt_name)){
            params.addBodyParameter("usdt_name",usdt_name)
        }
        if (!TextUtils.isEmpty(usdt_address)){
            params.addBodyParameter("usdt_address",usdt_address)
        }
        ApiTool().postApi2(params, apiListener, "withdraw")
    }


    //充值列表
    fun a35(page:Int,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/log/list/recharge")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("page_size","20")
        ApiTool().postApi2(params, apiListener, "recharge/list")
    }

    //收益记录
    fun a36(page:Int,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/log/list/profit")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("page_size","20")
        ApiTool().postApi2(params, apiListener, "profit/list")
    }

    //收益记录
    fun a37(page:Int,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/log/list/withdraw")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("page_size","20")
        ApiTool().postApi2(params, apiListener, "withdraw/list")
    }

    //私信设置
    fun a38(message_type:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/message/update")
        params.addBodyParameter("message_type",message_type)
        ApiTool().postApi2(params, apiListener, "message/update")
    }

    //兑换码信息
    fun a39(code: String, apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/bindcode")
        params.addBodyParameter("code", code)
        ApiTool().postApi2(params, apiListener, "bind/yqm")
    }

    //搜索用户列表
    fun a40(page:Int,keywords:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/user/list/search")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("keywords",keywords)
        params.addBodyParameter("page_size","20")
        ApiTool().postApi2(params, apiListener, "search/user/list")
    }

    //模糊搜索 搜索用户列表
    fun a41(keywords:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url + "/app/api/search/by/elastic")
        params.addBodyParameter("keywords",keywords)
        ApiTool().postApi2(params, apiListener, "search/elastic")
    }

    fun a42(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/site/version")
        params.addQueryStringParameter("device_type","A")
        params.addQueryStringParameter("version_code",MyUtils2.getAppVersionName())
        ApiTool().getApi2(params, apiListener, "version/update")
    }

    // 
    fun a43(to_user_code:String,user_code:String,msg:String,time:String,message_id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/message/create")
        params.addBodyParameter("dst",to_user_code)
        params.addBodyParameter("src",user_code)
        params.addBodyParameter("payload",msg)
        params.addBodyParameter("ms",time)
        params.addBodyParameter("message_id",message_id)
        ApiTool().postApi2(params, apiListener, "message/create")
    }

    //聊天用户列表记录
    fun a44(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/message/history/list")
        ApiTool().postApi2(params, apiListener, "message/history/list")
    }

    //聊天用户列表记录
    fun a45(page:Int,to_user_code:String,ms:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/message/user/list")
        params.addBodyParameter("page",page)
        params.addBodyParameter("page_size","10")
        params.addBodyParameter("dst",to_user_code)
        if (!TextUtils.isEmpty(ms)){
            params.addBodyParameter("ms",ms)
        }
        ApiTool().postApi2(params, apiListener, "/app/api/message/user/list")
    }

 //用户举报
    fun a46(to_user_id:String,content:String,img:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/user/report")
        params.addBodyParameter("to_user_id",to_user_id)
        params.addBodyParameter("content",content)
        if (!TextUtils.isEmpty(img)){
            params.addBodyParameter("img",img)
        }
        ApiTool().postApi2(params, apiListener, "user/report")
    }


 //私信鉴权
    fun a47(to_user_id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/message/auth")
        params.addBodyParameter("dst",to_user_id)
        ApiTool().postApi2(params, apiListener, "message/auth")
    }

    //官方通知列表
    fun a48(page:Int,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/message/official/notice")
        params.addBodyParameter("page",page.toString())
        params.addBodyParameter("page_size","10")
        ApiTool().postApi2(params, apiListener, "/app/api/message/official/notice")
    }

    //统计帖子分享数
    fun a49(id:String,apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/forum/share")
        params.addBodyParameter("id",id)
        ApiTool().postApi2(params, apiListener, "forum/share")
    }

   //随机在线人数
    fun a50(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/site/random/people")
        ApiTool().postApi2(params, apiListener, "random/people")
    }

    //客戶问答
    fun a51(apiListener: ApiListener) {
        val params = RequestParams(AppConfig.Host_Url  + "/app/api/message/question/answer")
        ApiTool().postApi2(params, apiListener, "question/answer")
    }

}