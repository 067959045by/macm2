package com.mail.myapplication

import cn.jpush.android.api.JPushInterface
import com.lzy.okgo.OkGo
import com.lzy.okgo.cache.CacheEntity
import com.lzy.okgo.cache.CacheMode
import com.lzy.okgo.cookie.CookieJarImpl
import com.lzy.okgo.cookie.store.DBCookieStore
import com.lzy.okgo.interceptor.HttpLoggingInterceptor
import com.mail.comm.app.AppConfig
import com.mail.comm.app.BaseApp
import com.mail.myapplication.ui.msg.ChatManager
import com.tencent.ugc.TXUGCBase
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import java.util.logging.Level

class MyApp : BaseApp() {

    lateinit var mChatManager: ChatManager

    override fun onCreate() {
        super.onCreate()
        mChatManager = ChatManager(this)
        JPushInterface.setDebugMode(AppConfig.debug)
        JPushInterface.init(this)
    }

    fun getChatManager(): ChatManager {
        return mChatManager
    }

    fun initChatIm(){
        mChatManager.init()
    }

    fun initTxVideo(licenceUrl:String,licenseKey:String){
        TXUGCBase.getInstance().setLicence(this, licenceUrl, licenseKey)
    }

}